<?PHP


// Fetches a list of jobs from a database
class jobs {

    var $jobs;
		var $result;

    function jobs ( &$dbConn ) {
		
		
		
		    //bbCode Stuff
        /* require PEAR and the parser */
        require_once('PEAR.php');
        require_once('HTML/BBCodeParser.php');
        /* get options from the ini file */
        $config = parse_ini_file('BBCodeParser.ini', true);
        $options = &PEAR::getStaticProperty('HTML_BBCodeParser', '_options');
        $options = $config['HTML_BBCodeParser'];
        unset($options);
        
       
		
        // Pull the jobs from the database
        $sql="SELECT job_id, title, intro, body, employer, logo, salary, location, reference, applicationinfo, closingdate, attachment, website, email, author, published FROM jobs ORDER BY published DESC;";
				
				//echo "the SQL " . $sql;
				
        $result = $dbConn->query($sql);
				
        while ( $row = $result->fetch() ) {
						
            $this->jobs[]=$row;
        }
		}
			
		
		// Factory method creates new instances of job
    function & getjob () {
        $row=each($this->jobs);

		  if ( $row ) {
				    					
						  return new job($row['value']);
								
        } else {
            reset ( $this->jobs );
            return false;
        }
		
		}
		function & getjobByID($id) {
												
				foreach($this->jobs as $name=>$value){
																		
						if($value['job_id']==$id){
																										
									return new job($value);																			 
						}
						
				 }
		}
		
}

// The class built by the factory method
class job {
    var $job_id;
	  var $title;
		var $intro;
		var $body;
		
		var $employer;
		var $logo;
		
		var $salary;
		var $location;
		var $reference;
		
		var $applicationinfo;
	  var $closingdate;
		
		var $attachment;
		var $website;
		var $email;
		
    var $author;
		var $published; // Unix timestamp
		
    var $date; // Unix timestamp
   
		
    function job($data) {
		
						 								
						 
				$this->job_id=      $data['job_id'];
        $this->title=				$data['title'];
				$this->intro=				$data['intro'];
				$this->body=				$data['body'];
				
				$this->employer=		$data['employer'];
				$this->logo=		$data['logo'];
				
		    $this->salary=			$data['salary'];
		    $this->location=		$data['location'];
		    $this->reference=		$data['reference'];
				
				$this->applicationinfo=$data['applicationinfo'];
				$this->closingdate=	$data['closingdate'];
				
				$this->attachment=  $data['attachment'];
				$this->website=     $data['website'];
				$this->email=       $data['email'];
				
				
        $this->author=			$data['author'];
        $this->published=		$data['published'];
        
						
				
				
    }
		 function job_id() {
        return ($this->job_id);
    }
		
    function title() {
        return htmlspecialchars($this->title);
    }
    
		function intro() {
        return $this->intro;
    }
		
		function employer() {
        return $this->employer;
    }
		
		function logo() {
        return $this->logo;
    }
		
		function salary() {
        return $this->salary;
    }
		
		function location() {
        return $this->location;
    }
		
		function reference() {
        return $this->reference;
    }
		
		
		
		function applicationinfo() {
        return $this->applicationinfo;
    }
		function closingdate() {
        return $this->closingdate;
    }
		
		 function website() {
		    if(isset($this->website)){	
        				return $this->website;
				}
				else return false;
    }
		function websitelink($websitetext) {
		    if(isset($this->website)){	
        				return '<a href="http://'.$this->website.'">'.$websitetext.'</a>';
				}
				else return false;				
    
		}
		
		 function email() {
		    if(isset($this->email)){	
                return $this->email;
				}
				else return false;
    }
		 function emaillink($emailtext) {
		    if(isset($this->email)){	
                return '<a href="mailto:'.$this->email.'">'.$emailtext.'</a>';
				}
				else return false;
								
    }
		 function attachment() {
		    if(isset($this->attachment)){	
                return $this->attachment;
				}
				else return false;
    }
		 function attachmentlink($attachmenttext) {
		 		if(isset($this->attachment)){		
		            
								$filename = $this->attachment		;					
							  $filepath = 'files/'. $filename;
								$size = $this->fileSizeUnit( filesize ($filepath ) );
	
        				return '<a href="'.$filepath.'">'.$attachmenttext.' (.pdf '.  $size['size'].' '.$size['unit'].')</a>';
			  }
				else return false;
    }
		
		
		
		
		
		
		
		
    function author() {
        return htmlspecialchars($this->author);
    }
		 function published() {
		     //return $this->published;
        return $this->published;
    }

    function date() {
        return $this->date;
    }
		

    function body() {
		
        $tempString = $this->body;		
		    $tempString = htmlspecialchars($tempString);
				
				$parser = new HTML_BBCodeParser();
        $parser->setText($tempString);
        $parser->parse();
        $parsed = $parser->getParsed();
							
				//Replace double linebreaks with double breaks
				$parsed = str_replace("\r\n\r\n","</p>\n<p>",$parsed);
				//top and tail it
				$parsed ="\n<p>".$parsed ."</p>\n";
				
        return $parsed;
		    }
		

    function fileSizeUnit($size) {
        if ($size >= 1073741824) {
            $size=number_format(($size / 1073741824),1);
            $unit='GB';
        } else if ($size >= 1048576) {
            $size=number_format(($size / 1048576),1);
            $unit='MB';
        } else if ($size >= 1024) {
            $size=number_format(($size / 1024),1);
            $unit='KB';
        } else if ($size >= 0) {
            $unit='B';
        } else {
            $size='0';
            $unit='B';
        }
        return ( array ( 'size'=>$size, 'unit'=>$unit ) );
    }

        
   
}

