<?php

require_once('articles/Items.php'); 

Class EventItems extends Items {
        
     function __construct ( $db,$active=true, $params='') {
                                       
                                       
                                         
          $this->db=$db;		 
          $this->params=$params; 	 
          
          $this->sortOrder='DESC'; //can be overridden
          
          //give the parameters to the function		 	 
          $this->_addParameters();
          
          $sql  = 'SELECT * ';
          $sql .= ' FROM events ';
          $sql .= ' WHERE 1=1 ';
          $sql .=  $this->_getParameters();
          if($active){
          $sql .= ' AND published < ' . time() . ' AND depublish > ' . time() ;             
          }
          
          $sql .= " ORDER BY 0+startDate $this->sortOrder;";
          
          
          //echo "the SQL " . $sql;
          $result = $db->query($sql);
          //echo "number of records: ".$result->size();s
          
          $numberOfResults=$result->size();
          $this->itemArray=array();
          if($numberOfResults != 0){
          
                  while ( $row = $result->fetch() ) {
                  
                          $this->itemArray[]=$row;
                  }
          }      
          //echo '<pre>';
          //print_r($this->itemArray);           
        }
        function fetch () {
                
                $row=@each($this->itemArray);
                if ( $row ) {
                        return new EventItem($row['value'],$this->db);
                } else {
                        @reset ( $this->itemArray );
                        return false;
                }
        
        }
        
        function getItemByID($id) {
                
        $isMatched=false;					
        foreach($this->itemArray as $name=>$value){
                                                                                                                                                
                        if($value['id']==$id){
                                                return new EventItem($value,$this->db);																			 
                        }
                        
         }
         //only gets this far if article not found
         reset($this->itemArray);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
         //echo "<p>No article found";
         return false;
                
                 
        }
     //collects SQL parameters
     function _addParameters(){
                     
          if($this->params !=''){
               
               foreach($this->params as $name=>$value){
                                                                                                             
                    //echo '<p>' . $name  .'|'. $value;			
                                                            
                    switch($name){
                    
                         case ('section'):
                         
                              $this->sqlParams[]=		'section ="'. $value .'"';		
                         
                         break;
                         
                         case ('active'):
                         
                              if($value==1){				     
                                   $today=mktime(0,0,0,date('m'), date('d'), date('Y'));																		
                                   $this->sqlParams[]=		'published <= '.$today.' AND depublish > '.$today ;	
                              }	
                         break;
                    
                         case ('sortOrder'):
                         
                              $this->sortOrder=$value;
                              
                         break;
                         
                         //Default: take param literatlly
                         default:
                         
                                 $this->sqlParams[]=		$value ;			
                    }	
            }

             }			
                                      
                             //print_r( $this->sqlParams);
                             //exit;
                                        
        }
        
        function _getParameters(){
                                         
                                        
                                         
                $sqlSubstring='';
        
                if(isset($this->sqlParams) && is_array($this->sqlParams ) ){
                        foreach($this->sqlParams as $name=>$value){
                
                                $sqlSubstring .=  ' AND  ' . $value;
                
                        }
                        
                        return $sqlSubstring;
                }
                else{
                        return false;        
                }
        }
        
     function createNewItem(){
        
          //print_r($_POST);
          
          $title= '****New item****';												
          $datePublish =   mktime(0,0,0,date('m'), date('d'), date('Y'));//today
          $dateDepublish = mktime(0,0,0,date('m'), date('d')-1, date('Y')) ;// yesterday - forces it to be "expired"
          
          
          // Construct a query using the submitted values
          $sql='INSERT INTO events (title, published, depublish) VALUES ("'.$title.'",'.$datePublish.', '.$dateDepublish.');';    						
          
          $this->contentString .=  $sql;
          
          //Perform the query
          $result=$this->db->query($sql);
     }


}





// The class built by the factory method
class EventItem extends item {

        //ONLY the unique variables need be declared

        
       // var $short_title;
        //var $meta_title;
       // var $meta_description;
        
        var $startDate;
        var $endDate;
        var $textdate;
        var $meeting_date;
        var $venue;
        var $location;
        
        var $image;
        var $width;
        var $height;
        var $alt;
        
        
        var $email;
        var $website;
        var $attachment;
        var $highlight;
        
        var $dateRange;
        var $data;
        var $db;

                
        function __construct($data, $db) {
                //All the data needs to be declared here
        
                
                $this->db=$db; //removing & for rackspace
                
                $this->id=          $data['id'];
                
               // echo 'the article ID should be ' . $this->id;
                $this->title=			$data['title'];
                $this->shortTitle=		$data['short_title'];
                $this->metaTitle=		$data['meta_title'];
                $this->metaDescription=	$data['meta_description'];
                
                
               // $this->intro=			$data['intro'];
                $this->isHTML=			$data['isHTML'];
                $this->highlight=	        @$data['highlight'];
                $this->body=			$data['body'];	
                
                $this->startDate=		@$data['startDate'];
                $this->endDate=		        @$data['endDate'];
                
                
                $this->textdate=		@$data['textdate'];
                
                $this->meeting_date=		@$data['meeting_date'];
                
                
                $this->venue=		        @$data['venue'];
                $this->location=		@$data['location'];
                
                $this->image=			$data['image'];
                $this->width=			$data['width'];
                $this->height=	                $data['height'];
                $this->alt=			$data['alt'];	
                
                
                //$this->attachment=  @$data['attachment'];
                //$this->website=     @$data['website'];
                //$this->email=       @$data['email'];
                        
                        
                $this->author=			@$data['author'];
                $this->published=		$data['published'];
                $this->depublish=		$data['depublish'];
                                        
            
                }
       
                                
//Only the UNIQUE functions need to follw here... other will be inherited		
                
        
        function highlight() {
                return $this->highlight;
        }
                        
        function venue() {
                return $this->venue;
        }
        function location() {
                return $this->location;
        }
       function startDate() {
                return $this->startDate;
        }
        //function textdate() {
        //        return $this->textdate;
       // }
       
       
       
       
     function notFinished(){
          
          //echo '<hr>Now is ' . time();
          //echo '<p>Start Date is ' . $this->startDate . ' or ' . date('d M Y h:I:s',(int)$this->startDate);
          //echo '<p>Now  - Start Date (hours)' . (time() - $this->startDate)/60/60;
          
          
          // echo '<hr>here';
          //depublsihed?
          if(time() < $this->depublish   ){
               
              
         
          
               //If only a start date 
               if($this->endDate=='' || $this->endDate < $this->startDate ){ //Ignore endDate if it's before the start date
               
                    //Tricky: not over until the start date has ENDED
                    if( time() < $this->startDate+24*60*60){
                         //echo 'Should get here for 13th';
                         return true;
                    }
                    else{
                           //echo 'Should get here for 12th';
                         return false;
                    }
                    
               }
               //if an end date exists
               else{
                     //Tricky: not over until the end date has ENDED
                    if( time() < $this->endDate+24*60*60){
                         return true;
                    }
                    else{
                         return false;
                    }
                    
               }
          }
          else{
               return false;
          }
          
     }
    
     /**
      * Added the date formatting for Shack's benefit. Only applies to one day events at present
      *
      */
     
     function dateRange($includeDay=false){
     
          
          if(!$includeDay){
               
              $dMY= 'd M Y';
          }
          else {
               
               $dMY= 'l j M Y';
          
          }
          
          //easy case first: one-day event;
          //echo '<hr>' . $this->startDate;
          //echo '<hr>' . $this->endDate;
          // echo '<hr>' . date('d M Y', $this->startdate);
          //exit;
          
          
          if($this->endDate=='' || $this->endDate < $this->startDate ||  $this->startDate==$this->endDate){ //Ignore endDate if it's before the start date
               
               
               return date($dMY, $this->startDate);
          
          
          }
          //more tricky!
          else {
               //Eveything is different!
               if( date('Y', $this->startDate)!=date('Y', $this->endDate)  ){
                    return date('d M Y', $this->startDate) . ' - ' . date('d M Y', $this->endDate);
               
               }
               //Differnet months
               elseif(date('m', $this->startDate)!=date('m', $this->endDate) ){
                    return date('d M', $this->startDate) . ' - ' . date('d M Y', $this->endDate);
               
               }
               else{
                    return date('d', $this->startDate) . ' - ' . date('d M Y', $this->endDate);
               }
          }
     }
     
      /**
       * Jan 2008 - Have added includeDay for Shack's benefit. Only does sowmthing for one-day events
       *
       */
     
     
     function comingSoon(){

                //now on = start date in the past
                if($this->startdate < time() ){
                return 'now';
                }
                //this month
                elseif( $this->startdate < mktime(0,0,0,date('m')+1,1,date('y') )  ){
                    return 'thismonth';
                }
                //following month
                elseif( $this->startdate < mktime(0,0,0,date('m')+2,1,date('y') )   ){
                    return 'nextmonth';
                }
                else{
                   return 'future';
                }
        }
        function daysToGo(){
        
                $days=  floor( ($this->startdate - mktime(0,0,0,date('m'),date('d'),date('y') ) )/(60*60*24));
            
                if($days<0){
                        $days=0;
                }   
                        return $days;
        }
        
        
        function finalise(){
	    
                //echo '<hr>FINALISINGSSSSSSS';
                //$this->_applyBusinessRules();
                
                
                //title =          	"'.mysql_real_escape_string($this->title) .'",  
               // echo '<p>' . $this->shortTitle ;
               // echo '<p>' . $this->metaTitle ;
                //echo '<p>' . $this->metaDescription ;
               // 
             
        
        
                $sql='  UPDATE events 
                SET
                title =          	"'.mysql_real_escape_string($this->title) .'",  
                short_title =          	"'.mysql_real_escape_string($this->shortTitle) .'", 
                meta_title =          	"'.mysql_real_escape_string($this->metaTitle) .'",  
                meta_description =      "'.mysql_real_escape_string($this->metaDescription) .'",
                
                startDate =      	"'.mysql_real_escape_string($this->startDate) .'", 
                endDate =      	        "'.mysql_real_escape_string($this->endDate) .'", 
                
                
                published =      	"'.mysql_real_escape_string($this->published) .'", 
                depublish =      	"'.mysql_real_escape_string($this->depublish) .'", 
                last_update = 		"' . time() . '" ,
                isHTML =          	"'.mysql_real_escape_string($this->isHTML) .'",  
                body =           	"'.mysql_real_escape_string($this->body) .'"
                WHERE id=		'.$this->id.';';
                
                //echo $sql;
                //exit;
                //print_r($this->db);
                
                
                $this->db->query($sql);
                //exit;
            }		            
                

}


