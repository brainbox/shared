<?PHP

#error_reporting (E_ALL);

require_once('articles/Items.php'); 

/*-------------------

We're storing depuiblish date, but the losing date is ONE DAY BEFORE!!!!

------------------*/

class jobItems extends Items {
     
     protected $db;

     function jobItems ($db,$active) {

		 		$this->db=$db;
				
				#print_r($this->db);
        
				//FOR the  LIVE SITE
				if($active){
				
				$today=mktime(0,0,0,date('m'), date('d'), date('Y'));
        			$sql='SELECT id, title, highlight, intro, body, author, applicationinfo, employer, salary, location, reference, image, width, height, alt, email, website, attachment, published, depublish
							      FROM jobs 
										WHERE published <= '.$today.' 
										AND depublish > '.$today.' 
										ORDER BY 0+depublish ;'; 
				}     
				
				
				//FOR ADMINISTRATION
				else{
				      $sql="SELECT id, title, highlight, intro, body, author, applicationinfo, employer, salary, location, reference, image, width, height, alt, email, website, attachment, published,depublish FROM jobs ORDER BY depublish DESC;";
				}
				//echo "the SQL " . $sql;
				$result = $db->query($sql);
				
        //echo "<pre>the result is ";
				#var_dump($result->fetch());
				
        while ( $row = $result->fetch() ) {
							//echo $row;
            $this->jobItems[]=$row;
        }
     
     #print_r($this->jobItems);
     
     }
			
		
		// Factory method creates new instances of jobItem
    function  getItem () {
		
	if(!isset($this->jobItems)) {
	  
		return false;
	  
	}
		
        $row=each($this->jobItems);
		    if ( $row ) {
						  return new jobItem($row['value'],$this->db);
        } else {
            reset ( $this->jobItems);
            return false;
        }
		
		}
		
		function getItemByID($id) {
												
				foreach($this->jobItems as $name=>$value){

																		
						if($value['id']==$id){
											//echo "got here";															
									return new jobItem($value,$this->db);																			 
						}
						
				 }		
				//only gets this far if article not found
				reset($this->jobItems);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
				//echo "<p>No article found";
				return false;
						
						
				
		}
		
		
}

// The class built by the factory method
class jobItem extends item {
  	
		var $employer;
		var $image;
		
		var $salary;
		var $location;
		var $reference;
		
		var $highlight;
		
		
		var $applicationinfo;
	  var $closingdate;
			
    function jobItem($data, $db) {
		
		#echo 'here';
		#print_r($db);
				$this->db=$db;		 								
			      #print_r($this->db);
			  $this->id=          $data['id'];
        $this->title=				$data['title'];
				$this->highlight=		$data['highlight'];
				$this->intro=				$data['intro'];
				$this->body=				$data['body'];
				
				$this->employer=    $data['employer'];
				$this->location=    $data['location'];
				$this->salary=      $data['salary'];
				$this->reference=   $data['reference'];
				
				$this->attachment=  $data['attachment'];
				$this->website=     $data['website'];
				$this->email=       $data['email'];
				
				$this->image=       $data['image'];
				$this->width=       $data['width'];
				$this->height=      $data['height'];
				$this->alt=         $data['alt'];
				
        $this->author=			$data['author'];
        $this->published=		@$data['published'];
        $this->depublish=		@$data['depublish'];
				
				
				//Get FILE the INFO but not the contents of the file
   			$sql='SELECT id, filename, description, mimetype, filesize, upload_date 
            FROM downloads
    			  WHERE article_id='.$this->id.'
    			  AND section="jobs";';
    
          // Perform the query
          	#echo $sql;
				//This should have given access to the CREATOR's dbConn	
		#print_r($this->db);
		#exit;
				
        $result=$this->db->query($sql);
				
				if(!$result->size()==0){
				
				   while ( $row = $result->fetch() ) {
					     # echo "got to the rows";
                $this->downloads[]=$row;
					 }
        }
		#print_r($this->downloads);				
		}
   
		
		function employer() {
        return htmlentities($this->employer);
    }
		
		function salary() {
			
			//echo $this->salary;
			
        return $this->salary;
    }
		
		function location() {
        return htmlentities($this->location);
    }
		
		function reference() {
        return htmlentities($this->reference);
    }
		
		
		
		function applicationinfo() {
        return htmlentities($this->applicationinfo);
    }
	
     function closingdate() {
		
	   //  echo $this->depublish;
	   //echo  date('m',$this->depublish);
	     
	    //$closingDate=mktime(0,0,0,date('m',$this->depublish),date('d',($this->depublish-1)),date('Y',$this->depublish) );
            
            $closingDate=mktime(0,0,0,date('m',$this->depublish),date('d',$this->depublish)-1,date('Y',$this->depublish) );
           
	       //  echo $closingDate;;
		 return  $closingDate;
    }
    function closingdateOLD() {
		    if($this->closingdate=="0"){
				      return false;
				}
				else{
              return $this->closingdate;
				}
    }
		
}




