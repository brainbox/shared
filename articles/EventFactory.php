<?PHP


// Fetches a list of events from a database
class events {

    var $events;
		var $result;
		

    function events ( &$dbConn ) {
		
				 //bbCode Stuff
        /* require PEAR and the parser */
        require_once('PEAR.php');
        require_once('HTML/BBCodeParser.php');
        /* get options from the ini file */
        $config = parse_ini_file('BBCodeParser.ini', true);
        $options = &PEAR::getStaticProperty('HTML_BBCodeParser', '_options');
        $options = $config['HTML_BBCodeParser'];
        unset($options);		 
						 
        // Pull the events from the database
        $sql="SELECT event_id, title, intro, body, textdate, venue, moreinfo, logo, website, email, attachment, author, published FROM events ORDER BY startdate DESC;";
				
				//echo "the SQL " . $sql;
				
        $result = $dbConn->query($sql);
				
        while ( $row = $result->fetch() ) {
						
            $this->events[]=$row;
        }
		}
			
		
		// Factory method creates new instances of event
    function & getevent () {
        $row=each($this->events);

		  if ( $row ) {
				    					
						  return new event($row['value']);
								
        } else {
            reset ( $this->events );
            return false;
        }
		
		}
		function & geteventByID($id) {
												
				foreach($this->events as $name=>$value){
																		
						if($value['event_id']==$id){
																										
									return new event($value);																			 
						}
						
				 }
		}
		
}

// The class built by the factory method
class event {
    var $event_id;
	  var $title;
    var $author;
		var $published; // Unix timestamp
		var $intro;
		var $body;
		
    var $textdate; 
    var $venue; 
		var $moreinfo; 
		var $logo; 
		var $website; 
		var $email; 
		
		var $websitetext;
		var $emailtext;
		var $attachmenttext;


    function event($data) {			 								
						 
				$this->event_id=    $data['event_id'];
        $this->title=				$data['title'];
        $this->author=			$data['author'];
				$this->intro=				$data['intro'];
        $this->published=		$data['published'];
        $this->body=				$data['body'];
				
				$this->textdate=			$data['textdate'];
				$this->venue=				$data['venue'];
				$this->moreinfo=		$data['moreinfo'];
 				$this->logo=				$data['logo'];
				$this->website=			$data['website'];
				$this->email=				$data['email'];
				$this->attachment=				$data['attachment'];		
				
				
    }
		 function event_id() {
        return ($this->event_id);
    }
		
    function title() {
        return htmlspecialchars($this->title);
    }

    function author() {
        return htmlspecialchars($this->author);
    }
		 function published() {
		     //return $this->published;
        return $this->published;
    }

    function textdate() {
        return $this->textdate;
    }
		 function intro() {
        return $this->intro;
    }

    function body() {
        $tempString = $this->body;		
		    $tempString = htmlspecialchars($tempString);
				
				$parser = new HTML_BBCodeParser();
        $parser->setText($tempString);
        $parser->parse();
        $parsed = $parser->getParsed();
							
				//Replace double linebreaks with double breaks
				$parsed = str_replace("\r\n\r\n","</p>\n<p>",$parsed);
				$parsed = str_replace("</ul>","</ul>\n<p>",$parsed);
				$parsed = str_replace("</ol>","</ol>\n<p>",$parsed);
				
				//top and tail it
				$parsed ="\n<p>".$parsed ."</p>\n";
				
        return $parsed;
		
    }
		
		function venue() {
        return $this->venue;
    }
		 function moreinfo() {
        return $this->moreinfo;
    }
		 function logo() {
		    if(isset($this->logo)){	
                return $this->logo;
				}
				else return false;
    }
		 function website() {
		    if(isset($this->website)){	
        				return $this->website;
				}
				else return false;
    }
		function websitelink($websitetext) {
		    if(isset($this->website)){	
        				return '<a href="http://'.$this->website.'">'.$websitetext.'</a>';
				}
				else return false;				
    
		}
		
		 function email() {
		    if(isset($this->email)){	
                return $this->email;
				}
				else return false;
    }
		 function emaillink($emailtext) {
		    if(isset($this->email)){	
                return '<a href="mailto:'.$this->email.'">'.$emailtext.'</a>';
				}
				else return false;
								
    }
		 function attachment() {
		    if(isset($this->attachment)){	
                return $this->attachment;
				}
				else return false;
    }
		 function attachmentlink($attachmenttext) {
		 		if(isset($this->attachment)){		
		            
								$filename = $this->attachment		;					
							  $filepath = 'files/'. $filename;
								$size = fileSizeUnit ( filesize ($filepath ) );
	
        				return '<a href="'.$filepath.'">'.$attachmenttext.' (.pdf '.  $size['size'].' '.$size['unit'].')</a>';
			  }
				else return false;
    }
		
		function fileSizeUnit($size) {
    if ($size >= 1073741824) {
        $size=number_format(($size / 1073741824),1);
        $unit='GB';
    } else if ($size >= 1048576) {
        $size=number_format(($size / 1048576),1);
        $unit='MB';
    } else if ($size >= 1024) {
        $size=number_format(($size / 1024),1);
        $unit='KB';
    } else if ($size >= 0) {
        $unit='B';
    } else {
        $size='0';
        $unit='B';
    }
    return ( array ( 'size'=>$size, 'unit'=>$unit ) );
    }
		
		
}






