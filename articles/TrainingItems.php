<?PHP

require_once('articles/Items.php'); 

class trainingItems extends Items {

   // var $trainingItems;
		//var $result;
		var $isMatched;		

    function trainingItems ( $dbConn,$active ) {
						 
				$this->dbConn=$dbConn;		 
        
				//FOR the  LIVE SITE
				if($active){
							$today=mktime(0,0,0,date('m'), date('d'), date('Y'));
        			$sql='SELECT id, title, intro, isHTML, highlight, body, startdate, deadline, price, memberprice, textdate, venue, image, height, width, alt, email, website, attachment  
							      FROM training 
										WHERE published <= '.$today.' 
										AND depublish > '.$today.' 
										ORDER BY startdate;'; 
				}     
				//FOR ADMINISTRATION
				else{
				      $sql="SELECT id, title, intro, isHTML, highlight, body, startdate, deadline, price, memberprice, textdate, venue, image, height, width, alt, email, website, attachment, author, published,depublish FROM training ORDER BY startdate DESC;";
				}
				
				//echo "the SQL " . $sql;
				$result = $dbConn->query($sql);
				//echo "number of records: ".$result->size();
				
				
				
				while ( $row = $result->fetch() ) {
				
				       
                $this->Items[]=$row;
        }
					
					
					
		}
		
		 function  getItem () {
		
        $row=@each($this->Items);
		    if ( $row ) {
						  return new trainingItem($row['value'],$this->dbConn);
        } else {
            @reset ( $this->Items );
            return false;
        }
		
		}
		
		function  getItemByID($id) {
				
				$isMatched=false;					
				foreach($this->Items as $name=>$value){
																					
						if($value['id']==$id){
									return new trainingItem($value,$this->dbConn);																			 
						}
						
				 }
				 //only gets this far if article not found
				 reset($this->Items);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
				 //echo "<p>No article found";
				 return false;
				
				 
		}
		
		
}

// The class built by the factory method...
//... though not for admen where paramaters are passed directly
class trainingItem extends item {
   
	 //ONLY the unique variables need be declared
		
		public $dbConn;
		var $startdate;
		var $textdate;
		var $venue;
		
		var $image;
		var $width;
		var $height;
		var $alt;
		
		var $deadline;
		
		var $price;
		var $memberprice;
		
		var $email;
		var $website;
		var $attachment;
		var $highlight;
   
	 
				
    function trainingItem($data, $dbConn) {
		//All the data needs to be declared here
		
		    //print_r($data);
				//echo 'Why is this kicking twice??<hr>';
		    $this->dbConn=$dbConn;
		
				$this->id=          $data['id'];
        $this->title=				$data['title'];
				$this->intro=				$data['intro'];
				$this->isHTML=			$data['isHTML'];
				$this->highlight=	  $data['highlight'];
				$this->body=				$data['body'];	
				
				$this->startdate=		$data['startdate'];
				
				
				
				
				$this->textdate=		$data['textdate'];
				
				
				$this->deadline=		$data['deadline'];
				
				
				$this->venue=		    $data['venue'];
				
				$this->image=				$data['image'];
				$this->width=			  $data['width'];
				$this->height=	    $data['height'];
				$this->alt=				  $data['alt'];	
				
				$this->price=				$data['price'];	
				$this->memberprice=	$data['memberprice'];	
				
				//$this->attachment=  @$data['attachment'];
				//$this->website=     @$data['website'];
				//$this->email=       @$data['email'];
				
				
        $this->author=			@$data['author'];
        $this->published=		@$data['published'];
        $this->depublish=		@$data['depublish'];
						
				
				//Get FILE the INFO but not the contents of the file
   			$sql='SELECT id, filename, description, mimetype, filesize, upload_date 
            FROM downloads
    			  WHERE article_id='.$this->id.'
    			  AND section="training";';
    
          // Perform the query
          //	echo $sql;
				//This should have given access to the CREATOR's dbConn	
				
				
        $result=$this->dbConn->query($sql);
				
				if(!$result->size()==0){
				
				   while ( $row = $result->fetch() ) {
					     // echo "got to the rows";
                $this->downloads[]=$row;
					 }
        }
			
						
				
		
    }
		//Only the UNIQUE functions need to follw here... other will be inherited		
				
		
		function highlight() {
        return $this->highlight;
    }
				
		function venue() {
        return $this->venue;
    }
		function startdate() {
        return $this->startdate;
    }
		function deadline() {
        return $this->deadline;
    }
		function textdate() {
		    
				if($this->textdate != ''){
				
				    return $this->textdate;
			 }
			 else{
			      
						return date('j F Y',$this->startdate);
			 
			 }
		    
        
    }
		function price() {
        return $this->price;
    }
		function memberprice() {
        return $this->memberprice;
    }
		
}





