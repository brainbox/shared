<?PHP
/*
This is the base class for a number of subclasses: news, events, articles, etc.
The constructor of the child classes provides data to the getItem and getItemById
classes - both of which are "factory classes" that create the Item class.

The item class is in turn extended for each of the child items: news, events, etc.

Sept 07
Attempting to clean up (I'm going to break it!)... and ups 5.0 stuff such as get and set. There was nothing in Items,
so I'm going to start there

Jan 08
New approach to metadata: by default, they (metaTitle and metaDescription) are BLANK!!!!


*/

class items {

    protected $justOrphans;
    protected $justNavigation;
    protected $downloads;
    		
    function __construct ( $db,$dbPDO=null) {

	$this->db=$db;
	
	//if($dbPDO){
	    $this->dbPDO=$dbPDO;//may be null
	//}
	//print_r($this->dbPDO);
	//echo '<hr>';
	//exit;
	
	//print_r($this->db);
	
	//set up some "constants"
	$this->table='articles';
	$this->active=1;
	$this->section=null;
	$this->limit=null;
	$this->offset=0;
	$this->navOnly=false;
	$this->orphanOnly=false;
	
    }
    function setTable($table){
	$this->table=$table;
    }
    function setActive($active){
	$this->active=$active;
    }
    
    //A function to allow selection of just Nav pages, or just orphans.
    //Default is all
    function setType($type){
	
	switch($type){
	    
	    case('navigation'):
		$this->navOnly=true;
	    break;
	
	    case('orphan'):
		$this->orphanOnly=true;
	    break;
		
	}
	
    }
   
    function setLimit($limit){
	$this->limit=$limit; 
    }
    function setOffset($offset){
	$this->offset=$offset; 
    }
    function setID($id){
	$this->id=$id;
    }
    function setLocation($location){
        $this->location=$location;
    }
    
    function initialise(){
    
	$sql='SELECT * FROM ' . $this->table;
	$sql .= ' WHERE 1=1 ';
	if($this->active){ // This is the default
	    
	    $today=mktime(0,0,0,date('m'), date('d'), date('Y'));
	    $sql .= " AND published <= $today AND depublish > $today ";
	}
	if($this->section){  
	    $sql .= " AND section= '" . $this->section . "' ";
	}
	if($this->navOnly){  
	    $sql .= " AND isOrphan= '0' ";
	}
	if($this->orphanOnly){  
	    $sql .= " AND isOrphan= '1' ";
	}
	
	if (isset($this->id) ){
	     $sql .= " AND id=" . $this->id;
	}
        if(isset($this->location) ){ //NOTE need to use isset, because it will be clank for the home page.
	     $sql .= " AND location='" . $this->location . "' ";
	}
	else{
	    
	    if($this->justOrphans){
		$sql .= " AND isOrphan ='1'";
	    }
	    
	    if($this->justNavigation){
		$sql .= " AND isOrphan ='1'";
	    }
	    
	    
    
	    $sql .= ' ORDER BY 0+published DESC ';
   
	    if($this->limit){

		$sql .= " LIMIT " . $this->offset . " , " . $this->limit	;
    
	    }
	}
	
	//echo '<hr />';																
	//echo "the SQL " . $sql;
	//echo '<hr />';	
	
	$result = $this->db->query($sql);
	    
	   // $this->items=array();
	    
	    if(($result->size())!=0){
	    
		//echo "got here";
		while ( $row = $result->fetch() ) {
		//echo $row;
		    $this->items[]=$row;
		}
	    }
	    else{
		return false;
	    
	    
	    }
	   // echo '<pre>';
	    //print_r($this->items);
	   // exit;
	    
    }
		
		
    // Factory method creates new instances of flexiItem
    function fetch () {
		    
	//echo '<pre>';
	//print_r($this->items);	    
		    
	if(isset($this->items)){
    		
	    $row=each($this->items);
	    if ( $row ) {
		//echo '<pre>';
		//print_r($row);
		//exit;
		return new item($this->db,$row['value'],$this->dbPDO);
	    
	    }
	    else {
		reset ( $this->items);
		return false;
	    }
	}
	else{
	    return false;
	}		 
	    
    }
		
    function  getItemByID($id) {
	//echo '<pre>';
	//print_r($this->items);

	foreach($this->items as $name=>$value){
	
	    if($value['id']==$id){

		return new item($this->db,$value,$this->dbPDO);																			 
	    }
	
	}
	//only gets this far if article not found
	//print_r($this->items);
	//exit;
	reset($this->items);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
	
	return false;
    
    }
    
    
    
}

// The class built by the factory method
class item {
    protected $id;
    protected $parentID;
    protected $title;
  
    protected $shortTitle;
    protected $metaTitle;
    protected $metaDescription;
    
    protected $location;
    protected $filename;
    protected $filepath;
    
    protected $width;
    protected $height;
    
    protected $isHTML;
    protected $body;
    protected $bodyHTML;
    
    protected $attachment;
    protected $website;
    protected $email;
    
    protected $author;
    protected $published; // Unix timestamp
    protected $depublish; // Unix timestamp
    
    protected $code;
    
    protected $db;
    protected $dbPDO;
    

    
    public $downloads;

   

    function __construct($db,$data,$dbPDO=null) {
	//print_r($data);
	//exit;
	//Not needed for reading, but IS needed for writing
	//echo '<hr>';
	$this->db=$db;
	
	If($dbPDO){
	    $this->dbPDO=$dbPDO;
	}
	
	
	
	
	$this->id=          	$data['id'];
	$this->parentID=	$data['parent_id'];
	
	
	$this->section=     	$data['section'];
	$this->title=	 	$data['title'];
	$this->shortTitle=	$data['short_title'];
	
	
	$this->titleNote=	$data['title_note'];
	
	$this->location=		@$data['location'];
	    
	$locationArray=explode('/',$this->location);
	    
	$this->filename=array_pop($locationArray);
	    
        $this->filepath=implode('/',$locationArray);	
	
	
	$this->metaTitle=	$data['meta_title']; 
	
	
	//$this->intro=		$data['intro'];
	$this->metaDescription=	$data['meta_description']; 
	
	$this->body=		$data['body'];
	$this->isHTML=		$data['isHTML'];
					
	$this->image=		$data['image'];
	$this->width=		$data['width'];
	$this->height=		$data['height'];
	$this->alt=		$data['alt'];
	
	
	$this->attachment=  	$data['attachment'];
	$this->website=     	$data['website'];
	$this->email=       	$data['email'];
	
	$this->author=		$data['author'];
	$this->published=	@$data['published'];
	$this->depublish=	@$data['depublish'];
	
	$this->code=		$data['code'];
	
     
        
        
    }
    
    
    /**
     * Fills in metaTitle where necessary
     *
     * By default, both metaTitle and metaData are blank. Need to make sure that
     * if the start BLANK, they end up blank (i.e. so they are not written to the database)
     */
    public function metaTitle(){
        
        if (trim($this->metaTitle)!='') {

            return $this->metaTitle;
        } else {
        
            $metaTitleLength=(int)META_TITLE_LENGTH;
            
            $shortNameLength=(int) count_chars(SHORT_NAME);
            $tempString=$this->title;
            
            $maxTitleLength= $metaTitleLength-$shortNameLength-3;
            $tempstring=substr($tempString,0,$maxTitleLength );
            
            return $tempstring . ' | ' . SHORT_NAME;

        }
    }
    
    /**
     * Fills in metaTitle where necessary
     *
     * By default, both metaTitle and metaData are blank. Need to make sure that
     * if the start BLANK, they end up blank (i.e. so they are not written to the database)
     */
    public function metaDescription() {
        
        if (trim($this->metaDescription)!='') {
            
            return substr($this->metaDescription,0,META_DESCRIPTION_LENGTH);
        } else {
            
	    //echo '<hr>' . $this->metaDescription;
	    $tempString=$this->bodyHTML();
	     //echo '<hr>' . $tempString;
	    $tempString=strip_tags($tempString);
	    
	    
	    $tempString=substr($tempString,0,META_DESCRIPTION_LENGTH);
	    
	    return $tempString;

        }
    }
    
    
    public function __get($nm){
	
	if (isset($this->$nm)) {
	    return $this->$nm;
	}
	else{
	    return false;
	}
	
	
    }
    
    
    public function __set($nm, $val){
		
	//echo '<p>name is ' . $nm . ' and value is ' . $val;
	//First check if we have one...
	if (isset($this->$nm)) {
	    
	    //Then check if it has changed
	    if($this->$nm !=$val){
		
		//Some items will need special treatment... do that here, or have a separate
		//CHANGE OF location is a biggie... sot it gets different treatment
		
		switch($nm){
		    
		    case('filename'):
			//Note that the "comit" is instant
                        
                        //echo $val;
                        
			
			$this->_changeFilename($this->location, $val);//pass the old LOCATION and the new filename
			$this->$nm =$val;
                        
                        if($this->filePath!=''){
                            $this->location=$this->filepath . '/' . $val;
                        }
                        else{
                            $this->location=$val;
                        }
		    break;
		
		    default:
		
                        $this->$nm =$val;
		
		}
	    }
	    return true;
	}
	else{
	    return false;
	}
    }
    
    
    //Function to get a word count from the meta description
    function teaser($wordCount){
        
        $wordArray=explode(' ',$this->metaDescription);
        if($wordCount>count($wordArray)){
            
            return $this->metaDescription;
        }
        else{
            
            $wordArray=array_slice($wordArray,0,$wordCount);
            
            $teaser=implode(' ',$wordArray) . '&hellip;';;
            
            //echo $teaser;
            
            return $teaser;
        }
        
        
    }
    
    
    function _changeFilename($oldLocation,$newFilename){
		
	//To do this properly, need to be able to change
	// section/page_name to section/new-page-name
	// AND  section/page_name/child to section/new-page-name/child
	
	// APPROACH:
	//    *  Get this one. Split it up. Work out the index of the last part
	//    * get the rest. Split them up. Test the index against the OLD location
	//    * update any matches.
	
	$oldArray=explode('/',$oldLocation);
	$position=count(  $oldArray) -1;  //COUNT BERFORE POPPING!!!!!!
	
	$oldFilename=array_pop($oldArray);	
	
	//print_r( $oldArray);
	
	
	//echo '<hr> old filename '  . $oldFilename;
	//echo '<hr> new filename '  . $newFilename;
	//echo '<hr> position '  . $position;
	//exit;
	
	
	
	$sql= "SELECT id, location from articles WHERE isOrphan='0'";
	//echo $sql;
	
	//print_r($this->dbPDO);
	
	$stmnt=$this->dbPDO->prepare($sql);
	
	
	try {
		$stmnt->execute();
		//echo 'tryyyy';
		$results= $stmnt->fetchAll(PDO::FETCH_ASSOC);
		
	}
	catch (PDOException $e) {
		print "Error!: " . $e->getMessage() . "<br/>";
		die();
	}

	
	$SQLarray=array();
	foreach($results as $name=>$value){
			
	    $genLocationArray=explode('/',$value['location']);
	    
	    //replace ALL that have the same value at the same "index"
	    if( $genLocationArray[$position] == $oldFilename) {
			    
			    
	     
                // echo '<hr>XXXXXXXXXX' . $value['location'];
                 
                 //cHANGE THE ARRAY
                $genLocationArray[$position]=$newFilename;
                
                
                //print_r($genLocationArray);
                //exit;
                
                $newLocation=trim(implode('/',$genLocationArray), '/');
                //echo '<hr>New location ' . $newLocation;
                
                $SQLarray[]="UPDATE articles set location='$newLocation' WHERE id=" . $value['id']; 
                
	    }

	}
	
	
	// print_r($SQLarray);
	try {
	    //$dbh = new PDO('odbc:SAMPLE', 'db2inst1', 'ibmdb2',
	    //array(PDO::ATTR_PERSISTENT => true));
	    //echo "Connected\n";
	    //$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	    
	    $this->dbPDO->beginTransaction();
	    
	    foreach($SQLarray as $name=>$value){
			    
			    $this->dbPDO->exec($value);
			    
			    //echo '<hr>' . $value;
			    
			    
	    }
	    
	    $this->dbPDO->commit();
	
	} catch (Exception $e) {
			$this->dbPDO->rollBack();
			echo "Failed: " . $e->getMessage();
	}
	
    
    
	     
    }
    
    function id() {
	//echo 'In the parent in id... and the id is ' .$this->id ;
        return ($this->id);
    }
    /*	
    function title() {
		     			 
        require_once("string/stringCleaner.php"); 				 
	$cleanString = new Cleaner($this->title);
        return $cleanString->getCleanText(300);

    }
    */
    function intro($wordLimit=null) {
	
		    If(!$this->intro){
			return false;
		    }
		    
	if(! $wordLimit){
	     return $this->intro;
		    }
		    else{
		       
			      $words = explode(" ", $this->intro);
				    //print_r($words);
			      $words = array_slice ( $words, 0, $wordLimit); //30 words
				    //print_r($words);
				    $words = implode(' ',$words);
				    //print_r($words); 
			return $words;
		    }
}
	    
     function website() {
	if(isset($this->website)){	
	    return $this->website;
	}
	else return false;
}
    function websitelink($websiteText='') {
	if(isset($this->website)){	
			    
	    if($websiteText==''){
		$websiteText=$this->website;
	    }											
	    
	    return '<a href="http://'.$this->website.'">'.$websiteText.'</a>';
	}
	else return false;				

    }
    
    
    
    function getImageLink($baseURL, $classList='floatright'){
	if($this->image){		
	
	
	    return '<img src="'.$baseURL.'/images/'.$this->section.'/'.$this->image.'" width="'.$this->width.'" height="'.$this->height.'"  alt="'.$this->alt.'" class="' . $classList . '" />';
	}
	else return false;					 
    }		
    
    function getThumbLink($baseURL, $baseFilePath, $maxWidth, $maxHeight){
	if($this->image){		 
	
	    //Test if a thumb exists - create it
	    $thumbFullPath =$baseFilePath.'images/'.$this->section.'/thumb_'.$this->image;
	    //echo '<p>' .$thumbFullPath;
	    
	    //File exists needs to use a windows path on... windows
	    
	    if(file_exists($thumbFullPath)){
	    
	    
	    //do nothing
	    
	    
	    }
	    else{	 									 
	    
		//echo "<p>got here";
		require_once('Images/Thumbnail.php');
	
		// Instantiate the thumbnail
		$tn=new ThumbNail($maxWidth,$maxHeight);
		
		// Load an image into a string (this could be from a database)
		$image=file_get_contents($baseFilePath.'images/'.$this->section.'/'.$this->image);
												
		// Load the image data
		$tn->loadData($image,'image/jpeg');
		
		// Build the thumbnail and store as a file
		$tn->buildThumb($thumbFullPath);
						   
	    }
	//get its dimensions
	
	$imageinfo = getimagesize($thumbFullPath);
	
	//print_r($imageinfo);
	
	return '<img src="'.$baseURL.'/images/'.$this->section.'/thumb_'.$this->image.'" '.$imageinfo[3].' alt="'.$this->alt.'" class="floatleft" />';
	}
      //$baseURL.'images/'.$this->section.'/'.$this->image){.'" width="'.$this->width.'" height="'.$this->height.'"  alt="'.$this->alt.'" class="floatright" />';
		    
	else return false;					 
    }		
    
    
    
    
    
    function getImageLinkNoSection($baseURL, $subdirectory, $classList='floatright'){
        
        
             
        
        
	if($this->image){		 				
	    return '<img src="'.$baseURL.'/images/'.$subdirectory.'/'.$this->image.'" width="'.$this->width.'" height="'.$this->height.'"  alt="'.$this->alt.'" class="'.$classList.'" />';
	}
	else return false;					 
    }			
    
    
    function getImagePath(){
	if($this->image){		 			 
	    //return 'images/'.$this->section.'/'.$this->image;
	    return $this->image;
	}
	else return false;					 
    }		
    
    
    
    
    function email() {
	if(isset($this->email)){	
	    return $this->email;
	}
	else return false;
}
     function emaillink($emailText='') {
	if(isset($this->email)){	
		    
						    if($emailText==''){
							  $emailText=$this->email;
						    }									
    return '<a href="mailto:'.$this->email.'">'.$emailText.'</a>';
		    }
		    else return false;
						    
}
    
     function attachment() {
	if(isset($this->attachment)){	
    return $this->attachment;
		    }
		    else return false;
}
    function attachmentlink($attachmentText='') {
     
	//Would be better to store the file size in the database
     
	if(isset($this->attachment)){		
		
            $filename = $this->attachment		;					
            $filepath = 'files/'. $filename;
            if(!is_file($filepath)){  //Test that it exists
                return false;
            }
            $size = $this->fileSizeUnit ( filesize ($filepath));
            
            if($attachmentText==''){
                  $attachmentText=$filename;
            }

	    return '<a href="'.$filepath.'">'.$attachmentText.' (.pdf '.  $size['size'].' '.$size['unit'].')</a>';
    
        }
        else return false;
}
	
    function author() {
        return htmlspecialchars($this->author);
    }
    function published() {
		    
        return $this->published;
    }
    function depublish() {
		    
        return $this->depublish;
    }
		
		
    function status(){
	//NOT YET PUBLISHED?
	//Three values: pending, active, 		 
		     
	//Timestamp for the start of today
	
	$today=mktime(0,0,0,date('m'), date('d'), date('Y'));
	
	if($today<$this->published){
	    return 'pending';
	}
	else if($today>$this->depublish){
	    return 'expired';
	}
	else{
	    return 'active';
	}				
    }

    function date() {
        return $this->date;
    }
		

    function body() {
		
	require_once('string/stringCleaner.php');
	$cleanString= new Cleaner($this->body);
	
	return $cleanString->getCleanHTML();
	
	}
   
    function bodyHTML($isHTML='false') {
	
	//echo "<p>Got here ". $this->isHTML;
	
	if($this->isHTML==0){ //BBCode Option
	
	   //echo "<p>BBCODE";
										       
	    /* require PEAR and the parser */
	    require_once('PEAR.php');
	    require_once('HTML/BBCodeParser.php');
	    /* get options from the ini file */
	    /*
	    $config = parse_ini_file('BBCodeParser.ini', true);
	    $options = &PEAR::getStaticProperty('HTML_BBCodeParser', '_options');
	    $options = $config['HTML_BBCodeParser'];
	    unset($options);		 
	    */				
	    $tempString = $this->body;		
	    $tempString = htmlspecialchars($tempString);
	    //$tempString = mb_convert_encoding($tempString, "UTF-8", "auto"); //an experiment!
	    //$tempString = htmlentities($tempString, ENT_QUOTES,'UTF-8');//an experiment
	    
	    
	    
	    $parser = new HTML_BBCodeParser(
		       array(  'quotestyle'    => 'double',
	    'quotewhat'     => 'all',
	    'open'          => '[',
	    'close'         => ']',
	    'xmlclose'      => true,
	    'filters'       => 'Basic,Extended,Links,Lists,Email,Images,Div'
	    )
									    );
			       /// 21 Dec 06 - removed Images dfeom the filter list								 
												
												
	    $parser->setText($tempString);
	    $parser->parse();
	    $parsed = $parser->getParsed();
	    
	    //echo '<textarea rows="130" cols="430" style="width: 400px; height: 300px" >';
	    //echo $parsed;
	    //echo '</textarea>';
	    
	    //$parsed=nl2br($parsed);//DOES A RUBBISH JOB
	
	
	
	    //Replace double linebreaks with double breaks
	    //$parsed = str_replace("\r\n\r\n","</p>\n<p>",$parsed);
	    //$parsed = str_replace("</ul>","</ul>\n<p>",$parsed);
	    //$parsed = str_replace("</ol>","</ol>\n<p>",$parsed);
	    
	    //top and tail it
	    //$parsed ="\n<p>".$parsed ."</p>\n";
	    
	    //echo $parsed;
	    
	    //EXPERIMENT
				    
	    $lines = explode("\r\n",$parsed);
	    
			   
				
	    //print_r($lines);
		
	    
	    $newlines = array();
	    
	    foreach($lines as $name=>$value){
	    
				    //echo '<hr>' . htmlentities($value);
				 
				  // echo "<p>Lenth of line is" .strlen($value);
	    if (strlen($value)>1){
					
					 
		$excludeArray=array('<h','<d','<o','<u','<l','</');
		
		$firstTwoChars=substr($value,0,2);
		
		if(array_search($firstTwoChars,$excludeArray) === false){//not on the list, then....
		
		$value='<p>'.$value .'</p>';           ///wrap it in a paragraph
	    
							 
		}
		$newlines[]=  $value;
	    }
				 }	
	    $parsed = implode("\r\n",$newlines);						
	    
	    //get rid of any empty list items
	    
	    
	    
	    
	    
	    
	    //echo '<hr>';
	    //echo '<textarea rows="130" cols="430" style="width: 400px; height: 300px" >';
	    //echo $parsed;
	    //echo '</textarea>';
	    
	    
	    
	    $html=str_replace("<li>\r\n</li>",'',$parsed);
	    
	    
	    //echo '<hr>';
	    //echo '<textarea rows="130" cols="430" style="width: 400px; height: 300px" >';
	    //echo $parsed;
	    //echo '</textarea>';
	    
	    //return $parsed;

		       
	}
	else{
	
	   $html=$this->body;

	}
	
	//Final clean-up
		    
	require_once('string/stringCleaner.php');
	$cleanString= new Cleaner($html);
	
	return $cleanString->getCleanHTML();

	
    }

    function fileSizeUnit($size) {
		
						 //returns false if file not found
						 
						
						 
						 
        if ($size >= 1073741824) {
            $size=number_format(($size/1073741824),2);
            $unit='GB';
        } else if ($size >= 1048576) {
            $size=number_format(($size/1048576),2);
            $unit='MB';
        } else if ($size >= 1024) {
            $size=number_format(($size/1024),2);
            $unit='KB';
        } else if ($size > 0) {
            $unit='B';
        } else {
				
						$size='0';
            $unit='B';
        }
        return ( array ( 'size'=>$size, 'unit'=>$unit ) );
      }
			
	//tests for the existance of downloads
		function areDownloads(){
		     
				 	if(isset($this->downloads)){
					     return true;
					}
					else{
					     return false;
					}

		}
		
		// Factory method creates new instances of ArticleDownloads
    function getDownload() {
		
		  
	#echo 'gggg' . $this->downloads;
        	    
	if(isset($this->downloads)){
            $row=each($this->downloads);
    				
    		    if ( $row ) {
    				     // echo "next line should create the download";
    						  return new ArticleDownload($row['value']);
            } else {
                reset ( $this->downloads);
                return false;
            }
		        return true;  //will be used to test if there are any 
			  }
				else{
				     return false;
			 }
			 
		}
		
    private function _applyBusinessRules(){
	
	//Fill in some gaps;
	
	 // echo '<hr>' . $this->title;
	if($this->shortTitle==''){
	   
	    $this->shortTitle=$this->title;
	    // echo '<hr>' . $this->shortTitle;
	}
        
        if (strlen($this->metaTitle) > META_TITLE_LENGTH) {
            
            $this->metaTitle=substr($this->metaTitle,0,META_TITLE_LENGTH);
            
        }
        
        

	
    }
    
    function finalise(){
    
	$this->_applyBusinessRules();
    
	$sql='  UPDATE articles 
	SET
	title =          	"'.mysql_real_escape_string($this->title) .'",  
	short_title =          	"'.mysql_real_escape_string($this->shortTitle) .'", 
	meta_title =          	"'.mysql_real_escape_string($this->metaTitle) .'",  
	meta_description =      "'.mysql_real_escape_string($this->metaDescription) .'", 
	location =              "'.mysql_real_escape_string(strtolower($this->location) ) .'", 
	published =      	"'.mysql_real_escape_string($this->published) .'", 
	depublish =      	"'.mysql_real_escape_string($this->depublish) .'", 
	last_update = 		"' . time() . '" ,

	code =          	"'.mysql_real_escape_string($this->code) .'",  								

	isHTML =          	"'.mysql_real_escape_string($this->isHTML) .'",  
	body =           	"'.mysql_real_escape_string($this->body) .'"
	
	WHERE id=		'.$this->id.';';
	
	//echo $sql;
	//exit;
	//print_r($this->db);
	$this->db->query($sql);
	//exit;
	}		
    
    
    
}


// The class built by the factory method	
class ArticleDownload{
		
        //Constructor
        function ArticleDownload($data) {
				
				//echo "<hr>got to Article download";				 
				//print_r($data);				 
				
				 
			  $this->id=              $data['id'];
			  $this->filename=        $data['filename'];
        $this->description=		  $data['description'];
				$this->mimetype=			  $data['mimetype'];
				$this->filesize=				$data['filesize'];
				$this->upload_date=			$data['upload_date'];
				
				
				
      			
				}     
		
		    function getID(){
				     return $this->id;
				}
		    function filename(){
				     return $this->filename;
				}
				function description(){
				     return $this->description;
				}
				function mimetype(){
				     return $this->mimetype;
				}
				
				function mimetypelabel(){
				    
						$mimeTypeArray=array( 
																  'applicaton/pdf'=>						        '.pdf',
																	'application/pdf'=>						        '.pdf',
																	
																	'application/msword'=>                '.doc',
																	'application/vnd.openxmlformats-officedocument.wordprocessingml.document'=>                '.docx',

																	'application/powerpoint'=>            '.ppt',
																	'application/mspowerpoint'=>          '.ppt',   
																	'application/vnd.ms-powerpoint'  =>   '.ppt'       
																	
						                    );
																
						//echo "and the MIME is: " . 		$this->mimetype;			
						
						
											
						return $mimeTypeArray[$this->mimetype];										
				
				}
				
				
				
				function filesizelabel(){
				    return human_size($this->filesize);
				}
				
				
				
							
				function filesize(){
				     return $this->filesize;
				}
								
				function upload_date(){
				     return $this->upload_date;
				}
				
				
				
    
				
								
}



function human_size($size, $decimals = 1) {
    $suffix = array('Bytes','KB','MB','GB','TB','PB','EB','ZB','YB','NB','DB');
    $i = 0;
 
    while ($size >= 1024 && ($i < count($suffix) - 1)){
      $size /= 1024;
      $i++;
    }
 
    return round($size, $decimals).' '.$suffix[$i];
}
 






