<?PHP
/*Intended to be used with the Item class to write the various "articles" to an HTML page
*/
Class ItemReader{

    var $item;
		var $itemHTML;
		var $subdirectory;
		var $returnLink;
		var $trainingDatasheet;
		
    //constructor
		function ItemReader($theItem, $returnPath='', $baseURL='/', $pathToImages=null){
		     
				 $this->theItem =       $theItem;
				 $this->returnPath =    $returnPath;
				 $this->baseURL =       $baseURL;
                 $this->pathToImages =  $pathToImages;
				 //echo '<pre>';
				 //print_r($theItem);
				 //exit;
				 
		
		}
    function addReturnLink($returnText,$class="instructions"){
		    
		     $this->itemHTML .= '<div class="'.$class.'"><a href="'.$this->returnPath.'">'.$returnText.'</a></div>';  
		     
				 
				 //echo "ddd". $this->itemHTML;
		}
		function getReturnLink($returnText,$class="instructions"){
		    
			  return $this->returnLink ='<div class="'.$class.'"><a href="'.$this->returnPath.'">'.$returnText.'</a></div>'; 
				 
		     
		}
		function addHeader(){
		
		
				 $this->itemHTML .=  '<h2>'.$this->theItem->title.'</h2>' ;
				 
		}
		/*
		function OLDaddImage(){
		     if ($this->theItem->logo()){
			$logoFullPath='images/'.$this->theItem->logo();
			$logoDimensions= getimagesize($logoFullPath);
			$width =  $logoDimensions[0];
			$height = $logoDimensions[1];
						
			$this->itemHTML .=   '<div class="floatright"><img src="'.$logoFullPath.'" width="'.$width.'" height="'.$height.'"   alt="article"></div>';	
		     }
		}
		*/
		function addImage($classList="floatright"){
		     
			$this->itemHTML .=   $this->theItem->getImageLink($this->pathToImages,$classList);	
		    
		}
		
		//Above function assumes Item has a section - so doesnm't work for JOBS and EVETNS
		function addImageNoSection($subdirectory, $classList="floatright"){
		     
			$this->itemHTML .=   $this->theItem->getImageLinkNoSection($this->baseURL,$subdirectory, $classList);	
		    
		}
		
		
		
		function addEventDate($dateText='', $class="date"){
		 
		// print_r($this->theItem);
		 //echo '<hr>' . $this->theItem->startDate;
		 //echo $this->theItem->enddate;
		
		     $this->itemHTML .=  '<div class="'.$class.'">';
				 
				 if($dateText){
				     $this->itemHTML .= $dateText . ': ';
				 } 
				 
				 //If there's an end date, work out what to do with it
				 if($this->theItem->endDate()){
				     //same year 
						 if(date('Y',$this->theItem->startDate() ) == date('Y',$this->theItem->endDate() ) ){
						 
								//same year and same month
								if(date('M',$this->theItem->startDate() ) == date('M',$this->theItem->endDate() ) ){
										$startDate= date('d',$this->theItem->startDate() );
								}
								//same year, diff month
								else{
								     $startDate= date('d M',$this->theItem->startDate() );
								}
						 
						 
						 }
						 //diff year
						 else{
						       $startDate= date('d M Y',$this->theItem->startDate() );
						 }
		   //Construct the string
					 $this->itemHTML .=$startDate . ' - ' . date('d M Y',$this->theItem->endDate()).'</div>';	 
						 
				 } 
				 else{
				      $this->itemHTML .= date('d M Y',$this->theItem->startDate()).'</div>';
				 }			
							
		}
		
		
		
		function addEventDateNew($dateText='', $class="date"){
		 
		 //print_r($this->theItem);
		 //echo '<hr>' . $this->theItem->startDate;
		// echo $this->theItem->enddate;
		
		     $this->itemHTML .=  '<div class="'.$class.'">';
				 
				  
				  
				  //If there's an end date, work out what to do with it
				  //echo '<p>S ' . $this->theItem->startdate();
				  //echo '<p>E ' . $this->theItem->enddate;
				  if(!$this->theItem->enddate ||  $this->theItem->startdate() == $this->theItem->enddate){
                                                   
                                                  // echo '<hr>here';
						   $this->itemHTML .= date('d M Y',$this->theItem->startdate());
				  } 
				  else{
					//echo '<hr> complex';	   
				  
				  //same year 
						   if(date('Y',$this->theItem->startdate() ) == date('Y',$this->theItem->enddate ) ){
						   
                                                                    //same year and same month
                                                                    if(date('M',$this->theItem->startdate() ) == date('M',$this->theItem->enddate ) ){
                                                                                     $startDate= date('d',$this->theItem->startdate() );
                                                                    }
                                                                    //same year, diff month
                                                                    else{
                                                                                     $startDate= date('d M',$this->theItem->startdate() );
                                                                    }
						   
						   
						   }
						   //diff year
						   else{
                                                                    $startDate= date('d M Y',$this->theItem->startdate() );
						   }
						   //Construct the string
                                                   //echo '<p>Start date ' . $startDate;
                                                   //exit;
                                                   
						  $this->itemHTML .= $startDate . ' - ' . date('d M Y',$this->theItem->enddate );	 
				  
				  }
		  $this->itemHTML .=  '</div>';			
		}
		
		

		
		function addEventDateXXX(){
	    
	   
	    //easy case first: one-day event;
		    
	    if($this->startdate == $this->enddate  ){
		    
		    return date('d M Y', $this->startdate);
	    }
	    //more tricky!
	    else {
		    //Eveything is different!
		    if( date('Y', $this->startdate)!=date('Y', $this->enddate)  ){
			    return date('d M Y', $this->startdate) . ' - ' . date('d M Y', $this->enddate);
			    
		    }
		    //Differnet months
		    elseif(date('m', $this->startdate)!=date('m', $this->enddate) ){
			    return date('d M', $this->startdate) . ' - ' . date('d M Y', $this->enddate);
				    
		    }
		    else{
			    return date('d', $this->startdate) . ' - ' . date('d M Y', $this->enddate);
		    }
	    }
    }
		
		function addNewsDate($dateText='', $class="date"){
		
		     $this->itemHTML .=  '<div class="'.$class.'">';
				 if($dateText){
				     $this->itemHTML .= $dateText . ': ';
				 } 
				 $this->itemHTML .= date('d M Y',$this->theItem->published()).'</div>';
		}
		
		function addDate($dateText='', $class="date"){
		
		     $this->itemHTML .=  '<div class="'.$class.'">';
				 if($dateText){
				     $this->itemHTML .= $dateText . ': ';
				 } 
				 $this->itemHTML .= date('j F Y',$this->theItem->published()).'</div>';
		}
		
		function addEmployer($class="employer"){
		
		     $this->itemHTML .=  '<div class="'.$class.'">'.$this->theItem->employer().'</div>';
		}
		function addSalary($class="salary"){
		
		     $this->itemHTML .=  '<div class="'.$class.'">'.$this->theItem->salary().'</div>';
		}
		
		function addEventDetails($class="venue"){
		
		     $this->itemHTML .=  '<div class="'.$class.'">';
				 $this->itemHTML .=  $this->theItem->dateRange();
				 $this->itemHTML .=  ' | ';
				 $this->itemHTML .=  $this->theItem->location();
				 $this->itemHTML .=  '<br />';
				 $this->itemHTML .=  $this->theItem->venue();
				 $this->itemHTML .=  '</div>';
		
		}
		function addtrainingDetails($class="venue"){
		
		     $this->itemHTML .=  '<div class="'.$class.'">';
				 $this->itemHTML .=  $this->theItem->textDate();
				 $this->itemHTML .=  ' | ';
				 $this->itemHTML .=  $this->theItem->venue();
				 $this->itemHTML .=  '</div>';
		
		}
		
		
		
		
		function addArticleBody($class=null){ 
		
		     if(isset($class)){ //If class supplied, wrap it
						  $this->itemHTML .=  '<div class="'.$class.'">'.$this->theItem->bodyHTML().'</div>';
				 }
				 else{
						$this->itemHTML .=  $this->theItem->bodyHTML();
				 }
		}
		
		 function getDeadline(){
		    
				return $this->theItem->deadline();
		 
		 }
		 
		 function addCancellationPolicy(){
		  
			 $cancellationString= '<h3>Cancellation Policy</h3>';
						 $cancellationString .= '<p>Cancellations made in writing (by email to info@irms.org.uk 
								or fax to 01494 488590) by ' .date('l dS \of F Y',$this->theItem->deadline() ) . ' will be refunded,
											less an administration charge of �35. <strong>Cancellations after this 
											date will not be refunded</strong>. Organisations can, however, change the name of a delegate at any time.</p>';
						
						 $this->itemHTML .=  $cancellationString;
		 
		 
		 
		 
		 }
		 
		  function addTrainingDatasheet(){
																					
					 
					 
					 $this->itemHTML .= '<h3>Price and Booking Information</h3>';
					 $this->itemHTML .=  $this->getTrainingDatasheet();
		   
		// 
		
		} 
		function getTrainingDatasheet($includePrice=true){
		       require_once('HTML/Table.php'); 		
											 
		       $table =new HTML_Table(array( 'summary'=>    'Training Datasheet', 'class'=>			 'trainingdata'				));
	   
					 $table->addRow(array('Course Title:',				     $this->theItem->title								    ));
					 $table->addRow(array('Date:',		 								 $this->theItem->textDate()								    )); 			 
					 $table->addRow(array('Venue:',		 								 $this->theItem->venue()								    ));
					 
					 //Get the costs
					 
					 
					// echo 'the intro should be: ' . $this->theItem->intro()	;
					 
					 if($includePrice==true){
					    $table->addRow(array('Price (members):',		 			 $this->_vatString($this->theItem->memberprice()	)						    ));
					    $table->addRow(array('Price (non-members):',		 	 $this->_vatString($this->theItem->price()				)				    ));
							
							
			 }// failing that, put the course summary in
					 else{
					    $introString=$this->theItem->intro();
							
							if($introString != '' ){
							   //echo 'got here';
							   $table->addRow(array('Course summary:',		 	 $introString		    ));
							}
					 
					 
					 }
						
				  
				   
					 $table->setColType(0,"th");		
				   $table->setColAttributes(0,'scope=row');
				   
					
					
					 
					return $table->toHTML();
		   
		}
		
		function getFinalPriceString($memberStatus){
		
		    switch ($memberStatus){
				
				  Case 'member':
					  return  $this->_vatString($this->theItem->memberprice()  );
					  
					Case 'non-member':
					  
				     return $this->_vatString($this->theItem->price() );
				
				  Default:
				    return false;
				}
		
		
		
		}
		
		function _vatString($net){
		   
			 $net=round($net,2);
			 $vat=round( ($net*0.175),2);
		   $gross=round( ($net+$vat),2);
			 
		   return '&pound;' . number_format($gross,2) . ' (&pound;' . number_format($net,2) . ' plus &pound;' .  number_format($vat,2)  . ' VAT)';
		}
		
		
		
		 function addJobDatasheet(){
		 
				   require_once('HTML/Table.php'); 		
											 
		       $table =new HTML_Table(array( 'summary'=>    'Job Datasheet', 'class'=>			 'jobdata'				));
	   
					 $table->addRow(array('Job Title:',				     $this->theItem->title								    ));
				
		   
					 
					 
					 $table->addRow(array('Employer:',				     $this->theItem->employer()								    ));
				   if($this->theItem->salary()){
								 $table->addRow(array('Salary:',				 $this->theItem->salary()										  ));
		   }
					 if($this->theItem->location()){
						 $table->addRow(array('Location:',		 $this->theItem->location()								    ));
					 }
					 if($this->theItem->reference()){
			  $table->addRow(array('Job Reference:',	$this->theItem->reference()							    ));
		   }
			
			    $table->addRow(array('Closing Date:',	 date('j F Y',$this->theItem->closingdate())  ));   
			
			    $table->setColType(0,"TH");		
				   $table->setColAttributes(0,'scope=row');
				   
					 
					 $this->itemHTML .=  $table->toHTML();
		    
			
		    
	   
		    
		
		}
				
		function addDownloads($downloadLocation='download'){
		
	
	
		   # echo "the damn download is" .	$this->theItem->getDownload();		
			#exit;
            
				 if($this->theItem->areDownloads()){
				    
				  
				    $pdfPresent=false;//used to test for the existence of a .pdf
		    
				require_once('HTML/Table.php'); 
			//Downloads
			$table =new HTML_Table(array( 'summary'=>    'Downloads',
																		'class'=>			 'downloads'				));
			$classArray=array('class=col1','class=col2','class=col3');
																
																		
			//get access to the Download Class
			while ( $theDownload=$this->theItem->getDownload() ) {
						
						    //Set up infor required to serve the correct icon
								//NOTE: NO NEED TO HANDLE THE DIFF PDF TYPES HERE
								
						    $mimeInfo=array('applicaton/pdf'=>    array('image'=>'pdficon.gif',
													     'alt'  =>'PDF download file'),				
																														 
																'application/pdf'=>    array('image'=>'pdficon.gif',
													     'alt'  =>'PDF file'),																	 											 
																																												 
																'application/msword'=> array('image'=>'wordicon.gif',
													     'alt'  =>'Word file'),
																
															'application/vnd.openxmlformats-officedocument.wordprocessingml.document'=> array('image'=>'wordicon.gif',
													     'alt'  =>'Word file'),
																
																
																
																
																'application/vnd.ms-powerpoint'=> array('image'=>'ppicon.gif',
													     'alt'  =>'PowerPoint file'),
																														 
																 'text/xml'=> 							array(
																															'image' =>'xmlicon.gif',
													      'alt'   =>'XML  file'),	
																
																'application/vnd.ms-excel'=> array(
																															'image' =>'csvicon.gif',
													      'alt'   =>'CSV  file'),													 
																														 
																          
            'application/postscript'=> array(
                'suffix'=>'.eps',
                'image' =>'epsicon.gif',
                'alt'   =>'EPS file'),
            
                 
            'image/pjpeg'=> array(
                'suffix'=>'.jpg',
                'image' =>'jpgicon.gif',
                'alt'   =>'JPEG file'),
            
            'image/tiff'=> array(
                'suffix'=>'.jpg',
                'image' =>'tifficon.gif',
                'alt'   =>'TIFF file') 		
            														 
																
																);
																
																
								//Keep track of .pdfs
								
								if(  ($theDownload->mimetype=='applicaton/pdf')  ||     ($theDownload->mimetype=='application/pdf')  ){
								    $pdfPresent=true;
										
								}							
															
																
									/*						
								print_r($mimeInfo);			
								echo "the download is" . $theDownload->mimetype;
								print_r($mimeInfo[$theDownload->mimetype]);		    
								echo $mimeInfo[$theDownload->mimetype]['image'];
		     */
					
								//Note - need to remove any "&" characters from the thing
								/*
					$table->addRow( 
						   array('<a href="'.$this->baseURL.'download/'.$theDownload->getID().'"><img src="'.$this->baseURL.'images/'.$mimeInfo[$theDownload->mimetype]['image'].'" height="32" width="32" alt="'.$mimeInfo[$theDownload->mimetype]['alt'].'" /></a>',
							  str_replace('&','&amp;', $theDownload->description() ),
													 '<a href="'.$this->baseURL.'download/'.$theDownload->getID().'">Download ('.$theDownload->mimetypelabel().' '.$theDownload->filesizelabel().')</a>'
													 ),$classArray
												);
								*/
								
								
								$imageString = '';
								if(isset($mimeInfo[$theDownload->mimetype])){
									
									$imageString = '<img src="'.$this->baseURL.'images/'.$mimeInfo[$theDownload->mimetype]['image'].'" height="32" width="32" alt="'.$mimeInfo[$theDownload->mimetype]['alt'].'" />';
									
								}
								
														
								$table->addRow( 
						   array('<a href="'.$this->baseURL.$downloadLocation . '/'.$theDownload->getID().'">'.$imageString.'</a>',
							  '<a href="'.$this->baseURL.$downloadLocation . '/'.$theDownload->getID().'">' . str_replace('&','&amp;', $theDownload->description() ) . '&nbsp;&nbsp;(' .$theDownload->filesizelabel().')</a>',
																	''
													 ),$classArray
												);							
															
				
	    }
						
						$this->itemHTML .=  $table->toHTML();
						
			//add in the .pdf instruction if required
						if($pdfPresent==true){
						
						     $PDFtable =new HTML_Table(array( 'summary'=>    'Instructions for viewing PDFs',
																		'class'=>			 'pdf'				));
					 $classArray=array('class=col1','class=col2','class=col3');
						     $pdfInstruction='
	 
							   Acrobat Reader is a helper application manufactured by Adobe Systems Incorporated that
								 lets you view and print Portable Document Format (PDF) files. Click the button to download 
								 Acrobat Reader from the Adobe website.
														 
								       ';
											 
																			 
											 
						
							   $PDFtable->addRow( 
						   array('<img src="'.$this->baseURL.'images/'.$mimeInfo['applicaton/pdf']['image'].'" height="32" width="32" alt="'.$mimeInfo['applicaton/pdf']['alt'].'" />',
													 $pdfInstruction, 
													 '<a href="http://www.adobe.com/products/acrobat/readstep.html">
													    <img height="31" width="88" src="'.$this->baseURL.'images/getacro.gif" 
									      alt="Get Adobe Reader" /></a>'),
													 $classArray
												);
						     $this->itemHTML .=  $PDFtable->toHTML();
						}
				
				}
		
		}
		
                                  	
		
		function getHTML(){
		
		     return $this->itemHTML;
		}
		
		
		
		function getTitle(){
									 
			 require_once("string/stringCleaner.php"); 				 
			 $cleanString = new Cleaner($this->theItem->title);
       return $cleanString->getCleanText(400);
				
		}
		
		function getDescription(){
		  
					 
			 require_once("string/stringCleaner.php"); 				 
			 $cleanString = new Cleaner($this->theItem->body);
       return $cleanString->getCleanText(400);
				
		
		
		}
		
		
		
}
?>