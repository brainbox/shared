<?PHP

require_once('articles/Items.php'); 

class BBitems extends Items {

   // var $flexiItems;
		//var $result;
		
		var $sqlParams=array();

		/*
		$params is an array of parameters for the SELECT statment
		section - string
		active - 0 or 1
		*/
		
    function BBitems ( $dbConn, $table, $params='', $limit='') {

	$this->dbConn=$dbConn;		 
	$this->params=$params; 	 
	$this->limit=$limit; 	 
	
	//give the parameters to the function		 	 
	$this->_addParameters();
	
	
	
	$sql='SELECT id, title, short_title, title_note, section, code, intro, body, isHTML, author, image, width, height, alt, email, website, attachment, published, depublish FROM '.$table;
	$sql .= $this->_getParameters();
	$sql .= ' ORDER BY 0+published DESC ';
	$sql .= $this->_getLimit();
	$sql .= ';' ; 
	
	
	//echo '<hr />';																
	//echo "the SQL " . $sql;
	//echo '<hr />';	
	
	$result = $dbConn->query($sql);
				
        //echo "<pre>the result is ";
				//var_dump($result->fetch());
				
				if(($result->size())!=0){
				
							//echo "got here";
            while ( $row = $result->fetch() ) {
    						//echo $row;
                $this->BBitems[]=$row;
            }
				}
				else{
						 return false;
		
				
				}
					
				
		}
		//collects SQL parameters
		function _addParameters(){
				
				if($this->params !=''){
    				foreach($this->params as $name=>$value){
    						 									
    						//echo '<p>' . $name  .'|'. $value;									
    															
    						if($name=='section'){
    									$this->sqlParams[]=		'section ="'. $value .'"';													
    						}  
    
    						if($name=='active' && $value==1){
    									$today=mktime(0,0,0,date('m'), date('d'), date('Y'));																		
    									$this->sqlParams[]=		'published <= '.$today.' AND depublish > '.$today ;													
    						} 
    	 
    				}
				}
		//echo '<hr>';
		//print_r($this->sqlParams);
		
		}
		//returns SQL Paramter
		function _getParameters(){
						 
						
						 
						 $sqlSubstring='';
						 $counter=0;
						 
						 
						 foreach($this->sqlParams as $name=>$value){
						 
						 
						       //  echo '<h2>got sshere</h2>';
									//	echo '<i>' . $name . 'PPPPPP'. $value . ' </i>';
						     
						 				if ($counter==0){
											 		$sqlSubstring .= ' WHERE ';
										}				
										else{
												 $sqlSubstring .= ' AND ';
										}
										
													     
						     		$sqlSubstring .= $value;
										$counter=$counter +1;
										
						 }
		
		         return $sqlSubstring;
		}	
		
		function _getLimit(){
		
						 if($this->limit!=''){
						 
						      $sqlSubstring=' LIMIT ' . 	$this->limit['offset'] .' , '.		$this->limit['number']	;
									return $sqlSubstring; 
						 }
						 
		}
		
		// Factory method creates new instances of flexiItem
    function & getItem () {
		    
				//echo "<h1>here";
				//echo $section;
				if(isset($this->BBitems)){
    				
    		
            $row=each($this->BBitems);
    		    if ( $row ) {
    				   					
    						  return new BBitem($row['value'],$this->dbConn);
            } else {
                reset ( $this->BBitems);
                return false;
            }
        }
				else{
						 return false;
				}		 
		
		}
		
		function  getItemByID($id) {
				//this may be wrong: stepping through an objext as if it's an array		 											
				foreach($this->BBitems as $name=>$value){
		
						if($value['id']==$id){
											//echo "got here";															
									return new BBitem($value,$this->dbConn);																			 
						}
						
				 }
				  //only gets this far if article not found
				    reset($this->BBitems);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
				    //echo "<p>No article found";
				    return false;
						
		}
		
		
}

// The class built by the factory method
class BBitem extends item {
   
	var $dbConn;
	var $titleNote;
	var $shortTitle;
	var $intro;
	var $code;
		
    function BBitem($data,$dbConn) {
		
		//echo 'here';
		//print_r($data);
			$this->dbConn=$dbConn;
		
	//print_r($this->dbConn);
	
			
			 
	$this->id=          $data['id'];
	$this->section=     $data['section'];
	$this->title=	 	$data['title'];
	$this->shortTitle=	$data['short_title'];
	$this->titleNote=		$data['title_note'];
	
	$this->code=				$data['code'];
	
	
	$this->intro=				$data['intro'];
	
	
	$this->body=				$data['body'];
	$this->isHTML=			$data['isHTML'];
					
	$this->image=				$data['image'];
	$this->width=				$data['width'];
	$this->height=			$data['height'];
	$this->alt=				  $data['alt'];
	
	
	$this->attachment=  $data['attachment'];
	$this->website=     $data['website'];
	$this->email=       $data['email'];
	
	$this->author=			$data['author'];
	$this->published=		@$data['published'];
	$this->depublish=		@$data['depublish'];
						
						
		/*				
				//Get FILE the INFO but not the contents of the file
   			$sql='SELECT id, filename, description, mimetype, filesize, upload_date 
            FROM downloads
    			  WHERE article_id='.$this->id.'
    			  AND section="'.$this->section.'";';
		
          // Perform the query
          //	echo $sql;
				//This should have given access to the CREATOR's dbConn	
				
				
        $result=$this->dbConn->query($sql);
				
				if(!$result->size()==0){
				
				   while ( $row = $result->fetch() ) {
					     // echo "got to the rows";
                $this->downloads[]=$row;
					 }
					
        }
		 */	
				
			
				
    }
	function __get($nm){
	    //echo 'here';
	    if(isset($this->$nm)){
	       return $this->$nm;
	    } else {
	       return false;
	    }
	}
    
	
	function finalise(){
	    
	    //Fill in some gaps;
	    if($this->shortTitle==''){
		$this->shortTitle=$this->title;
	    }
	     if($this->titleNote==''){
		$this->titleNote=$this->title;
	    }
	    	    
                
	    $sql='  UPDATE articles 
	    SET
	    title =          "'.$this->title.'", 
	    short_title =          "'.$this->shortTitle.'", 
	    
	    
	    published =      "'.$this->published.'",
	    depublish =      "'.$this->depublish.'",
	    last_update = "' . time() . '" ,
					    
	    code =          "'.$this->code.'", 								
	    intro =          "'.$this->intro.'",
	    title_note =          "'.$this->titleNote.'",
	    
	    isHTML =          "'.$this->isHTML.'", 
	    body =           "'.$this->body.'"
	    
	    
	    
	    WHERE id='.$this->id.';';
	    
	    //echo $sql;
	    $this->dbConn->query($sql);
            }		
		
		
		
}		
	
		

?>
