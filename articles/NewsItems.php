<?PHP

require_once('articles/Items.php'); 

class NewsItems extends Items {

   // var $flexiItems;
		//var $result;
		
		var $sqlParams=array();

		/*
		$params is an array of parameters for the SELECT statment
		section - string
		active - 0 or 1
		*/
		
    function __construct ( $db, $table, $params='', $limit='') {

	$this->db=$db;		 
	$this->params=$params; 	 
	$this->limit=$limit; 	 
	$this->NewsItems=array();
	
	//give the parameters to the function		 	 
	$this->_addParameters();
	
	
	
	$sql='SELECT * FROM '.$table;
	$sql .= $this->_getParameters();
	$sql .= ' ORDER BY 0+published DESC ';
	$sql .= $this->_getLimit();
	$sql .= ';' ; 
	
	
	//echo '<hr /><p><p>';																
	//echo "the SQL " . $sql;
	//echo '<hr />';	
	
	$result = $this->db->query($sql);
				
        //echo "<pre>the result is ";
				//var_dump($result->fetch());
				
				if(($result->size())!=0){
				
							//echo "got here";
            while ( $row = $result->fetch() ) {
    							//echo $row;
                $this->NewsItems[]=$row;
            }
				}
				else{
						 return false;
		
				
				}
					
				
		}
		//collects SQL parameters
		function _addParameters(){
				
				if($this->params !=''){
    				foreach($this->params as $name=>$value){
    						 									
    						//echo '<p>' . $name  .'|'. $value;			
								
						  switch($name){
							
							   case ('section'):
								 
								     $this->sqlParams[]=		'section ="'. $value .'"';		
								 
								 break;
								 
								 case ('active'):
								 
								     if($value==1){				     
										      $today=mktime(0,0,0,date('m'), date('d'), date('Y'));																		
    									    $this->sqlParams[]=		'published <= '.$today.' AND depublish > '.$today ;	
											}	
								 break;
								 
								 //Default: take param literatlly
								 default:
								     
										 $this->sqlParams[]=		$value ;			
								
							}
								
								
								

														
    						/*									
    						if($name=='section'){
    									$this->sqlParams[]=		'section ="'. $value .'"';													
    						}  
    
    						if($name=='active' && $value==1){
    									$today=mktime(0,0,0,date('m'), date('d'), date('Y'));																		
    									$this->sqlParams[]=		'published <= '.$today.' AND depublish > '.$today ;													
    						} 
								
								if($name=='dateRange' ){
    																									
    									$this->sqlParams[]=		$value ;													
    						} 
    	          */
    				}
				}
		//echo '<hr>';
		//print_r($this->sqlParams);
		
		}
		//returns SQL Paramter
    function _getParameters(){
						 
						
						 
		$sqlSubstring='';
		$counter=0;
		
		
		foreach($this->sqlParams as $name=>$value){
		
		
		      //  echo '<h2>got sshere</h2>';
				       //	echo '<i>' . $name . 'PPPPPP'. $value . ' </i>';
		    
					       if ($counter==0){
								       $sqlSubstring .= ' WHERE ';
					       }				
					       else{
								$sqlSubstring .= ' AND ';
					       }
					       
									    
			       $sqlSubstring .= $value;
					       $counter=$counter +1;
					       
		}
		
		         return $sqlSubstring;
		}	
		
		function _getLimit(){
		
						 if($this->limit!=''){
						 
						      $sqlSubstring=' LIMIT ' . 	$this->limit['offset'] .' , '.		$this->limit['number']	;
									return $sqlSubstring; 
						 }
						 
		}
		
		// Factory method creates new instances of flexiItem
    function fetch () {
		    
	//echo "<h1>here";
	//echo $section;
	if(isset($this->NewsItems)){
	
	    
	    $row=each($this->NewsItems);
	    if ( $row ) {
		    //print_r($row['value']);
		return new NewsItem($row['value'],$this->db);
	    } else {
		reset ( $this->NewsItems);
		return false;
	    }
	}
	else{
	    return false;
	}		 
	
    }
		
    function  getItemByID($id) {
        //this may be wrong: stepping through an objext as if it's an array		 											
        foreach($this->NewsItems as $name=>$value){

            if($value['id']==$id){
                //echo "got here";
                
                
                return new NewsItem($value,$this->db);																			 
            }
                        
        }
            //only gets this far if article not found
            reset($this->NewsItems);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
            //echo "<p>No article found";
            return false;
                                    
    }
    
    function createNewItem(){
        
         //print_r($_POST);
                        
        $title= '****New item****';												
        $datePublish =   mktime(0,0,0,date('m'), date('d'), date('Y'));//today
        $dateDepublish = mktime(0,0,0,date('m'), date('d')-1, date('Y')) ;// yesterday - forces it to be "expired"
        
        
        // Construct a query using the submitted values
        $sql='INSERT INTO news (section, title, published, depublish) VALUES ("news","'.$title.'",'.$datePublish.', '.$dateDepublish.');';    						
        
        $this->contentString .=  $sql;
        
        //Perform the query
        $result=$this->db->query($sql);
    }
		
}

// The class built by the factory method
class NewsItem extends item {
    
	//private $title;
	//private $published;
	//var $image;
	//var $width;
	//var $height;
        //var $section;
        //var $data;
        //var $db;
        
       // private $metaDescription;
		
    function __construct($data,$db) {
	
	
	//print_r($data);
	//exit;	    
	$this->db=$db; ///TOOK OUT THE ANDPERSANDS for DB
			
			 
	$this->id=      $data['id'];
	$this->section= $data['section'];
	$this->title=	$data['title'];
        
        
	$this->shortTitle=	$data['short_title'];
        $this->metaTitle=	$data['meta_title'];
        $this->metaDescription=	$data['meta_description'];
	
        $this->body=	$data['body'];
	$this->isHTML=  $data['isHTML'];
	//$this->code=	$data['code'];				
	$this->image=	$data['image'];
	$this->width=	$data['width'];
	$this->height=  $data['height'];
	$this->alt=	$data['alt'];
	
	
	//$this->attachment=  $data['attachment'];
	//$this->website=     $data['website'];
	//$this->email=       $data['email'];
	
	$this->author=	    $data['author'];
	$this->published=   @$data['published'];
	$this->depublish=   @$data['depublish'];
		
	//echo '<hr>In the NewItem class, the title data is ' . $data['title'] . ' and the $this->title is ' . $this->title . '<hr>';
						
						
				//Get FILE the INFO but not the contents of the file
				/*
   			$sql='SELECT id, filename, description, mimetype, filesize, upload_date 
            FROM downloads
    			  WHERE article_id='.$this->id.'
    			  
						ORDER BY filename, mimetype';
    
          // Perform the query
          //echo $sql;
				//This should have given access to the CREATOR's dbConn	
				
				
        $result=$this->db->query($sql);
				
				if(!$result->size()==0){
				
				   while ( $row = $result->fetch() ) {
					     //echo "got to the rows";
                $this->downloads[]=$row;
					 }
        }
	*/		
	

		}
                
    function finalise(){
	    
	//echo '<hr>FINALISINGSSSSSSS';
	//$this->_applyBusinessRules();
        
        
        //title =          	"'.mysql_real_escape_string($this->title) .'",  
	//echo '<p>' . $this->shortTitle ;
	//echo '<p>' . $this->metaTitle ;
	//echo '<p>' . $this->metaDescription ;
	
	//published =      	"'.mysql_real_escape_string($this->published) .'", 
	//depublish =      	"'.mysql_real_escape_string($this->depublish) .'", 
	//last_update = 		"' . time() . '" ,
	//isHTML =          	"'.mysql_real_escape_string($this->isHTML) .'",  
	//body =           	"'.mysql_real_escape_string($this->body) .'"	    





	$sql='  UPDATE news 
	SET
	title =          	"'.mysql_real_escape_string($this->title) .'",  
	short_title =          	"'.mysql_real_escape_string($this->shortTitle) .'", 
	meta_title =          	"'.mysql_real_escape_string($this->metaTitle) .'",  
	meta_description =      "'.mysql_real_escape_string($this->metaDescription) .'", 
	
	published =      	"'.mysql_real_escape_string($this->published) .'", 
	depublish =      	"'.mysql_real_escape_string($this->depublish) .'", 
	last_update = 		"' . time() . '" ,
	isHTML =          	"'.mysql_real_escape_string($this->isHTML) .'",  
	body =           	"'.mysql_real_escape_string($this->body) .'"
	WHERE id=		'.$this->id.';';
	
	//echo $sql;
	//exit;
	//print_r($this->db);
	
	
	$this->db->query($sql);
	//exit;
    }		            
                
                
}		
	
		

?>
