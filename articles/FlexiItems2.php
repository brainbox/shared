<?PHP

require_once('articles/Items.php'); 

class flexiItems extends Items {

   // var $flexiItems;
		//var $result;
		
		var $sqlParams=array();
                

		/*
		$params is an array of parameters for the SELECT statment
		section - string
		active - 0 or 1
		*/
		
    function flexiItems ( $dbConn, $table, $params='', $limit='') {

				$this->dbConn=&$dbConn;		 
				$this->params=$params;
				
			   if(isset($params['section'])){
				  $this->section = $params['section'];
			   }
				
				$this->limit=$limit; 	 
				
				//give the parameters to the function		 	 
				$this->_addParameters();
				
				
				
				$sql='SELECT id, title, section, intro, body, isHTML,  author, image, width, height, alt, email, website, attachment, published, depublish, last_update FROM '.$table;
				$sql .= $this->_getParameters();
				$sql .= ' ORDER BY 0+published DESC ';
				$sql .= $this->_getLimit();
				$sql .= ';' ; 
				
				
				//echo '<hr />';																
				//echo "the SQL " . $sql;
				//echo '<hr />';	
				
				$result = $dbConn->query($sql);
				
        //echo "<pre>the result is ";
				//var_dump($result->fetch());
				
				if(($result->size())!=0){
				
							//echo "got here";
            while ( $row = $result->fetch() ) {
    							//echo $row;
                $this->flexiItems[]=$row;
            }
				}
				else{
						 return false;
		
				
				}
					
				
		}
		//collects SQL parameters
		function _addParameters(){
				
				if($this->params != ''){
    				foreach($this->params as $name=>$value){
    						 									
    						//echo '<p>' . $name  .'|'. $value;			
								
						  switch($name){
							
							   case ('section'):
								 
								     $this->sqlParams[]=		'section ="'. $value .'"';		
								 
								 break;
								 
								 case ('active'):
								 
								     if($value==1){				     
										      $today=mktime(0,0,0,date('m'), date('d'), date('Y'));																		
    									    $this->sqlParams[]=		'published <= '.$today.' AND depublish > '.$today ;	
											}	
								 break;
								 
								 //Default: take param literatlly
								 default:
								     
										 $this->sqlParams[]=		$value ;			
								
							}
								
								
								

														
    						/*									
    						if($name=='section'){
    									$this->sqlParams[]=		'section ="'. $value .'"';													
    						}  
    
    						if($name=='active' && $value==1){
    									$today=mktime(0,0,0,date('m'), date('d'), date('Y'));																		
    									$this->sqlParams[]=		'published <= '.$today.' AND depublish > '.$today ;													
    						} 
								
								if($name=='dateRange' ){
    																									
    									$this->sqlParams[]=		$value ;													
    						} 
    	          */
    				}
				}
		//echo '<hr>';
		//print_r($this->sqlParams);
		
		}
		//returns SQL Paramter
    function _getParameters(){
						 
						
						 
		$sqlSubstring='';
		$counter=0;
		
		
		foreach($this->sqlParams as $name=>$value){
		
		
		      //  echo '<h2>got sshere</h2>';
				       //	echo '<i>' . $name . 'PPPPPP'. $value . ' </i>';
		    
					       if ($counter==0){
								       $sqlSubstring .= ' WHERE ';
					       }				
					       else{
								$sqlSubstring .= ' AND ';
					       }
					       
									    
			       $sqlSubstring .= $value;
					       $counter=$counter +1;
					       
		}
		
		         return $sqlSubstring;
		}	
		
		function _getLimit(){
		
						 if($this->limit!=''){
						 
						      $sqlSubstring=' LIMIT ' . 	$this->limit['offset'] .' , '.		$this->limit['number']	;
									return $sqlSubstring; 
						 }
						 
		}
		
		// Factory method creates new instances of flexiItem
    function  getItem () {
		    
				//echo "<h1>here";
				//echo $section;
				if(isset($this->flexiItems)){
    				
    		
            $row=each($this->flexiItems);
    		    if ( $row ) {
    				   		
									
    						  return new flexiItem($row['value'],$this->dbConn);
            } else {
                reset ( $this->flexiItems);
                return false;
            }
        }
				else{
						 return false;
				}		 
		
		}
		
		function  getItemByID($id) {
				//this may be wrong: stepping through an objext as if it's an array		 											
				foreach($this->flexiItems as $name=>$value){
		
						if($value['id']==$id){
											//echo "got here";															
									return new flexiItem($value,$this->dbConn);																			 
						}
						
				 }
				  //only gets this far if article not found
				    reset($this->flexiItems);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
				    //echo "<p>No article found";
				    return false;
						
		}
		
		
}

// The class built by the factory method
class flexiItem extends item {
   
   
    public $downloads;
    var $image;
    var $width;
    var $height;
    var $section;
    var $data;
    var $dbConn;

    public $intro;

    public $lastUpdateDate;
		
    function flexiItem($data,$dbConn) {
		

        $this->dbConn=$dbConn; ///TOOK OUT THE ANDPERSANDS for DB


        $this->id=          $data['id'];
        $this->section=     $data['section'];
        $this->title=				$data['title'];
        $this->intro=				$data['intro'];
        $this->body=				$data['body'];
        $this->isHTML=			$data['isHTML'];
        //$this->code=			$data['code'];				
        $this->image=				$data['image'];
        $this->width=				$data['width'];
        $this->height=			$data['height'];
        $this->alt=				  $data['alt'];


        $this->attachment=  $data['attachment'];
        $this->website=     $data['website'];
        $this->email=       $data['email'];

        $this->author=			$data['author'];
        $this->published=		@$data['published'];
        $this->depublish=		@$data['depublish'];


        $this->lastUpdateDate=		$data['last_update'];
        
       // echo 'ffff' . $this->lastUpdateDate;
       // exit;
        
						
				
						
				//Get FILE the INFO but not the contents of the file
   			$sql='SELECT id, filename, description, mimetype, filesize, upload_date 
            FROM downloads
    			  WHERE article_id='.$this->id.'
			  
			  AND section = "' . $this->section . '"
    			  
						ORDER BY filename, mimetype';
    
          // Perform the query
          //echo $sql;
	   // exit;
	 			//This should have given access to the CREATOR's dbConn	
				
				
        $result=$this->dbConn->query($sql);
				
				if(!$result->size()==0){
				
				   while ( $row = $result->fetch() ) {
					     //echo "got to the rows";
                $this->downloads[]=$row;
					 }
        }
			
				
			
				
		}			
}		

