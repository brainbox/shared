<?PHP


// Fetches a list of bulletins from a database
class bulletins {

    var $bulletins;
		var $result;
		

    function bulletins ( &$dbConn ) {
		
		
        // Pull the bulletins from the database
        $sql="SELECT bulletin_id, title, intro, body, textdate, venue, moreinfo, logo, website, email, attachment, author, published FROM bulletins ORDER BY startdate DESC;";
				
				//echo "the SQL " . $sql;
				
        $result = $dbConn->query($sql);
				
        while ( $row = $result->fetch() ) {
						
            $this->bulletins[]=$row;
        }
		}
			
		
		// Factory method creates new instances of bulletin
    function & getbulletin () {
        $row=each($this->bulletins);

		  if ( $row ) {
				    					
						  return new bulletin($row['value']);
								
        } else {
            reset ( $this->bulletins );
            return false;
        }
		
		}
		function & getbulletinByID($id) {
												
				foreach($this->bulletins as $name=>$value){
																		
						if($value['bulletin_id']==$id){
																										
									return new bulletin($value);																			 
						}
						
				 }
		}
		
}

// The class built by the factory method
class bulletin {
    var $bulletin_id;
	  var $title;
    var $author;
		var $published; // Unix timestamp
		var $intro;
		var $body;
		
    var $textdate; 
    var $venue; 
		var $moreinfo; 
		var $logo; 
		var $website; 
		var $email; 
		
		var $websitetext;
		var $emailtext;
		var $attachmenttext;


    function bulletin($data) {			 								
						 
				$this->bulletin_id=    $data['bulletin_id'];
        $this->title=				$data['title'];
        $this->author=			$data['author'];
				$this->intro=				$data['intro'];
        $this->published=		$data['published'];
        $this->body=				$data['body'];
				
				$this->textdate=			$data['textdate'];
				$this->venue=				$data['venue'];
				$this->moreinfo=		$data['moreinfo'];
 				$this->logo=				$data['logo'];
				$this->website=			$data['website'];
				$this->email=				$data['email'];
				$this->attachment=				$data['attachment'];		
				
				
    }
		 function bulletin_id() {
        return ($this->bulletin_id);
    }
		
    function title() {
        return htmlspecialchars($this->title);
    }

    function author() {
        return htmlspecialchars($this->author);
    }
		 function published() {
		     //return $this->published;
        return $this->published;
    }

    function textdate() {
        return $this->textdate;
    }
		 function intro() {
        return $this->intro;
    }

    function body() {
        return $this->body;
    }
		
		function venue() {
        return $this->venue;
    }
		 function moreinfo() {
        return $this->moreinfo;
    }
		 function logo() {
		    if(isset($this->logo)){	
                return $this->logo;
				}
				else return false;
    }
		 function website() {
		    if(isset($this->website)){	
        				return $this->website;
				}
				else return false;
    }
		function websitelink($websitetext) {
		    if(isset($this->website)){	
        				return '<a href="http://'.$this->website.'">'.$websitetext.'</a>';
				}
				else return false;				
    
		}
		
		 function email() {
		    if(isset($this->email)){	
                return $this->email;
				}
				else return false;
    }
		 function emaillink($emailtext) {
		    if(isset($this->email)){	
                return '<a href="mailto:'.$this->email.'">'.$emailtext.'</a>';
				}
				else return false;
								
    }
		 function attachment() {
		    if(isset($this->attachment)){	
                return $this->attachment;
				}
				else return false;
    }
		 function attachmentlink($attachmenttext) {
		 		if(isset($this->attachment)){		
		            
								$filename = $this->attachment		;					
							  $filepath = 'files/'. $filename;
								$size = fileSizeUnit ( filesize ($filepath ) );
	
        				return '<a href="'.$filepath.'">'.$attachmenttext.' (.pdf '.  $size['size'].' '.$size['unit'].')</a>';
			  }
				else return false;
    }
		
		
		
		
}


function fileSizeUnit($size) {
    if ($size >= 1073741824) {
        $size=number_format(($size / 1073741824),2);
        $unit='GB';
    } else if ($size >= 1048576) {
        $size=number_format(($size / 1048576),2);
        $unit='MB';
    } else if ($size >= 1024) {
        $size=number_format(($size / 1024),2);
        $unit='KB';
    } else if ($size >= 0) {
        $unit='B';
    } else {
        $size='0';
        $unit='B';
    }
    return ( array ( 'size'=>$size, 'unit'=>$unit ) );
}



