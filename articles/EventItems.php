<?PHP

require_once('articles/Items.php'); 

class eventItems extends Items {

// var $eventItems;
//var $result;
var $isMatched;
protected $sqlParams;

function eventItems ( $dbConn,$active=true, $params='') {
                                 
                           
        $this->dbConn=$dbConn;		 
        $this->params=$params; 	 
        //give the parameters to the function		 	 
        $this->_addParameters();
        
        $sql  = 'SELECT * ';
        $sql .= ' FROM events ';
        $sql .= ' WHERE 1=1 ';
        $sql .=  $this->_getParameters();
        if($active){
        
            $sql .= ' AND published < ' . time() . ' AND depublish > ' . time() ;
            $sql .= ' ORDER BY 0+startdate ASC;';
        
        } else{ #for Admin
            
            $sql .= ' ORDER BY 0+startdate DESC;';
        }
        
       
        
        
        #echo "the SQL " . $sql;
        $result = $dbConn->query($sql);
        //echo "number of records: ".$result->size();
        
        $numberOfResults=$result->size();
        
        if($numberOfResults != 0){
        
        while ( $row = $result->fetch() ) {
        
        $this->Items[]=$row;
}
                                        return true;
                }
                else{
                    //echo 'GOT HERE';
                    return false;
                }
                
                        
}
function  getItem () {

$row=@each($this->Items);
    if ( $row ) {
                                  return new eventItem($row['value'],$this->dbConn);
} else {
@reset ( $this->Items );
return false;
}

}

function  getItemByID($id) {
                
                $isMatched=false;					
                foreach($this->Items as $name=>$value){
                                                                                                                                                        
                                if($value['id']==$id){
                                                        return new eventItem($value,$this->dbConn);																			 
                                }
                                
                 }
                 //only gets this far if article not found
                 reset($this->Items);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
                 //echo "<p>No article found";
                 return false;
                
                 
}
//collects SQL parameters
function _addParameters(){
                
                if($this->params !=''){
                foreach($this->params as $name=>$value){
                                                                                                        
            //echo '<p>' . $name  .'|'. $value;			
                                                
switch($name){

case ('section'):

$this->sqlParams[]=		'section ="'. $value .'"';		

break;

case ('active'):

if($value==1){				     
                 $today=mktime(0,0,0,date('m'), date('d'), date('Y'));																		
       $this->sqlParams[]=		'published <= '.$today.' AND depublish > '.$today ;	
                   }	
break;

//Default: take param literatlly
default:

            $this->sqlParams[]=		$value ;			
}	
}

                        }			
                                                

                                        
}



function _getParameters(){
                                 
          $sqlSubstring='';                      
     if(is_array($this->sqlParams)){                       
        
        
        
        foreach($this->sqlParams as $name=>$value){
        
        
        
        $sqlSubstring .=  ' AND  ' . $value;
        
        
        }
     }
         return $sqlSubstring;
}	


}





// The class built by the factory method
class eventItem extends item {

//ONLY the unique variables need be declared

var $startdate;
var $enddate;
var $textdate;
var $meeting_date;
var $venue;
var $location;
var $title;

var $image;
var $width;
var $height;
var $alt;


var $email;
var $website;
var $attachment;
var $highlight;

var $dateRange;
var $data;
var $dbConn;

                
function eventItem($data, $dbConn) {
//All the data needs to be declared here


    $this->dbConn=$dbConn; //removing & for rackspace

                $this->id=          $data['id'];
                
                //echo 'the article ID should be ' . $this->id;
                $this->title=				$data['title'];
                $this->intro=				$data['intro'];
                $this->isHTML=			$data['isHTML'];
                $this->highlight=	  @$data['highlight'];
                $this->body=				$data['body'];	
                
                $this->startdate=		@$data['startdate'];
                $this->enddate=		  @$data['enddate'];
                
                
                $this->textdate=		@$data['textdate'];
                
                $this->meeting_date=		@$data['meeting_date'];
                
                
                $this->venue=		    @$data['venue'];
                $this->location=		    @$data['location'];
                
                $this->image=				$data['image'];
                $this->width=			  $data['width'];
                $this->height=	    $data['height'];
                $this->alt=				  $data['alt'];	
                
                
                //$this->attachment=  @$data['attachment'];
                //$this->website=     @$data['website'];
                //$this->email=       @$data['email'];
                
                
$this->author=			@$data['author'];
$this->published=		@$data['published'];
$this->depublish=		@$data['depublish'];
$this->enddate=$this->depublish-(60*60*24);
                                
                
            //Get FILE the INFO but not the contents of the file
            //
            ///  wOULD BE gREAT TO GET RID OF THIS
            ///
            ///
           
            
            $sql='SELECT id, filename, description, mimetype, filesize, upload_date 
            FROM downloads
            WHERE article_id='.$this->id.'
            AND section="events";';
            
            
            

// Perform the query
//	echo $sql;
                //This should have given access to the CREATOR's dbConn	
                
                
$result=$this->dbConn->query($sql);
                
                if(!$result->size()==0){
                
                   while ( $row = $result->fetch() ) {
                             // echo "got to the rows";
$this->downloads[]=$row;
                         }
}
         
                                
                

}
//Only the UNIQUE functions need to follw here... other will be inherited		
function title() {
return $this->title;
}         

function highlight() {
return $this->highlight;
}
                
function venue() {
return $this->venue;
}
function location() {
return $this->location;
}
function startdate() {
return $this->startdate;
}
function textdate() {
return $this->textdate;
}

function dateRange(){


//easy case first: one-day event;
    
if($this->startdate == $this->enddate  ){
    
    return date('d M Y', $this->startdate);
}
//more tricky!
else {
    //Eveything is different!
    if( date('Y', $this->startdate)!=date('Y', $this->enddate)  ){
            return date('d M Y', $this->startdate) . ' - ' . date('d M Y', $this->enddate);
            
    }
    //Differnet months
    elseif(date('m', $this->startdate)!=date('m', $this->enddate) ){
            return date('d M', $this->startdate) . ' - ' . date('d M Y', $this->enddate);
                    
    }
    else{
            return date('d', $this->startdate) . ' - ' . date('d M Y', $this->enddate);
    }
}
}
function comingSoon(){


    
//now on = start date in the past
if($this->startdate < time() ){
return 'now';
}
//this month
elseif( $this->startdate < mktime(0,0,0,date('m')+1,1,date('y') )  ){
    return 'thismonth';
}
//following month
elseif( $this->startdate < mktime(0,0,0,date('m')+2,1,date('y') )   ){
    return 'nextmonth';
}
else{
   return 'future';
}
}
        function daysToGo(){
          
        
          $days=  floor( ($this->startdate - mktime(0,0,0,date('m'),date('d'),date('y') ) )/(60*60*24));
              
          if($days<0){
             $days=0;
          }   
          return $days;
        }

}


