<?PHP


// Fetches a list of newsItems from a database
class newsItems extends Items {

    var $newsItems;
		var $result;

    function items ( &$dbConn ) {
		
		
        // Pull the newsItems from the database
        $sql="SELECT newsItem_id, title, intro, body, author, published,depublish FROM news ORDER BY published DESC;";
				
				//echo "the SQL " . $sql;
				
        $result = $dbConn->query($sql);
				
        while ( $row = $result->fetch() ) {
						
            $this->newsItems[]=$row;
        }
		}
			
		
		// Factory method creates new instances of newsItem
    function & getnewsItem () {
        $row=each($this->newsItems);

		  if ( $row ) {
				    					
						  return new newsItem($row['value']);
								
        } else {
            reset ( $this->newsItems );
            return false;
        }
		
		}
		function & getnewsItemByID($id) {
												
				foreach($this->newsItems as $name=>$value){
																		
						if($value['newsItem_id']==$id){
																										
									return new newsItem($value);																			 
						}
						
				 }
		}
		
}

// The class built by the factory method
class newsItem extends item {
    var $newsItem_id;
	  var $title;
		var $intro;
		var $body;
		
		var $employer;
		var $logo;
		
		var $salary;
		var $location;
		var $reference;
		
		var $applicationinfo;
	  var $closingdate;
		
		var $attachment;
		var $website;
		var $email;
		
    var $author;
		var $published; // Unix timestamp
		var $depublish; // Unix timestamp
		
    var $date; // Unix timestamp
   
		
    function newsItem($data) {
		
						 								
						 
				$this->newsItem_id=      $data['newsItem_id'];
        $this->title=				$data['title'];
				$this->intro=				$data['intro'];
				$this->body=				$data['body'];
				
				
				//$this->attachment=  $data['attachment'];
				//$this->website=     $data['website'];
				//$this->email=       $data['email'];
				
				
        $this->author=			$data['author'];
        $this->published=		$data['published'];
        $this->depublish=		$data['depublish'];
						
				
				
    }
		 function newsItem_id() {
        return ($this->newsItem_id);
    }
		
    function title() {
        return htmlspecialchars($this->title);
    }
    
		function intro() {
        return $this->intro;
    }
		
		function employer() {
        return $this->employer;
    }
		
		function logo() {
        return $this->logo;
    }
		
		function salary() {
        return $this->salary;
    }
		
		function location() {
        return $this->location;
    }
		
		function reference() {
        return $this->reference;
    }
		
		
		
		function applicationinfo() {
        return $this->applicationinfo;
    }
		function closingdate() {
        return $this->closingdate;
    }
		
		 function website() {
		    if(isset($this->website)){	
        				return $this->website;
				}
				else return false;
    }
		function websitelink($websitetext) {
		    if(isset($this->website)){	
        				return '<a href="http://'.$this->website.'">'.$websitetext.'</a>';
				}
				else return false;				
    
		}
		
		 function email() {
		    if(isset($this->email)){	
                return $this->email;
				}
				else return false;
    }
		 function emaillink($emailtext) {
		    if(isset($this->email)){	
                return '<a href="mailto:'.$this->email.'">'.$emailtext.'</a>';
				}
				else return false;
								
    }
		 function attachment() {
		    if(isset($this->attachment)){	
                return $this->attachment;
				}
				else return false;
    }
		 function attachmentlink($attachmenttext) {
		 		if(isset($this->attachment)){		
		            
								$filename = $this->attachment		;					
							  $filepath = 'files/'. $filename;
								$size = fileSizeUnit ( filesize ($filepath ) );
	
        				return '<a href="'.$filepath.'">'.$attachmenttext.' (.pdf '.  $size['size'].' '.$size['unit'].')</a>';
			  }
				else return false;
    }
		
		
		
		
		
		
		
		
    function author() {
        return htmlspecialchars($this->author);
    }
		 function published() {
		     //return $this->published;
        return $this->published;
    }
		function depublish() {
		     //return $this->published;
        return $this->depublish;
    }

    function date() {
        return $this->date;
    }
		

    function body() {
        return $this->body;
    }
}



function fileSizeUnit($size) {
    if ($size >= 1073741824) {
        $size=number_format(($size / 1073741824),2);
        $unit='GB';
    } else if ($size >= 1048576) {
        $size=number_format(($size / 1048576),2);
        $unit='MB';
    } else if ($size >= 1024) {
        $size=number_format(($size / 1024),2);
        $unit='KB';
    } else if ($size >= 0) {
        $unit='B';
    } else {
        $size='0';
        $unit='B';
    }
    return ( array ( 'size'=>$size, 'unit'=>$unit ) );
}
