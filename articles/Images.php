<?php


Class Images {
	
	function __construct($dbPDO){
		
		$this->dbPDO=$dbPDO;
		
	}
	
	function initialise(){
	
		$sql='SELECT * from image ';
		//echo $sql;
		
		$stmnt=$this->dbPDO->prepare($sql);
		$stmnt->execute();

		
		$this->images=$stmnt->fetchAll();

		print_r($this->images); 
		
	}
	
	function fetch(){
		
		$row=each($this->images);
		if ( $row ) {
			return new Image($row['value']);
		} else {
		    reset ( $this->images);
		    return false;
		}
			
		
	}
	
	function getByID($id){
		
		
	}
	
	function getByArticleID($articleID){
		
		$sql="SELECT * from image, article_image_xref
			WHERE
			image.id=article_image_xref.image_id
			AND
			article_image_xref.article_id=$articleID
			ORDER BY
			sort_order
			";
		
		//echo $sql;
		
		$stmnt=$this->dbPDO->prepare($sql);
		$stmnt->execute();

		
		$this->images=$stmnt->fetchAll();
		//echo '<pre>';
		//print_r($this->images); 
		
	}
	
	function assign($articleID, $imageID){
		
		
	}
	
	function unAssign($articleID, $imageID){
		
		
	}
	
}

Class Image{
	
	
	private $filename;
	
	
	function __construct($data){
		
		
	        $this->data=$data;
		
		//print_r($this->data);
		//exit;
		
		$this->initialise();
		//exit;
		
		
	}	
	
	
	//Idea is to get an array of results- either from the factory class...
	// ... or froma  query that used an id
	
	private function initialise(){
	
		$this->id=$this->data['id'];
		$this->filename=$this->data['filename'];
		$this->mimetype=$this->data['mimetype'];
		$this->filesize=$this->data['filesize'];
		 
		$this->upload_date=$this->data['upload_date'];;
		
		$this->width=$this->data['width'];
		$this->height=$this->data['height'];
			
			
		

	}
	
	//Use overload for the simple cases
	
	public function __get($nm){
	     	
	       if (isset($this->$nm)) {
		   return $this->$nm;
	       } else {
		   return false;
	       }
	   }
	
	
	
	
	
}
?>
