<?PHP

require_once('articles/Items.php'); 

class ConferenceItems extends Items {

   // var $flexiItems;
		//var $result;
		
		var $sqlParams=array();

		/*
		$params is an array of parameters for the SELECT statment
		section - string
		active - 0 or 1
		*/
		
    function ConferenceItems ( &$dbConn, $table='conference', $params='', $limit='') {

				$this->dbConn=&$dbConn;		 
				$this->params=$params; 	 
				$this->limit=$limit; 	 
				
				
				
				$sql='SELECT id, intro, session_number, session_day,session_time, title, body, isHTML, speaker_name, speaker_biog, speaker_company, sort_order FROM '.$table;
				//$sql .= $this->_getParameters();
				$sql .= ' ORDER BY sort_order ';
				//$sql .= $this->_getLimit();
				
				$sql .= ';' ; 
				
				
				//echo '<hr />';																
				//echo "the SQL " . $sql;
				//echo '<hr />';	
				
				$result = $dbConn->query($sql);
				
        //echo "<pre>the result is ";
				//var_dump($result->fetch());
				
				if(($result->size())!=0){
				
							//echo "got here";
					    while ( $row = $result->fetch() ) {
											//echo $row;
						$this->flexiItems[]=$row;
					    }
				}
				else{
					 return false;
				}
									
								
    		}
		
		
		// Factory method creates new instances of flexiItem
    function  getItem () {
		    
				//echo "<h1>here";
				//echo $section;
				if(isset($this->flexiItems)){
    				
    		
            $row=each($this->flexiItems);
    		    if ( $row ) {
    				   					
    						  return new flexiItem($row['value'],$this->dbConn);
            } else {
                reset ( $this->flexiItems);
                return false;
            }
        }
				else{
						 return false;
				}		 
		
		}
		
		function & getItemByID($id) {
				//this may be wrong: stepping through an objext as if it's an array		 											
				foreach($this->flexiItems as $name=>$value){
		
						if($value['id']==$id){
											//echo "got here";															
									return new flexiItem($value,$this->dbConn);																			 
						}
						
				 }
				  //only gets this far if article not found
				    reset($this->flexiItems);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
				    //echo "<p>No article found";
				    return false;
						
		}
		
		
}

// The class built by the factory method
class flexiItem extends item {
   
   
      var $intro;
      var $sortOrder;
      var $sessionNumber;
      var $sessionDay;
      var $sessionTime;
      
      
      var $speakerName;
      var $speakerCompany;
      var $speakerBiog;
      
		
    function flexiItem($data,$dbConn) {
		
		//print_r($data);    
		    $this->dbConn=$dbConn;
		    
		    
		 			 			
		$this->id=          	$data['id'];
		$this->sortOrder=	$data['sort_order'];
		
		 
		$this->intro=		$data['intro'];
		
		$this->sessionNumber=	$data['session_number'];
		$this->sessionDay=	$data['session_day'];
		$this->sessionTime=	$data['session_time'];
		
		  
		  
		$this->title=		$data['title'];
		
		$this->body=		$data['body'];
		$this->isHTML=		$data['isHTML'];
		
		
		$this->speakerName=     $data['speaker_name'];
		$this->speakerBiog=     $data['speaker_biog'];
		$this->speakerCompany=     $data['speaker_company'];
		
		
		$this->width=				$data['width'];
		
		
		  $this->image=				$data['image'];
		  $this->width=				$data['width'];
		  $this->height=			$data['height'];
		  $this->alt=				  $data['alt'];
		  
		  
		  $this->attachment=  $data['attachment'];
		  $this->website=     $data['website'];
		  $this->email=       $data['email'];
				
       // $this->author=			$data['author'];
        $this->published=		@$data['published'];
        $this->depublish=		@$data['depublish'];
						
						
				/*		
				//Get FILE the INFO but not the contents of the file
				
				
   			$sql='SELECT id, filename, description, mimetype, filesize, upload_date 
            FROM downloads
    			  WHERE article_id='.$this->id.'
    			  AND section="'.$this->section.'"
						ORDER BY filename, mimetype';
   
          // Perform the query
          //echo $sql;
				//This should have given access to the CREATOR's dbConn	
				
				
        $result=$this->dbConn->query($sql);
				
				if(!$result->size()==0){
				
				   while ( $row = $result->fetch() ) {
					     // echo "got to the rows";
                $this->downloads[]=$row;
					 }
        }
			
	 */		
			
				
		}	
		function sortOrder(){
			return $this->sortOrder;
		}
		function sessionNumber(){
			return $this->sessionNumber;
		}
		function sessionDay(){
			return $this->sessionDay;
		}
		function sessionTime(){
			return $this->sessionTime;
		}
		
		function speakerName(){
			return $this->speakerName;
		}
		function speakerBiog(){
			return $this->speakerBiog;
		}
		function speakerCompany(){
			return $this->speakerCompany;
		}
		

		
}		
	
		

?>
