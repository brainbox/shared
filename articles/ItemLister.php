<?PHP
/*Intended to be used with the Item class to write the various "articles" to an HTML page
*/


Class ItemLister{

    var $item;
		var $listHTML;
		var $intro;
		
    //constructor
		function ItemLister(&$items,$returnPath){
		     
				 $this->items=&$items;
				 $this->returnPath=$returnPath;
		
		}
		function addIntro($text){
						 //echo "<p>intro should be".$text;
		   $this->intro=$text;
			 $this->listHTML .=  $this->intro;
		
		}
		function getDescription(){
		     require_once("string/stringCleaner.php"); 				 
				 $cleanString = new Cleaner($this->intro);
         return $cleanString->getCleanText(254);
			 
		} 
		
		
		
		function addNotFoundMessage($message){
		    $this->listHTML .=  $message;
		}
		
		
		
		
		
		function addClickText($text, $class='instructions'){
		   
			 $this->listHTML .=  '<div class="'.$class.'">'.$text.'</div>';
		
		}
		
		function addItems($includeIntro=TRUE){
		
		    while ( $theItem=$this->items->getItem() ) {
            $this->listHTML .=   '<h2>';
    				$this->listHTML .=   '<a href="'.$this->returnPath.'/'. $theItem->id().'">'.$theItem->title.'</a>';
    				$this->listHTML .=   '</h2>';
    				$this->listHTML .=   '<div class="date">Added: ' .date('j F Y',$theItem->published()).'</div>' ;
    				if($includeIntro){
						     $this->listHTML .=   '<p class="intro">'.$theItem->intro().'</p>';
						}
		    }
		
		}
		function addTrainingItems(){
		      
					
					
				  while ( $theItem=$this->items->getItem() ) {
					
					 
					
					  //WRAP in a DIV if it's a highlighted item
					  if($theItem->highlight()){
						         $this->listHTML .=  '<div class="highlight">';  
				    }	
            $this->listHTML .=   '<h2>';
    				$this->listHTML .=   '<a href="'.$this->returnPath.'/'. $theItem->id().'">'.$theItem->title.'</a>';
    				$this->listHTML .=   '</h2>';
						$this->listHTML .=   '<div class="venue">'.$theItem->textdate().' | '.$theItem->venue() ."</div>\n";
						if ($theItem->intro() != ''){
						      $this->listHTML .=   '<div class="intro">'.$theItem->intro() ."</div>\n";
						}
    				
						//Close the WRAP 
						if($theItem->highlight()){
						         $this->listHTML .=  '</div>';  
				    }	
						
						
		    }
		 }		
		function addEventItems(){
		      
					
					
				  while ( $theItem=$this->items->getItem() ) {
					
					 
					
					  //WRAP in a DIV if it's a highlighted item
					  if($theItem->highlight()){
						         $this->listHTML .=  '<div class="highlight">';  
				    }	
            $this->listHTML .=   '<h2>';
    				$this->listHTML .=   '<a href="'.$this->returnPath.'/'. $theItem->id().'">'.$theItem->title().'</a>';
    				$this->listHTML .=   '</h2>';
						$this->listHTML .=   '<div class="venue">'.$theItem->textdate().' | '.$theItem->venue() ."</div>\n";
    				
						//Close the WRAP 
						if($theItem->highlight()){
						         $this->listHTML .=  '</div>';  
				    }	
						
						
		    }
		 }		
		 function addJobItems(){
		 
		 		require_once('HTML/Table.php'); 		
		    
				while ( $theItem=$this->items->getItem() ) {
					
	  
		       $this->listHTML .=     '<h2>';
				   $this->listHTML .=        '<a href="'.$this->returnPath.'/'. $theItem->id().'">'.$theItem->title. ', ' . $theItem->employer() . '</a>';
				   $this->listHTML .=     '</h2>';
				   
					 
					 
		       $table =new HTML_Table(array( 'summary'=>    'Job Datasheet', 'class'=>			 'jobdatasummary'				));

    		  
		        
		             $table->addRow(array('Closing Date:',	 date('j F Y',$theItem->closingdate())  ));
		      
		   
				   if($theItem->salary()){
					 			 $table->addRow(array('Salary:',				 $theItem->salary()										  ));
    		   }
		    
					 if($theItem->location()){
					         $table->addRow(array('Location:',		 $theItem->location()								    ));
					 }
    		   //$table->addRow(array('Job Reference:',	$theItem->reference()							    ));
    		
				 
			//$table->addRow(array('Closing date:',		       date('j F Y',$theItem->depublish())		 	  ));
			
			 $table->addRow(array('Date added:',		       date('j F Y',$theItem->published())		 	  ));
			
					 $table->setColType(0,"TH");		
				   $table->setColAttributes(0,'scope=row');
				   $this->listHTML .=  $table->toHTML();
		   
	     }  
		    
		
		}
		
		 function addCommitteeMembers($baseURL){
		 
		 		require_once('HTML/Table.php'); 		
				
				
				function ShortenText($text,$chars) {

        

        // Change to the number of characters you want to display
        //$chars = 25;

        $text = $text." ";
        $text = substr($text,0,$chars);
        $text = substr($text,0,strrpos($text,' '));
        $text = $text."... ";

        return $text;

    }
      
				
				// $table =new HTML_Table(array( 'summary'=>    'Committee Members', 'class'=>			 'committee'				));
					 
				
				
				 $this->listHTML  .= '<div class="container">';  //a container
		    
				while ( $theItem=$this->items->getItem() ) {
					
	  
				   
					 $this->listHTML  .= '<div class="box">';
					 $this->listHTML  .= $theItem->getThumbLink($baseURL,BASE_FILE_PATH, 100, 150);
					// $this->listHTML .= '<img src="'.$this->returnPath.'/../images/committee/alison-north.jpg" width="80"  class="floatleft" alt="">';
					 
					 $this->listHTML .= '<h2><a href="'.$this->returnPath.'/' . $theItem->id().'">'.$theItem->title.'</a></h2>';
					 $this->listHTML .= '<p>'.$theItem->intro().'</p>';
					 $this->listHTML .= '<p><a href="'.$this->returnPath.'/' . $theItem->id().'"> More on '.$theItem->title.'...</a></p>';
					
					 $this->listHTML .= '<div class="spacer">&nbsp;</div>';
					 $this->listHTML .= '</div>';
		      
					 /*
					 
					 $personDetails = '
					 
					    <img src="'.$this->returnPath.'/../images/committee/alison-north.jpg" width="80"  class="floatright" alt="">
												
					 
					
					 ShortenText($theItem->body(),150) . '<a href="'.$this->returnPath.'/' . $theItem->id().'">more...</a>
					         ';
            
						
    		   $table->addRow(array($theItem->intro() ,
					              	 $personDetails		    ));
					 
					 */
				  
	     }  
		    
					// $table->setColType(0,"TH");		
			//	   $table->setColAttributes(0,'scope=row');
			//  $this->listHTML .=  $table->toHTML();
		   	
				 $this->listHTML .= '<div class="spacer">&nbsp;</div>';
					
				 $this->listHTML  .= '</div>';  //close the container
		
		}


		function getHTML(){
		
		     return $this->listHTML;
		}
	
}
?>





