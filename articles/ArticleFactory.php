<?PHP


// Fetches a list of articles from a database
class Articles {

    var $articles;
		var $result;

		//the constructor
    function Articles ( &$dbConn ) {
				
				/*		
				//BBCode Class		 
				require_once 'ThirdParty/bbcode/bbcode.inc.php';	
				//Custom Tags definition
				require_once 'TextManipulation/SitepointBBCode.php';	 
				*/
        
				//bbCode Stuff
        /* require PEAR and the parser */
        require_once('PEAR.php');
        require_once('HTML/BBCodeParser.php');
        /* get options from the ini file */
        $config = parse_ini_file('BBCodeParser.ini', true);
        $options = &PEAR::getStaticProperty('HTML_BBCodeParser', '_options');
        $options = $config['HTML_BBCodeParser'];
        unset($options);
        
       
				
				
				
				
				
		
		
        // Pull the articles from the database
        $sql="SELECT article_id, title, intro, body, author, published FROM articles ORDER BY published DESC;";
				
				//echo "the SQL " . $sql;
				
        $result = $dbConn->query($sql);
				
        while ( $row = $result->fetch() ) {
						
            $this->articles[]=$row;
        }
		}
			
		
		// Factory method creates new instances of Article
    function & getArticle () {
        $row=each($this->articles);

		  if ( $row ) {
				    					
						  return new Article($row['value']);
								
        } else {
            reset ( $this->articles );
            return false;
        }
		
		}
		function & getArticleByID($id) {
												
				foreach($this->articles as $name=>$value){
																		
						if($value['article_id']==$id){
																										
									return new Article($value);																			 
						}
						
				 }
		}
		
}

// The class built by the factory method
class Article {
    var $article_id;
	  var $title;
    var $author;
		var $published; // Unix timestamp
		var $intro;
    var $date; // Unix timestamp
    var $body;
		
    function Article($data) {
		
						 								
						 
				$this->article_id=  $data['article_id'];
        $this->title=				$data['title'];
        $this->author=			$data['author'];
				$this->intro=				$data['intro'];
        $this->published=		$data['published'];
        $this->body=				$data['body'];
						
				
				
    }
		 function article_id() {
        return ($this->article_id);
    }
		
    function title() {
        return htmlspecialchars($this->title);
    }

    function author() {
        return htmlspecialchars($this->author);
    }
		 function published() {
		     //return $this->published;
        return $this->published;
    }

    function date() {
        return $this->date;
    }
		 function intro() {
        return $this->intro;
    }

    function body() {
		    
				$tempString = $this->body;
				//escape HTMl characters				              
				$tempString = htmlspecialchars($tempString);
				
				$parser = new HTML_BBCodeParser();
        $parser->setText($tempString);
        $parser->parse();
        $parsed = $parser->getParsed();
				
			  $parsed = nl2br($parsed);
				$parsed = str_replace('<ul><br />','<ul>',$parsed);
				$parsed = str_replace('</ul><br />','</ul>',$parsed);
				$parsed = str_replace('</li><br />','</li>',$parsed);
				$parsed = str_replace("</li>\r<br />",'</li>',$parsed);  //don't work
				$parsed = str_replace("</li>\n<br />",'</li>',$parsed);  //don't work
				
        return $parsed;
				
    }
}



