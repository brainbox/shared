<?php

/**
 * 27 Apr 08 - Slight change of emphasis: going to make this class more "front-end" oriented: less "raw" database
 * values, more business rules. A consequence is that the Admin site will now talk drectly to the Model
 *
 */


Class Item{
    
    public $id;
    public $parentId;
    
    public $title;
    public $shortTitle;
    
    public $metaTitle;
    public $metaDescription;
    
    public $path;
    public $body;
                 
    public $image;
    public $alt;
    public $height;
    public $width;

     
    //Derived 
    public $subtitleArray;
    public $isActive;

    function __construct(){
        
        
      
    }
    
    
    
    function getTitle($minimumCharacters = false){
        
        $string=$this->title;
        
        //Truncate if required
        if($minimumCharacters){
            $string = $this->truncate($string, $minimumCharacters);
        }
        
        return $string;
        
    }
    
    /**
     * Populates the class with values from the Model
     *
     */
    

    public function populate($data){
        
        #echo '<pre>';
        #print_r($data);
        #exit;
        
        
        if(isset($data['id']))
            $this->id = $data['id'];
            
        if(isset($data['parent_id']))
            $this->parentId = $data['parent_id'];
        
        if(isset($data['title']))
            $this->title = $data['title'];
        
        if(isset($data['short_title']))
            $this->short_title = $data['short_title'];
            
            
        if(isset($data['meta_title']))
            $this->metaTitle = $data['meta_title'];
            
        if(isset($data['meta_description']))
            $this->metaDescription = $data['meta_description'];    
            
            
        //Don't need published/expiry dates
        
        
        if(isset($data['path']))
            $this->path = $data['path'];
            
        #if(isset($data['code']))
        #    $this->code = $data['code'];    
            
        if(isset($data['body']))
            $this->body = $data['body'];
        
            
        if(isset($data['image']))
            $this->image = $data['image'];
            
        if(isset($data['alt']))
            $this->alt = $data['alt'];
        
        if(isset($data['width']))
            $this->width = $data['width'];
         
        if(isset($data['height']))
            $this->height = $data['height'];
        
        
        $this->_applyBusinessRules();
         
    }
    
    /**
     *
     *
     */
    
    private function _applyBusinessRules(){
        
        
        //Clean up
        $pattern = '/&(?!amp;)/';
	$this->title = preg_replace('/&(?!amp;)/', '&amp;', $this->title);
        
        if(isset($this->subtitle) ){
            
            $this->subtitle = preg_replace('/&(?!amp;)/', '&amp;', $this->subtitle);
        
        }
        
        
        $this->alt = preg_replace('/&(?!amp;)/', '&amp;', $this->alt);

        
        require_once('Markdown/markdown.php');
        $this->body=Markdown($this->body);
    

        //Derived parameters
        
        
        // NO!!! the metat title "creation" is not the item's job
        //if(!$this->metaTitle){
        //    $this->metaTitle = $this->truncate($this->title, 60);
        //}
        
        if(isset($this->subtitle) ){
            
            $this->subtitleArray=explode('|',$this->subtitle);
        
        }
        
        $this->_createMetaDescription();
        $this->_cleanMetaDescription();
      
        /*
        if(strtotime($this->published)<time() ){
            
            $this->isActive=true;
        } else {
            $this->isActive=false;
        }
        */
    }
       
    
    
    /**
     * If a metaDescription is entered explicitly, it is used. If not, it's created from:
     *
     *    + the subtitle array (if any)
     *    + the first paragraph (if any)
     *    + the title
     *
     */
    private function _createMetaDescription(){
        
        if($this->metaDescription){
            return true;
            die();
        }
        
        if(count($this->subtitleArray) ){
          
            $this->metaDescription = trim(implode('. ', $this->subtitleArray), '. ');
            return true;
            die();
            
        }
        
      
        
        #Grab the first paragraph... if tere is one
        $pattern = '/^.*?\<p>.*?\/p>/is'; #first paras
        
        
        if(preg_match($pattern, $this->body, $matches) ){
        
            $this->metaDescription = strip_tags($matches[0]);
            
        } else {
            
            $this->metaDescription = $this->title;
            
        }
    
        
    }
    
    /**
     * metadescription must not contain double quotes
     *
     *
     */
    
    private function _cleanMetaDescription(){
        
        $this->metaDescription = htmlspecialchars($this->metaDescription);
    
    }
    
    
    private function splitBody(){
        
       
       
        $pattern = '/^.*?\<p>.*?\/p>.*?\/p>/is'; #firs two paras
        
        preg_match($pattern, $this->body, $matches); 
	
        #print_r($matches);
        
        $this->intro = $matches[0];
        
        $this->bodyWithoutIntro = str_replace($this->intro,'',$this->body);
        
        
    }
    
    
    
    
    /**
     * truncates string to given length... then adds a bit on to get the whole
     * word.. and adds the ellipsis.
     *
     */
    
    
    public function truncate($string, $length) {
        #echo $string;
        #exit;
        #echo strlen($string);
        
	if (strlen($string) > $length) {
		$pos = strpos($string, " ", $length);
                //$pos may return false. If it does the whole string is returned
                if($pos){
                    return substr($string, 0, $pos) . "...";
                }
               
	}
	
	return $string;
    }


    function getDownloads($section = '')
    {
	  
        //var_dump($this->dbPDO);
        //exit;
        
        require_once 'Download/Download.php';
        
        $this->downloadArray = Download::getByArticleId($this->dbPDO, $this->id, $section);
	  
    }
    
    
    
    function getImages($section = ''){
        
        
        require_once 'Image/Model/Image.php';
        
		
		$resultArray = Model_Image::getByArticleId( $this->dbPDO, $this->id, $section);
        
		
        $this->imageArray =  $resultArray;
        
    }
    
}