<?php
//print_r($_POST);
require_once 'Item.php';

Class Model_NavItem extends Model_Item {

   
    		
    function __construct ($dbPDO) {

	$this->dbPDO=$dbPDO;
	
    }
   

   
    /**
     * Want to change the operation without changing the database
     *
     * @todo - change the datbase - and loads of code - to avoid dates for pub/expire
     *
     *
     */
    
    function updatefromPOST(){
	
		
		
		$sql = "
				UPDATE articles	   
				SET
			
				title = :title,
				short_title= :shortTitle,
				
				meta_title = :metaTitle,
				meta_description = :metaDescription,
				
			  
				depublish= :depublish,
				
				body= :body
				
				WHERE id=:id
		   
		";
			
		   
			$stmnt = $this->dbPDO->prepare($sql);
			
			
		
			$stmnt->bindParam(':title', $title);
			
		 
		$stmnt->bindParam(':shortTitle', $shortTitle);
			
		$stmnt->bindParam(':metaTitle', $metaTitle);
		$stmnt->bindParam(':metaDescription', $metaDescription);
		  
	
			
			//$stmnt->bindParam(':published', $published);
		$stmnt->bindParam(':depublish', $depublish);
	
	
		$stmnt->bindParam(':body', $body);
		  
		$stmnt->bindParam(':id', $id);
		
		
		
		
		
		$title=                 $_POST[ 'title'];
		
		if(isset( $_POST[ 'shortTitle'])){
			$shortTitle=            $_POST[ 'shortTitle'];
		} else {
			
			$shortTitle=            '';
		}
		
	
		$metaTitle=             $_POST[ 'metaTitle']; 
		$metaDescription=       $_POST[ 'metaDescription']; 
			
		
		
		
		
		
			
		// need to determine if fielpath has changed
		//DON'T mess with the path for Home page, sections (news, events).
		if( !isset($_GET['type']) || ($_GET['type'] == 'default' && isset($_POST['filepath']) ) ){
			
			$this->updatePaths();
			
		}
		
			if(!empty($_POST['isActive'])){
			
			$depublish = null;
			
		} else {
			
			$depublish = '2008-01-01';
			
		}
		   
		   
			
		$body =           $_POST[ 'body' ] ;
			
		
		
		
		$id = $_POST['id']; 
		
		
		$stmnt->execute();
		
		//print_r($stmnt->ErrorInfo() );
        //exit;
    }	
   
   
    function updatePaths(){
	
		$filepath =         $_POST[ 'filepath'];
		$filename =         $_POST[ 'filename'];
		
		$filename=strtolower($filename);
		
		//Convert the posted values to       
		if($filepath){
			
			$pathNEW=$filepath . '/' . $filename;
			
		} else {
			
			$pathNEW =  $filename;
		}
		
		
		$sql = "SELECT path from articles where id=" .  $_POST['id']; 
		
		$stmnt = $this->dbPDO->prepare($sql);
		
		$stmnt->execute();
		
		$data = $stmnt->fetch();
		
		$path =  $data['path'];
		
		//Has the path changed?
		if ($path != $pathNEW){
	
			//Next function needs this
			$this->id = $_POST['id']; 
	
			//Fix the path of any children
			$this->updateChildPaths($path, $pathNEW);
			
		}
		//exit;
		
    }
    
}