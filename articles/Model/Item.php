<?php
//print_r($_POST);
Class Model_Item {

    public $id;
    protected $justOrphans;
    protected $justNavigation;
    protected $downloads;
    
    public $table='articles';
    public $active=1;
    public $section;
    
    public $path;
    
    public $limit;
    public $offset=0;
    public $navOnly=0;
    public $orphanOnly=0;
    
    protected $stmnt;
    
    		
    function __construct ($dbPDO) {

	$this->dbPDO=$dbPDO;
	
    }
   

    public function getItems(){
	
	$sql='SELECT * FROM articles ';
	$sql .= ' WHERE 1=1 ';
	if($this->active){ // This is the default
	    
	   # $today=mktime(0,0,0,date('m'), date('d'), date('Y'));
	    $sql .= " AND published <= NOW() AND (depublish > NOW() OR depublish IS NULL ) ";
	}
	if($this->section){  
	    $sql .= " AND section= '" . $this->section . "' ";
	}
	if($this->navOnly){  
	    $sql .= " AND isOrphan= '0' ";
	}
	if($this->orphanOnly){  
	    $sql .= " AND isOrphan= '1' ";
	}
	
	if (isset($this->id) ){
	     $sql .= " AND id=" . $this->id;
	}
        if(isset($this->path) ){ //NOTE need to use isset, because it will be clank for the home page.
	     $sql .= " AND path='" . $this->path . "' ";
	}
    
        $sql .= ' ORDER BY published DESC ';

        if($this->limit){

            $sql .= " LIMIT " . $this->offset . " , " . $this->limit	;

        }
	#echo $sql;
	
        $this->stmnt=$this->dbPDO->prepare($sql);
        $this->stmnt->execute();
	#$this->stmnt->errorInfo();
        
    }
    
    public function getSections(){
	
	$sql='SELECT * FROM articles ';
	$sql .= ' WHERE parent_id=1 ';
	$sql .= " AND published <= NOW() AND (depublish > NOW() OR depublish IS NULL ) ";
	  $sql .= " AND isOrphan= '0' ";
	$sql .= ' ORDER BY published DESC ';
	

        $this->stmnt=$this->dbPDO->prepare($sql);
        $this->stmnt->execute();
	#$this->stmnt->errorInfo();
        
    }

    function fetch () {
        
	#echo 'fetching';
	#print_r($this->stmnt->fetch() );
	
	return $this->stmnt->fetch(PDO::FETCH_ASSOC);
	    
    }
    
    
    
    
  
    /**
     * June 08 - New approach: grabbing the values and CONDITIONALLY overwriting them
     * with values from $_POST
     *
     * June 08 - Changes to path are made via a transactions: path has been removed from the
     * SQL in this method
     *
     */
   
    
    function updatefromPOST(){
        #echo 'hhhhhhhheeeee';
        #exit;
        //Get existing values
	
	//ISN'T THIS POINTLESS???
        
        $sql= 'SELECT * from articles where id=' . $this->id;
        
        $stmnt = $this->dbPDO->prepare($sql);
        
        $stmnt->execute();
        
        $result=$stmnt->fetch();
        
        #print_r($result);
        #exit;
        
        
        
	$title =            $result['title'];      
	$shortTitle =       $result['short_title'];   
	$metaTitle =        $result['meta_title'];         
	$metaDescription =  $result['meta_description'];   
	$path  =            $result['path'];         
        $published =        $result['published'];   
        $depublish  =       $result['depublish'];   
	$body  =            $result['body'];         
      

        
        
        ####################################################
        
        //Conditionally update existing values
        
        
	//NB path excluded from this update on purpose (see separate update paths method.)
	$sql = "
            UPDATE articles	   
            SET
            title = :title,
           
            short_title= :shortTitle,
            
            meta_title = :metaTitle,
            meta_description = :metaDescription,
            
            published= :published,
            depublish= :depublish,
            
            body= :body
            
            WHERE id=:id
	   
	";
        
        #echo $sql;
	#exit;
        $stmnt = $this->dbPDO->prepare($sql);
        
        
	
        $stmnt->bindParam(':title', $title);
        
     
	$stmnt->bindParam(':shortTitle', $shortTitle);
        
	$stmnt->bindParam(':metaTitle', $metaTitle);
	$stmnt->bindParam(':metaDescription', $metaDescription);
      
       
      
        
        $stmnt->bindParam(':published', $published);
	$stmnt->bindParam(':depublish', $depublish);
       
        #$stmnt->bindParam(':lastUpdate', $lastUpdate);

	$stmnt->bindParam(':body', $body);
	  
	$stmnt->bindParam(':id', $id);
	
	
	
	#    *** IMPORTANT: save for PHP 5.2 ***
	/*
	$title=                 filter_input(INPUT_POST, 'title', FILTER_UNSAFE_RAW); //No parameter: does nothing at present
       
	$shortTitle=            filter_input(INPUT_POST, 'shortTitle', FILTER_UNSAFE_RAW);  //No parameter, so has ne effect at present
	
	$metaTitle=             filter_input(INPUT_POST, 'metaTitle', FILTER_UNSAFE_RAW);  //No parameter, so has ne effect at present
	$metaDescription=       filter_input(INPUT_POST, 'metaDescription', FILTER_UNSAFE_RAW);  //No parameter, so has ne effect at present
        
        $expire = filter_input(INPUT_POST, 'expire', FILTER_SANITIZE_STRING);
	
	
    
        
	
        if(isset($_POST['filepath'])){
            $filepath =         filter_input(INPUT_POST, 'filepath', FILTER_UNSAFE_RAW);//No parameter, so has ne effect at present
            $filename =         filter_input(INPUT_POST, 'filename', FILTER_UNSAFE_RAW);//No parameter, so has ne effect at present
            
            $filename=strtolower($filename);
            
            //Convert the posted values to       
            if($filepath){
                
                $pathNEW=$filepath . '/' . $filename;
                
            } else {
                
                $pathNEW =  $filename;
            }
            
            //Has the path chagned?
            if ($path != $pathNEW){
                #echo 'asdasdasd';
                #exit;
                //Fix the path of any children
                $this->updateChildPaths($path, $pathNEW);
                
                #And update the value of path ready for the updte
                $path=$pathNEW;
                
            }
            
            #echo $path;
            #exit;
        }
	
       
        if(isset($_POST['published'])){
            $dateArray = explode("-", filter_input(INPUT_POST, 'published', FILTER_UNSAFE_RAW) );//No parameter, so has ne effect at present
            $published = $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
        }
        
        if(isset($_POST['depublish'])){
            $depublish = filter_input(INPUT_POST, 'depublish', FILTER_UNSAFE_RAW);//No parameter, so has ne effect at present
            
            #echo $depublish;
            #exit;
            
            //New logic: use the "expire" (radio control) value to decide what to do with depub
            if($expire == 'never'){
                
                $depublish = NULL;
            
            } else {
                
                $dateArray = explode("-", $depublish );
                $depublish=  $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
            
            }
        }
	
	$body =           filter_input(INPUT_POST, 'body', FILTER_UNSAFE_RAW) ;//No parameter, so has ne effect at present
        
	*/
	
	
	$title=                 $_POST[ 'title'];
       
        if(isset($_POST[ 'shortTitle'])){
	    
	    $shortTitle=            $_POST[ 'shortTitle'];
	    
	}
	
	
	$metaTitle=             $_POST[ 'metaTitle']; 
	$metaDescription=       $_POST[ 'metaDescription']; 
        
	
	if(isset($_POST[ 'expire'])){
        
	    $expire = $_POST[ 'expire'];
	    
	}
	
	
    
        
	
        if(isset($_POST['filepath'])){
            $filepath =         $_POST[ 'filepath'];
            $filename =         $_POST[ 'filename'];
            
            $filename=strtolower($filename);
            
            //Convert the posted values to       
            if($filepath){
                
                $pathNEW=$filepath . '/' . $filename;
                
            } else {
                
                $pathNEW =  $filename;
            }
            
            //Has the path chagned?
            if ($path != $pathNEW){
                #echo 'asdasdasd';
                #exit;
                //Fix the path of any children
                $this->updateChildPaths($path, $pathNEW);
                
                #And update the value of path ready for the updte
                $path=$pathNEW;
                
            }
            
            #echo $path;
            #exit;
        }
	
       
        if(isset($_POST['published'])){
            $dateArray = explode("-", $_POST[ 'published'] );
            $published = $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
        }
        
        if(isset($_POST['depublish'])){
            $depublish = $_POST[ 'depublish'];
            
            #echo $depublish;
            #exit;
            
            //New logic: use the "expire" (radio control) value to decide what to do with depub
            if($expire == 'never'){
                
                $depublish = NULL;
            
            } else {
                
                $dateArray = explode("-", $depublish );
                $depublish=  $dateArray[2] . '-' . $dateArray[1] . '-' . $dateArray[0];
            
            }
        }
	
        #echo 'DEPUB is ' . isset($depublish);
        #exit;
        
	$body =           $_POST[ 'body' ] ;
        
	
	
	
	$id = $this->id; 
	
	
	#echo '<hr>SAVING';
	$stmnt->execute();
	
	#print_r($stmnt->ErrorInfo() );
        #exit;
    }	
   
   
    
    
    function addChild($parentID, $parentLocation, $title='New Page'){
		
                                
        $filename = $this->createPathfromTitle($title);
        
        //need to check if the given filename has already been taken.
        $path =  $parentLocation. '/'. $filename;
        $path=strtolower($path);
        
        $sql="SELECT count(*) FROM articles WHERE path = '$path'";
        //echo '<p>' . $sql;
        
        //Perform the query
        
        $stmnt=$this->dbPDO->prepare($sql);
        
        
        try {
            $stmnt->execute();
            //echo 'tryyyy';
            $results= $stmnt->fetchAll(PDO::FETCH_ASSOC);
        
        }
        catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        
        
        
        $result=$results[0]['count(*)'];
        
        //If already exists, force it to be different
        if($result['count(*)'] !=0 ){
        
            $path = $path . '-' . time();           
        }
        
        //Depublish yesterday
        $depublish = date('Y',mktime(0,0,0,date('m'),date('d')-1,date('y'))) . '-' . date('m',mktime(0,0,0,date('m'),date('d')-1,date('y'))). '-' . date('d',mktime(0,0,0,date('m'),date('d')-1,date('y')));
     

        
        // Construct a query using the submitted values
        $sql="INSERT INTO articles (parent_id, path, title, published, depublish, isOrphan)
        VALUES ($parentID, '$path', '$title',NOW(), '$depublish', '0');";    						
                        
        #echo $sql;
        #exit;                                                 
        //Perform the query
        $stmnt=$this->dbPDO->prepare($sql);
	

        try {
            
            $stmnt->execute();
            return true;
        
        }
        catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
        #print_r($stmnt->ErrorInfo() );

    }

    public function addOrphan($title = 'New Item'){
   
        $path = $this->createPathfromTitle($title);
        
        //Check the path is OK
        
        $sql = "SELECT count(*) from articles where path = '$path'";
        $stmnt = $this->dbPDO->prepare($sql);
        $stmnt->execute();
        $result = $stmnt->fetch(PDO::FETCH_ASSOC);
        
        //Path already taken? append the date
        if($result['count(*)'] !=0){
            $path = $path . '-' . time();
        }
        
        // Use "NOW()" rather than an generated date to preserve the order of multiple items added as
        // part of the same session
        
        $sql = "INSERT INTO articles (
                    title,
                    path,
                    published,
                    depublish,
                    isOrphan
                    
                    )
                VALUES (
                    :title,
                   :path,
                   NOW(),
                   :depublish,
                   '1'
                

                    );";

        #echo $sql;
        
        $stmnt = $this->dbPDO->prepare($sql);
       
        $stmnt->bindParam(':title', $title);
        $stmnt->bindParam(':path', $path);
        #$stmnt->bindParam(':published', $published);// Now has a DB default value
        $stmnt->bindParam(':depublish', $depublish);
        
       
        //Depublish yesterday
        $depublish = date('Y',mktime(0,0,0,date('m'),date('d')-1,date('y'))) . '-' . date('m',mktime(0,0,0,date('m'),date('d')-1,date('y'))). '-' . date('d',mktime(0,0,0,date('m'),date('d')-1,date('y')));
     
        
        try
        {
            $stmnt->execute ();
        }
        catch (PDOException $e)
        {
            print ("The statement failed.\n");
            print ("getCode: ". $e->getCode () . "\n");
            print ("getMessage: ". $e->getMessage () . "\n");
        }
        
        
    }
    
    
    /**
     * A function to create a path from a title. Checks that the
     * resulting path is not already taken
     *
     *
     *
     */
    public function createPathfromTitle($title){
        
        $path=trim(strtolower($title) );
        
        #Replace all special characters
        $pattern = '/[^a-z0-9\s-_]/';
        $path = preg_replace( $pattern,'',$path);
        
        //remove double dashes
        $pattern = '/-+/';
        $path = preg_replace( $pattern,'-',$path);
        
        //remove double undescores
        $pattern = '/_+/';
        $path = preg_replace( $pattern,'_',$path);
        
        //Change spaces to dashes
        $pattern = '/\s+/';
        $path = preg_replace( $pattern,'-',$path);
        
        return $path;
        
    }
    
    
     /**
     * Usually called STATICALLY for the admin site
     *
     * IMPORTANT: depublish can be NULL
     */
    function status($published, $depublish){
	
        #echo 'pub' . $published;
        #echo 'expi' . $depublish;
        #exit;
        
        $today=time();
	
	if($published > $today){
            
	    return 'pending';
	
        }
        
        if($published < $today && ($depublish==null || $depublish > $today)  ){
            
            return 'active';
        }
        
	    return 'expired';
        
    }

    /**
     * Grabs the posted image and creates each of the required versions
     *
     */
    
    public function updateImage($imagePath){
        
        $id=$_POST['id'];
        #echo '<pre>';
        #print_r($_FILES);
        #exit;
        //Get image height and width;
        
        $imageInfo = getimagesize($_FILES['image']['tmp_name']);
        $imageName=$_POST['id'] . '.jpg';
        
        move_uploaded_file  ( $_FILES['image']['tmp_name'], $imagePath .  $imageName  );
        
        #print_r($imageInfo); ##0-width; 1=height
        
        $sql = "UPDATE articles
                SET
                image = '" . $imageName . "',
                width = " . $imageInfo[0] . ",
                height = " . $imageInfo[1] . "
                WHERE
                id = " . $_POST['id']
                ;
        #echo $sql;
        
        $stmnt = $this->dbPDO->prepare($sql);
        $stmnt->execute();
        #print_r( $stmnt->ErrorInfo() );
    }

    
    /**
     * Changes to paths are tricky, as they can impact child - and grandchild - pages :(
     *
     * Changing the path of an item can change the paths of its children. So:
     *
     *      rubbish/poop >>>>           rubbish/pooper
     *
     * ... should also change:
     * 
     *      rubbish/poop/scoop       >>>>>   rubbish/pooper/scoop
     *
     * ... and:
     * 
     *      rubbish/poop/scoop/doop  >>>>>   rubbish/pooper/scoop/doop
     *
     */
    protected function updateChildPaths($pathOLD, $pathNEW){
    
        
        $sql = "SELECT id, path FROM articles WHERE path LIKE '" . $pathOLD . "/%'"; ##The % at the end (only) will bring ONLY those that need to change
        #echo $sql;
        
      
    
        //Use a transaction, so that all are changed... or none       
        try{
            $this->dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);
              $this->dbPDO->beginTransaction();
              $stmnt = $this->dbPDO->prepare($sql);
            $stmnt->execute();
        
        //Couldn't get fetch to work for the next part. Trying fetchAll instead.
            $results = $stmnt->fetchAll();
            
           ## echo 'in the try';
           
            //Change to exception mode (from silent mode)
           # $this->dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
             ##exit;
             
          
            # exit;
            //Update the current page
            $sql = "UPDATE articles set path = '$pathNEW'  WHERE id=" . $this->id;
            #echo '<p>' . $sql;
            #exit;
            
            $stmnt = $this->dbPDO->prepare($sql);
            $stmnt->execute();
            #
           
            
            //Updae all pages that contain current page's path + '/'
            foreach ($results as $row){
            
                $sql = "UPDATE articles set path = REPLACE(path,'$pathOLD/', '$pathNEW/'  ) WHERE id=" . $row['id'];
                #echo '<p>' . $sql;
               
                $stmnt = $this->dbPDO->prepare($sql);
                $stmnt->execute();
            
            }
          
           
            
            
           # exit;
            //Back to silent mode (just in case)
            
            //print_r($this->dbPDO->errorInfo() );
            $this->dbPDO->commit();
             
             
           
        }
        catch(PDOException $e) {
            $this->dbPDO->rollBack();
            echo 'Error : '.$e->getMessage();
        }
        //print_r($this->dbPDO->errorInfo() );
            
    }
    
}