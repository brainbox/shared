<?php
//this is just for reading - will use a different class for ADMIN


class diary {
  
	
	function diary( &$dbConn, $params='', $limit='') {

				$this->dbConn=&$dbConn;		 
				$this->params=$params; 	 
				$this->limit=$limit; 	 
				
				//give the parameters to the function		 	 
				$this->_addParameters();	
				
				
				$sql='SELECT id, title, intro, body, highlight, isHTML, startDate, textDate, published, depublish FROM events';
				$sql .= $this->_getParameters();
				$sql .= ' ORDER BY 0+startDate  ';
				$sql .= $this->_getLimit();
				$sql .= ';' ; 
				
				
			//	echo '<hr />';																
			//	echo "the SQL " . $sql;
				//echo '<hr />';	
				
				$result = $dbConn->query($sql);
				
        //echo "<pre>the result is ";
				//var_dump($result->fetch());
				
				if(($result->size())!=0){
				
							//echo "got here";
            while ( $row = $result->fetch() ) {
    							//echo $row;
                $this->diary[]=$row;
            }
				}
				else{
						 return false;
		
				
				}
					
				
		}
		
		//might be handy!
		function getItemsXML($startDate,$endDate){
		
		
		}
		
		function getSummary(){
				
				$summaryList=array();		 
		   
			 $dummyArray=$this->diary;
			 
		    foreach ($dummyArray as $name=>$value){
				  
					//echo $value['title'];
					 
					  
					 //echo 	'<p>' . $value['startDate'] . ' - ' . mktime();
					 
					 $countdown = $value['startDate'] -  mktime();
					 $countdown=$countdown/(24*60*60);
					 $countdown=ceil($countdown);			 
					 
					 if ($value['textDate'] != ''){
					 		$displayDate=$value['textDate'];
					 }
					 else{
					    $displayDate=date('j F Y',$value['startDate']);
					 }
					 
				
					 
					 //note that the id is made javascript-friendly here
				   $summaryList[]=array(	 'id'=>'id' . $value['id'],    
					 												 'title'=>$value['title'], 
																	 'highlight'=>$value['highlight'], 
																	 'intro'=>$value['intro'],
																	 'body'=>$value['body'],
					                         'displayDate'=>$displayDate, 
																	 'countDown'=>$countdown
																	 
															 );
								
				
				} 
		    return $summaryList;
		
		}
		
		
		
		
		function getItems(){
		
		   return $this->diary;
		
		
		}
		function getCountdowns(){
		
		
		
		
		}
	  	//collects SQL parameters
		function _addParameters(){
				
							 
				foreach($this->params as $name=>$value){
						 									
						//echo '<p>' . $name  .'|'. $value;									
															
						if($name=='section'){
									$this->sqlParams[]=		'section ="'. $value .'"';													
						}  

						if($name=='active' && $value==1){
									$today=mktime(0,0,0,date('m'), date('d'), date('Y'));																		
									$this->sqlParams[]=		'published <= '.$today.' AND depublish > '.$today ;													
						} 
	 
				}
		//echo '<hr>';
		//print_r($this->sqlParams);
		
		}
		
		//returns SQL Paramter
		function _getParameters(){
						 
						
						 
						 $sqlSubstring='';
						 $counter=0;
						 
						 
						 foreach($this->sqlParams as $name=>$value){
						 
						 
						       //  echo '<h2>got sshere</h2>';
									//	echo '<i>' . $name . 'PPPPPP'. $value . ' </i>';
						     
						 				if ($counter==0){
											 		$sqlSubstring .= ' WHERE ';
										}				
										else{
												 $sqlSubstring .= ' AND ';
										}
										
													     
						     		$sqlSubstring .= $value;
										$counter=$counter +1;
										
						 }
		
		         return $sqlSubstring;
		}	
		
		function _getLimit(){
		
						 if($this->limit!=''){
						 
						      $sqlSubstring=' LIMIT ' . 	$this->limit['offset'] .' , '.		$this->limit['number']	;
									return $sqlSubstring; 
						 }

					
						 
		}
	
	
	
}
?>