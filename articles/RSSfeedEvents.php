<?php
//echo 'Made it to the start of the script';

include('config/rms_config.php');
$host=  $config['host'];   // Hostname of MySQL server
$dbUser=$config['dbUser'];    // Username for MySQL
$dbPass=$config['dbPass'];    // Password for user
$dbName=$config['dbName']; // Database name
$baseURL=$config['basePath'];
$imagesRelativePath=$config['imagesRelativePath'];

require_once('Database/MySQL.php');
$db=new mysql($host, $dbUser, $dbPass, $dbName);

// Include the RssGenerator class
require_once('XML/RssGenerator.php');

//echo 'here';
// Define variables to be used in building the feed
$title='Information and Records Management Society Events';
$link='http://www.irms.org.uk/events';
$desc='At the forefront of Records Management';
$about='http://www.irms.org.uk/about';
$logo='http://www.irms.org.uk/images/logo-not-home.gif';
//$searchTitle='Search';
//$searchDesc='Search the IRMS website...';
//$searchUrl='http://www.sitepoint.com/search/search.php';
//$searchVar='q';

// Instantiate the RssGenerator
$rssGen=new RssGenerator();

// Add the channel information
$rssGen->addChannel($title,$link,$desc,$about);

// Add the image description
$rssGen->addImage($logo,$title,$link);

// Add the search description
//$rssGen->addSearch($searchTitle,$searchDesc,$searchUrl,$searchVar);


	require_once('articles/EventItems.php');
	
	//Will always be required
	$articles=new eventItems($db,true);//true means active only 
	
	//echo 'here';
	
	//Changed to include ALL events
	//$counter=1;
	while ( $theItem=$articles->getItem() ) {
		 
		//Title should be title + date + venue + location
		$rssTitle= $theItem->title() . ' | ' . $theItem->dateRange() . ' | ' .  $theItem->location();
    
		//Use the first fdew words only.
		$shortDescription=$theItem->bodyHTML();
		$shortDescription=strip_tags($shortDescription);
		$wordcount=50;
		$words=preg_split('/([\s.,;]+)/',$shortDescription,$wordcount+1,PREG_SPLIT_DELIM_CAPTURE);
		array_pop($words);
		$shortDescription = (implode('',$words)); 
		
		$shortDescription = $shortDescription . '...';
		
		//echo 	$shortDescription;
	        $rssGen->addItem($rssTitle, 'http://www.irms.org.uk/events/' .$theItem->ID(), $shortDescription );
    

		  
		 //  if($counter==10){
		//	   break;
		//   }
		//   $counter++;
	 }



// Send the XML Mime type
//header ( 'Content-type: text/xml' );

//Display the document
//echo $rssGen->toString();

//$rssGen->toFile('c:/events.xml') ;

$rssGen->toFile(BASE_FILE_PATH . '/rss/events.xml') ;
//echo 'Made it to the end of the script';
?>