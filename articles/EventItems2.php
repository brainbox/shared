<?PHP

require_once('articles/Items.php'); 

class eventItems extends Items {

   // var $eventItems;
		//var $result;
		var $isMatched;		

    function eventItems ( $dbConn,$active, $params='') {
		
            #echo 'active ' . $active;    				 
            $this->dbConn=$dbConn;		 
            $this->params=$params; 	 
            //give the parameters to the function		 	 
            $this->_addParameters();
            
            

            //FOR the  LIVE SITE
            if($active){
                                    $today=mktime(0,0,0,date('m'), date('d'), date('Y'));
                                    
            #NB - active now means "NOT YET FINISHED". NO!!! Changing to the normal definition: PUBLISHED!!!!
            
            
            $sql  = 'SELECT id, title, intro, isHTML, highlight, body, startdate, enddate, venue, image, height, width, alt, email, website, attachment  ';
                                    $sql .= ' FROM events ';
                                    $sql .= ' WHERE published <= '.$today;
                                    $sql .= ' AND depublish > ' . $today ;
                                    #$sql .= ' AND startdate < ' . ($today + 24*60*60) ;
                                    #$sql .= ' AND endDate > ' . ($today - 24*60*60) ;
                                    $sql .=   $this->_getParameters();
                                    $sql .= ' ORDER BY 0+startdate;'; 
            }     
            //FOR ADMINISTRATION
            else{
                  $sql  = 'SELECT id, title, intro, isHTML, highlight, body, startdate, enddate, venue, image, height, width, alt, email, website, attachment, author, published,depublish ';
                                    $sql .= ' FROM events ';
                                    $sql .= ' WHERE 1=1 ';
                                   $sql .=  $this->_getParameters();
                                    $sql .= ' ORDER BY 0+startdate DESC;';
            }
            
            //echo "the SQL " . $sql;
            $result = $dbConn->query($sql);
            #echo "number of records: ".$result->size();
            
            $numberOfResults=$result->size();
				
			  if($numberOfResults != 0){
				
      				while ( $row = $result->fetch() ) {
          							
                      $this->Items[]=$row;
              }
							return true;
				}
				else{
				    #echo 'GOT HERE';
				    return false;
				}
				
					
		}
   function getItem () {
      
        $row=@each($this->Items);
		    if ( $row ) {
						  return new eventItem($row['value'],$this->dbConn);
        } else {
            @reset ( $this->Items );
            return false;
        }
		
   }
   #Carbon copy of "getItem"
   function fetch () {

		
        $row=@each($this->Items);
		    if ( $row ) {
						  return new eventItem($row['value'],$this->dbConn);
        } else {
            @reset ( $this->Items );
            return false;
        }
		
   }
		
		function getItemByID($id) {
				
				$isMatched=false;					
				foreach($this->Items as $name=>$value){
																					
						if($value['id']==$id){
									return new eventItem($value,$this->dbConn);																			 
						}
						
				 }
				 //only gets this far if article not found
				 reset($this->Items);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
				 //echo "<p>No article found";
				 return false;
				
				 
		}
      //collects SQL parameters
      function _addParameters(){
                      
                      if($this->params !=''){
                      foreach($this->params as $name=>$value){
                                                                                                              
                     //echo '<p>' . $name  .'|'. $value;			
                                     //echo $name;
                                     
                       switch($name){
                             
                                case ('section'):
                                          
                                                     $this->sqlParams[]=		'section ="'. $value .'"';		
                                      
                                      break;
                                      
                                      case ('active'):
                                      
                                          if($value==1){				     
                                                           $today=mktime(0,0,0,date('m'), date('d'), date('Y'));																		
                                                 $this->sqlParams[]=		'published <= '.$today.' AND depublish > '.$today ;	
                                                             }	
                                      break;
                                      
                                      //Default: take param literatlly
                                      default:
                                          
                                                      $this->sqlParams[]=		$value ;			
                                }	
                             }
                                                      
                              }			
                                                      

                                              
      }
		function _getParameters(){
						 
						
						 
						 $sqlSubstring='';
						 
						 //echo 'HERE<hr>';
						 //print_r($this->sqlParams);
						 
						 if(isset($this->sqlParams) ){
						 
    						 foreach($this->sqlParams as $name=>$value){
		     
    						     		$sqlSubstring .=  ' AND  ' . $value;

    						 }
						 }
		
		         return $sqlSubstring;
		}	
		
		
}





// The class built by the factory method
class eventItem extends item {
   
	 //ONLY the unique variables need be declared
		
		var $startdate;
		var $enddate;
		var $meeting_date;
		var $venue;
		
		var $image;
		var $width;
		var $height;
		var $alt;
		
		
		var $email;
		var $website;
		var $attachment;
		var $highlight;
   
				
    function eventItem($data, $dbConn) {
		//All the data needs to be declared here
		//echo '<p>' . date('d M Y',$data['startdate']);
		
		    $this->dbConn=$dbConn;
		
				$this->id=          $data['id'];
				
				//echo 'the article ID should be ' . $this->id;
        $this->title=				$data['title'];
				$this->intro=				$data['intro'];
				$this->isHTML=			$data['isHTML'];
				$this->highlight=	  @$data['highlight'];
				$this->body=				$data['body'];	
				
				$this->startdate=		@$data['startdate'];
				$this->enddate=		  @$data['enddate'];
				
				
							
				$this->meeting_date=		@$data['meeting_date'];
				
				
				$this->venue=		    @$data['venue'];
				
				$this->image=				$data['image'];
				$this->width=			  $data['width'];
				$this->height=	    $data['height'];
				$this->alt=				  $data['alt'];	
				
				
				//$this->attachment=  @$data['attachment'];
				//$this->website=     @$data['website'];
				//$this->email=       @$data['email'];
				
				
        $this->author=			@$data['author'];
        $this->published=		@$data['published'];
        $this->depublish=		@$data['depublish'];
						
				
				//Get FILE the INFO but not the contents of the file
   			$sql='SELECT id, filename, description, mimetype, filesize, upload_date 
            FROM downloads
    			  WHERE article_id='.$this->id.'
    			  AND section="events";';
    
          // Perform the query
          //	echo $sql;
				//This should have given access to the CREATOR's dbConn	
			//print_r($dbConn);
			//print_r($this->dbConn);
				
        $result=$dbConn->query($sql);
				
				if(!$result->size()==0){
				
				   while ( $row = $result->fetch() ) {
					     // echo "got to the rows";
                $this->downloads[]=$row;
					 }
        }
			
						
				
		
    }
		//Only the UNIQUE functions need to follw here... other will be inherited		
				
		
		function highlight() {
        return $this->highlight;
    }
				
		function venue() {
        return $this->venue;
    }
		function startdate() {
        return $this->startdate;
    }
		function enddate() {
        return $this->enddate;
    }
		
   function getEventDate(){
   
      #echo 'start/enddate'  . $this->enddate . '/' . $this->startdate;
      
        //simple case first 
      if( !$this->enddate() || $this->enddate==$this->startdate ){

         return date('d M Y',$this->startdate() );

      }			
           
   
        //If there's an end date, work out what to do with it
                    else{
                        //same year 
                                    if(date('Y',$this->startDate() ) == date('Y',$this->endDate() ) ){
                                    
                                                   //same year and same month
                                                   if(date('M',$this->startDate() ) == date('M',$this->endDate() ) ){
                                                                   $startDate= date('d',$this->startDate() );
                                                   }
                                                   //same year, diff month
                                                   else{
                                                        $startDate= date('d M',$this->startDate() );
                                                   }
                                    
                                    
                                    }
                                    //diff year
                                    else{
                                          $startDate= date('d M Y',$this->startDate() );
                                    }
      //Construct the string
                            return $startDate . ' - ' . date('d M Y',$this->endDate());	 
                                    
                    } 
                   
   
   }
   
		
}





