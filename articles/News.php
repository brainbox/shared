<?php

/**
 * 27 Apr - Slight change of emphasis: going to make this class more "front-end" oriented: less "raw" database
 * values, more business rules. A consequence is that the Admin site will now talk drectly to the Model
 *
 */

require_once('Item.php');

Class News extends Item{
    
   
    
    public $subtitle; #Delimited string 
   

    function __construct(){
        
        
      
    }
    
    
    
    /**
     * Populates the class with values from the Model
     *
     */
    

    public function populate($data){
        
        
       # print_r($data);
       # exit;
        
        
        if(isset($data['id']))
            $this->id = $data['id'];
        
        if(isset($data['title']))
            $this->title = $data['title'];
            
        if(isset($data['subtitle']))
            $this->subtitle = $data['subtitle'];    
            
        if(isset($data['path']))
            $this->path = $data['path'];
            
        if(isset($data['path_raw']))
            $this->pathOld = $data['path_raw'];    
            
        if(isset($data['body']))
            $this->body = $data['body'];
        
        
        if(isset($data['the_date']))
            $this->theDate = $data['the_date'];
        
        if(isset($data['place']))
            $this->place = $data['place'];
            
        if(isset($data['image']))
            $this->image = $data['image'];
            
        if(isset($data['alt']))
            $this->alt = $data['alt'];
        
        if(isset($data['width']))
            $this->width = $data['width'];
         
        if(isset($data['height']))
            $this->height = $data['height'];
        
        if(isset($data['last_update']))
            $this->lastUpdate = $data['last_update'];
           
        
        if(isset($data['awkward'])){
            
            if($data['awkward']==1){
                $this->onHold = false;
            } else {
                $this->onHold = true;
            }
            
        }
        
        if(isset($data['notes']))
            $this->notes = $data['notes'];
        
        
        $this->_applyBusinessRules();
        
        
    
        
        
        
        
    }
    
    /**
     *
     *
     */
    
    private function _applyBusinessRules(){
        
        
        //Clean up
        $pattern = '/&(?!amp;)/';
	$this->title = preg_replace('/&(?!amp;)/', '&amp;', $this->title);
        $this->subtitle = preg_replace('/&(?!amp;)/', '&amp;', $this->subtitle);
        $this->body = preg_replace('/&(?!amp;)/', '&amp;', $this->body);
        $this->alt = preg_replace('/&(?!amp;)/', '&amp;', $this->alt);

    
        
        //Derived parameters
        
        
        
        
        $this->metaTitle = $this->truncate($this->title, 60);
        
        if($this->subtitle){
                    $this->subtitleArray=explode('|',$this->subtitle);
        }
        
        $this->_createMetaDescription();
      
        
        if(!$onHold && strtotime($this->theDate)<time() ){
            
            $this->isActive=true;
        } else {
            $this->isActive=false;
        }
        
    }
    
    
    private function _createMetaDescription(){
        
        if(count($this->subtitleArray) ){
          
            $this->metaDescription = trim(implode('. ', $this->subtitleArray), '. ');
            
        } else {
            #Grab the first paragraph
            $pattern = '/^.*?\<p>.*?\/p>/is'; #firs two paras
            preg_match($pattern, $this->body, $matches); 
            
            $this->metaDescription = strip_tags($matches[0]);

        }
    
        
    }
    
    private function splitBody(){
        
       
       
        $pattern = '/^.*?\<p>.*?\/p>.*?\/p>/is'; #firs two paras
        
        preg_match($pattern, $this->body, $matches); 
	
        #print_r($matches);
        
        $this->intro = $matches[0];
        
        $this->bodyWithoutIntro = str_replace($this->intro,'',$this->body);
        
        
    }
    

    
    


}