<?PHP

/** A class to - hopefull- make the handling of downloads easier!
 *
 * 
 */


Class downloads{
		
    //constructor
    function downloads($baseURL){
    
    
        $this->baseURL=$baseURL;
        
        $this->tempString='';
        $this->downloadsData=array();		      
        $this->pdfPresent=false;//used to test for the existence of a .pdf		
        
        
        //Set up info required to serve the correct icon
        $this->mimeInfo=array(
            
            'applicaton/pdf'=>    array(
            
            'suffix'=>'.pdf',
            'image' =>'pdficon.gif',
            'alt'   =>'PDF file'),
            
            'application/pdf'=>    array(
            'suffix'=>'.pdf',
            'image' =>'pdficon.gif',
            'alt'   =>'PDF file'), 
            
            'application/msword'=> array(
            'suffix'=>'.doc',
            'image' =>'wordicon.gif',
            'alt'   =>'Word file'),
			
			
			'application/vnd.openxmlformats-officedocument.wordprocessingml.documentFile'=> array(
            'suffix'=>'.docx',
            'image' =>'wordicon.gif',
            'alt'   =>'Word file'), 	
            
            'application/vnd.ms-powerpoint'=> array(
            'suffix'=>'.ppt',
            'image' =>'ppicon.gif',
            'alt'   =>'PowerPoint file'),
			
            'text/xml'=> array(
            'suffix'=>'.xml',
            'image' =>'xmlicon.gif',
            'alt'   =>'XML file'),	
            
            'application/vnd.ms-excel'=> array(
            'suffix'=>'.xls',
            'image' =>'excelicon.gif',
            'alt'   =>'Excel file') ,
            
            'application/octet-stream'=> array(
            'suffix'=>'.csv',
            'image' =>'csvicon.gif',
            'alt'   =>'CSV file'),
            
            
                
            'application/postscript'=> array(
                'suffix'=>'.eps',
                'image' =>'epsicon.gif',
                'alt'   =>'EPS file'),
            
                 
            'image/pjpeg'=> array(
                'suffix'=>'.jpg',
                'image' =>'jpgicon.gif',
                'alt'   =>'JPEG file'),
            
            'image/tiff'=> array(
                'suffix'=>'.jpg',
                'image' =>'tifficon.gif',
                'alt'   =>'TIFF file') 		
            
            
            
            );

						 
        //start the table
        require_once('HTML/Table.php'); 
        //Downloads
        $this->table =new HTML_Table(array( 'summary'=>    'Downloads',
        'class'=>			 'downloads'				));
        $this->classArray=array('class=col1','class=col2','class=col3');
        						
						 
						 
						 					
	}
				 
    //add data for a single donwload to the class
    function addDownload($data){
    
        // print_r($data);
        
        
        //add the new data for a single download to the pile		
        $this->downloadsData[]=$data;
        
        
        //Keep track of .pdfs
        
        $mimeType=$data['mimetype'];
		

        
        if( ($mimeType=='applicaton/pdf') || ($mimeType=='application/pdf')  ){
        $this->pdfPresent=true;
        //echo "got her";
        }												
        
		$imageString='';
		
		if(isset($this->mimeInfo[$mimeType])){
			
			$imageString = '<img src="'.$this->baseURL.'images/'.$this->mimeInfo[$mimeType]['image'].'" height="32" width="32" alt="'.$this->mimeInfo[$mimeType]['alt'].'" />';
			
		}
		
        $this->table->addRow( 
        array('<a href="/download/'.$data['id'].'">'.$imageString.'</a>',
        str_replace('&','&amp;', $data['description']),
        '<a href="/download/'.$data['id'].'">Download ('.$this->mimeInfo[$mimeType]['suffix'].' '.$this->human_size($data['filesize']).')</a>'
        ),$this->classArray
        );

						
						
    }
				 
    //add data for a single donwload to the class
    function addDownloadByID($id, $db){
                       
               $this->db =	$db;
               
        $sql='SELECT id, filename, description,mimetype, filesize, upload_date 
           FROM downloads
               WHERE id='.$id.';';
        
        // Perform the query
        //echo $sql;
        $result=$this->db->query($sql);
        
        //print_r($result->number_of_results);
        if ($result->size()!=0){       
        
             // Loop through the directory
             while ( $row=$result->fetch() ) {
                                   
                       //Pass it over to the addDownload function
                                           $this->addDownload($row);
                       
             }
        }
           
    }

				 
    function getDownloadTable(){
    
            //add on the "footer"
            $this->addInstructions();
    
        return $this->table->toHTML() . $this->tempString;
            
    
    
    }
    
    function addInstructions(){
    
        if($this->pdfPresent==true){
            
            $PDFtable =new HTML_Table(array( 'summary'=>    'Instructions for viewing PDFs',
                                                                'class'=>			 'pdf'				));
            $classArray=array('class=col1','class=col2','class=col3');
            $pdfInstruction='
            
            Acrobat Reader is a helper application manufactured by Adobe Systems Incorporated that
            lets you view and print Portable Document Format (PDF) files. Click the button to download 
            Acrobat Reader from the Adobe website.
                                
              ';
            
            
            
            $PDFtable->addRow( 
            array('<img src="'.$this->baseURL.'images/'.$this->mimeInfo['applicaton/pdf']['image'].'" height="32" width="32" alt="'.$this->mimeInfo['applicaton/pdf']['alt'].'" />',
                            $pdfInstruction, 
                            '<a href="http://www.adobe.com/products/acrobat/readstep.html">
                               <img height="31" width="88" src="'.$this->baseURL.'images/getacro.gif" 
                     alt="Get Adobe Reader" /></a>'),
                            $classArray
                           );
            
            
            $this->tempString .= $PDFtable->toHTML();
           }
               
    
    }
				 
				 
				
    function human_size($size, $decimals = 1) {
        $suffix = array('Bytes','KB','MB','GB','TB','PB','EB','ZB','YB','NB','DB');
        $i = 0;
   
        while ($size >= 1024 && ($i < count($suffix) - 1)){
            $size /= 1024;
            $i++;
        }
           
        return round($size, $decimals).' '.$suffix[$i];
	}
			
			
    function iconFilename($mimeType){
    
       
       return $this->mimeInfo[$mimeType]['image'];
    }
    function iconAlt($mimeType){
    
       
       return $this->mimeInfo[$mimeType]['alt'];
    }
    function extension($mimeType){
    
       
       return $this->mimeInfo[$mimeType]['suffix'];
    }

}

Class Download{
    
    
    function __construct($dbPDO,$data){
        
        
        
    }
    
    private function _initialise(){
        
    }
    
    
    
    
}

?>