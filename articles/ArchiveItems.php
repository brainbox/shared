<?PHP

//This is very similar to FlexiItems - and I want to keep the API the same if possible
//Key change will be to take the minimum from the database for achiveItems

require_once('articles/Items.php'); 

class archiveItems extends Items {

   // var $archiveItems;
		//var $result;

    function archiveItems ( &$dbConn, $table, $section, $active ) {

				$this->dbConn=&$dbConn;
						 
				$sectionClause =''; 
		    				
				//FOR the  LIVE SITE
				if($active){
				
							if($section != ''){
				   								$sectionClause = 'AND section = "'.$section.'"';
				      }
				
							$today=mktime(0,0,0,date('m'), date('d'), date('Y'));
        			$sql='SELECT id, title,intro
							      FROM '.$table.' 
										
										WHERE published <= '.$today.' 
										AND depublish > '.$today.' 
										'.$sectionClause.'
										ORDER BY published DESC;'; 
				//echo $sql;
					
				}     
				
				//FOR ADMINISTRATION
				else{
						  if($section != ''){
				   								$sectionClause = 'WHERE section = "'.$section.'"';
				      }
				
				      $sql='id, title,intro 
							FROM '.$table.' 
							'.$sectionClause.'
							ORDER BY published DESC;';
							//echo $sql;
				}
				
				//echo "the SQL " . $sql;
				$result = $dbConn->query($sql);
				
        //echo "<pre>the result is ";
				//var_dump($result->fetch());
				
        while ( $row = $result->fetch() ) {
							//echo $row;
            $this->archiveItems[]=$row;
        }
		}
			
		/*
		// Factory method creates new instances of archiveItem
    function & getItem () {
		    
				//echo "<h1>here";
				//echo $section;
	     
			 
			 
				
				
		
		
        $row=each($this->archiveItems);
		    if ( $row ) {
				   
								
						  return new archiveItem($row['value'],$this->dbConn);
        } else {
            reset ( $this->archiveItems);
            return false;
        }
		
		   
		
		
		}
		
		function & getItemByID($id) {
				//this may be wrong: stepping through an objext as if it's an array		 											
				foreach($this->archiveItems as $name=>$value){
		
						if($value['id']==$id){
											//echo "got here";															
									return new archiveItem($value,$this->dbConn);																			 
						}
						
				 }
				  //only gets this far if article not found
				    reset($this->archiveItems);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
				    //echo "<p>No article found";
				    return false;
						
		}
		*/
		
}

// The class built by the factory method
class archiveItem extends item {
   
	
		
    function archiveItem($table, $section, $id,&$dbConn) {
		
		    $this->table=$table;
				$this->section=$section;
				$this->id=$id;
		    $this->dbConn=&$dbConn;
				
				
				//get the details of the item
				
				if($section != ''){
				   								$sectionClause = 'AND section = "'.$section.'"';
				}
				
							$today=mktime(0,0,0,date('m'), date('d'), date('Y'));
        			$sql='SELECT title, intro, body, isHTML, author, image, width, height, alt, email, published, depublished
							      FROM '.$table.' 
										WHERE id='.$id.'
										'.$sectionClause.'
										;'; 
				##echo $sql;
				
				
				$result=$this->dbConn->query($sql);
				
				if($result->size()!=0 ){
				
				   while ( $data = $result->fetch() ) {
					     // echo "got to the rows";
             
              $this->title=				$data['title'];
      				$this->intro=				$data['intro'];
      				$this->body=				$data['body'];
      				$this->isHTML=			$data['isHTML'];
      								
      				$this->image=				$data['image'];
      				$this->width=				$data['width'];
      				$this->height=			$data['height'];
      				$this->alt=				  $data['alt'];
      				
              $this->author=			$data['author'];
              $this->published=		@$data['published'];
              $this->depublish=		@$data['depublish'];
					 }
        }
				
				/* downloads needs to be changed!!!	
					
				//Get FILE the INFO but not the contents of the file
   			$sql='SELECT id, filename, description, mimetype, filesize, upload_date 
            FROM downloads
    			  WHERE article_id='.$this->id.'
    			  AND section="'.$this->section.'";';
    
          // Perform the query
          //	echo $sql;
				//This should have given access to the CREATOR's dbConn	
				
				
        $result=$this->dbConn->query($sql);
				
				if(!$result->size()==0){
				
				   while ( $row = $result->fetch() ) {
					     // echo "got to the rows";
                $this->downloads[]=$row;
					 }
        }
			  */
				
			
				
		}			
}		
	
		

?>





