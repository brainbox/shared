<?php

/**
 *
 * Designed to be called daily
 * Needs to decide what - if anything - needs to be sent
 * 
 *
 *
 */





require_once('email/Alerts.php');



Class VoteAlerts extends Alerts{
    
    public $alertName 			= 'Voting Emails';
    
    public $emailSubjectPrefix		= 'IRMS Vote:';
    public $fromEmailAddress 	= 'no-reply@irms.org.uk';
    public $fromEmailName 		= 'Information and Records Management Society';
    
    public $senderEmailAddress 		= 'no-reply@irms.org.uk'; // This is for bouncers
    
    public $isPreview = false;
    public $isTest = true;
    
    
    public $sendCount;
    
   
    
    /**
     *
     *
     *
     */
	
	
    function __construct($dbPDO){
	
        parent::__construct($dbPDO);
        
        $this->dbPDO = $dbPDO;
        $this->copyEmailsToBrainbox = true; // Too many emails for me!
	
	
    }
    
    public function initialise(){
	
	
	
        $this->getVotes();
	
	
	
    }
    
    
    function getVotes(){
	
        require_once('Vote/Vote.php');
        
        $isActive= true;
        $this->voteArray = Vote::getAll($this->dbPDO,$isActive);
        
        
        if(empty($this->voteArray)){
            
            //nothing to do
            //echo 'do nothing';
            return;
        
        } else {
            
            $this->prepareEmailer();   
            
            foreach($this->voteArray as $this->vote){
                
                $this->sendEmails();
                
            }
                
        }
	
    }
    
    
    
    
    
    function sendEmails(){
	
	//Three cases:
	// * launch
	// * reminder
	// * lastchance
        
        
        if($this->isTest){
            
			fwrite(STDOUT, "\nSending LAUNCH");
            $this->sendEmailsByType('launch');
            
			fwrite(STDOUT, "\nSending LAST CHANCE");
            $this->sendEmailsByType('final');
            
			fwrite(STDOUT, "\nSending REMINDER");
            $this->sendEmailsByType('reminder');
            
			fwrite(STDOUT, "\n\nEND OF TEST");
            return;
            
        }
        
        
        
        
        
        
        
        if($this->vote->startDate ==  date( 'Y-m-d') ){
            
            fwrite(STDOUT, "\nSending LAUNCH");
            $this->sendEmailsByType('launch');
            return;
            
        }
	
	
        
        if ($this->vote->endDate ==  date( 'Y-m-d' )){
            
            fwrite(STDOUT, "\nSending LAST CHANCE");
            $this->sendEmailsByType('final');
            return;
            
        }
        // not sure about the reminders! -- they go every week
        
        
        //Integet number of weeks since launch?
        
        $startDateUNIX = strtotime($this->vote->startDate);
        $today = mktime(0,0,0, date('m'), date('d'), date('y') );
        
        $diff = $today - $startDateUNIX;
        $days = $diff/(60*60*24);
        
           
        if ( $days % 7 == 0   ){     
            
			fwrite(STDOUT, "\nSending REMINDER");
            $this->sendEmailsByType('reminder');
            return;
            
        }
		
		fwrite(STDOUT, "\Nothing to send");
                
        
    }
    
    
    function sendEmailsByType($type){
	
        
        
           
        switch ($type){
            
            case 'reminder':
            
            $this->subject = $this->emailSubjectPrefix . ' ' . $this->vote->emailSubject . ' - REMINDER' ;
            break;
            
            case 'final':
            
            $this->subject = $this->emailSubjectPrefix . ' ' . $this->vote->emailSubject . ' - FINAL DAY';
            break;
            
            default:
            
            $this->subject = $this->emailSubjectPrefix . ' ' . $this->vote->emailSubject;
            
        }
		
		fwrite(STDOUT, "\nEmail subject: " . $this->subject );
        
        
        ob_start();
                
                require("Views/email-vote-$type.php");
            
        $this->bodyText= ob_get_clean();
            
            
        $this->getVoters();
            
        $counter = 0;
        foreach ($this->voterArray as  $member){
            
            $counter++;
            
            if($this->isTest && $counter > 10){
            
                return;
            
            }
            
            $member = (object) $member;
            
            $this->toName=$member->forename . ' ' . $member->surname;
            $this->toEmail=$member->email;
            
			fwrite(STDOUT, "\nSending to " . $member->email);
			
			
            $this->sendEmail();
            
        }
            
        $this->sendCount = count($this->voterArray);
	
	
    }
    
    
    
    /**
     * Watch out: this is specific to IRMS
     * this class in overridden for ASLTIP... mainly for this function
     *
     *
     *
     */
    
    function getVoters(){
                
        require_once 'Vote/Model/Vote.php';
        $this->voterArray = Model_Vote::getVoters($this->dbPDO, $this->vote->id);
        
    }
    
    

    
    protected function _createLaunchReport(){
        
        if($this->sendCount){
	
            $this->launchReport= 'Job complete. ' . $this->sendCount . ' emails sent.';
            $this->launchReport .= "\n\n\n";
            $this->launchReport .= $this->bodyText;
        
        }
	
	
    }
    
    
    
    function __destruct(){
        
		if(!$this->isPreview)
		{
			
			parent::__destruct();
			
		}
	
    }
    
}
