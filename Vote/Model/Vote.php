<?php


Class Model_Vote{




    public static function checkEligibility($dbPDO, $voteId, $user){

        $memberId = $user->userId;
                  
        $sql = "SELECT
                    count(*)
                
                FROM
                    vote_member_xref
                
                WHERE
                    1=1
                    AND vote_id = $voteId
                    AND member_id= $memberId";

        //echo $sql;
        
        $stmnt = $dbPDO->prepare($sql);
        
        $stmnt->execute();
        
        $result = $stmnt->fetch();
        
        if($result['count(*)']){
            
            return false; //already voted
        } else {
            
            return true; //not yet voted.
        }
        

    }






    public function incrementCount($dbPDO, $voteId, $response){


        if($response=='yes'){
                  
            $sql = "UPDATE
                        vote
                    
                    SET
                        yes_count = yes_count+1
                    
                    WHERE
                        id = $voteId";
                      
                  
        } elseif ($response=='no'){
                  
            $sql = "UPDATE
                        vote
                    
                    SET
                        no_count = no_count+1
                    
                    WHERE
                        id = $voteId";
                  
        }
        
        //echo $sql;
        
        
        $stmnt = $dbPDO->prepare($sql);
        
        $stmnt->execute();
	  
    }



    public function recordVote($dbPDO, $voteId, $user){

        $memberId = $user->userId;
                  
        $sql = "INSERT INTO
                    vote_member_xref (vote_id, member_id, the_time)
                
                VALUES
                    ( $voteId  , $memberId  , NOW() )
                
                ";
                      
    
        //echo $sql;
        
        $dbPDO->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION); 
        $stmnt = $dbPDO->prepare($sql);
        
        
        try {
            
            $stmnt->execute();
            
            
        } catch (PDOException $e) {
            
            //Typically an integrity violation from multiple voting
            //This is an exception, because it shouldn't really happen
            print '<p>Vote already registered. Please <a href="/">click here</a>
                to return to the Home Page.</p>';
            
            die();
            
            //return false;
        
        }

        return true;
        
	  
    }

	public function getVoters($dbPDO, $voteId){
        
        $sql = "SELECT
                    u.user_id as id
					,	u.login as email
					,	u.firstName AS forename
					, 	u.lastName AS surname
                
                FROM
                    user_new AS u
					
					LEFT OUTER JOIN vote_member_xref
						ON (u.user_id = vote_member_xref.member_id)
						AND  vote_member_xref.vote_id = $voteId  -- WOW never done that before
						
						-- http://searchoracle.techtarget.com/answer/OUTER-JOIN-instead-of-a-NOT-EXISTS-subquery
					
                WHERE
                    1=1
					AND u.active = '1'
					AND vote_member_xref.member_id IS NULL  -- want the unmatched ones only
                   
					";

	
        $stmnt = $dbPDO->prepare($sql);
        
        $stmnt->execute();
        
        return $stmnt->fetchAll();
        
        
        

    }



    public function getVotersASLTIP($dbPDO, $voteId){
        
        $sql = "SELECT
                    u.id
					,	u.email
					,	u.forename
					, 	u.surname
                
                FROM
                    member AS u
					
					LEFT OUTER JOIN vote_member_xref
						ON (u.id = vote_member_xref.member_id)
						AND  vote_member_xref.vote_id = $voteId  -- WOW never done that before
						
						-- http://searchoracle.techtarget.com/answer/OUTER-JOIN-instead-of-a-NOT-EXISTS-subquery
					
                WHERE
                    1=1
					AND u.active = '1'
                    AND u.email IS NOT NULL
					AND vote_member_xref.member_id IS NULL  -- want the unmatched ones only
                   
					";
	
        $stmnt = $dbPDO->prepare($sql);
        
        $stmnt->execute();
        
        return $stmnt->fetchAll();
        
        
    }





	function updateFromPOST($dbPDO, $id){
				           
        $sql = "UPDATE

					vote             
                
				SET
					
					email_subject 	= :email_subject
					, question 		= :question
					, start_date 	= :start_date
					, end_date 		= DATE_ADD(:start_date, INTERVAL :weeks*7  DAY)
					, is_published	= :is_published
					, background 	= :background
					, banner_text 	= :banner_text
                
                WHERE
                   
					id = $id";
		            
            $stmnt=$dbPDO->prepare($sql);
			
			
            $stmnt->bindParam(':email_subject',  	$_POST['email_subject'] );
            $stmnt->bindParam(':question',  	$_POST['question'] );
			$stmnt->bindParam(':is_published',  $_POST['is_published'] );
            $stmnt->bindParam(':start_date',    $_POST['start_date']);
            $stmnt->bindParam(':weeks',         $_POST['weeks']);
            $stmnt->bindParam(':background',    $_POST['background']);
			$stmnt->bindParam(':banner_text',    $_POST['banner_text']);
            
            $stmnt->execute();
            		
			//var_dump($dbPDO->errorInfo() );
		
	}



	function createNew($dbPDO){
		
		//Find Next Tuesday
		$startDate =  date('Y-m-d', strtotime('next Tuesday') );
		$endDate =  date('Y-m-d', strtotime('third Tuesday') );
				  
		
		
		
		
		$sql = "INSERT INTO
					vote (question, start_date, end_date)
				
				VALUES
					(
						'******* NEW ********'
						, '$startDate'
						, '$endDate'
					
					
					)";
			
		$stmnt=$dbPDO->prepare($sql);
        $stmnt->execute();
	}
}

