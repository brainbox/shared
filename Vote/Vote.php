<?php

/**
 *    Need to get a grip of the dates here
 *
 *    Database
 *     * start date - the date of the first day of the Vote
 *     * end date - the date of the last day of the vote
 *
 *    Note that SQL's BETWEEN includes the first day AND last day (so no special treatment is needed for the end date)
 *
 *    
 *
 */



define('VOTE_STATUS_DRAFT', 	'Draft');

define('VOTE_STATUS_PENDING', 	'Pending');  
define('VOTE_STATUS_ACTIVE', 	'Active');
define('VOTE_STATUS_COMPLETE', 	'Complete');


Class Vote{


    public $timeRemainingText;
    public $errorArray = array();
    public $status = VOTE_STATUS_DRAFT;

    function __construct($dbPDO){
        
        $this->dbPDO = $dbPDO;
        
    }
    
    
    function populate($data)
    {
        
        //var_dump($data);
        //exit;
        
        $this->id           = $data['id'];
       
        $this->question     = $data['question'];
		
		$this->emailSubject = $data['email_subject'];
		
		$this->background    = $data['background'];
		$this->bannerText    = $data['banner_text'];
		
		$this->isPublished    = $data['is_published'];
	
        $this->startDate    = $data['start_date'];
        $this->endDate      = $data['end_date'];
        $this->daysRemaining = $data['days_remaining'];  // Watch out: 0 means "less than a day"
        
		$this->yesCount = $data['yes_count'];
		$this->noCount = $data['no_count'];
	
        if( isset($data['voter_count']) ){
            $this->voterCount = $data['voter_count'];
        }
	
        $this->applyBusinessRules();
        
    }
    
    function applyBusinessRules(){
	
	
		if(!$this->emailSubject){
			
			$this->emailSubject = $this->question;
			
		}
		
        
		switch($this->daysRemaining){
			
			case 1:
			$this->timeRemainingText = 'Just ONE day remaining';
			break;
			
			case 0:
			$this->timeRemainingText = 'Voting closes TODAY';
			break;
			
			default:
			//die ('asdasd');
			$this->timeRemainingText = 'Vote closes in ' . $this->daysRemaining . ' days';
			
		}
		
	
	
        require_once('Markdown/markdown.php');
        
        $this->backgroundHTML = markdown($this->background);
		$this->bannerTextHTML = markdown($this->bannerText);
		
		
	
		$this->getStatus();
		
		$this->getWeeks();
		
	
    }
    
    /**
     * Oveerides trhe default if necessary
     *
     *
     */
    function getStatus(){
	
	if(!$this->isPublished){
	    return;
	}
	
	$startDateUNIX = strtotime($this->startDate);
	$endDateUNIX = 	strtotime($this->endDate);
	
	if($this->daysRemaining >= 0){
	    
	    $this->status = VOTE_STATUS_PENDING;
	    
	    if( time() > $startDateUNIX ){
		
		 $this->status = VOTE_STATUS_ACTIVE;
	    }
	    
	    
	} else {
	    
	    $this->status = VOTE_STATUS_COMPLETE;
	    
	}
	
	return;
	
	
	
    }
    
    function getWeeks(){
	
	
	$startDateUNIX = strtotime($this->startDate);
	$endDateUNIX = 	strtotime($this->endDate);
	
	$duration = $endDateUNIX - $startDateUNIX;
	//echo $duration;
	$this->weeks = floor($duration /(60*60*24*7));
	
	//echo '<hr>' . $this->weeks . '<hr>';
	
	
    }
    
    
    
    /**
     *
     *
     *
     * @return array of objects or false
     *
     */
   
    public static function getAll($dbPDO, $active=true)
    {
        
        $sql = "SELECT
		    *
		    , DATEDIFF(end_date, CURDATE()) AS days_remaining
                FROM
                    vote
                WHERE
                    1=1
		    
		    ";
		    
	if ($active){		    
		    
             $sql .= "
		    AND is_published = '1'
		    AND CURDATE() BETWEEN start_date AND end_date
		    ";
	}
                    
	$sql .= " ORDER BY start_date DESC ";    
	    
	    
        //echo $sql;
	//exit;
        
        $stmnt = $dbPDO->prepare($sql);
        
        $stmnt->execute();
        
	
	
        $voteArray = array();
        
        while($row = $stmnt->fetch()){
	    
	    
            
            $vote = new self($dbPDO);
            $vote->populate($row);
            
            $voteArray[] = $vote;
            
        }
        
	if(!empty($voteArray)){
	 
	    return $voteArray;
	
	}
	
	return false;
    
    }
    
    
    
    /**
     *
     *
     *
     * @return object or false
     *
     */
    
    public static function getById($dbPDO, $id, $isActive=true)
    {
        
        
        $sql = "SELECT
		    *
		    , DATEDIFF(end_date, CURDATE()) AS days_remaining
                FROM
                    vote
                WHERE
		    1=1    
                    AND id = $id";
		    
	if($isActive){
	    
	    $sql .= "
			AND is_published = '1'
			AND CURDATE() BETWEEN start_date AND end_date
			
		    ";
                    
	}
        //echo $sql;
        
        $stmnt = $dbPDO->prepare($sql);
        
        $stmnt->execute();
        
        $result = $stmnt->fetch();
        
        if(!empty($result)){
	    $vote = new self($dbPDO);
	    $vote->populate($result);
	    
	    return $vote;
	}
        return false;
    }
    
    
    public function checkEligibility($user){
	
        require_once('Vote/Model/Vote.php');
        return Model_Vote::checkEligibility($this->dbPDO, $this->id, $user );
        
    }
    
    
     
    public function incrementCount(){
	
        require_once('Vote/Model/Vote.php');
        Model_Vote::incrementCount($this->dbPDO, $this->id, $_POST['response'] );
      
    }
    
    
    public function recordVote($user){
	  
        // var_dump($user); 
          
	require_once('Vote/Model/Vote.php');
        Model_Vote::recordVote($this->dbPDO, $this->id, $user );  
          
    }
    
    
    public function updateFromPOST(){
        
        
        if($this->validate() ){
            
            require_once('Vote/Model/Vote.php');
            Model_Vote::updateFromPOST($this->dbPDO, $_GET['id']);
            return true;
            
        } else {
            
            return $this->errorArray;
            
        }
            
	
    }
    
    
    
    
    private function validate(){
	
	if(!$_POST['question']){
	    $this->errorArray[] = 'Question missing';
	}
	
	if(!$_POST['start_date']){
	    $this->errorArray[] = 'Start date missing';
	}
	
	if(!$_POST['weeks']){
	    $this->errorArray[] = 'Duration missing';
	}
	
	if(!$_POST['background']){
	    $this->errorArray[] = 'Background missing';
	}
	
	if(!$_POST['banner_text']){
	    $this->errorArray[] = 'Banner text missing';
	}
	
	
	
	
	//Start Date
	if($_POST['start_date']){
	    
	    $startDateUNIX = strtotime($_POST['start_date']);
	    
	    if($startDateUNIX < mktime(0,0,0,date('m'), date('d')+1, date('y') ) ){
		
		$this->errorArray[] = 'Start date must be in the future';
		return false;
		
	    }
	    
	    
	    
	    
	    //Start day
	    
	    $dayNumber = date('N', $startDateUNIX);
	    
	    if(! in_array($dayNumber, array(2,3,4) ) ){
		
		$this->errorArray[] = 'Start date must be a Tuesday, Wednesday or Thursday';
		return false;
		
	    }
	    
	    
	    
	    
	}
	
	
	if(!empty($this->errorArray) ){
	    
	    return false;
	}
	
	return true;
	
    }
    
    

    /**
     * Remove "who voted" data AFTER the vote has run
     *
     *
     */
	
	
    public static function cleanUserData($dbPDO){
	
	$sql = "DELETE
		    vote_member_xref.*
		    
		FROM
		    
		    vote_member_xref
		    
		    LEFT JOIN  vote
			ON (vote.id = vote_member_xref.vote_id )
		    
		WHERE
		    NOW() > end_date ";
		
	//echo $sql;
	    
	$stmnt = $dbPDO->prepare($sql);
	    
        $stmnt->execute();
	
    }


}