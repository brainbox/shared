<?php

/**
 *
 * Designed to be called daily
 * Needs to decide what - if anything - needs to be sent
 * 
 *
 *
 */


require_once('VoteAlerts.php');



Class VoteAlertsASLTIP extends VoteAlerts{
        
	
    function __construct($dbPDO){
        
	parent::__construct($dbPDO);
	
	$this->dbPDO = $dbPDO;	
	
    }
        
      
    /**
     * Overriding so that we get the right people
     *
     *
     */    
        
    function getVoters(){
        
        require_once 'Vote/Model/Vote.php';
        $this->voterArray = Model_Vote::getVotersASLTIP($this->dbPDO, $this->vote->id);
        
    }
    
    
}
