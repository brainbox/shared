<?php
error_reporting (E_ALL);
/**
* @package SPLIB
* @version $Id: Auth.php,v 1.7 2003/12/09 06:06:13 kevin Exp $
*/
/**
* Constants to modify behaviour of Auth Class
*/
# Modify these constants to match the $_POST variable used in login form
// Name to use for login variable e.g. $_POST['login']
@define ( 'USER_LOGIN_VAR','login');
// Name to use for password variable e.g. $_POST['password']
@define ( 'USER_PASSW_VAR','password');

# Modify these constants to match your user login table
// Name of users table
@define ( 'USER_TABLE','user');
// Name of login column in table
@define ( 'USER_TABLE_LOGIN','login');
// Name of password column in table
@define ( 'USER_TABLE_PASSW','password');
// Name of status column in table
@define ( 'USER_TABLE_STATUS','active');
/**
* Authentication class<br />
* Automatically authenticates users on construction<br />
* <b>Note:</b> requires the Session/Session class be available
* @access public
* @package SPLIB
*/
class Auth {
    /**
    * Instance of database connection class
    * @access private
    * @var object
    */
    var $dbPDO;
    /**
    * Instance of Session class
    * @access private
    * @var Session
    */
    var $session;
    /**
    * Url to re-direct to in not authenticated
    * @access private
    * @var string
    */
    var $redirect;
    /**
    * String to use when making hash of username and password
    * @access private
    * @var string
    */
    var $hashKey;
    /**
    * Are passwords being encrypted
    * @access private
    * @var boolean
    */
    var $md5;
    /**
    * Auth constructor
    * Checks for valid user automatically
    * @param object database connection
    * @param string URL to redirect to on failed login
    * @param string key to use when making hash of username and password
    * @param boolean if passwords are md5 encrypted in database (optional)
    * @access public
		
		
		
		
		
		
		The "remember" parameter means that cookies are "possible" - not that "remember me" has nbeen selected
		
    */
    function __construct ( $dbPDO, $redirect, $hashKey, $md5=true, $remember=false, $protect=true) {

   
        $this->dbPDO = $dbPDO;
        $this->redirect=$redirect;
        $this->hashKey=$hashKey;
        $this->md5=$md5;
				
	$this->session= new Session();
	$this->remember=$remember;
	

	if($remember){
	    //Doesn't mean we'll do anything with it.
	    $this->cookie= new Cookie();
	}
	
	
	
	if($protect){
	    $this->login();
	}
    }
    
    
    
    /**
    * Checks username and password against database
    * @return void
    * @access private
    */
		
		
    function redirect($from=true) {
        if ( $from ) {
				
			//++++++++++++++++++++++++++++++
			//WHT ABOUT SETTING A COOKIE HERE... JUST BEFORE THE REDIRECT????
			
			setcookie('cookietest');
			
			//>>>>>>>>>>>>>>>>>>
	   
						
			$headerstring = 'Location: http://'. $_SERVER['HTTP_HOST']  . $this->redirect.'?from='. $_SERVER['REQUEST_URI']. '&login='. @$this->login . '&status=' .@$this->status ;
					
			//die($headerstring);
					
			header ( $headerstring  );
					 
        } else {
				    //think this only applies to logout?
            header ( 'Location:  http://'. $_SERVER['HTTP_HOST']  . $this->redirect);
        }
        exit();
    }
		
		
		
		
		
    function login() {
        
        
        #echo 'jerere';
        // See if we have values already stored in the session
	//Note that things like ADMIN changing things - like email address or "isActive" - can't be expected to work for the session
        
	//print_r($_SESSION);
	
	//var_dump($_SESSION);
	//exit;
	//print_r($this->session);
	$this->session->get('login_hash');
	
	//exit;
	
	//echo 'login hash ' . $this->session->get('login_hash');
	//echo '<p>' . $_SESSION['login_hash'];
	//exit;
	
	if ( $this->session->get('login_hash') ) {
				    
		   // echo '<hr>The session has is ' . $this->session->get('login_hash');
		   // exit;
            $this->confirmAuth();
            return;
						exit();
        }
				
				
 				//If not, check for a valid cookie
				if($this->remember){
				      
						if ( $this->cookie->get('login_hash') ) {
            	  $this->confirmAuthCookie();
							 
							  //echo '<hr>FOUND A COOKIE'; 
							 
						    return;
						    exit();
            }
				
				}
				      
        //exit;
				
				
				
				
        // If this is a fresh login, check $_POST variables
				
        if ( !isset($_POST[USER_LOGIN_VAR]) ||
                !isset($_POST[USER_PASSW_VAR]) ) {
								
								//NOT LOGGED IN
								
            $this->redirect();
						exit();
        }

        if ( $this->md5 )
            $password=md5($_POST[USER_PASSW_VAR]);
        else
            $password=$_POST[USER_PASSW_VAR];

        // Escape the variables for the query
        $this->login=mysql_escape_string($_POST[USER_LOGIN_VAR]);
				
					
				
        $password=mysql_escape_string($password);

	//First check the username
	$sql=   'SELECT COUNT(*) AS num_users FROM '.USER_TABLE.' WHERE '.USER_TABLE_LOGIN.'="'.$this->login.'";';

	    #echo $sql;
	   //exit;
	
       # print_r($this->dbPDO);
        $stmnt = $this->dbPDO->prepare($sql);    
	 
        $stmnt->execute();
        $row=$stmnt->fetch();
				
				//echo "<p>and the number of results is" . $row['num_users'];	
        //USERNAME NOT FOUND
				if ( $row['num_users']!=1 ){			
                
							 //username does not exist	
					     $this->status='username';
               $this->redirect();
						   exit();
				}		
				
				//TEST THE ACCOUNT STATUS - NO POINT BEING PICCY ABOUT THE PASSWORD IF THE ACCOUNT HAS EXPIRED!
			  $sql='SELECT COUNT(*) AS num_users
                  FROM '.USER_TABLE.'
                  WHERE '.USER_TABLE_LOGIN.'="'.$this->login.'"
                  AND '.USER_TABLE_STATUS.'="1"';
					
			//echo "<p>" .$sql;
			//exit;
        $stmnt = $this->dbPDO->prepare($sql);    
	 
        $stmnt->execute();
        $row=$stmnt->fetch();

	if ( $row['num_users']!=1 ){			
	
	    $this->status='status';		
	    $this->redirect();
	    exit();
	}
	
	
	//TEST THE PASSWORD
	$sql='SELECT COUNT(*) AS num_users
	    FROM '.USER_TABLE.'
	    WHERE '.USER_TABLE_LOGIN.'="'.$this->login.'"
	    AND '.USER_TABLE_PASSW.'="'.$password.'"
	    AND '.USER_TABLE_STATUS.'="1"';

			  //echo "<p>" .$sql;
        $stmnt = $this->dbPDO->prepare($sql);    
	 
        $stmnt->execute();
        $row=$stmnt->fetch();

	//Let 'em in!
	if ( $row['num_users']==1 ){			
	
	    $this->storeAuth($this->login,$password);	
	
	}	 
	else{	 
	
	    $this->status='password';		
	    $this->redirect();
	    exit();
	}
			
    }
		
    /**
    * Sets the session variables after a successful login
    * @return void
    * @access protected
    */
    function storeAuth($login,$password) {
	
	
        $this->session->set(USER_LOGIN_VAR,$login);
        $this->session->set(USER_PASSW_VAR,$password);
	    
	//$_SESSION[USER_LOGIN_VAR]=$login;
	//$_SESSION[USER_PASSW_VAR]=$password;
	
	
	
				
        // Create a session variable to use to confirm sessions
        $hashKey = md5($this->hashKey.$login.$password);
	//echo $hashKey;
        $this->session->set('login_hash',$hashKey);
	
	//exit;
	
	//$_SESSION['login_hash']=$hashKey;
	//exit;
	
	//echo $hashKey;
	
	
	//Cookie thing is interesting....
	//Want to kill the cookies if someone logs in
	//... BUT
	// This is also called by the CHANGING password class
	//
	//Lets assume that someone who is remembered doesn't log in.
	
	
	//echo 'Remembering.... ';
		    
	if($this->remember && isset($_POST['remember_me']) ){		 
	$this->cookie->set(USER_LOGIN_VAR,$login);
	$this->cookie->set(USER_PASSW_VAR,$password);		 				     
	$this->cookie->set('login_hash',$hashKey);				
	}
				
	//echo '$session is';    		
	//print_r($_SESSION);
	//exit;
				
    }
    /**
    * Confirms that an existing login is still valid
    * @return void
    * @access private
    */
    function confirmAuth() {
        $login=$this->session->get(USER_LOGIN_VAR);
        $password=$this->session->get(USER_PASSW_VAR);
        $hashKey=$this->session->get('login_hash');
	
	
	
        if (md5($this->hashKey.$login.$password) != $hashKey ) {
	    
	    //echo 'oooops';
	    //exit;
	    
            $this->logout(true);
        }
				
    }
		
		/**
		* Very like the above - this time for Cookies
		*
		*/
		
		
		function confirmAuthCookie() {
		
				//Need to check the cookie AND that the user is still active!!!!		
				//AND THEN GIVE CONTROL TO THE SESSION 
		   
		
        $login=$this->cookie->get(USER_LOGIN_VAR);
        $password=$this->cookie->get(USER_PASSW_VAR);
        $hashKey=$this->cookie->get('login_hash');
				
				//echo '<hr>Cookie details ';
				//echo  $login;
        //echo $password;
        //echo $hashKey;
				
				
				
        if (md5($this->hashKey.$login.$password) != $hashKey ) {
				    
						//echo '<hr>No cookie action';
            $this->logout(true);
						return;
        }
				else{
      				    //echo '<hr>We should let this one in!!!';
      						//Before doing so, we need to know that they are still
      						//active on the system
						
						    
						 			$sql='SELECT COUNT(*) AS num_users
                  FROM '.USER_TABLE.'
                  WHERE '.USER_TABLE_LOGIN.'="'.$login.'"
                  AND '.USER_TABLE_STATUS.'="1"';
					
          			  //echo "<p>" .$sql;
									//exit;
                 $stmnt = $this->dbPDO->prepare($sql);    
	 
        $result=$stmnt->execute();
        $row=$result->fetch();
          
          				if ( $row['num_users']!=1 ){			
                        					
          			      //$this->status='status';		
                      $this->logout(true);
          						exit();
            			}
          				else{		
											
          						
          						$this->storeAuth($login,$password);
          						return;
          				
          				}
          				
					}				
    }
		
		
    /**
    * Logs the user out
    * @param boolean Parameter to pass on to Auth::redirect() (optional)
    * @return void
    * @access public
    */
    function logout ($from=false,$redirect=true) {
        
	
	$this->session->del(USER_LOGIN_VAR);
        $this->session->del(USER_PASSW_VAR);
        $this->session->del('login_hash');
				
				
				
	if($this->remember){ 
	
	    $this->cookie->del(USER_LOGIN_VAR);
	    $this->cookie->del(USER_PASSW_VAR);		 				     
	    $this->cookie->del('login_hash');				
	
	}
	//exit;
	
	if($redirect){
	    $this->redirect($from);
	}
    }
    /**
    * Redirects browser and terminates script execution
    * @param boolean adverstise URL where this user came from (optional)
    * @return void
    * @access private
    */
		
		
		
    function addError($message){
		$this->errorArray[]=$theError; 
	    
    }
	    
	    
    function getErrors(){
		return $this->errorArray; 
    }
		
		
		
}