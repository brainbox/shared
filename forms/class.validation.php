<?php
/*
All source code copyright and proprietary Melonfire, 2002. All content, brand names and trademarks copyright and proprietary Melonfire, 2002. All rights reserved. Copyright infringement is a violation of law.
This source code is provided with NO WARRANTY WHATSOEVER. It is produced and meant for illustrative purposes only, and is NOT recommended for use in production environments. 
Read more articles like this one at http://www.melonfire.com/community/columns/trog/ and http://www.melonfire.com/
*/

// FormValidator.class.inc
// class to perform form validation
class FormValidator
{
	//
	// private variables
	// 
	
	var $_errorList;

	//
	// methods (private)
	// 
	
	// function to get the value of a variable (field)
	//Note also that the _getValue() method can be improved by modifying it to return the value of the form variable from the special $HTTP_POST_VARS array, rather than from the global scope. 
	
	function _getValue($field)
	{
		//global ${$field};
		//return ${$field};  //These were in the original 
			
		return $_POST[$field]; //Gary's modification!  does this work? YES!
		
	}

	//
	// methods (public)
	// 

	// constructor
	// reset error list
	function FormValidator()
	{
     //print '<p>made it to the constructor';	 
		$this->resetErrorList();
	}
	
	// check whether input is empty
	function isEmpty($field, $msg)
	{
		$value = $this->_getValue($field);
		if (trim($value) == "")
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is a string
	function isString($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_string($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is a number
	function isNumber($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_numeric($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is an integer
	function isInteger($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_integer($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is a float
	function isFloat($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_float($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is alphabetic
	function isAlpha($field, $msg)
	{
		$value = $this->_getValue($field);
		$pattern = "/^[a-zA-Z]+$/";
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}

	// check whether input is within a valid numeric range
	function isWithinRange($field, $msg, $min, $max)
	{
		$value = $this->_getValue($field);
		if(!is_numeric($value) || $value < $min || $value > $max)
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is a valid email address
	function isEmailAddress($field, $msg)
	{
		$value = $this->_getValue($field);
		$pattern = "/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/";
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}
	
	
	
	
	// GDS - check whether FILE has an allowable extensionw
	function isOKextension($field, $msg, $extensions){
	
	   //echo "the field is :" . $field;
		 //echo "the array is : <pre>";
		 //var_dump($extensions);
		 
		 
		  if(in_array($field, $extensions)){
			 			return true;
			}
			else{
					 $this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
					 return false;
			
			} 										 
			 
			
	}
	
	//Check the filesize of an attachment
	function isOKfilesize($filesize, $maxsize, $msg){	 
		 
		  if($filesize<=$maxsize){
			 			return true;
			}
			else{
					 $this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
					 return false;
			
			} 										 
			 
			
	}
	
	
	
	
	
	
	// return the current list of errors
	function getErrorList()
	{
		return $this->_errorList;
	}
	
	// check whether any errors have occurred in validation
	// returns Boolean
	function isError()
	{
		if (sizeof($this->_errorList) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// reset the error list
	function resetErrorList()
	{
		$this->_errorList = array();
	}
	
// end 
}

?>