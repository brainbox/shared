<?php


Class AwardEntry extends ContactForm{

   function __construct( $db, $emailDetails){
	            
	       //Kick the parent class into action
	       parent::ContactForm($db, $emailDetails);
						
	 }						
	 
	 function setForm(){           					 
          
												  
              $this->form->removeAttribute('name'); //for XHTML compliance
              $this->form->addElement('text','name','Your name',array('class'=>'text'));
              //$this->form->addElement('text','company','Organisation',array('class'=>'text'));
              $this->form->addElement('text','email','Email address',array('class'=>'text'));
              $this->form->addElement('text','phone','Contact phone number',array('class'=>'text'));
              
	//						$attrs = array("rows"=>"10", "cols"=>"50"); 
         //     $this->form->addElement('textarea','description','Message',$attrs);
							
	       //The file upload:
	       $this->form->addElement('file','file','Select a Word file',array('class'=>'text'));

							
              $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
              
              //RULES
              $this->form->addRule('name',      'Please enter your name',               'required','' ,'client');
              $this->form->addRule('email',     'Please enter your email address',      'required','' ,'client');
              $this->form->addRule('email',     'Please enter a valid email address',   'email','' ,'client');  
             
	      $this->form->addRule('file', 'You must select a file', 'required', null, 'client');

	      $this->form->addRule('file', 		'The file type must be Word (.doc or .docx)', 'mimetype', array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document'));
	      $this->form->addRule('file', 		'The maximum file size is 1 MB', 'maxfilesize', 1048576);
				
				
							
							//low key spam protection: no puctuation (to prevent email addresses in evey field
	      $this->form->registerRule('no_at','regex','/^[^@]+$/');
	      $this->form->addRule('name',       'The "@" character is not allowed in this field.',      'no_at'   );
              $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_at'   );
              $this->form->addRule('description','The "@" character is not allowed in this field.',      'no_at'   );
	      
	      $this->form->registerRule('noBBCode','function','noBBCode');
	      $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_'   );
              $this->form->addRule('description','The "@" character is not allowed in this field.',      'no_at'   );
							
							
	      //SPAM PREVENTION
	      $this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
              $this->form->addRule('name',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('company',    'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
	      $this->form->addRule('phone',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('description','The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
							
           //    $this->form->registerRule('noBBCode','function','nCBBCode');
	   //   $this->form->addRule('company',    'BBCode not allowed in this field.',      'noBBCode'   );
          //    $this->form->addRule('description','BBCode not allowed in this field.',      'noBBCode'   );
     }
		
      				
			
}  





Class Contribute extends ContactForm{

   function Contribute(& $db, $emailDetails){
	            
							//Kick the parent class into action
							parent::ContactForm($db, $emailDetails);
						
	 }						
	 
	 function setForm(){           					 
          
												  
              $this->form->removeAttribute('name'); //for XHTML compliance
              $this->form->addElement('text','name','Your name',array('class'=>'text'));
              $this->form->addElement('text','company','Organisation',array('class'=>'text'));
              $this->form->addElement('text','email','Email address',array('class'=>'text'));
              $this->form->addElement('text','phone','Contact phone number',array('class'=>'text'));
              
							$attrs = array("rows"=>"10", "cols"=>"50"); 
              $this->form->addElement('textarea','description','Message',$attrs);
							
							//The file upload:
							$this->form->addElement('file','file','Select a file',array('class'=>'text'));

							
              $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
              
              //RULES
              $this->form->addRule('name',      'Please enter your name',               'required','' ,'client');
              $this->form->addRule('email',     'Please enter your email address',      'required','' ,'client');
              $this->form->addRule('email',     'Please enter a valid email address',   'email','' ,'client');  
              $this->form->addRule('description','Please enter a short message',    'required','' ,'client');
							
	      $this->form->addRule('file', 		'The file type must be Word (.doc or .docx)', 'mimetype', array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document'));
	      $this->form->addRule('file', 		'The maximum file size is 1 MB', 'maxfilesize', 1048576);
				
				
							
							//low key spam protection: no puctuation (to prevent email addresses in evey field
	      $this->form->registerRule('no_at','regex','/^[^@]+$/');
	      $this->form->addRule('name',       'The "@" character is not allowed in this field.',      'no_at'   );
              $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_at'   );
              $this->form->addRule('description','The "@" character is not allowed in this field.',      'no_at'   );
	      
	      $this->form->registerRule('noBBCode','function','noBBCode');
	      $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_'   );
              $this->form->addRule('description','The "@" character is not allowed in this field.',      'no_at'   );
							
							
	      //SPAM PREVENTION
	      $this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
              $this->form->addRule('name',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('company',    'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
	      $this->form->addRule('phone',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('description','The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
							
           //    $this->form->registerRule('noBBCode','function','nCBBCode');
	   //   $this->form->addRule('company',    'BBCode not allowed in this field.',      'noBBCode'   );
          //    $this->form->addRule('description','BBCode not allowed in this field.',      'noBBCode'   );
     }
		
      				
			
 }     
 
 
Class AdvertiseEventForm extends ContactForm{

   function AdvertiseEventForm(& $db, $emailDetails){
	            
							//Kick the parent class into action
							parent::ContactForm($db, $emailDetails);
						
	 }						
	 
	 function setForm(){           					 
          
												  
              $this->form->removeAttribute('name'); //for XHTML compliance
							

							$this->form->addElement('HTML','<tr><td colspan="2">Your contact details (for administrative purposes: will not be displayed on the website):</td></tr>');

              $this->form->addElement('text','name',      'Your name', array('class'=>'text'));
              $this->form->addElement('text','email', 'Your email address', array('class'=>'text'));
              $this->form->addElement('text','phone', 'Your phone number', array('class'=>'text'));
              
              $this->form->addElement('HTML','<tr><td colspan="2">Details of the event (for display on the website):</td></tr>');
              
              $this->form->addElement('text','title','Event title', array('class'=>'text'));
              $this->form->addElement('text','date',  'Event start date', array('class'=>'text'));
              $this->form->addElement('text','enddate',  'Event end date', array('class'=>'text'));
              $this->form->addElement('text','venue','Event venue', array('class'=>'text'));
              
              $attrs = array("rows"=>"40", "cols"=>"50"); 
              $this->form->addElement('textarea','description','Event description',$attrs);
              
              
              
              $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
							

              //RULES
              $this->form->addRule('name',      'Please enter your name',               'required','' ,'client');
              $this->form->addRule('email',     'Please enter your email address',      'required','' ,'client');
              $this->form->addRule('email',     'Please enter a valid email address',   'email','' ,'client');
              $this->form->addRule('phone',     'Please enter your phone number',      'required','' ,'client');
              
              $this->form->addRule('title',     'Please enter the title of the event',  'required','' ,'client');
              $this->form->addRule('date',      'Please enter the date of the event',   'required','' ,'client');
              $this->form->addRule('venue',     'Please enter the event venue',        'required','' ,'client');
              $this->form->addRule('description','Please enter a description for the event',    'required','' ,'client');
							
							
	      $this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
              $this->form->addRule('name',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
              $this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
							$this->form->addRule('phone',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
              $this->form->addRule('title',			 'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
							$this->form->addRule('date',    	 'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
							$this->form->addRule('enddate',    	 'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
              $this->form->addRule('venue',			 'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
							$this->form->addRule('description','The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
              
							
	      $this->form->registerRule('noBBCode','function','noBBCode');
              $this->form->addRule('name',       'BBCode not allowed in this field.',      'noBBCode');
              $this->form->addRule('email',      'BBCode not allowed in this field.',      'noBBCode');
		$this->form->addRule('phone',     'BBCode not allowed in this field.',      'noBBCode');
              $this->form->addRule('title',		'BBCode not allowed in this field.',      'noBBCode');
		$this->form->addRule('date',    	'BBCode not allowed in this field.',      'noBBCode');
		$this->form->addRule('enddate',    	 'BBCode not allowed in this field.',      'noBBCode');
              $this->form->addRule('venue',			 'BBCode not allowed in this field.',      'noBBCode');
		$this->form->addRule('description','BBCode not allowed in this field.',      'noBBCode');
              
							
     }
				
}     
?>

