<?php


Class ContactFormASLTIP extends ContactForm{

   function __construct(& $db, $emailDetails){
	            
		$this->db=& $db;
		$this->emailDetails=$emailDetails;
		//call the parent constructor
		parent::ContactForm($this->db, $this->emailDetails);	

	 }						
   
	 
	function setForm(){           					 
		
		$this->form->removeAttribute('name'); //for XHTML compliance
		
		$this->form->addElement('text','honey','',array('id'=>'honey'));
		
		$this->form->addElement('text','name','Name',array('class'=>'text'));
		$this->form->addElement('text','email','Email',array('class'=>'text'));
		$this->form->addElement('text','phone','Telephone',array('class'=>'text'));
		
		$attrs = array("rows"=>"10", "cols"=>"50"); 
		$this->form->addElement('textarea','description','Message',$attrs);
		
		$this->form->addElement('submit','submit','Submit', array('class'=>'button'));
		
		
		//Spam-killer rules - work on the SERVER side only
		
		//RULES
		$this->form->addRule('honey',      'Must be left empty',               	'maxlength',	'0',	'client');
	
		$this->form->addRule('name',      'Please enter your name',               'required','' ,'client');
		$this->form->addRule('email',     'Please enter your email address',      'required','' ,'client');
		$this->form->addRule('email',     'Please enter a valid email address',   'email','' ,'client');  
		$this->form->addRule('description','Please enter a short message',    'required','' ,'client');
		
		
		
		//SPAM STUFF
		$this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
		$this->form->addRule('name',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
		$this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
		$this->form->addRule('description','The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
            
    }
}
