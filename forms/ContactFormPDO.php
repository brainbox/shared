<?php

/** Updating the old ContactForm class...
 *   - to use PDO
 *   - to make it easier to work out what parameters are being set
 *   - to remove the default form - it's the job of the entending class to set this
 */





Class ContactFormPDO{
   
   public  $templateHTML = 'forms/emailtemplate.php';
   public  $templateText = 'forms/emailtemplatetext.php';
   
   public $fromAddress;  // 
   
   
   protected $_templateHtml;
   protected $_templateText;
         
   
   

   #function ContactForm($dbPDO, $emailDetails, $templateHTML='forms/emailtemplate.php', $templateText='forms/emailtemplatetext.php'){
   function __construct($dbPDO, $emailDetails){
            
      $this->dbPDO=$dbPDO;
      $this->emailDetails=$emailDetails;
    
      
      //echo '<pre>';
      //print_r($emailDetails);
      
      require_once ("HTML/QuickForm.php");
      
      $this->form = new HTML_QuickForm($emailDetails['shortName'], 'post', $_SERVER['REQUEST_URI'],null,null, true);
                                              
      $this->setForm();
   }						
   
	 function validate(){
	     
		//OOOPS thing I've over-ridden a key QUICKFORM funcuib!"!!! 
	     if($this->form->validate()){
			     $this->process();
			     return true;
			 }
			 else{
			     return false;
			 }
			 
	 }
	 
	 //Hope this one doesn't over-write
	 function isValid(){
	     
		//OOOPS thing I've over-ridden a key QUICKFORM funcuib!"!!! 
	     if($this->form->validate()){
			     $this->process();
			     return true;
			 }
			 else{
			     return false;
			 }
			 
	 }
	 
	 
	 function addElement($type,$name,$value){
	 
	 					$this->form->addElement($type,$name,$value);
	 }
	 
	 
	 
/**
 *       //This will overriden by the extending clas
 *
 *
 *
 */	 
	 
	 
   function setForm(){           					 
      
	  
	  $this->form->removeAttribute('name'); //for XHTML compliance
		
		$this->form->addElement('text','honey','',array('id'=>'honey'));
		
		$this->form->addElement('text','name','Name',array('class'=>'text'));
		$this->form->addElement('text','email','Email',array('class'=>'text'));
		$this->form->addElement('text','phone','Telephone',array('class'=>'text'));
		
		$attrs = array("rows"=>"10", "cols"=>"50"); 
		$this->form->addElement('textarea','description','Message',$attrs);
		
		$this->form->addElement('submit','submit','Submit', array('class'=>'button'));
		
		
		//Spam-killer rules - work on the SERVER side only
		
		//RULES
		$this->form->addRule('honey',      'Must be left empty',               	'maxlength',	'0',	'client');
	
		$this->form->addRule('name',      'Please enter your name',               'required','' ,'client');
		$this->form->addRule('email',     'Please enter your email address',      'required','' ,'client');
		$this->form->addRule('email',     'Please enter a valid email address',   'email','' ,'client');  
		$this->form->addRule('description','Please enter a short message',    'required','' ,'client');
		
		
		
		//SPAM STUFF
		$this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
		$this->form->addRule('name',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
		$this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
		$this->form->addRule('description','The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
            
	  
	  
              
   }
     
     function setDefaults($defaultsArray){
	     
	      $this->form->setDefaults($defaultsArray);
     }
     
     
     public function setTemplateHtml($template)
     {
         $this->_templateHtml = $template;
         
     }
     
     public function setTemplateText($template)
     {
         $this->_templateText = $template;
         
     }
     
     
     function process(){
		 
        $this->form->applyFilter('__ALL__','escapeValue');							

        require_once 'forms/parseform.php';
        $Parse = new parseFormClass();
        $Parse->setDelimiter('|');

        $submittedEmail = $this->form->getSubmitValue('email');
        $submittedName = $this->form->getSubmitValue('name');


        //ADDED FOR PHP5
        //Convert form values to XML for database storage.
        $dom=new DOMDocument;

        //echo '--' . $this->emailDetails['shortName'];
        $root=$dom->createElement($this->emailDetails['shortName']);
        $dom->appendChild($root);

        foreach($this->form->getSubmitValues() as $name=>$value){

            if (strtolower($name) == 'submit' ||  strtolower($name) == 'max_file_size'){
                continue;
            }
            //for the emails
            $Parse->addField($name,$value); 

            //for the database 

            $element=$dom->createElement($name);
            $root->appendChild($element);

            $elementValue=$dom->createTextNode(utf8_encode ($value) );
            $element->appendChild($elementValue);

        }

        $xml= $dom->saveXML();          		

      
        $sql = "

            INSERT INTO formdata (form_name, xml)
            VALUES (:shortName,:xml);

        ";

  
        $stmnt = $this->dbPDO->prepare($sql);

        $stmnt->bindParam(':shortName', $this->emailDetails['shortName']);


        $stmnt->bindParam(':xml', $xml);

        $stmnt->execute();



        $Parse->addField("Form submit time", date('l jS F Y h:i:s A'));

        $this->dataString =   $Parse->getStringDelimited();
        $this->plainString =  $Parse->getStringPlain(': ');
        $this->HTMLstring =   $Parse->getStringHTML('');


        $this->applyTemplates();



        require_once 'phpmailer/class.phpmailer.php';

        $mail=new phpmailer();

        foreach($this->emailDetails['to'] as $name=>$value){
            $mail->addAddress(     $value );
        }			

        if(isset($this->emailDetails['cc'])){
            foreach($this->emailDetails['cc'] as $name=>$value){
                $mail->addCC(     $value );
            }	
        }
        if(isset($this->emailDetails['bcc'])){
            foreach( $this->emailDetails['bcc'] as $name=>$value){
                $mail->addBCC(     $value );
            }						
        }

 
    

        $mail->addBCC( BRAINBOX_EMAIL_ADDRESS );


        if($this->fromAddress){

            $mail->From     = 	$this->fromAddress;
            $mail->FromName =     	$this->fromAddress;

        } else {

            $mail->From     =  	$submittedEmail;
            $mail->FromName =      $submittedEmail;
            $mail->AddReplyTo =    $submittedEmail;

        }

      

        $mail->WordWrap =     	70;                              // set word wrap
        //$mail->AddAttachment($f_tmp_name,$f_name);      // attachment


        $mail->IsHTML(true);                               // send as HTML
        $mail->Subject  =  		  $this->emailDetails['subject'];


        if($this->_templateHtml){
            $body =  $this->HTMLstring; 
            ob_start();
                require($this->_templateHtml);
            $this->HTMLstring = ob_get_clean();
        }

        if($this->_templateText){
            $body =  $this->plainString; 
            ob_start();
                require($this->_templateText);
            $this->plainString = ob_get_clean();
        }



        $mail->Body     =  			$this->HTMLstring;
        $mail->AltBody  =  			$this->plainString;



        if(isset($this->form->_submitFiles['file'])){
         
            $filename=(    $this->form->_submitFiles['file']['name']);
            $tmp_filename=($this->form->_submitFiles['file']['tmp_name']);

            $mail->AddAttachment(   $tmp_filename ,$filename);      // attachment

        }
      
      
      
      
      
        if($mail->Send()){
            return true;
        }
        else{

            $errorMessage="Mailer Error: " . $mail->ErrorInfo;
            return false;
        }
      
   }
      

   function getForm(){	 
      
      return $this->form->toHTML();
     
   }        
			
   function escapeValue($value) {
          return mysql_real_escape_string($value);
   }		
               
   function applyTemplates(){
               
      $title=$this->emailDetails['subject'];
      
      
      $content=$this->HTMLstring;
      
      ob_start();
      include ($this->templateHTML);
      $this->HTMLstring=ob_get_contents();
      ob_end_clean();			 
      
      
      $content=$this->plainString;
      
      ob_start();
      include ($this->templateText);
      $this->plainString=ob_get_contents();
      ob_end_clean() ;			 


   }
               
}     
 
 
 
 
//Functions used by QuickForm
function noEmailHeaders($elementName,$element_value) { 
    $excludeString='Content-type';		
      if(strpos(strtolower($element_value), strtolower($excludeString) ) !==false){
                      return false;
      }
      else{
                              return true;
      }
}				
function noAtCharacter($elementName,$element_value) { 
   $excludeString='@';
   if(strpos($element_value, $excludeString)!==false){
                                   return false;
   }
   else{ 
                   return true;
   }
}
function noBBCode($elementName,$element_value) { 
   $excludeString='[/';		
   if(strpos(strtolower($element_value), strtolower($excludeString) ) !==false){
      return false;
   }
   else{
     return true;
   }
}
