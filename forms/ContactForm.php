<?php

Class ContactForm{

   function ContactForm(& $db, $emailDetails, $templateHTML='forms/emailtemplate.php', $templateText='forms/emailtemplatetext.php'){
	            
		$this->db=& $db;
		$this->emailDetails=$emailDetails;
		$this->templateHTML=$templateHTML;
		$this->templateText=$templateText;
		
		//echo '<hr>';
		//print_r($emailDetails);
		
		require_once ("HTML/QuickForm.php");
		
		$this->form = new HTML_QuickForm($emailDetails['shortName'], 'post', $_SERVER['REQUEST_URI'],null,null, true);
							
              $this->setForm();
	 }						
   
	 function validate(){
	     
		//OOOPS thing I've over-ridden a key QUICKFORM funcuib!"!!! 
	     if($this->form->validate()){
			     $this->process();
			     return true;
			 }
			 else{
			     return false;
			 }
			 
	 }
	 
	 //Hope this one doesn't over-write
	 function isValid(){
	     
		//OOOPS thing I've over-ridden a key QUICKFORM funcuib!"!!! 
	     if($this->form->validate()){
			     $this->process();
			     return true;
			 }
			 else{
			     return false;
			 }
			 
	 }
	 
	 
	 function addElement($type,$name,$value){
	 
	 					$this->form->addElement($type,$name,$value);
	 }
	 
	 function setForm(){           					 
          
												  
              $this->form->removeAttribute('name'); //for XHTML compliance
              $this->form->addElement('text','name','Your name',array('class'=>'text'));
              $this->form->addElement('text','company','Organisation',array('class'=>'text'));
              $this->form->addElement('text','email','Email address',array('class'=>'text'));
              $this->form->addElement('text','phone','Phone number',array('class'=>'text'));
              
							$attrs = array("rows"=>"10", "cols"=>"50"); 
              $this->form->addElement('textarea','description','Message',$attrs);
							
              $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
              
							
              //Spam-killer rules - work on the SERVER side only
           
              //RULES
              $this->form->addRule('name',      'Please enter your name',               'required','' ,'client');
              $this->form->addRule('email',     'Please enter your email address',      'required','' ,'client');
              $this->form->addRule('email',     'Please enter a valid email address',   'email','' ,'client');  
              $this->form->addRule('description','Please enter a short message',    'required','' ,'client');
							
	      //low key spam protection: no puctuation (to prevent email addresses in evey field
		      $this->form->registerRule('no_at','regex','/^[^@]+$/');
		      $this->form->addRule('name',       'The "@" character is not allowed in this field.',      'no_at');
              $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_at');
              $this->form->addRule('description','The "@" character is not allowed in this field.',     'no_at');
							
		//SPAM STUFF
		$this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
              $this->form->addRule('name',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
              $this->form->addRule('company',    'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
              $this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
		$this->form->addRule('phone',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
              $this->form->addRule('description','The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
	      
	      
	         $this->form->registerRule('noBBCode','function','noBBCode');
              $this->form->addRule('name',       'BBCode not allowed in this field.',      'noBBCode');
              $this->form->addRule('email',      'BBCode not allowed in this field.',      'noBBCode');
		$this->form->addRule('phone',     'BBCode not allowed in this field.',      'noBBCode');
              $this->form->addRule('title',		'BBCode not allowed in this field.',      'noBBCode');
		$this->form->addRule('date',    	'BBCode not allowed in this field.',      'noBBCode');
		$this->form->addRule('enddate',    	 'BBCode not allowed in this field.',      'noBBCode');
              $this->form->addRule('venue',			 'BBCode not allowed in this field.',      'noBBCode');
		$this->form->addRule('description','BBCode not allowed in this field.',      'noBBCode');
              
     }
     
     function setDefaults($defaultsArray){
	     
	      $this->form->setDefaults($defaultsArray);
     }
     
     
     function process(){
		 
          $this->form->applyFilter('__ALL__','escapeValue');							
          
          require_once 'forms/parseform.php';
          $Parse = new parseFormClass();
          $Parse->setDelimiter('|');
          
          $submittedEmail = $this->form->getSubmitValue('email');
          $submittedName = $this->form->getSubmitValue('name');
    
	    
			 			 //Convert form values to XML for database storage.
	$dom=new DOMDocument;
	
	//echo '--' . $this->emailDetails['shortName'];
	$root=$dom->createElement($this->emailDetails['shortName']);
	$dom->appendChild($root);
            
     foreach($this->form->getSubmitValues() as $name=>$value){
     
          
          if (	     strtolower($name) == 'submit' ||
		     strtolower($name) == 'max_file_size' ||
		     strtolower($name) == 'honey' 
		  ){
	    continue;
          }
          //for the emails
          $Parse->addField($name,$value); 
          
          //for the database 
          
          $element=$dom->createElement($name);
          $root->appendChild($element);
          
          $elementValue=$dom->createTextNode(utf8_encode ($value) );
          $element->appendChild($elementValue);
               
     }
						 
	$xml= $dom->saveXML();          		
						 
						 
        	        		
     $sql = 'insert into formdata (form_name, xml, the_date) VALUES (';
          $sql .= '"' .$this->emailDetails['shortName'] . '", ';
          $sql .= '"' . $this->escapeValue($xml) . '", ';
          $sql .= time() . ')';
          
          //echo $sql;
          
          $result = $this->db->query($sql);
     
     
                         
     $Parse->addField("Form submit time", date('l jS F Y h:i:s A'));
     
     $this->dataString =   $Parse->getStringDelimited();
     $this->plainString =  $Parse->getStringPlain(': ');
     $this->HTMLstring =   $Parse->getStringHTML('');
     
         
     $this->applyTemplates();
         
     
		
     require_once 'phpmailer/class.phpmailer.php';

     $mail=new phpmailer();
     
     //$mail->Mailer   = "sendmail"; /// IS THIS NEEDED FOR CCing?
     
        
                    
     foreach($this->emailDetails['to'] as $name=>$value){
         $mail->addAddress(     $value );
     
     }			
     
     
     if(isset($this->emailDetails['cc'])){
          foreach($this->emailDetails['cc'] as $name=>$value){
              $mail->addCC(     $value );
          }	
     }
     if(isset($this->emailDetails['bcc'])){
          foreach( $this->emailDetails['bcc'] as $name=>$value){
              $mail->addBCC(     $value );
          }						
     }
     
     
        
      $mail->addBCC( BRAINBOX_EMAIL_ADDRESS );
         
         
         
        $mail->From     =      $submittedEmail;
        //$mail->From     =      'test@irms.org.uk';
         
        $mail->FromName =      $submittedEmail;
        $mail->AddReplyTo =    $submittedEmail;



        $mail->WordWrap =     	70;                              // set word wrap
        //$mail->AddAttachment($f_tmp_name,$f_name);      // attachment
                              
                              
        $mail->IsHTML(true);                               // send as HTML
        $mail->Subject  =  		  $this->emailDetails['subject'];
        $mail->Body     =  			$this->HTMLstring;
        $mail->AltBody  =  			$this->plainString;
              
							
							//Attachment
							
							//print_r($form->_submitFiles);
							
							
							if(isset($this->form->_submitFiles['file'])){
							      
									//	echo 'got here';
										//print_r($this->form->_submitFiles['file']);
										
										
										$filename=(    $this->form->_submitFiles['file']['name']);
					   				$tmp_filename=($this->form->_submitFiles['file']['tmp_name']);
										
										//echo 'filename: ' . $filename;
										//echo 'temp filename: ' . $tmp_filename;
										
																																											
					          $mail->AddAttachment(   $tmp_filename ,$filename);      // attachment
							
							}

							
							
							
							
              if($mail->Send()){
							//if (1==1){
                                                        
                          //echo 'email sent';
                          //exit;
                      return true;
             }
							else{
									
                     $errorMessage="Mailer Error: " . $mail->ErrorInfo;
										 return false;
                         //exit;
              }
              				
        						
        }
      				
					 
	function getForm(){	 
		 
		 return $this->form->toHTML();

              						
      }        
			
	 function escapeValue($value) {
              	return mysql_real_escape_string($value);
      }		
			
			function applyTemplates(){
			
							 $title=$this->emailDetails['subject'];
			
										
				$content=$this->HTMLstring;
				
				ob_start();
                		include ($this->templateHTML);
                		$this->HTMLstring=ob_get_contents();
				ob_end_clean();			 
							 
							
										$content=$this->plainString;
										
							      ob_start();
                		include ($this->templateText);
                		$this->plainString=ob_get_contents();
				ob_end_clean() ;			 
													 
							 
			}
			
 }     
 
 
 
 
//Functions used by QuickForm
function noEmailHeaders($elementName,$element_value) { 
    $excludeString='Content-type';		
		if(strpos(strtolower($element_value), strtolower($excludeString) ) !==false){
		  		return false;
		}
		else{
					return true;
		}
}				
function noAtCharacter($elementName,$element_value) { 
      $excludeString='@';
  		if(strpos($element_value, $excludeString)!==false){
  						return false;
  		}
  		else{ 
  				return true;
  		}
}
function noBBCode($elementName,$element_value) { 
    $excludeString='[/';		
		if(strpos(strtolower($element_value), strtolower($excludeString) ) !==false){
		  		return false;
		}
		else{
					return true;
		}
}
?>

