<?php


Class subscribeForm extends ContactForm{

   function subscribeForm(& $db, $emailDetails){
	            
							$this->db=& $db;
							$this->emailDetails=$emailDetails;
							
						 //print_r($emailDetails);
							//call the parent constructor
							parent::ContactForm($this->db, $this->emailDetails);
							
							
	 }						
   
	 
	 function setForm(){           					 
              
							$this->form->removeAttribute('name'); //for XHTML compliance
              
							//$this->form->addElement('text','name','Name',array('class'=>'text'));
							$this->form->addElement('text','forename','Forename',array('class'=>'text'));
							$this->form->addElement('text','surname' ,'Surname' ,array('class'=>'text'));
							
							
							$this->form->addElement('text','occupation','Occupation',array('class'=>'text'));
							$this->form->addElement('text','skills','Relevant Skills',array('class'=>'text'));
							
							
              //$this->form->addElement('text','company','Organisation',array('class'=>'text'));
              $this->form->addElement('text','email','Email',array('class'=>'text'));
							
							$this->form->addElement('text','phone','Phone Number',array('class'=>'text'));
              $this->form->addElement('text','mobile','Mobile',array('class'=>'text'));
							 
							$this->form->addElement('text','a1','Postal Address',array('class'=>'text'));
							$this->form->addElement('text','a2','',array('class'=>'text'));
							$this->form->addElement('text','a3','',array('class'=>'text'));
							$this->form->addElement('text','town','Town/County',array('class'=>'text'));
							$this->form->addElement('text','postcode','Post/Zip Code',array('class'=>'text'));
							$this->form->addElement('text','country','Country',array('class'=>'text'));
              
							
							//$this->form->addElement('text','phone','Phone number',array('class'=>'text'));
              
							$attrs = array("rows"=>"10", "cols"=>"50"); 
              $this->form->addElement('textarea','description','Name and Address<br />of Organisation<br />(if relevant)',$attrs);
							
              $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
              
							
              //Spam-killer rules - work on the SERVER side only
           
              //RULES
              //$this->form->addRule('name',      'Please enter your name',               'required','' ,'client');
							$this->form->addRule('forename',      'Please enter your forename',               'required','' ,'client');
							$this->form->addRule('surname',      'Please enter your surname',               'required','' ,'client');
							
              $this->form->addRule('email',     'Please enter your email address',      'required','' ,'client');
              $this->form->addRule('email',     'Please enter a valid email address',   'email','' ,'client');  
              //$this->form->addRule('description','Please enter a short message',    'required','' ,'client');
							
							//low key spam protection: no puctuation (to prevent email addresses in evey field
							$this->form->registerRule('no_at','regex','/^[^@]+$/');
						  $this->form->addRule('name',       'The "@" character is not allowed in this field.',      'no_at');
             // $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_at');
              $this->form->addRule('description','The "@" character is not allowed in this field.',     'no_at');
							
							//SPAM STUFF
							$this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
              $this->form->addRule('name',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
             // $this->form->addRule('company',    'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
              $this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
							//$this->form->addRule('phone',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
              $this->form->addRule('a1','The text "Content-Type" is not allowed in this field.',      'noEmailHeaders');
              
     }
}
 ?>

