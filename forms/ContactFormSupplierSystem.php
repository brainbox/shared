<?php


Class SupplierSystemForm extends ContactForm{

   function SupplierSystemForm($db, $emailDetails){
	            
                     
                     //Kick the parent class into action
                     parent::ContactForm($db, $emailDetails);
						
	 }						
	 
	 function setForm(){           					 
  
          $this->form->removeAttribute('name'); //for XHTML compliance          
          $this->form->addElement('text','company','Company',array('class'=>'text'));
          $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
          
          //RULES
          $this->form->addRule('company',      'Please enter the name of the company',               'required','' ,'client');
       
     }
		
      				
			
 }     



?>

