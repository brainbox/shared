<?PHP

class parseFormClass {
			
			var $myString="";
  
  // Default constructor,
	function parseFormClass(){
	
	
					// $this->myString='';	
					 $this->delimiter="|";
					 $this->nameValueDelimiter=": ";
					 
	  //print '<p>start of the parseFormClass class';
	}
	
  //Intended to produce a string suitable for storing in a flat file
	function parseToString(){
	
			$formString=""; //Define the string
		  
			$keys =array_keys($_POST); // store all variable names into a numericaly indexed array begining at 0
			for ($i =0; $i <count($keys); $i++){ // go through the created array and do....
					
					//Write out the item name
					$formString .= $keys[$i] . ": " ;
					
					//Handle the array fields
					if (is_array($_POST[$keys[$i]])) {
						 			for ($z=0;$z<count($_POST[$keys[$i]]);$z++){				
											//add a delimiter if necessary
											if ($z>0){
												 $formString .= ';' ;
											}
					          	$formString .= $_POST[$keys[$i]][$z];
									}		
          }
					else{ 					
						$formString .= $_POST[$keys[$i]]; // print data in the format var_name: var_value
					}
					
					//finish off the record
					$formString .=  "<br>\n";
	    }	
			return $formString;
	}		 
	
	
	//Intended to produce a string suitable for storing in a flat file
	function getString($fields,$outputFormat){
	
			$formString=""; //Define the string
			
			$fieldsArray=explode(",",$fields);
			for ($i =0; $i <count($fieldsArray); $i++){ // go through the created array 
			
					$name=$fieldsArray[$i];
			
					if($outputFormat=="html"){
					$formString .= "<p>$name: $_POST[$name]</p>";
					}
					if($outputFormat=="plain"){
					$formString .= "$name: $_POST[$name]\n";
					}
					if($outputFormat=="data"){
					$formString .= "$_POST[$name];";
					}
			}		
			
			//add date into the mix
			
			$submitTime = date("l dS F Y h:i:s a");
			
			if($outputFormat=="html"){
					$formString .= "<p>Submit time: $submitTime</p>";
					}
					if($outputFormat=="plain"){
					$formString .= "Submit time: $submitTime\n";
					}
					if($outputFormat=="data"){
					$formString .= $submitTime;
					}			
			
			
			
			
			return $formString;
			}
			
			
			
			function setDelimiter($newDelimiter){
			
							 $this->delimiter=$newDelimiter;
			}
			
			
			
	
			//A new approach: build it up a bi at a time
			/*
			function addField($myValue){
			
					$this->fieldCount=$this->fieldCount + 1;
					
							 
					$this->myString .=  $this->delimiter . $myValue;
									
 							 
			}
			*/
			function addField3($myValue){
					$this->fieldArray[]=$myValue;				 
			}
			
			
			function addField($myname,$myvalue,$SQLname=''){
			
			    //echo '<hr />' . $myname . '# '.$myvalue;

			 
					 if(!isset($myvalue)){
					     $myvalue='null';
					 }
					 
	
					 if( is_array($myvalue)  ){
					 		$arrayString = '';									 
					 		foreach($myvalue as $name=>$value){
							
							   $arrayString .= $name . ",";
							}									 
					 	
							$myvalue=$arrayString;				
					 }
	
					 $this->fieldArray[]=array(
					 						 'name'=>$myname, 
					 						 'value' => $myvalue,
											 'SQLname' => $SQLname);
											 
				 
							  
			}
			
			//returns a delimited string of values - intended for file storage
			function getStringDelimited(){
			
			
			
							 $myString="";
							 $tempString="";
							 
							 foreach($this->fieldArray as $key=>$value){
							      
									
							     $tempString=$value['value'];
									 
									 
									 $tempString = str_replace(chr(10),'<br>', $tempString); //replace any line breaks with comas
									 $tempString = str_replace(chr(13),''  , $tempString); 
							 		 $tempString = strip_tags($tempString); // strip out HTML
							 		 
							 		$myString .= $tempString. $this->delimiter;								
							 }
							 
							 
							 
						 return $myString;
			}
			//returns HTML suitable for an email
     function getStringHTML($nameValueDelimiter){
                         
          $myString="";
          $tempString="";
          
          //get the globally-defined email element 
          //include('config/admin_config.php');
          
          $htmlString  ='<table summary="form submission">';
          
          
          foreach($this->fieldArray as $key=>$value){
           
               //clean up the value
               $tempString=$value["value"];		 
               // AUG 05 - trying without this to try to improve the HTML email
               
               $tempString = str_replace(chr(13),''  , $tempString); 
               $tempString = strip_tags($tempString); // strip out HTML
               //$tempString = str_replace(chr(10),'<br>', $tempString); //replace any line breaks with comas
               //add to the list
               $htmlString  .='<tr><th>' . $value['name'] . '</th><td>' .  $tempString . '</td></tr>';	
                                   
                                                                                                                         
          }
          $htmlString  .='</table>';      			
          //add global end-of-email stuff here
          //$htmlString .= $adminElements['HTMLfooter'];
          
          
          //$htmlString .= '</body>';
          //$htmlString .= '</html>';
          //echo $htmlString;
          
          return $htmlString;
     }
			//returns plain text (with line breaks) suitable for a plan text email
			function getStringPlain($nameValueDelimiter){
							 $myString="";
							 $tempString="";
							 foreach($this->fieldArray as $key=>$value){
							 
							 		 //clean up the value
							  	 $tempString=$value["value"];
									 //$tempString = str_replace(chr(10),', ', $tempString); //replace any line breaks with comas
									 //$tempString = str_replace(chr(13),''  , $tempString); 
							 		 $tempString = strip_tags($tempString); // strip out HTML
							     //add to the list
							 		 $myString .= $value['name'] . $nameValueDelimiter . $tempString . "\n";											 
							 }
						 
						  //get the globally-defined email element 
							//include('config/admin_config.php');
						  //$myString .= $adminElements['textFooter'];
						 
						  return $myString;
			}
			
			function getSQLinsert($tableName){
							 
							 $SQLarray=array();
							 														
	 
							 foreach($this->fieldArray as $key=>$value){
							 
									//colle<ct values ONLY if SQLname is present      
									if($value['SQLname']!=''){
									
												$SQLarray[]=array('SQLname'=>$value['SQLname'],
																					'SQLvalue'=>$value['value']);
																				 
							   	}
							 }		
							 
							 //now have the correct array for the SLQ statm
							 $mySQLstring='INSERT INTO '.$tableName;
							 $mySQLstring .= ' (';
							 //field names
							 foreach($SQLarray as $name=>$value){
			 						 
							       $mySQLstring .= $value['SQLname'] . ', ';							 
							 
							 }
							 //remove the last comma
							 $mySQLstring=substr($mySQLstring, 0, strlen($mySQLstring)-2); 
							
							 $mySQLstring .=') ';
							 $mySQLstring .=' VALUES (';
							
							 //values
							 foreach($SQLarray as $name=>$value){ 
							     
							       $mySQLstring .= '"' .$value['SQLvalue'] . '", ';
							 
							 }
							 //remove the last comma
							 $mySQLstring=substr($mySQLstring, 0, strlen($mySQLstring)-2); 
							 
							 $mySQLstring .= ');';
							 
							 return $mySQLstring;
							 
			
			}
			
			
			
			
			function getString2(){
							 return $this->myString;
			}
			
	
	
}
?>