<?php

//
Class ConfForm extends ContactForm{

   function ConfForm(& $db, $emailDetails){
	            
						
						 	       				 
						 parent::ContactForm($db, $emailDetails);
	
	
	 }						
	 
	 function setForm(){           					 
              
							
							
							//overide the parent's POST details
							
							$this->form = new HTML_QuickForm('conferenceformb', 'POST', 'conference-form-b');
							
							//print_r($_POST);
					
												  
              $this->form->removeAttribute('name'); //for XHTML compliance
							
							
							//$poop= $this->the_session->get('member');
							// echo '<hr>' . $poop;
							
							
							//HIDDEN elements
							
							//print_r( $this->formData );
							
							
						
							
							
							
							
							$params=array('style'=>'width: 240px');
							
							$this->form->addElement('textarea','special-requirements','Any dietary or<br />special needs<br />requirements',$params);
							
							
							if(   isset($_POST['day']) && isset($_POST['day']['full']) || 
							      isset($_POST['extra-saturday']) ||
									  isset($_POST['extra-sunday']) ||   
										isset($_POST['extra-monday']) ||  
										isset($_POST['extra-tuesday'])
										 ){   
							
										$this->form->addElement('checkbox','smoking','I require a<br />\'smoking\' room');
						
							}
						
							
							
							
							
							
							$this->form->addElement('HTML','<tr><td><h2>Your Details</h2></td></tr>');
							
							/*
							if( $this->formData['member']== 'member' ){
							     
									  $this->form->addElement('text','membernumber','Member No.',array('class'=>'text'));
										
							    
							}
							*/
              
							
							
							
							
							
							
							
							
              $this->form->addElement('text','title','Title',array('class'=>'text'));
							
              $this->form->addElement('text','forename','Forename',array('class'=>'text'));
							$this->form->addElement('text','surname','Surname',array('class'=>'text'));
							
						
							
							
              $this->form->addElement('text','position','Position',array('class'=>'text'));
              $this->form->addElement('text','company','Company/Organisation',array('class'=>'text'));
							
							//$attrs = array("rows"=>"10", "cols"=>"50"); 
              //$this->form->addElement('textarea','address','Address',$attrs);
							
							//ADDRESS
							
							$this->form->addElement('text','address1','Address',array('class'=>'text'));
							$this->form->addElement('text','address2','',array('class'=>'text'));
							$this->form->addElement('text','address3','',array('class'=>'text'));
							$this->form->addElement('text','address4','',array('class'=>'text'));
							
							$this->form->addElement('text','town','Town',array('class'=>'text'));
							$this->form->addElement('text','county','County',array('class'=>'text'));
							$this->form->addElement('text','postcode','Postcode',array('class'=>'text'));
							$this->form->addElement('text','country','Country',array('class'=>'text'));
							
							
							
							
							
							
              $this->form->addElement('text','tel','Tel',array('class'=>'text'));
							$this->form->addElement('text','fax','Fax',array('class'=>'text'));
							
						  $this->form->addElement('text','email','Email address',array('class'=>'text'));
					
							
							
								//PAYMENT SECTION
							
							$this->form->addElement('HTML','<tr><td><h2>Payment</h2></td></tr>');
							//$this->form->addElement('checkbox','use-above-address','', 'Please invoice me at the above address');			
							//$attrs = array("rows"=>"10", "cols"=>"50"); 
             // $this->form->addElement('textarea','invoiceaddress','',$attrs);
							
							
							
							//ADDRESS
							
							$this->form->addElement('text','invoice-address1','Invoice Address<br>(if different to above)',array('class'=>'text', 'rowspan'=>'2'));
							$this->form->addElement('text','invoice-address2','',array('class'=>'text'));
							$this->form->addElement('text','invoice-address3','',array('class'=>'text'));
							$this->form->addElement('text','invoice-address4','',array('class'=>'text'));
							
							$this->form->addElement('text','invoice-town','Town',array('class'=>'text'));
							$this->form->addElement('text','invoice-county','County',array('class'=>'text'));
							$this->form->addElement('text','invoice-postcode','Postcode',array('class'=>'text'));
							$this->form->addElement('text','invoice-country','Country',array('class'=>'text'));
							
							
							
									 $this->form->addElement('HTML','<tr><td>&nbsp;</td></tr>');
					
							
					
							
							
											
							
						$this->form->addElement('text','invoice-contact','Invoice Contact Name',array('class'=>'text'));
						$this->form->addElement('text','invoice-telephone','Invoice Contact<br />Telephone Number',array('class'=>'text'));
						
						
							 $this->form->addElement('HTML','<tr><td>&nbsp;</td></tr>');
							 
							 	$this->form->addElement('text','po-number','Purchase Order No<br />if required',array('class'=>'text'));
						
							 $this->form->addElement('HTML','<tr><td>&nbsp;</td></tr>');
							 
							 
							 
								 $this->form->addElement('HTML','<tr><td></td><td class="highlight" style="width: 400px; ">
              							Cancellations received after Friday 30 March 2006 will not be refunded.
Written cancellations received before 30 March 2006 will be refunded less a
              �50 administration fee.</div></td></tr>');
							 
							
							
							//Finish Off
							
							$this->form->addElement('HTML','<tr><td><h2>Data Protection</h2></td></tr>');
							$this->form->addElement('HTML','<tr><td></td><td style="width: 400px"> The information you provide will be held on a database by IRMS.  The delegate list, produced at the Conference, will be provided only for the use of the delegates and those companies actually exhibiting at the Conference.  It will be used for no other purpose.<br /></td></tr>');
							
							
							 $this->form->addElement('checkbox','remove-from-list','','If you do <u>NOT</u> wish your details to be included in this list, please tick here.');
							
							$this->form->addElement('HTML','<tr><td></td><td style="width: 400px">&nbsp;<br /></td></tr>');
						
							
              $this->form->addElement('submit','submit','Book now', array('class'=>'button'));
							
							
							
              
              //RULES
				      /*
				      if( $this->formData['member']== 'member' ){
							     
									  //$this->form->addElement('text','membernumber','Member No.',array('class'=>'text'));
										$this->form->addRule('membernumber',      'Please enter your IRMS Membership Number',               'required','' ,'client');
										$this->form->addRule('membernumber',      'Member Number must be numeric. Please re-enter.',               'numeric','' ,'client');
										$this->form->addRule('membernumber', 'Member Number must be 4 figures or less. Please re-enter', 'maxlength', 4, 'client');
										
										
										
							    
							}
							*/
              
							
				
				
              $this->form->addRule('forename',      'Please enter your forename',               'required','' ,'client');
							$this->form->addRule('surname',      'Please enter your surname',               'required','' ,'client');
							
							
							
							
							$this->form->addRule('address','Please enter your address',    'required','' ,'client');
							$this->form->addRule('tel','Please enter your telephone number',    'required','' ,'client');
							
							
							$this->form->addRule('address1',     'Please enter the first line of your address',      'required','' ,'client');
              $this->form->addRule('town',     'Please enter your town',      'required','' ,'client');
              $this->form->addRule('postcode',     'Please enter your postcode',      'required','' ,'client');
            	$this->form->addRule('country',     'Please enter your country',      'required','' ,'client');
             				
			         
              $this->form->addRule('email',     'Please enter your email address',      'required','' ,'client');
              $this->form->addRule('email',     'Please enter a valid email address',   'email','' ,'client');  
              
							
							
							//$this->form->addRule('file', 'The file must be a Word .doc file', 'mimetype', array('application/msword'));
							//$this->form->addRule('file', 		'The maximum file size is 1 MB', 'maxfilesize', 1048576);
							
							//low key spam protection: no puctuation (to prevent email addresses in evey field
							$this->form->registerRule('no_at','regex','/^[^@]+$/');
						  $this->form->addRule('forename',       'The "@" character is not allowed in this field.',      'no_at'   );
							$this->form->addRule('surname',       'The "@" character is not allowed in this field.',      'no_at'   );
             
              $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_at'   );
             // $this->form->addRule('address','The "@" character is not allowed in this field.',      'no_at'   );
							
							
							
							
							
							
							
							
							//SPAM PREVENTION
							$this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
              $this->form->addRule('forename',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
							$this->form->addRule('surname',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
             
              $this->form->addRule('company',    'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
							$this->form->addRule('address',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
             
              
     }
		
      				
			
 }     
 
?>

