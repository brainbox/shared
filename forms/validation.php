<?php


class FormValidator {
	
	
	private $_errorList;
	private $msg='';

	function __construct()
	{
		
	}
	
	// check whether input is empty
	
	
	
	function _getValue($field){
			
  		if (isset($_POST[$field])){
  			   return $_POST[$field];
  		}
  		else{
  				 return "";
  		}
	}
	
    
    /**
     * A way of adding to the error list
     *
     * (I've used if for separate handlie of an uploaded file)
     *
     */
    
    public function addError($msg){
        
        $this->_errorList[] = array("field" => '', "value" => '', "msg" => $msg);
        
    }
    
    
	
	//Another check for an empty value - for checkboxes & radios
	
	function isSelected($field, $msg){
	
					// $value = $this->_getValue($field);
					 
		if(!isset($_POST[$field])){
		
			$this->_errorList[] = array("field" => $field, "value" => 'null', "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
    
    /**
     * Badly named - seems to be "is NOT empty"
     *
     *
     *
     */
	
	function isEmpty($field, $msg)
	{
		$value = $this->_getValue($field);
		
		if (trim($value) == "")
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is a string
	function isString($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_string($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is a number
	function isNumber($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_numeric($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is an integer
	function isInteger($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_integer($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}

	// check whether input is a float
	function isFloat($field, $msg)
	{
		$value = $this->_getValue($field);
		if(!is_float($value))
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is alphabetic
	function isAlpha($field, $msg)
	{
		$value = $this->_getValue($field);
		$pattern = "/^[a-zA-Z]+$/";
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}
	function isAlphaNumbericAndDash($field, $msg)
	{
               
                
                
		$value = $this->_getValue($field);
		
                //Empty string is tested separately
                if($value==''){
                        
                        return true;
                }
                
                
                $pattern = "/^[a-zA-Z0-9-]+$/";
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}
	function isAlphaNumbericDashSlash($field, $msg)
	{
		$value = $this->_getValue($field);
		$pattern = "/^[a-zA-Z0-9-\/]+$/";
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}


	// check whether input is within a valid numeric range
	function isWithinRange($field, $msg, $min, $max)
	{
		$value = $this->_getValue($field);
		if(!is_numeric($value) || $value < $min || $value > $max)
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	// check whether input is a valid email address
	function isEmailAddress($field, $msg)
	{
		$value = $this->_getValue($field);
		$pattern = "/^([a-zA-Z0-9])+([\.a-zA-Z0-9_-])*@([a-zA-Z0-9_-])+(\.[a-zA-Z0-9_-]+)+/";
		if(preg_match($pattern, $value))
		{
			return true;
		}
		else
		{
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
	}
	
	// GDS check whether input does not include the supplied string
	function doesNotContain($field, $excludeString, $msg)
	{
		$value = $this->_getValue($field);
		
		if(strpos($value, $excludeString)!==false){
			$this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
			return false;
		}
		else
		{
			return true;
		}
	}
	
	
	
	
	// GDS - check whether FILE has an allowable extensionw
	function isOKextension($field, $msg, $extensions){
	
	   //echo "the field is :" . $field;
		 //echo "the array is : <pre>";
		 //var_dump($extensions);
		 
		 
		  if(in_array($field, $extensions)){
			 			return true;
			}
			else{
					 $this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
					 return false;
			
			} 										 
			 
			
	}
	
	//Check the filesize of an attachment
	function isOKfilesize($filesize, $maxsize, $msg){	 
		 
		  if($filesize<=$maxsize){
			 			return true;
			}
			else{
					 $this->_errorList[] = array("field" => $field, "value" => $value, "msg" => $msg);
					 return false;
			
			} 										 
			 
			
	}
	
	
	
	
	// return the current list of errors
	function getErrorList()
	{
		return $this->_errorList;
	}
	
	// check whether any errors have occurred in validation
	// returns Boolean
	function isError()
	{
		if (sizeof($this->_errorList) > 0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	// reset the error list
	function resetErrorList()
	{
		$this->_errorList = array();
	}
	
// end 
}

?>
