<?php


Class ConfDVDmember extends ContactForm{

   function ConfDVDmember(& $db, $emailDetails){
	            
			//Kick the parent class into action
			parent::ContactForm($db, $emailDetails);
						
	 }						
	 
	 function setForm(){           					 
											  
          $this->form->removeAttribute('name'); //for XHTML compliance
          
          $this->form->addElement('hidden','confdvd');
          
          $this->form->addElement('text','forename','Forename',array('class'=>'text'));
          $this->form->addElement('text','surname','Surname',array('class'=>'text'));
          $this->form->addElement('text','rms_number','IRMS membership number',array('class'=>'text'));
          $this->form->addElement('text','company','Organisation',array('class'=>'text'));
          $this->form->addElement('text','phone','Contact phone number',array('class'=>'text'));
          $this->form->addElement('text','postcode','Post code',array('class'=>'text'));
          
		  $attrs = array("rows"=>"10", "cols"=>"50"); 
          $this->form->addElement('textarea','instructions','Delivery instructions (if any)',$attrs);
						
		  $this->form->addElement('checkbox','send_access_details','','Please also send me access details for 
		  the Member Area of the IRMS website. (Please enter your email address below.)');
		  $this->form->addElement('text','email','Email address',array('class'=>'text'));
						
          $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
          
          //RULES
          $this->form->addRule('forename',      'Please enter your forename',               'required','' ,'client');
          $this->form->addRule('surname',      'Please enter your surname',               'required','' ,'client');
          $this->form->addRule('phone',      'Please enter your phone number',               'required','' ,'client');
          $this->form->addRule('postcode',      'Please enter your postcode',               'required','' ,'client');
          
							
							//low key spam protection: no puctuation (to prevent email addresses in evey field
	      $this->form->registerRule('no_at','regex','/^[^@]+$/');
	      $this->form->addRule('name',       'The "@" character is not allowed in this field.',      'no_at'   );
          $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_at'   );
          $this->form->addRule('description','The "@" character is not allowed in this field.',      'no_at'   );
	      
	      $this->form->registerRule('noBBCode','function','noBBCode');
	      $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_'   );
              $this->form->addRule('description','The "@" character is not allowed in this field.',      'no_at'   );
							
							
	      //SPAM PREVENTION
	      $this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
              $this->form->addRule('forename',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('company',    'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
	      $this->form->addRule('phone',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('description','The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
							
               $this->form->registerRule('noBBCode','function','noBBCode');
	      $this->form->addRule('company',    'BBCode not allowed in this field.',      'noBBCode'   );
              $this->form->addRule('description','BBCode not allowed in this field.',      'noBBCode'   );
     }
		
      				
			
 }     


Class ConfDVDnonmember extends ContactForm{

   function ConfDVDnonmember(& $db, $emailDetails){
	            
			//Kick the parent class into action
			parent::ContactForm($db, $emailDetails);
						
	 }						
	 
	 function setForm(){           					 
											  
          $this->form->removeAttribute('name'); //for XHTML compliance
          
          $this->form->addElement('text','forename','Forename',array('class'=>'text'));
          $this->form->addElement('text','surname','Surname',array('class'=>'text'));
          $this->form->addElement('text','email','Email address',array('class'=>'text'));
          $this->form->addElement('text','phone','Contact phone number',array('class'=>'text'));
          
          $this->form->addElement('text','company','Organisation',array('class'=>'text'));
          $this->form->addElement('text','position','Position',array('class'=>'text'));
          
          
        
          $this->form->addElement('text','a1','Address',array('class'=>'text'));
          $this->form->addElement('text','a2','',array('class'=>'text'));
          $this->form->addElement('text','a3','',array('class'=>'text'));
          $this->form->addElement('text','town','Town',array('class'=>'text'));
          $this->form->addElement('text','county','County',array('class'=>'text'));
                   
          $this->form->addElement('text','postcode','Post code',array('class'=>'text'));
          $this->form->addElement('text','country','Country',array('class'=>'text'));
          
          
		  $attrs = array("rows"=>"10", "cols"=>"50"); 
          $this->form->addElement('textarea','instructions','Delivery instructions (if any)',$attrs);
						
						
						
		  $attrs = array("rows"=>"10", "cols"=>"50"); 
          $this->form->addElement('textarea','invoice','Invoice Address (if different)',$attrs);				
		  $this->form->addElement('text','po_number','PO number',array('class'=>'text'));
          $this->form->addElement('text','vat_number','VAT number',array('class'=>'text'));
		  
						
          $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
          
          //RULES
          $this->form->addRule('forename',      'Please enter your forename',               'required','' ,'client');
          $this->form->addRule('surname',      'Please enter your surname',               'required','' ,'client');
						$this->form->addRule('phone',      'Please enter your phone number',               'required','' ,'client');
					
					 $this->form->addRule('company',      'Please enter the name of your organisation',               'required','' ,'client');
					
          $this->form->addRule('a1',      'Please enter the first line of your address',               'required','' ,'client');
					$this->form->addRule('town',      'Please enter your town',               'required','' ,'client');
					$this->form->addRule('postcode',      'Please enter your postcode',               'required','' ,'client');
					$this->form->addRule('country',      'Please enter your country',               'required','' ,'client');
					
				
          
							
							//low key spam protection: no puctuation (to prevent email addresses in evey field
	      $this->form->registerRule('no_at','regex','/^[^@]+$/');
	      $this->form->addRule('name',       'The "@" character is not allowed in this field.',      'no_at'   );
          $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_at'   );
          $this->form->addRule('description','The "@" character is not allowed in this field.',      'no_at'   );
	      
	      $this->form->registerRule('noBBCode','function','noBBCode');
	      $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_'   );
              $this->form->addRule('description','The "@" character is not allowed in this field.',      'no_at'   );
							
							
	      //SPAM PREVENTION
	      $this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
              $this->form->addRule('forename',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('company',    'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
	      $this->form->addRule('phone',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('description','The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
							
               $this->form->registerRule('noBBCode','function','noBBCode');
	      $this->form->addRule('company',    'BBCode not allowed in this field.',      'noBBCode'   );
              $this->form->addRule('description','BBCode not allowed in this field.',      'noBBCode'   );
							
							
							 $this->form->setDefaults(array('country'=>'United Kingdom') );
							
							
     }
		
      				
			
 }  
 
 
 
Class ConfDVDauto extends ContactForm{

   function ConfDVDauto(& $db, $emailDetails){
	            
			//Kick the parent class into action
			parent::ContactForm($db, $emailDetails);
						
	 }						
	 
	 function setForm(){           					 
											  
      $this->form->removeAttribute('name'); //for XHTML compliance
          
          
          
		  $attrs = array("rows"=>"10", "cols"=>"50"); 
      $this->form->addElement('textarea','instructions','Delivery instructions (if any)',$attrs);
					
			$this->form->addElement('submit','submit','Submit', array('class'=>'button'));	
						
	}	
      				
			
}  
 

?>

