<?PHP

class parseFormClass {
			
			var $myString="";
  
  // Default constructor,
	function parseFormClass(){
	
	
					// $this->myString='';	
					 $this->delimiter="|";
					 $this->nameValueDelimiter=": ";
					 
	  //print '<p>start of the parseFormClass class';
	}
	
  //Intended to produce a string suitable for storing in a flat file
	function parseToString(){
	
			$formString=""; //Define the string
		  
			$keys =array_keys($_POST); // store all variable names into a numericaly indexed array begining at 0
			for ($i =0; $i <count($keys); $i++){ // go through the created array and do....
					
					//Write out the item name
					$formString .= $keys[$i] . ": " ;
					
					//Handle the array fields
					if (is_array($_POST[$keys[$i]])) {
						 			for ($z=0;$z<count($_POST[$keys[$i]]);$z++){				
											//add a delimiter if necessary
											if ($z>0){
												 $formString .= ';' ;
											}
					          	$formString .= $_POST[$keys[$i]][$z];
									}		
          }
					else{ 					
						$formString .= $_POST[$keys[$i]]; // print data in the format var_name: var_value
					}
					
					//finish off the record
					$formString .=  "<br>\n";
	    }	
			return $formString;
	}		 
	
	
	//Intended to produce a string suitable for storing in a flat file
	function getString($fields,$outputFormat){
	
			$formString=""; //Define the string
			
			$fieldsArray=explode(",",$fields);
			for ($i =0; $i <count($fieldsArray); $i++){ // go through the created array 
			
					$name=$fieldsArray[$i];
			
					if($outputFormat=="html"){
					$formString .= "<p>$name: $_POST[$name]</p>";
					}
					if($outputFormat=="plain"){
					$formString .= "$name: $_POST[$name]\n";
					}
					if($outputFormat=="data"){
					$formString .= "$_POST[$name];";
					}
			}		
			
			//add date into the mix
			
			$submitTime = date("l dS of F Y h:i:s A");
			
			if($outputFormat=="html"){
					$formString .= "<p>Submit time: $submitTime</p>";
					}
					if($outputFormat=="plain"){
					$formString .= "Submit time: $submitTime\n";
					}
					if($outputFormat=="data"){
					$formString .= $submitTime;
					}			
			
			
			
			
			return $formString;
			}
			
			
			
			function setDelimiter($newDelimiter){
			
							 $this->delimiter=$newDelimiter;
			}
			
			
			
	
			//A new approach: build it up a bi at a time
			function addField($myValue){
			
					$this->fieldCount=$this->fieldCount + 1;
					
							 
					$this->myString .=  $this->delimiter . $myValue;
									
 							 
			}
			function addField3($myValue){
					$this->fieldArray[]=$myValue;				 
			}
			
			
			function addField4($myname,$myvalue){
			
							 $temp=array('name'=>$myname, 
							 						 'value' => $myvalue);
							 
							 
					$this->fieldArray4[]=$temp;				 
			}
			
			//returns a delimited string of values - intended for file storage
			function getStringDelimited(){
			
							 $myString="";
							 foreach($this->fieldArray4 as $record){
							 
							 		$myString .= $record['value']. $this->delimiter;							
													 
							 }
						 return $myString;
			}
			//returns HTML suitable for an email
			function getStringHTML($nameValueDelimiter){
							 $myString="";
							 foreach($this->fieldArray4 as $record){
							 
							 		 $myString .="<p>" . $record['name'] . $nameValueDelimiter . $record['value'];											 
							 }
						 
						 return $myString;
			}
			//returns plain text (with line breaks) suitable for a plan text email
			function getStringPlain($nameValueDelimiter){
							 $myString="";
							 foreach($this->fieldArray4 as $record){
							 
							 		 $myString .= $record['name'] . $nameValueDelimiter . $record['value'] . "\n";											 
							 }
						 
						 return $myString;
			}
			
			
			
			function createDelimited($delimiter){
			
							 foreach($this->fieldArray as $value){
							 
							 		echo '<p>--' . $value;											 
							 }

			
			
							 
			}
			
			
			function getString2(){
							 return $this->myString;
			}
			
	
	
}
?>