<?php

//print_r($_POST);

Class RMSsupplierForms{

	function __construct ($dbPDO, $supplier){
	            
		$this->dbPDO=$dbPDO;
		$this->supplier=$supplier;
		
		require_once ("HTML/QuickForm.php");
		$this->form = new HTML_QuickForm('member_form', 'post', $_SERVER['REQUEST_URI'],null,null, true);					
              $this->setForm();
	 }						
   
	
	 //Hope this one doesn't over-write
	 function isValid(){
	     
		
	     if($this->form->validate()){
			     $this->process();
			     return true;
			 }
			 else{
			     return false;
			 }
			 
	 }
	 
	 
	 function addElement($type,$name,$value){
	 
	 					$this->form->addElement($type,$name,$value);
	 }
	 
	
     
     function setDefaults($defaultsArray){
	     
	      $this->form->setDefaults($defaultsArray);
     }
     
     
    
      				
					 
	function getForm(){	 
		 
		 return $this->form->toHTML();

              						
      }        
			
	 function escapeValue($value) {
              	return mysql_real_escape_string($value);
      }		
	
      
	function redirect(){
		
		$host  = $_SERVER['HTTP_HOST'];
		$uri   = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
		$extra = 'member-supplier';
		
		//echo "Location: http://$host$uri/$extra";
		
		header("Location: http://$host$uri/$extra");
		exit;
		
		
	}
	
 }     
 
 
 
###############################################################################
//Functions used by QuickForm
function noEmailHeaders($elementName,$element_value) { 
    $excludeString='Content-type';		
		if(strpos(strtolower($element_value), strtolower($excludeString) ) !==false){
		  		return false;
		}
		else{
					return true;
		}
}				
function noAtCharacter($elementName,$element_value) { 
      $excludeString='@';
  		if(strpos($element_value, $excludeString)!==false){
  						return false;
  		}
  		else{ 
  				return true;
  		}
}
function noBBCode($elementName,$element_value) { 
    $excludeString='[/';		
		if(strpos(strtolower($element_value), strtolower($excludeString) ) !==false){
		  		return false;
		}
		else{
					return true;
		}
}


###############################################################################


Class RMSsupplierForm extends RMSsupplierForms{

   function __construct($dbPDO, $supplier){
	            
			//Kick the parent class into action
			parent::__construct($dbPDO, $supplier);
						
	 }
	  function setForm(){
		
				 
        $this->form->addElement('hidden','ref','supplier');
        $this->form->addElement('text','name','Name');
      //  $this->form->addElement('text','alpha','Sort Letter');
      //  $this->form->addElement('checkbox','active','isActive');
      $this->form->addElement('text','website','Web address<br /> (Format: "www.website.co.uk")');
        
        $this->form->addElement('submit','submit','Save');
      //  echo 'asdfasdfsdf';
        //RULES
        $this->form->addRule( 'name', 'Please enter the supplier\'s name', 'required');
      //  $this->form->addRule( 'alpha', 'Please enter the supplier\'s sort letter', 'required');
     //   $this->form->addRule( 'alpha', 'Must be a letter', 'lettersonly');
     //   $this->form->addRule( 'alpha', 'Just one letter please', 'maxlength',1);
				
		
		}
		function process(){
		
						 //echo 'got here';
						 //exit;
						 $this->form->applyFilter('__ALL__','escapeValue'); 
						
        		$this->supplier->name=$this->form->getSubmitvalue('name');
        	//	$this->supplier->alpha=$this->form->getSubmitvalue('alpha');
        	//	$this->supplier->active=$this->form->getSubmitvalue('active');
        		$this->supplier->website=$this->form->getSubmitvalue('website');
        		$this->supplier->update();
			
			//$this->redirect();
		
		}

}
Class RMSservicesForm extends RMSsupplierForms{

   function __construct($dbPDO, $supplier){
	            
			//Kick the parent class into action
			parent::__construct($dbPDO, $supplier);
						
	 }
	  function setForm(){
		
      
          $services=new Services($this->dbPDO);
          

          $counter=0;
          while($service=$services->fetch() ){
          
          if ($counter==0){
           $this->form->addElement('checkbox','service[' . $service->id . ']','Service(s)',$service->name);	
          }
          else{
          $this->form->addElement('checkbox','service[' . $service->id . ']','',$service->name);	
          }
          $counter++;
          
          }
            $this->form->addElement('submit','submit','Save');
          
          
          }
	 function process(){
			
			$this->form->applyFilter('__ALL__','escapeValue'); 
			
			$servicesArray=$this->form->getSubmitValues() ;
			
			if(isset($servicesArray['service'])  ){
					$servicesArray=$servicesArray['service'];
			}
			else{
				$servicesArray=null;
			}
			
			
			//print_r($servicesArray);
			//exit;
			
			$services=new Services($this->dbPDO);
			
			
			
			$services->setServices($servicesArray, $this->supplier->id );
			$this->supplier->update();
			//$this->redirect();
	 
	 
	 }
}



Class RMSactiveForm extends RMSsupplierForms{

   function __construct($dbPDO, $supplier){
	            
			//Kick the parent class into action
			parent::__construct($dbPDO, $supplier);
						
	 }
	  function setForm(){

      $this->form->addElement('checkbox','active','isActive');	
      $this->form->addElement('submit','submit','Save');          
          
      }
	 function process(){
	 
			$this->form->applyFilter('__ALL__','escapeValue'); 
			$this->supplier->active=$this->form->getSubmitValue('active') ;
			$this->supplier->update();
			//$this->redirect();
	 
	 }
}


Class RMSassignmentForm extends RMSsupplierForms{

   function __construct($dbPDO, $supplier){
	            
			//Kick the parent class into action
			parent::__construct($dbPDO, $supplier);
						
	 }
	  function setForm(){

		  //GET ALL THE MATCHING PEOPLE
      $sql = "SELECT user_id, firstName, lastName, login FROM user_new WHERE active='1'"; 
      // AND substring(lastName,1,1) = '".$_GET['alpha']. 
       $sql .= " ORDER BY lastName ASC";
			$stmnt=$this->dbPDO->prepare($sql);
			$stmnt->execute();
		
			$results=$stmnt->fetchAll(PDO::FETCH_ASSOC) ;
			
			
			
			$radio=array();
			$selectArray=array();
			
		$selectArray[0]= '--- UNASSIGN ---';	
		 foreach ($results as $name=>$value ) {
  			
				$selectArray[$value['user_id']]= $value['lastName'] . ', ' .  $value['firstName'] . ' - ' . $value['login'];
				//$this->form->addElement('hidden','member','',$userString ,$value['user_id']);
				
				
		}
		$this->form->addElement('select','member','Member',$selectArray);

			
      $this->form->addElement('submit','submit','Save');          
          
      }
	 function process(){
	 
			$this->form->applyFilter('__ALL__','escapeValue'); 
			//echo '---';
			//print_r($this->form->getSubmitValues()  );
			//exit;
					
			//Kill the current association
			
			//$sql = "UPDATE sup_supplier SET member_id ='' WHERE member_id =" . $this->form->getSubmitValue('member');
			
		//	echo $sql;
		//  $stmnt=$this->dbPDO->prepare($sql);
		//	$stmnt->execute();
		
		
		
		
		$memberID=$this->form->getSubmitValue('member');
		
		
		//echo $memberID;
		
		if($memberID == 0){
			
			$memberID = 'NULL';
		}
		
		
		//if($memberID){
		
			$sql = "UPDATE sup_supplier SET member_id = $memberID  WHERE id=" . $this->supplier->id;

		//}
		//else{
		//	$sql = "UPDATE sup_supplier SET member_id ='' WHERE member_id =" . $this->supplier->id;
		//}
		$stmnt=$this->dbPDO->prepare($sql);
			$stmnt->execute();
			
			//print_r($stmnt->errorInfo() );
			//$this->redirect();
	 
	 }
}



Class RMSoutletForm extends RMSsupplierForms{

   function __construct($dbPDO, $outlet){
	            
			//Kick the parent class into action
			parent::__construct($dbPDO, $outlet);
						
	 }
	  function setForm(){
		
		
				 $this->form->addElement('text','name','Office name (e.g. Head Office)');		 
				
        $this->form->addElement('text','address','Address');
        $this->form->addElement('text','address2','');
				$this->form->addElement('text','address3','');
				$this->form->addElement('text','town','Town');
    		$this->form->addElement('text','county','County');
    		$this->form->addElement('text','postcode','Postcode');
                
                   $this->form->addElement('text','email','Email');
        $this->form->addElement('text','phone','Phone');
        $this->form->addElement('text','fax','Fax');
        
        $this->form->addElement('submit','submit','Save');
        
        //RULES
        $this->form->addRule( 'address', 'Please enter the first line of the address', 'required');
        $this->form->addRule( 'town', 'Please enter the town', 'required');
        $this->form->addRule( 'postcode', 'Please enter the postcode', 'required');
           $this->form->addRule( 'email', 'Please enter a valid email address', 'email');
				
				
				
				//The followong SAYS **** suppplier *** but it MEANS  ***outlet****
				
				$this->form->setDefaults(array(
					    
      			'name'=>	$this->supplier->name, 			
      			'address'=>	$this->supplier->address, 
      			'address2'=>	$this->supplier->address2, 
      			'address3'=>	$this->supplier->address3,	
                        'town'=>	$this->supplier->town,	
                        'county'=>	$this->supplier->county,	
                        'postcode'=>	$this->supplier->postcode,
                                                
                        'email'=>	$this->supplier->email,	
                        'phone'=>	$this->supplier->phone,	
                        'fax'=>		$this->supplier->fax
				));

		}
		   
	 function process(){
	 					
			//The followong SAYS **** suppplier *** but it MEANS  ***outlet****			
			
			$this->form->applyFilter('__ALL__','escapeValue'); 
			
			$this->supplier->name															 =$this->form->getSubmitvalue('name');
      $this->supplier->address							=$this->form->getSubmitvalue('address');
      $this->supplier->address2														 =$this->form->getSubmitvalue('address2');
      $this->supplier->address3							 =$this->form->getSubmitvalue('address3');
			$this->supplier->town															 =$this->form->getSubmitvalue('town');
      $this->supplier->county							 =$this->form->getSubmitvalue('county');
      $this->supplier->postcode							 =$this->form->getSubmitvalue('postcode');
        
        $this->supplier->phone	=$this->form->getSubmitvalue('phone');
        $this->supplier->email	=$this->form->getSubmitvalue('email');
			$this->supplier->fax															 =$this->form->getSubmitvalue('fax');
      
			
			$this->supplier->update();
			//$this->redirect();
	 
	 }
	 
}

/*

Class ASLTIPmemberPersonal extends ASLTIPmemberForm{

   function ASLTIPmemberPersonal($db, $member){
	            
			//Kick the parent class into action
			parent::__construct($db, $member);
						
	 }

	 function setForm(){

          $this->form->removeAttribute('name'); //for XHTML compliance
          
          $this->form->addElement('hidden','personal');
          
	  $this->form->addElement('text','title','Title',array('class'=>'text'));
          $this->form->addElement('text','forename','Forename',array('class'=>'text'));
          $this->form->addElement('text','surname','Surname',array('class'=>'text'));
	  
	  $this->form->addElement('text','home','Home phone',array('class'=>'text'));
	  $this->form->addElement('text','work','Work phone',array('class'=>'text'));
	  $this->form->addElement('text','mobile','Mobile',array('class'=>'text'));
	  $this->form->addElement('text','fax','Fax',array('class'=>'text'));
	  
	  $this->form->addElement('text','address','Address',array('class'=>'text'));
	  $this->form->addElement('text','address2','',array('class'=>'text'));
	  $this->form->addElement('text','address3','',array('class'=>'text'));
	  
	  
	  $this->form->addElement('text','town','Town',array('class'=>'text'));
	  $this->form->addElement('text','county','County',array('class'=>'text'));
          $this->form->addElement('text','postcode','Post code',array('class'=>'text'));

	  $this->form->addElement('text','country','Country',array('class'=>'text'));
          							
          $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
          
          //RULES
	  //$this->form->addRule('title',      	'Please enter your title',               	'required','' ,'client');
          $this->form->addRule('forename',      'Please enter your forename',               	'required','' ,'client');
          $this->form->addRule('surname',      	'Please enter your surname',               	'required','' ,'client');
	  
	  $this->form->addRule('address',      	'Please enter the first line of your address',  'required','' ,'client');
	  $this->form->addRule('town',      	'Please enter the first line of your town',     'required','' ,'client');
          //$this->form->addRule('county',      	'Please enter the first line of your county',   'required','' ,'client');
          $this->form->addRule('postcode',      'Please enter the first line of your postcode', 'required','' ,'client');
          //$this->form->addRule('country',      	'Please enter the first line of your country',  'required','' ,'client');
          
	  
     }

 } 
Class ASLTIPmemberOnsite extends ASLTIPmemberForm{

   function __construct($dbPDO, $member){
	
	
	//Kick the parent class into action
	parent::__construct($dbPDO, $member);
						
	 }	
	
	 
	 function setForm(){           					 
											  
		$this->form->removeAttribute('name'); //for XHTML compliance
			  
		
		
		$distanceArray=array(	0=>'Never travel',
					5=>'5 miles (one way)',
					10=>'10 miles (one way)',
					25=>'25 miles (one way)',
					40=>'40 miles (one way)',
					60=>'60 miles (one way)',
					1000=>'Negotiable (i.e. you are prepared to discuss any distance)'
					);
		
	
		$counter=0;
	    
		
		 foreach ($distanceArray as $name=>$value) {
			//echo 'dsdfsdf';
			
			
			if ($counter==0){
				$this->form->addElement('radio','range', 'Distance',$value,$name);	
			}
			else{
				$this->form->addElement('radio','range','' ,$value, $name);
			}
			$counter++;
		}
		  
		$this->form->addElement('text','postcodebase', 'Base Postcode',array('class'=>'text'));
		$this->form->addRule('postcodebase', 'Please enter a valid UK postcode', 'regex', '/^[a-zA-Z]{1,2}[0-9][0-9A-Za-z]{0,1} [0-9][A-Za-z]{2}$/i');

		
		//Our man
		
		$this->form->setDefaults(array('range'=>$this->member->range , 'postcodebase'=>$this->member->postcodebase ) );
		//$userForm->setDefaults(array('disorder['.$treatArray['id'] .']'=>1) );
	
		  
		  
		$this->form->addElement('submit','submit','Submit', array('class'=>'button'));
	  
	}
	
	 function isValid(){
	     
	    if($this->form->validate()){
		     
		     //echo $this->form->getSubmitValue('range') .  ' // ' . $this->form->getSubmitValue('postcodebase');
		     //echo count(explode(" ",$this->form->getSubmitValue('blurb')));
		   //  if($this->form->getSubmitValue('range')!=0 && $this->form->getSubmitValue('postcodebase')==''){
		//		$this->updateMessage= '<span class="error">Please select "Never travel"  <em>or</em> enter a Base Postcode. Details NOT saved</span>';	
		//		return false;
		  //   }
		   //  else{
			     $this->process();
			    // $this->isSuccess=true;
			    // $this->updateMessage='<span class="success">Details saved at ' .date('H:i:s') . '</span>. Click here <a href="../member-details">to return to the summary page</a>.';
		    
		           
		     
		//     }
			     return true;
	    
	     }
	     else{
		     return false;
	     }
			 
	 }
	 function getUpdateMessage(){
	     	 if(isset($this->updateMessage)){
			 return $this->updateMessage;
		 }
		 else{
			 return false;
		 }
	 }
	
	function process(){
		
		
		
		$this->form->applyFilter('__ALL__','escapeValue'); 
		
		$this->member->range=$this->form->getSubmitValue('range');
		echo '<p>FFFFFFF' . $this->member->range;
		$this->member->postcodebase=$this->form->getSubmitValue('postcodebase');
		$this->member->finalise();
	}
 
}
 

 Class ASLTIPmemberAge extends ASLTIPmemberForm{

   function ASLTIPmemberAge($db, $member){
	
	//$this->dbPDO=$dbPDO;
	//$this->memberID=$memberID;	
		//echo "memerb id is " . $this->memberID;
	//Kick the parent class into action
	parent::__construct($db, $member);
	
	//print_r($_POST);
						
	 }	
	
	 
	 function setForm(){       
		 
		 
		 
											  
		$this->form->removeAttribute('name'); //for XHTML compliance
			  
		$masterAgeArray=$this->member->getMasterAgeArray();
		 
		// print_r($masterAgeArray);
	
		$counter=0;
	    
		
		 foreach ($masterAgeArray as $name=>$value ) {
			//echo 'dsdfsdf';
			//($row);
			//echo '<p>' . $row['id'];
			
			if ($counter==0){
				 $checkbox[]= $this->form->createElement('checkbox',  $value['id'] ,'Age Ranges treated',$value['description'],$value['id']);	
			}
			else{
				$checkbox[]= $this->form->createElement('checkbox', $value['id'],'' ,$value['description'],$value['id']);	
			}
			$counter++;
		}
		
		$this->form->addGroup($checkbox, 'age', 'Age Range(s):', '<br />');
		// Simple rule: at least 2 checkboxes should be checked
		$this->form->addGroupRule('age', 'Please check at least one box', 'required', null, 1);
  
		
		$ageIDArray=$this->member->getAgeIDarray();
		//print_r($ageIDArray);
		
		foreach($ageIDArray as $name=>$value) {
		
			//print_r($treatAgeArray );
			$this->form->setDefaults(array('age['.$name . ']'=>1) );
			//$userForm->setDefaults(array('disorder['.$treatArray['id'] .']'=>1) );
		
		}

		  
		  $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
	  
	}
	
	//Trying to extend what the main function does... not very successfully so far.
	 function isValid(){
	     
		
	     //LET Quickform do its job first//OOOPS thing I've over-ridden a key QUICKFORM funcuib!"!!! 
	     if($this->form->validate()){
		     //echo '<hr>here';
		
		     $this->process();
		    // $this->isSuccess=true;
		     $this->updateMessage='<span class="success">Details saved at ' .date('H:i:s') . '</span>. Click here <a href="../member-details">to return to the summary page</a>.';
    
		     return true;
	    
	     }
	     else{
		     return false;
	     }
			 
	 }
	 function getUpdateMessage(){
	     	 if(isset($this->updateMessage)){
			 return $this->updateMessage;
		 }
		 else{
			 return false;
		 }
	 }
	
		
	
	function process(){
		
		
		$this->form->applyFilter('__ALL__','escapeValue'); 
				
		//New values
		$ageIDarray=$this->form->getSubmitValue('age'); 
		
		$this->member->setAgeIDarray($ageIDarray);
		$this->member->finalise();
		
		}
	

 }  
 
  
 
 
 Class ASLTIPmemberSpecialty extends ASLTIPmemberForm{

   function ASLTIPmemberSpecialty($dbPDO, $member){
	
	
	//Kick the parent class into action
	parent::__construct($dbPDO, $member);
						
	 }	
	
	 
	 function setForm(){           					 
											  
		$this->form->removeAttribute('name'); //for XHTML compliance
			  
		$disorderMasterArray=$this->member->getMasterDisorderArray();	
		
		$counter=0;
		 foreach  ($disorderMasterArray as $name=>$value  ) {
			
			if ($counter==0){
				 $this->form->addElement('checkbox','disorder[' . $value['id'] . ']','Specialties',$value['disorder']);	
			}
			else{
					$this->form->addElement('checkbox','disorder[' . $value['id'] . ']','',$value['disorder'],$value['id']);	
			}
			$counter++;
		}
		  
		
		//Our man
		
		//Disorders
		
		$disorderIDarrray=$this->member->getDisorderIDarray();
		
		//print_r($disorderIDarrray);
		//exit;
		foreach($disorderIDarrray as $name=>$value) {
		
			//print_r($treatArray );
			$this->form->setDefaults(array('disorder['.$name .']'=>1) );
		
		}
				  
		  
		  $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
	  
	}
	function process(){
		
		
		$this->form->applyFilter('__ALL__','escapeValue'); 
		
		$this->member->setDisorderIDarray($this->form->getSubmitValue('disorder') );
		$this->member->finalise();
	}

				
	
	
	 function getMessage(){
	     	 if(isset($this->updateMessage)){
			 return $this->updateMessage;
		 }
		 else{
			 return false;
		 }
	 }
	
		
      				
			
 }  
 
  
 
 
 
Class ASLTIPmemberTherapy extends ASLTIPmemberForm{

   function ASLTIPmemberTherapy(& $db, $emailDetails){
	            
	//Kick the parent class into action
	parent::ASLTIPmemberForm($db, $emailDetails);
						
	 }						
	 
	 function setForm(){           					 
											  
		$this->form->removeAttribute('name'); //for XHTML compliance
			  
			
			
		  
		$counter=0;
	
		  //SHOULD COME FROM THE DATABASE - CHANGE LATER
		$ageArray[]=array('age_id'=>1,'description' => '0 - 4 (pre-school)'); 
		//$ageArray[]=array('age_id'=>2,'description' => '2 - 4 (pre-school)');
		$ageArray[]=array('age_id'=>3,'description' => '5 - 10 (primary school)');
		$ageArray[]=array('age_id'=>4,'description' => '11 - 16 (secondary school)');
		$ageArray[]=array('age_id'=>5,'description' => '17 - 20 (adolescent?)');
		$ageArray[]=array('age_id'=>5,'description' => '21 - 64 (adult?)');
		$ageArray[]=array('age_id'=>5,'description' => '65 and above (?)');
	
		//while( $row=$result->fetch() ){
		foreach($ageArray as $name=>$row){
			//echo 'dsdfsdf';
			//print_r($row);
			
			if ($counter==0){
				 $this->form->addElement('checkbox','age','Age Ranges treated',$row['description'],$row['age_id']);	
			}
			else{
					$this->form->addElement('checkbox','age','',$row['description'],$row['age_id']);	
			}
			$counter++;
		}
		  
		  
		  
		$sql='select * from disorder';
		$result=$this->db->query($sql);
		$options=  array();
		
		
		$counter=0;
		while( $row=$result->fetch() ){
		 
			if ($counter==0){
				 $this->form->addElement('checkbox','speciality','Specialties',$row['disorder'],$row['id']);	
			}
			else{
					$this->form->addElement('checkbox','speciality','',$row['disorder'],$row['id']);	
			}
			$counter++;
		
		}
		
		
							
		$attrs = array("rows"=>"10", "cols"=>"100"); 
		  $this->form->addElement('textarea','blurb','100 words',$attrs);		
		  
		  
		  $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
	  
	}
		
      				
			
 }  
 
  
 
	
 
 Class ASLTIPmemberPassword extends ASLTIPmemberForm{

   function __construct($dbPDO, $member, $db, $auth){
	            
	   //echo '<hr><hr>';
		
		$this->auth=$auth;
		$this->db=$db;
		$this->isSuccess= false;
		//Kick the parent class into action
		parent::__construct($dbPDO, $member);

						
	 }	
	 
	 
		
	 function setForm(){           	

		 
		
		 
		$this->form->removeAttribute('name'); //for XHTML compliance

		   // A function for comparing password
		      // A function for comparing password
		function cmpPass($element, $confirm) {
		    
			//echo $element;
			//echo $confirm;
			//global $this->form;
		    $password = $this->form->getElementValue('newPassword');
		    return ($password == $confirm);
		}
	
		// Register the compare function
		$this->form->registerRule('compare', 'callback', 'cmpPass',' this');
	
		// Add a header to the form
		//$form->addHeader('Change your Password');
	
		// Add a field for the old password
		$this->form->addElement('password','oldPassword','Current Password');
		$this->form->addRule('oldPassword','Enter your current password', 'required',false,'client');
	
		// Add a field for the new password
		$this->form->addElement('password','newPassword','New Password');
		$this->form->addRule('newPassword','Please provide a new password','required',
			       false,'client');
		$this->form->addRule('newPassword','Password must be at least 6 characters',
			       'minlength',6,'client');
		$this->form->addRule('newPassword','Password must be 12 characters or less',
			       'maxlength',12,'client');
		$this->form->addRule('newPassword','Password may only contain letters and '.
			       'numbers','alphanumeric',NULL,'client');
	
	
		// Add a field for the new password
		$this->form->addElement('password','confirm','Confirm New Password');
		$this->form->addRule('confirm', 'Please confirm your new password','required',NULL, 'client');
			     
	
		$this->form->addElement('submit','submit','Change Password', array('class'=>'button'));
		
		

	
	}
	 function isValid(){
	     
		
	     //LET Quickform do its job first
	     if($this->form->validate()){
		     
		     //Check that both versions of the password match
		      if($this->form->getSubmitValue('newPassword') != $this->form->getSubmitValue('newPassword')){
				$this->errorMessage= '<span class="error">"New Password" and "Confirm New Password" must match. Please try again.</span>';	
				return false;
		     }
		     else{
			     $this->process();
			     //$this->isSuccess=true;
			    // $this->updateMessage='<span class="success">Details saved at ' .date('H:i:s') . '</span>. Click here <a href="../member-details">to return to the summary page</a>.';
		     }
			     return true;
	    
	     }
	     else{
		     return false;
	     }
			 
	 }
	
	
	
	 function getErrorMessage(){
	     	 if(isset($this->errorMessage)){
			 return $this->errorMessage;
		 }
		 else{
			 return false;
		 }
	 }
	 
	 function getUpdateMessage(){
	     	 if(isset($this->updateMessage)){
			 return $this->updateMessage;
		 }
		 else{
			 return false;
		 }
	 }
	
        
	function process(){
		
		 // Instantiate Account Maintenance class
		 
		 // Include AccountMaintenance class
		require_once ('members/MemberManagement.php');
		
		$aMaint=new AccountMaintenance($this->db);
							
		// Change the password
		if ( $aMaint->changePassword(
				$this->auth,
				$this->form->getSubmitValue('oldPassword'),
				$this->form->getSubmitValue('newPassword')) ) {
			$this->updateMessage=  '<p class="success">Your password has been changed successfully.</p>';
		    } else {
			$this->errorMessage=  $this->errorMessage= '<span class="error">Error changing your password: current password not recognised.</p>';
		    }
	
	}
	
 }
 //REMEMBER - WiLL NOT BE LOGGED IN!!!
 Class ASLTIPmemberForgotPassword extends ASLTIPmemberForm{

   function __construct(& $db, $attribs, $dbPDO){
	            
	   //echo '<hr><hr>';
		$this->dbPDO=$dbPDO;
		$this->db=$db;
		//$this->memberID=$memberID;
		$this->isSuccess= false;
		//Kick the parent class into action
		parent::__construct($dbPDO, null);
		
						
	 }	
	 
	 
		
	 function setForm(){           	

				 
		$this->form->removeAttribute('name'); //for XHTML compliance		
		// Instantiate the QuickForm class
		//$form = new HTML_QuickForm('password', 'POST', $baseURL .'password');
		//$form->removeAttribute('name'); //for XHTML compliance
		
		$attrArray=array('class'=>'text');
		//$form->addElement('text','login','Username',$attrArray);
		$this->form->addElement('text','email','Email',$attrArray);
		
		$attrArray=array('class'=>'button');
		$this->form->addElement('submit','submit','Submit',$attrArray);
		
		//RULES
		
		//$form->addRule('login',   'Please enter your username',             'required',FALSE ,'client');
		$this->form->addRule('email',   'Please enter your email address',          'required',FALSE ,'client');
		$this->form->addRule('email' ,  'Please enter a valid email address' ,      'email',FALSE,'client');


	}
	
	 function getMessage(){
	     	 if(isset($this->updateMessage)){
			 return $this->updateMessage;
		 }
		 else{
			 return false;
		 }
	 }
	
	
	 function getErrorMessage(){
	     	 if(isset($this->errorMessage)){
			 return $this->errorMessage;
		 }
		 else{
			 return false;
		 }
	 }
	
	
	 function isValid(){
	     
	    if($this->form->validate()){
			     $this->process();  
			     return true;
	   
	     }
	     else{
		     return false;
	     }
			 
	 }
        
	function process(){
		
		require_once('members/MemberManagement.php');
		require_once('members/MemberManagementASLTIP.php');
		$userManagement= new AccountMaintenanceASLTIP ($this->db);
		
		$email = $this->form->getSubmitValue('email');
		
		$userDetails=$userManagement->fetchLoginByLogin($email);
		
		if( ! $userDetails ){
		
			     $this->errorMessage = '<p class="error">Email address not recognised</p>';
			 
		}
		else{//email address exists, but what's the status of the user?
		
			if($userDetails['active']!=1){
				
				$this->errorMessage = '<p class="error">Account expired. Please contact us.</p>';
			}
			
			else{
				//echo $userManagement->active();
				//RESET PASSWORD BIT
				// Fetch a list of words
				$fp = fopen ( './admin/pass_words/pass_words.txt','rb' );
				$file= fread ( $fp, filesize('./admin/pass_words/pass_words.txt') );
				fclose($fp);		
				// Add the words to the class
				$userManagement->addWords(explode("\n",$file));
				
				$details=$userManagement->fetchLoginByLogin($email );
				
				$newPassword=$userManagement->resetPassword($details['email'],$details['email']);
				
					
				require_once 'phpmailer/class.phpmailer.php';
									
				$mail=new phpmailer();
									
				$mail->addAddress($details['email']);
				
				
				$mail->From     =     	'no-reply@helpwithtalking.com';
				$mail->FromName =     	'ASLTIP';
				$mail->AddReplyTo =   	'no-reply@helpwithtalking.com';
				$mail->WordWrap =     	75;                              // set word wrap
				//$mail->AddAttachment($f_tmp_name,$f_name);      // attachment
				$mail->IsHTML(true);                               // send as HTML
			
			
			
				$mail->Subject  = 'ASLTIP Member Area - the new password you requested';
				$mail->addBCC(	'asltip-password@contentAtoZ.co.uk');
			
				
				//prep the variables for the include:
				$firstName=$details['forename'];
				$emailAddress=$details['email'];
				ob_start();
				include('template/asltip-password.htm');
				$htmlString=ob_get_contents();
				ob_end_clean();
				//echo $htmlString;
				
				ob_start();
				include('template/asltip-password.txt');
				$plainString=ob_get_contents();
				ob_end_clean();
				//echo $plainString;
		
				$mail->Body     =  	$htmlString;
				$mail->AltBody  =  	$plainString;
				
				if(!$mail->Send()){
				       $this->errorMessage="Mailer Error: " . $mail->ErrorInfo;
				       //exit;
				}
				else{
				   $this->updateMessage='Thank you. Your password has been reset and emailed to <strong>' . $details['email'] .'</strong>.
				   Please check your email inbox.';
				}			
		
			    }
		}	
	}				
}
	
	
Class ASLTIPmemberChangeDetails extends ASLTIPmemberForm{

   function __construct($dbPDO, $member){
	            
	 	//Kick the parent class into action
		parent::__construct($dbPDO, $member);
		
						
	 }	
	 

	 function setForm(){           	

				 
		$this->form->removeAttribute('name'); //for XHTML compliance		
		// Instantiate the QuickForm class
		//$form = new HTML_QuickForm('password', 'POST', $baseURL .'password');
		//$form->removeAttribute('name'); //for XHTML compliance
		
		$attrArray=array('class'=>'text');
		//$form->addElement('text','login','Username',$attrArray);
		$this->form->addElement('textarea','message','Changes',$attrArray);
		
		$attrArray=array('class'=>'button');
		$this->form->addElement('submit','submit','Submit',$attrArray);
		
		//RULES
		
		//$form->addRule('login',   'Please enter your username',             'required',FALSE ,'client');
		$this->form->addRule('message',   'Please enter your changes',          'required',FALSE ,'client');
		


	}
	
	 function getUpdateMessage(){
	     	 if(isset($this->updateMessage)){
			 return $this->updateMessage;
		 }
		 else{
			 return false;
		 }
	 }
	
	
	 function getErrorMessage(){
	     	 if(isset($this->errorMessage)){
			 return $this->errorMessage;
		 }
		 else{
			 return false;
		 }
	 }
	
	
	 function isValid(){
	     
	    if($this->form->validate()){
			     $this->process();  
			     return true;
	   
	     }
	     else{
		     return false;
	     }
			 
	 }
        
	function process(){
		
		
		require_once 'phpmailer/class.phpmailer.php';
							
		$mail=new phpmailer();
							
		$mail->addAddress('asltip@awdry.demon.co.uk');
		
		
		$mail->From     =     	'no-reply@helpwithtalking.com';
		$mail->FromName =     	'ASLTIP';
		$mail->AddReplyTo =   	'no-reply@helpwithtalking.com';
		$mail->WordWrap =     	75;                              // set word wrap
		//$mail->AddAttachment($f_tmp_name,$f_name);      // attachment
		$mail->IsHTML(true);                               // send as HTML
	
	
	
		$mail->Subject  = 'ASLTIP Member Detail Change Request';
		$mail->addBCC(	'asltip-detail-change@contentAtoZ.co.uk');
	
		
		
		$plainString='';
		$htmlString='<h3>' . $this->member->userId . ' ' . $this->member->firstName . ' ' . $this->member->lastName . '</h3>';
		$htmlString .=  mysql_real_escape_string($this->form->getSubmitValue('message') );
		
		//echo $htmlString;
		
		
		
		//prep the variables for the include:
		//$firstName=$details['forename'];
		//$emailAddress=$details['email'];
		
		/*
		ob_start();
		include('template/asltip-password.htm');
		$htmlString=ob_get_contents();
		ob_end_clean();
		//echo $htmlString;
		
		ob_start();
		include('template/asltip-password.txt');
		$plainString=ob_get_contents();
		ob_end_clean();
		//echo $plainString;
		*/
		
		
		/*
		$mail->Body     =  	$htmlString;
		$mail->AltBody  =  	$plainString;
		
		if(!$mail->Send()){
		       $this->errorMessage="Mailer Error: " . $mail->ErrorInfo;
		       //exit;
		}
		else{
		   $this->updateMessage='Thank you for letting us have your updated details.';
		}			

	
	}				
}
	

/*

// Include QuickForm class
require_once ("HTML/QuickForm.php");
 // Instantiate the QuickForm class
        $form = new HTML_QuickForm('changePass','POST',
                                   'change-password');

        // A function for comparing password
        function cmpPass($element, $confirm) {
            global $form;
            $password = $form->getElementValue('newPassword');
            return ($password == $confirm);
        }

        // Register the compare function
        $form->registerRule('compare', 'function', 'cmpPass');

        // Add a header to the form
        //$form->addHeader('Change your Password');

        // Add a field for the old password
        $form->addElement('password','oldPassword','Current Password');
        $form->addRule('oldPassword','Enter your current password',
                       'required',false,'client');

        // Add a field for the new password
        $form->addElement('password','newPassword','New Password');
        $form->addRule('newPassword','Please provide a password','required',
                       false,'client');
        $form->addRule('newPassword','Password must be at least 6 characters',
                       'minlength',6,'client');
        $form->addRule('newPassword','Password must be 12 characters or less',
                       'maxlength',12,'client');
        $form->addRule('newPassword','Password may only contain letters and '.
                       'numbers','alphanumeric',NULL,'client');

        // Add a field for the new password
        $form->addElement('password','confirm','Confirm New Password');
        $form->addRule('confirm','Confirm your new password',
                       'compare',false,'client');

        // Add a submit button
        $form->addElement('submit','submit','Change Password');

        // If the form is submitted...
        if ( $form->validate() ) {

            // Instantiate Account Maintenance class
            $aMaint=new AccountMaintenance($dbConn);
						
            // Change the password
            if ( $aMaint->changePassword(
                        $auth,
                        $form->getSubmitValue('oldPassword'),
                        $form->getSubmitValue('newPassword')) ) {
                $successText=  'Your password has been changed successfully.';
            } else {
                $errorText=  'Error changing your password: current password not recognised.';
            }

       } else {
            // If not submitted, display the form
          
       }
*/
?>
