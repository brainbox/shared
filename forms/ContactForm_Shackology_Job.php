<?php
Class ApplicationForm extends ContactForm{

   function __construct(& $db, $emailDetails){
	            
	//Kick the parent class into action
	parent::ContactForm($db, $emailDetails);
						
	 }						
	 
	 function setForm(){           					 
          
												  
              $this->form->removeAttribute('name'); //for XHTML compliance
              $this->form->addElement('text','name','Your name',array('class'=>'text'));
             // $this->form->addElement('text','company','Organisation',array('class'=>'text'));
              $this->form->addElement('text','email','Email address',array('class'=>'text'));
              $this->form->addElement('text','phone','Contact phone number',array('class'=>'text'));
              
							$attrs = array("rows"=>"10", "cols"=>"50"); 
              $this->form->addElement('textarea','description','Message',$attrs);
							
							//The file upload:
							$this->form->addElement('file','file','Select a file',array('class'=>'text'));

							
              $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
              
              //RULES
              $this->form->addRule('name',      'Please enter your name',               'required','' ,'client');
              $this->form->addRule('email',     'Please enter your email address',      'required','' ,'client');
              $this->form->addRule('email',     'Please enter a valid email address',   'email','' ,'client');  
              $this->form->addRule('description','Please enter a short message',    'required','' ,'client');
							
	      $this->form->addRule('file', 'The file must be a Word .doc or .docx file', 'mimetype', array('application/msword', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'));
	      $this->form->addRule('file', 		'The maximum file size is 1 MB', 'maxfilesize', 1048576);
							
							//low key spam protection: no puctuation (to prevent email addresses in evey field
	      $this->form->registerRule('no_at','regex','/^[^@]+$/');
	      $this->form->addRule('name',       'The "@" character is not allowed in this field.',      'no_at'   );
            //  $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_at'   );
              $this->form->addRule('description','The "@" character is not allowed in this field.',      'no_at'   );
	      
	      $this->form->registerRule('noBBCode','function','noBBCode');
	      $this->form->addRule('company',    'The "@" character is not allowed in this field.',      'no_'   );
              $this->form->addRule('description','The "@" character is not allowed in this field.',      'no_at'   );
							
							
	      //SPAM PREVENTION
	      $this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
              $this->form->addRule('name',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('company',    'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
	      $this->form->addRule('phone',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('description','The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
							
           //    $this->form->registerRule('noBBCode','function','noBBCode');
	   //   $this->form->addRule('company',    'BBCode not allowed in this field.',      'noBBCode'   );
           //   $this->form->addRule('description','BBCode not allowed in this field.',      'noBBCode'   );
     }
		
      				
			
 }     
 
				     
?>

