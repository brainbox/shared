<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title><?php echo $title ?></title>
</head>
<body>
<style type="text/css">
<!--

html, body{  
	padding: 0;
	margin:0;
	background: none white;
}
h1{color: #62899D;font-size: 1.3em}
h2{color: #62899D;font-size: 1.1em}
a img{border:none}
.content{
	font-size: 80%;
	font-family: helvetica, sans-serif;
	color: #333;
	padding: 2em 2em 50px 20px;
	color: #333;
	background: none #fff}
.footer{
	margin-top: 20px;
	}
table{
 	border-collapse: collapse;
	font-size: 1em;
 }
td, th{padding-top: 0.5em;vertical-align: top;}	
th{  
text-align: right;
width:10em;
padding-right: 1em}				
.footer{font-size: 12px; padding: 1em}									 
-->
</style>
<div class="content">
	<?php echo $content ?>
</div>
<div class="footer">
<a href="http://www.brainboxweb.co.uk/">POWERED BY BRAINBOX</a> - INTELLIGENT WEB DEVELOPMENT - 0845 003 0025
</div>
</body>
</html>
