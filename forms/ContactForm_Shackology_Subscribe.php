<?php
Class Subscribe extends ContactForm{

   function __construct($db, $emailDetails, $dbPDO){
	            
	//Kick the parent class into action
	parent::ContactForm($db, $emailDetails);
	$this->dbPDO=$dbPDO;
					
	}						
	 
	function setForm(){           					 
          									  
              $this->form->removeAttribute('name'); //for XHTML compliance
              $this->form->addElement('text','name','Your name',array('class'=>'text'));
             // $this->form->addElement('text','company','Organisation',array('class'=>'text'));
              $this->form->addElement('text','email','Email address',array('class'=>'text'));
              $attrs = array("rows"=>"10", "cols"=>"50"); 
              $this->form->addElement('textarea','description','Add a quick message if you like',$attrs);			
              $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
              
              //RULES
              $this->form->addRule('email',     'Please enter your email address',      'required','' ,'client');
              $this->form->addRule('email',     'Please enter a valid email address',   'email','' ,'client');  
             
	      $this->form->registerRule('no_at','regex','/^[^@]+$/');
	      $this->form->addRule('name',       'The "@" character is not allowed in this field.',      'no_at'   );
	      $this->form->addRule('description',       'The "@" character is not allowed in this field.',      'no_at'   );
					
	      //SPAM PREVENTION
	      $this->form->registerRule('noEmailHeaders','function','noEmailHeaders');
              $this->form->addRule('name',       'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
              $this->form->addRule('email',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
	      $this->form->addRule('description',      'The text "Content-Type" is not allowed in this field.',      'noEmailHeaders' );
	    
	}

	function process(){
		
		//Do all the normal stuff first
		parent::process();
		
		//Next, create a new subscriber;
		
		require_once('email/Subscriber.php');
		
		$subscribers= new Subscribers($this->dbPDO);
		$subscribers->addNew(array('email'=>$this->form->getSubmitValue('email'), 'name'=>$this->form->getSubmitValue('name')));

		
	}


}
?>
