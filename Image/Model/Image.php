<?php

/**
 * attempting a many to many between images and articles
 *
 *
 *
 *
 */





class Model_Image

{
    
     public static function getAll($dbPDO )
    {
        
        
        
        $sql = "SELECT
                    *
                FROM
                    images
                      
               ";
        
        
        
        $stmnt = $dbPDO->prepare($sql);
        
        $stmnt->execute();
        
        $resultArray = $stmnt->execute();
        
        $resultArray = $stmnt->fetchAll();

        
        return $resultArray;
        
    }
    
   
    
    
    public static function getByArticleId($dbPDO, $articleId, $section=null )
    {
        
        
        
        $sql = "SELECT
                    *
                FROM
                    images
                      
                WHERE
                    1=1
                    AND article_id = $articleId ";
                    
        if($section)
        {
           $sql .= " AND section = '$section' ";
        }
        
        
        
        $stmnt = $dbPDO->prepare($sql);
        
        $stmnt->execute();
        
        $resultArray = $stmnt->execute();
        
        $resultArray = $stmnt->fetchAll();

        
        return $resultArray;
        
    }
    
    
    
    /**
     * Same filename over-writes?
     *
     *
     */
    public static function addFromPOST($dbPDO, $pathToUserFiles )
    {
        
        
        
       
        $sizeArray = getimagesize($_FILES['image']['tmp_name']);
        
       
        $filename = $_POST['articleId'] . '-' . $_FILES['image']['name'];
            
            
            //process the document
            $sql="REPLACE INTO images (
                            
                                
                                article_id
                            ,   section
                            
                            ,   filename
                           
                            
                            ,   width
                            ,   height
                            
                             ,   mimetype
                            ,   filesize
                            
                            ,   create_date
                    )
                VALUES (
                                :articleId
                            ,   :section
                            
                            ,   :filename
                            
                            ,   :width
                            ,   :height
                            
                            ,   :type
                            ,   :size
                            
                            ,   NOW()
                       
                    )
                    ";
                
            // echo $sql;
            
            ///exit;
            $stmt = $dbPDO->prepare($sql);
            $stmt->bindParam(':articleId', $_POST['articleId']);
            $stmt->bindParam(':section', $_POST['section']);
            
            $stmt->bindParam(':filename', $filename);
            $stmt->bindParam(':type', $_FILES['image']['type']);
            $stmt->bindParam(':size', $_FILES['image']['size']);
            
            $stmt->bindParam(':width', $sizeArray[0]);
            $stmt->bindParam(':height', $sizeArray[1]);
            
            $stmt->execute();
            
           
            
            $fullPath = $pathToUserFiles . '/images'  ;         
            
            
            if( isset($_POST['section']) ){
                
                $fullPath .= '/' . $_POST['section'];
                
            }
            
            
            $fullPath .= '/' . $filename ;
            
            
          
            
            move_uploaded_file($_FILES['image']['tmp_name'], $fullPath );
            
            return true;
    }
        
    /**
     *
     * Is this actually used???
     *
     *
     *
     */
    function delete($dbPDO, $id, $location )
    {
        
        //Filename must NOT be in the database;
        $sql="SELECT filename FROM
                image
                WHERE id= $id  ";
       
        //echo $sql;
        $stmt = $dbPDO->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        
        //Remove from db
         //Filename must NOT be in the database;
        $sql="DELETE FROM
                image
                WHERE id = $id  ";
       
        //echo $sql;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
     
        
        
        
        //Remove from filesystem
        unlink($location . '/' .  $result['filename']);
        
    }
    
    function deleteFromPOST($dbPDO, $pathToUserFiles )
    {        
        
        //Remove from db
         //Filename must NOT be in the database;
        $sql="DELETE FROM
                images
                WHERE id = :id  ";
       
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->bindParam(':id', $_POST['id'] );
        $stmt->execute();
     
        
        $fullPath = $pathToUserFiles . '/images';
        
        if(isset($_POST['section'])){
            
            $fullPath .= '/' . $_POST['section'];
            
        }
        
        $fullPath .= '/' .  $_POST['filename'];
        
       
        //Remove from filesystem
        unlink( $fullPath);
        
    }
    
    
}