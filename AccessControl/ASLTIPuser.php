<?php

class ASLTIPuser extends User {
   
    function ASLTIPuser (&$db) {
	
        $this->db=& $db;
        $this->populateFullDetails();
    }
    /**
    * Determines the user's id from the login session variable
    * @return void
    * @access private
    */
    function populateFullDetails() {
	    
	      $session=new Session();
       	
        $sql="SELECT * 
                 
              FROM
                  ".USER_TABLE."
              WHERE
                  ".USER_TABLE_LOGIN."='".$session->get(USER_LOGIN_VAR)."'";
        
				//echo $sql;
				
				$result=$this->db->query($sql);
        $row=$result->fetch();
	
	//print_r($row);
	
	$this->userId=$row[USER_TABLE_ID];
        $this->title=$row['title'];
        $this->firstName=$row[USER_TABLE_FIRST];
        $this->lastName=$row[USER_TABLE_LAST];
	
	$this->telHome=$row['tel_home'];
	$this->telWork=$row['tel_work'];
	$this->mobile=$row['mob_phone'];
	
        //$this->signature=$row[USER_TABLE_SIGN];
    }
    
    function title() {
        return $this->title;
    }
    function telHome() {
        return $this->telHome;
    }
    function telWork() {
        return $this->telWork;
    }
    function mobile() {
        return $this->mobile;
    }
    
    
    
    /**
    * Returns the users id
    * @return int
    * @access public
    */
    function id() {
        return $this->userId;
    }
    /**
    * Returns the users email
    * @return int
    * @access public
    */
    function email() {
        return $this->email;
    }
    /**
    * Returns the users first name
    * @return string
    * @access public
    */
    function firstName() {
        return $this->firstName;
    }
    /**
    * Returns the users last name
    * @return string
    * @access public
    */
    function lastName() {
        return $this->lastName;
    }
    /**
    * Returns the users signature
    * @return string
    * @access public
    */
    function signature() {
        return $this->signature;
    }
    /**
    * Checks to see if the user has the named permission
    * @param string name of a permission
    * @return boolean TRUE is user has permission
    * @access public
    */
   
	
		
		
		
		
}
?>