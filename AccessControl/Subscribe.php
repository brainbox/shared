<?php
/**
* @package SPLIB
* @version $Id: Signup.php,v 1.8 2003/09/23 22:31:17 harry Exp $
*/
/**
* Define constants for table
*/
# Modify these constants to match your user login and signup tables

// Name of users table
@define ( 'SUBSCRIBER_TABLE','subscriber');
// Name of email column in table
// Name of ID column in signup
@define ( 'SUBSCRIBER_TABLE_ID','id');
@define ( 'SUBSCRIBER_TABLE_EMAIL','email');
// Name of firstname column in table
@define ( 'SUBSCRIBER_TABLE_FIRST','firstName');
// Name of lastname column in table
@define ( 'SUBSCRIBER_TABLE_LAST','lastName');
// Name of comapny/org olumn table
@define ( 'SUBSCRIBER_TABLE_COMPANY', 'company');
// Name of created column in signup
@define ( 'SUBSCRIBER_TABLE_CREATED','created');


// Name of subscription table


/**
* SignUp Class<br />
* Provides functionality for for user sign up<br />
* <b>Note:</b> you will need to modify the createSignup() method if you
* are using a different database table structure<br />
* <b>Note:</b> this class requires
* @link http://phpmailer.sourceforge.net/ PHPMailer
* @access public
* @package SPLIB
*/
class Subscribe {
    /**
    * Database connection
    * @access private
    * @var object
    */
    var $db;

    /**
    * The name / address the Subscribe email should be sent from
    * @access private
    * @var array
    */
   
    /**
    * Subscribe constructor
    * @param object instance of database connection
    * @param string URL for confirming the the Subscribe
    * @param string name for confirmation email ("Your name")
    * @param string address for confirmation email ("you@yoursite.com")
    * @param string subject of the confirmation message
    * @param string the confirmation message containing <confirm_url/>
    & @access public
    */
    function Subscribe (&$db) {
        $this->db=& $db;
       
    }

    
		function getDetailsByID($userID){
		
						 
        		 $sql  = 'SELECT * FROM ' ; 
						 $sql .= SUBSCRIBER_TABLE;
						 $sql .= ' WHERE ';
             $sql .= SUBSCRIBER_TABLE_ID. '=' .$userID;
               
        $result = $this->db->query($sql);

        if ( $result->size() > 0 ) {
				   
					 $resultArray=$result->fetch();
					 return $resultArray;
				}
				else{
						 return false;
				}
		   
		
		}
		
		
    /**
    * Inserts a record into the Subscribe table
    * @param array contains user details. See constants defined for array keys
    * @return boolean true on success
    * @access public
    */
    function createSubscribe ($userDetails) {
        $email=      mysql_escape_string($userDetails[SUBSCRIBER_TABLE_EMAIL]);
        $firstName=	 mysql_escape_string($userDetails[SUBSCRIBER_TABLE_FIRST]);
        $lastName=	 mysql_escape_string($userDetails[SUBSCRIBER_TABLE_LAST]);
				$company=		 mysql_escape_string($userDetails[SUBSCRIBER_TABLE_COMPANY]);
       
        // First check name, surname and email are unique in user table
        $sql = "SELECT * FROM
                   ".SUBSCRIBER_TABLE."
                WHERE
                   ".SUBSCRIBER_TABLE_FIRST."='".$firstName."'
                AND
								   ".SUBSCRIBER_TABLE_LAST."='".$lastName."'
                AND
                   ".SUBSCRIBER_TABLE_EMAIL."='".$email."'";
        $result = $this->db->query($sql);

        if ( $result->size() > 0 ) {
           
					 //echo 'Welcome back!';
					 
					 $resultArray=$result->fetch();
					 $this->userID= $resultArray['id'];
					 
					 //Update the COMAPNY name if it's not null
					 if ($company !=''){
					 
					     //echo 'comapny is ' . $company;
    					 
							 
							     					 
    					 $sql  = 'UPDATE '.SUBSCRIBER_TABLE;
    					 $sql .= ' SET ' .SUBSCRIBER_TABLE_COMPANY. ' ="'.$company.'"';
    					 $sql .= ' WHERE '. SUBSCRIBER_TABLE_ID. ' = '. $resultArray['id'];
    					 
    					// echo $sql;
               $result = $this->db->query($sql);    
					 }
					 
        }
				else{

            //  $this->createCode($firstName . $lastName . $email);
            $toName=$firstName.' '.$lastName;
            $this->to[$toName]=$email;
    
            $sql="INSERT INTO ".SUBSCRIBER_TABLE." SET
                   
                    ".SUBSCRIBER_TABLE_FIRST."='".$firstName."',
                    ".SUBSCRIBER_TABLE_LAST."='".$lastName."',
										".SUBSCRIBER_TABLE_COMPANY."='".$company."',
										".SUBSCRIBER_TABLE_EMAIL."='".$email."',
                   
                    ".SUBSCRIBER_TABLE_CREATED."='".time()."'";
           
					 // echo $sql;
		
		
            $result=$this->db->query($sql);
						
						$this->userID= $result->insertID();
    
            if ( $result->isError() )
                return false;
            else
                return true;
        		}
		}    
		
		
		
		
		
   
}

// Name of users table
@define ( 'SUBSCRIBER_TABLE','subscriber');
@define ( 'SUBSCRIBER_TABLE_ID','id');
@define ( 'SUBSCRIBER_TABLE_EMAIL','email');
@define ( 'SUBSCRIBER_TABLE_FIRST','firstName');
@define ( 'SUBSCRIBER_TABLE_LAST','lastName');
@define ( 'SUBSCRIBER_TABLE_COMPANY', 'company');
@define ( 'SUBSCRIBER_TABLE_CREATED','created');

@define ( 'XREF_TABLE' 			 				 ,'subscriber_whitepaper_xref');
@define ( 'XREF_TABLE_SUBSCRIBERID'  ,'subscriber_id');
@define ( 'XREF_TABLE_WHITEPAPERID'  ,'whitepaper_id');
@define ( 'XREF_TABLE_CODE'					 ,'code');
@define ( 'XREF_TABLE_CREATEDATE'		 ,'create_date');

 	

Class Whitepaper {
       
			 var $db;

    /**
    * The name / address the Subscribe email should be sent from
    * @access private
    * @var array
    */
			  var $from;

    /**
    * The name / address the Subscribe email should be sent to
    * @access private
    * @var array
    */
    var $to;

    /**
    * The subject of the confirmation email
    * @access private
    * @var string
    */
    var $subject;

    /**
    * Text of message to send with confirmation email
    *
    * @var string
    */
    var $message;

    /**
    * Whether to send HTML email or not
    * @access private
    * @var boolean
    */
    var $html;

    /**
    * Url to use for confirmation
    * @access private
    * @var string
    */
    var $listener;

    /**
    * Confirmation code to append to $this->listener
    * @access private
    * @var string
    */
    var $confirmCode;


       function Whitepaper( & $db ) {
            
				    $this->db=& $db;
           
				
      }
			 function setWhitepaper($whitepaperShortName){
			
			  $this->whitepaper=$whitepaperShortName;
			   	// Construct a query using the submitted values
        $sql   = ' SELECT * from whitepaper WHERE short_name= "'.$whitepaperShortName.'"';
				//echo $sql;	
							 
        // Perform the query
       $result=$this->db->query($sql);
			
			 $this->whitepaperData=$result->fetch();
			 $this->whitepaperID= $this->whitepaperData['id'];
			}
			
			function requestDownload($code){

			    $this->whitepaperCode = $code;
					
					$sql    = ' SELECT '; 
					$sql   .= ' whitepaper.name AS whitepaper_name, ';
					$sql   .= ' filename, ';
					$sql   .= ' firstname, ';
					$sql   .= ' lastname, ';
					$sql   .= ' email, ';
					$sql   .= ' subscriber_whitepaper_xref.create_date AS code_create_date ';
					$sql   .= ' FROM whitepaper, subscriber, subscriber_whitepaper_xref ';
					$sql   .= ' WHERE ';
					$sql   .= ' subscriber_whitepaper_xref.subscriber_id = subscriber.id AND';
					$sql   .= ' subscriber_whitepaper_xref.whitepaper_id = whitepaper.id AND';
					$sql   .= '  code= "'.$code.'"';
					
					
				 // echo '<hr>' . $sql;	
							 
			    $result=$this->db->query($sql);
			    //echo '<pre>';
					
					
					
					$this->whitepaperData= $result->fetch();
					
					//test if the code is still fresh
					
				//	echo print_r( $this->downloadArray);
					
					//echo '<p>the time' .time();
					//echo '<p>the code create date' . $this->downloadArray['code_create_date'];
					
					
					$timeDifference =  time()- ($this->whitepaperData['code_create_date']);
					//echo '<p>DIFFERENCE: ' .$timeDifference;
					
					if( $timeDifference < 7*60*60*24){				  
              return true;
          }
					else{
              return false;
      		} 
					
			}

			
			function getWhitepaperDetails(){
			   
				 if(isset($this->whitepaperData)){
				    return $this->whitepaperData;
				 }
				 else{
				    return false;
				 }
			
			}
			function sendMagicLink(& $subscriber,$listener,$frmName,$frmAddress,$subj,$msg){
			
			      //instantiate a new subscriber class
            $this->subscriber= & $subscriber;
					  
						//get the full list of subscriber details using the subscriber class
						
					
			      $this->listener=$listener;
        		$this->frmName=$frmName;
						$this->frmAddress=$frmAddress;
        		$this->subject=$subj;
        		$this->message=$msg;
        		
						//echo '<pre>';
						//print_r($this->message);
			
			   //Create the code from the whitepaper name and the time...		
			   $this->createCode($this->whitepaper . time()  );
			   
			   //echo '<p>USER id is: ' .$this->subscriber->userID;				 
				 //echo '<p>WHITEPAPER id is: ' .$this->whitepaperID;
				 
				 $sql  = 'INSERT INTO ' .XREF_TABLE. ' SET ';           
         $sql .= XREF_TABLE_SUBSCRIBERID. '= '   . $this->subscriber->userID       . ', ';
         $sql .= XREF_TABLE_WHITEPAPERID. '= '   . $this->whitepaperID . ', ';
				 $sql .= XREF_TABLE_CODE. 				' = "' . $this->confirmCode  . '", ';         
         $sql .= XREF_TABLE_CREATEDATE. '=' .time();
           
			   //echo $sql;


        $result=$this->db->query($sql);
				
				$this->sendConfirmation();
				
				//$this->userID= $resultArray->insertID();
  
          
						
			}
			
			 function sendConfirmation () {
        				
					require_once 'phpmailer/class.phpmailer.php';	
					$mail = new phpmailer();
        	
					$subscriberDetails=$this->subscriber->getDetailsByID($this->subscriber->userID);
					   
					//echo ('<p>llllllllllllllllllllll');
				  //print_r( $subscriberDetails);

											
          $mail->addAddress(   	  $subscriberDetails['email']    );
          
          $mail->addBCC(					$this->frmAddress );
					
          $mail->From       =     $this->frmAddress;
          $mail->FromName   =     $this->frmName;
          $mail->AddReplyTo =     $this->frmAddress;
				
				  //echo  $subscriberDetails['email'] ;
				
        $mail->Subject= $this->subject;
				
				//fixt the message bodies
				
				$replace=$this->listener.'?code='.$this->confirmCode;
				$this->message['text'] = str_replace(
															 	   '<confirm_url />',
                                   $replace,
                                   $this->message['text']);
				
				
								
				$replace='<a href="'.$this->listener.'?code='. $this->confirmCode.'">'.$this->listener.'?code='. $this->confirmCode.'</a>';
        $this->message['HTML'] = str_replace(
															 	   '<confirm_url />',
                                   $replace,
                                   $this->message['HTML']);
       
			  $this->message['text'] = str_replace(
															 	   '<firstName />',
                                   $subscriberDetails['firstName'] ,
                                   $this->message['text']);
        $this->message['HTML'] = str_replace(
															 	   '<firstName />',
                                   $subscriberDetails['firstName'] ,
                                   $this->message['HTML']);
			  
				//print_r($this->downloadArray);
				
				
        $this->message['text'] = str_replace(
															 	   '<whitepaperName />',
                                   $this->whitepaper ,
                                   $this->message['text']);
        $this->message['HTML'] = str_replace(
															 	   '<whitepaperName />',
                                   $this->whitepaper ,
                                   $this->message['HTML']);    
						
						
						   
        $mail->IsHTML(true);
        
				
				//echo  '<hr>' .$this->message['HTML'];
				//echo  '<hr>' .$this->message['text'];
				
				
        $mail->Body=        $this->message['HTML'];
				$mail->AltBody  =  	$this->message['text'];
        
				//echo $mail->Body;
				
				if ( $mail->send() ){
				    //echo "email sent";
            return TRUE;
        }
				else{  
				
				   //echo "sending of email failed";
            return FALSE;
				}
    }
			
			
			
					
			/**
    * Confirms a Subscribe against the confirmation code. If it
    * matches, copies the row to the user table and deletes
    * the row from Subscribe
    * @return boolean true on success
    * @access public
    */
	  function confirm ($confirmCode) {
        $confirmCode = mysql_escape_string($confirmCode);
        $sql="SELECT *
              FROM
                  ".SUBSCRIBER_TABLE."
              WHERE
                  ".SUBSCRIBER_TABLE_CONFIRM."='".$confirmCode."'";
									
			  						
									
									
        $result=$this->db->query($sql);
        if ( $result->size() == 1 ) {
            $row=$result->fetch();

            // Copy the data from Subscribe to User table
            $sql="INSERT INTO ".SUBSCRIBER_TABLE." SET
                    ".SUBSCRIBER_TABLE_LOGIN."='".mysql_escape_string($row[SUBSCRIBER_TABLE_LOGIN])."',
                    ".SUBSCRIBER_TABLE_PASSW."='".mysql_escape_string($row[SUBSCRIBER_TABLE_PASSW])."',
                    ".SUBSCRIBER_TABLE_EMAIL."='".mysql_escape_string($row[SUBSCRIBER_TABLE_EMAIL])."',
                    ".SUBSCRIBER_TABLE_FIRST."='".mysql_escape_string($row[SUBSCRIBER_TABLE_FIRST])."',
                    ".SUBSCRIBER_TABLE_LAST."='".mysql_escape_string($row[SUBSCRIBER_TABLE_LAST])."',
                    ".SUBSCRIBER_TABLE_SIGN."='".mysql_escape_string($row[SUBSCRIBER_TABLE_SIGN])."'";
            
            $result=$this->db->query($sql);
            if ( $result->isError() ) {
               return FALSE;
            } else {
                // Delete row from Subscribe table
                $sql="DELETE FROM ".SUBSCRIBER_TABLE." WHERE ".SUBSCRIBER_TABLE_ID."='".
                        $row[SUBSCRIBER_TABLE_ID]."'";
                $this->db->query($sql);
                return TRUE;
            }
        } else {
            return FALSE;
        }
    }
			
			
			
			
		  /**
    * Creates a random string to be used in images
    * @return string
    * @access public
    */
		function createRandString () {
        srand((double)microtime()*1000000); 
        $letters=range ('A','Z');
        $numbers=range(0,9);
        $chars=array_merge($letters,$numbers);
        $randString='';
        for ( $i=0;$i<8;$i++ ) {
            shuffle($chars);
            $randString.=$chars[0];
        }
        return $randString;
    }

    /**
    * Creates the confirmation code
    * @return void
    * @access private
    */
    function createCode ($login) {
        srand((double)microtime()*1000000); 
        $this->confirmCode=md5($login.time().rand(1,1000000));
    }
		
		
		
}

?>