<?php
/**
* @package SPLIB
* @version $Id: FullTree.php,v 1.1 2003/12/12 08:06:02 kevin Exp $
*/
/**
*  FullTree clas
* <pre>
*  Home
*    - About
*    - Contact
*  Products
*    - Pets
*      - Cats
*      - Birds
*    - Books
*  Etc
* </pre>
* @access public
* @package SPLIB
*/
class FullTree extends Menu {
    
    
    protected $location='';
    
    /**
    * Stores the current location item
    * @access private
    * @var  object
    */
    var $current;

    /**
    * FullTree constructor
    * @param object database connection
    * @param string URL fragment
    * @access public
    */
    function FullTree ($db,$isActive=true,$includeOrphans=true) {
        
       
        Menu::Menu($db,$isActive, $includeOrphans);
         
        $this->current=$this->locate($this->location);
        
      
        $this->build();
    }

    /**
    * Resursive function that adds any children to the
    * current node
    * @param object instance of MenuItem
    * @return void
    * @access private
    */
    function appendChildren ($treeItem) {
        foreach ( $this->items as $item ) {
            
           // echo '<hr>tree id= ' . $treeItem->id();
           // echo 'parent id ' . $item->parent_id();
            //This isn't working correctly - getting START+ED where there should be nothing.
            
            if ( $treeItem->id() == $item->parent_id() ) {
                $this->menu[]=new Marker('start');
                if ( $item->id() == $this->current->id() )
                    $item->setCurrent();
                $this->menu[]=$item;
                $this->appendChildren($item);
                $check=end($this->menu);
                if ( $check->isStart() ) //if the last was a start, pop it off
                    array_pop($this->menu);
                else
                    $this->menu[]=new Marker('end');
            }
        }
    }

    /**
    * Starts construction, building the root elements
    * @return void
    * @access private
    */
    function build () {
        #echo '<pre>';
        #print_r($this->items);
         # echo 'lll';
        #exit;
        
        $counter=1;
        foreach ( $this->items as $item ) {
            
            $counter++;
            #echo $counter;
            #sleep(1);
            #exit;
            
            if ( $item->isRoot() ) {
                if ( $item->id() == $this->current->id() )
                    $item->setCurrent();
                $this->menu[]=$item;
                $this->appendChildren($item);
            }
        }
        reset ( $this->menu );
    }
}
?>