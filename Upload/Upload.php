<?php

Class Upload{
    
    public $errors=array();
    public $name;  //The field name 
    public $path;
    public $filename;
    
    function __construct($name){
        
        $this->name=$name;
        
        $this->filename=$_FILES[$name]['name'];
        
        #$this->checkFileType();
        #$this->checkFileSize();
        
    }
    
    function checkFileType(){
        
        $allowed = array();
        $allowed[] = 'application/msword';
        $allowed[] = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
        $allowed[] = 'application/pdf';
        $allowed[] = 'applicaton/pdf';
        
        
        
        if(!in_array($_FILES[$this->name]['type'],$allowed) ){
            $this->errors[] = 'Resume must be either a Word document or a PDF';   
        }    
        
        
    }
    
    function checkFileSize(){
        
        if ( $_FILES[$this->name]['size'] >  (2097152) ){
             $this->errors = 'The maximum accepted file size is 2MB';
        }    
        
    }
    function setPath($path){
        
        $this->path = $path;
        
    }
    
    
    
    function getErrors(){
        
        return $this->errors;
        
    }
    
     function store($path, $newBaseName=null){
        
        if($newBaseName){
            
            $filename=$this->_constructFilename($newBaseName);
            
        } else {
            
            $filename=$this->filename;
        }
        
        #echo $_FILES[$this->name]['tmp_name'];
        #exit;
        
        $destination = $path . $filename;
        
        #echo $destination;
        #exit;
        
        
        move_uploaded_file($_FILES[$this->name]['tmp_name'],$destination);
            
        return $filename; 
        
        
    }
    
    function _constructFilename($baseName){
        
        #Need to find the right extension
        $tempFileInfo=pathinfo($_FILES[$this->name]['name']);
        
        #print_r($tempFileInfo);
        
        $extension=$tempFileInfo['extension'];
        
        return $baseName . '.' . $extension;
       
        
    }
    
    
    function processOLD(){
        
        if(!count($this->errors) ){
            
            #echo '<p>Moving file to ' . $this->filePath . $this->filename;
            move_uploaded_file($_FILES[$this->name]['tmp_name'],$this->filePath . $this->filename);
            
            return true;
            
            
        } else {

            return false;
        
        }
        
        
    }
    
    
    
    
}