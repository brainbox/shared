<?php


Class Form_Send_Email {

    public $id;
    public $page_url;
	public $page_title;
    public $senders_email;
	public $recipients_email;
    public $subject;
    public $message;

 	   
    #private $insertID;
    

    public function __construct(){
    
    }
   
  /*  public function __set($name, $value) {
        echo 'SETTING ' . $name . ' TO ' . $value;
        if (isset($this->$name)) {
            
           $this->$name = $value;
        
        } else {
            
            return false;
        }
    }

    public function __get($name) {
        
         echo 'GETTING ' . $name;
        if (isset($this->$name)) {
            
            return $this->$name;
        
        } else {
            
            return false;
        }
    }
    */
     public function populate($row){
	
        $this->id =                 $row['id'];
        $this->senders_email =      $row['senders_email'];
        $this->recipients_email =   $row['recipients_email'];
        $this->subject =          	$row['subject'];
        $this->message =   			$row['message'];
        $this->page_url =     		$row['page_url']; 
		$this->page_title =     	$row['page_title']; 

	 	$this->create_date = $row['create_date'];
    }
 
    public function populateFromPost(){
	
        #echo 'POPULATING FROM PST';
        $this->type=INPUT_POST; 
	
        $this->senders_email =      trim(filter_input($this->type, 'senders_email',     FILTER_SANITIZE_STRING) );
        $this->recipients_email =   trim(filter_input($this->type, 'recipients_email',  FILTER_SANITIZE_STRING) );
        $this->subject =            trim(filter_input($this->type, 'subject',        	FILTER_SANITIZE_STRING) );
        $this->message =        	trim(filter_input($this->type, 'message', 	     	FILTER_SANITIZE_STRING) );
        
		$this->page_url =        	trim(filter_input($this->type, 'page_url',      	FILTER_SANITIZE_STRING) );
		$this->page_title =        	trim(filter_input($this->type, 'page_title',      	FILTER_SANITIZE_STRING) );	
    }
    
    /**
     * Dumps the public functions into a table
     *
     */
    
    function __toString () {
        
        foreach (get_class_vars(get_class($this)) as $name => $value) {
            
            if($name == 'id'){
                continue;
            }
            
            #Attempt to clean up the name
            $tidyName = str_ireplace('_', ' ', $name);
            $tidyName = ucfirst( $tidyName  );
            
           // $s .= "$tidyName: " . $this->$name . "<br/>";
        }
		
		if ($this->page_title == ""){
			$this->page_title = "Nakheel Asset Management";
		}
		
        $s = "";
        $s .= "Your friend wants you to check out <a href='". $this->page_url . "'>". $this->page_title ."</a>, it's a really interesting / useful page on the Nakheel Asset Management web site.";
        
		 
		$s .= "<p>". $this->message . "</p>";
        
        $s .= '<p style="font-size: 8pt">' . date('m-d-Y H:i:s') . ' | ' . $this->id;
		$s .= '</p>'; 
        #echo $s;
        #exit;
        
        return $s;
    
    }

    
    /*
    public function createRecord ($dbPDO){
        
        #echo 'creating record';
        
        require_once('Form_Feedback/Model/Form_Feedback.php');
        $applicationModel = new Model_JobApplication($dbPDO);
        
        $applicationModel->create($this);
        
    }
    
    //Not yet implemented in the Model
    public function updateRecord($dbPDO){
        
        require_once('model/Form_Feedback.php');
        $applicationModel = new Model_Application($dbPDO);
        
        $applicationModel->update($this);
        
        
    }
    */
    
    private function _buildDate($suffix){
        
        #Need all three elements to progress...
        if($_POST['year_of_' . $suffix]   &&   $_POST['month_of_' . $suffix]   &&   $_POST['day_of_' . $suffix] ){
            
            return  trim(filter_input($this->type, 'year_of_'  . $suffix, FILTER_SANITIZE_STRING) ) . "-" .
                    trim(filter_input($this->type, 'month_of_' . $suffix, FILTER_SANITIZE_STRING) ) . "-" .
                    trim(filter_input($this->type, 'day_of_'   . $suffix, FILTER_SANITIZE_STRING) );
          
        } else{
            
            return 0;
            
        }        
    }
    
}