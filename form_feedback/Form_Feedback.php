<?php


Class Form_Feedback {

    public $id;
    public $full_name;
    public $mobile_number;
	public $email;
    public $customer_number;
    public $property_code;
    public $subject;
    public $subject_2;
    public $message;
   
    #private $insertID;
    

    public function __construct(){
    
    }
    /*
    public function __set($name, $value) {
        echo 'SETTING ' . $name . ' TO ' . $value;
        if (isset($this->$name)) {
            
           $this->$name = $value;
        
        } else {
            
            return false;
        }
    }

    public function __get($name) {
        
         echo 'GETTING ' . $name;
        if (isset($this->$name)) {
            
            return $this->$name;
        
        } else {
            
            return false;
        }
    }
    */
     public function populate($row){
	
        $this->id =                 $row['id'            ];
        $this->full_name =          $row['full_name'            ];
        $this->mobile_number =      $row['mobile_number'       ];
        $this->email =          	$row['email'        ];
        $this->customer_number =    $row['customer_number'      ];
        $this->property_code =      $row['property_code'    ]; 
        $this->subject =        	$row['subject'   ];
        $this->subject_2=           $row['subject_2'   ];
        $this->message =            $row['message'             ];

	    $this->create_date = $row['create_date'];
    }
 
    public function populateFromPost(){
	
        #echo 'POPULATING FROM PST';
        $this->type=INPUT_POST; 
	
        $this->full_name =              trim(filter_input($this->type, 'full_name',            FILTER_SANITIZE_STRING) );
        $this->mobile_number =          trim(filter_input($this->type, 'mobile_number',       FILTER_SANITIZE_STRING) );
        $this->email =              	trim(filter_input($this->type, 'email',        FILTER_SANITIZE_STRING) );
        $this->customer_number =        trim(filter_input($this->type, 'customer_number',      FILTER_SANITIZE_STRING) );
         
        $this->property_code =          trim(filter_input($this->type, 'property_code',   FILTER_SANITIZE_STRING) );
        $this->subject=          		trim(filter_input($this->type, 'subject',   FILTER_SANITIZE_STRING) );
        $this->subject_2 =              trim(filter_input($this->type, 'subject_2',             FILTER_SANITIZE_STRING) );
        $this->message =                trim(filter_input($this->type, 'message',            FILTER_SANITIZE_STRING) );
    }
    
    /**
     * Dumps the public functions into a table
     *
     */
    
    function __toString () {
        
        #echo $this->title;
        
        $s = "";
        $s .= "<table>\n";
        #$s .= "<tr><td colspan=2><hr></td></tr>\n";
        
        foreach (get_class_vars(get_class($this)) as $name => $value) {
            
            if($name == 'id'){
                
                continue;
            }
            
            #Attempt to clean up the name
            $tidyName = str_ireplace('_', ' ', $name);
            $tidyName = ucfirst( $tidyName  );
            
            
            $s .= "<tr><td>$tidyName</td><td>" . $this->$name . "</td></tr>\n";
        }
        
        #$s .= "<tr><td colspan=2><hr></td></tr>\n";
        $s .= "</table>\n";
        
        $s .= '<p style="font-size: 8px">' . date('m-d-Y H:i:s') . ' | ' . $this->id; 
        #echo $s;
        #exit;
        
        return $s;
    
    }

    
    
    public function createRecord ($dbPDO){
        
        #echo 'creating record';
        
        require_once('Form_Feedback/Model/Form_Feedback.php');
        $feedbackModel = new Model_Feedback($dbPDO);
        
        $feedbackModel->create($this);
        
    }
    
    //Not yet implemented in the Model
    public function updateRecord($dbPDO){
        
        require_once('Form_Feedback/Model/Form_Feedback.php');
        $feedbackModel = new Model_Feedback($dbPDO);
        
        $feedbackModel->update($this);
        
        
    }
    
    /*
    public function storeFile($field,$filepath){
        
         #Attachment?
        if($_FILES[$field]['size']){
           
            //Create a prefix for the uploaded file
            $prefix=$this->insertID  . '_' .
                    trim(filter_input($this->type, 'first_name', FILTER_SANITIZE_STRING) ) . '_' . 
                    trim(filter_input($this->type, 'last_name', FILTER_SANITIZE_STRING) );
            echo $prefix;
            $filename=$prefix . $_FILES[$field]['name']; 
            
            
            
            move_uploaded_file($_FILES[$field]['tmp_name'], $filepath . $filename);
            
        }    
    
        
    }
    */
    
    
    private function _buildDate($suffix){
        
        
        
        #Need all three elements to progress...
        if($_POST['year_of_' . $suffix]   &&   $_POST['month_of_' . $suffix]   &&   $_POST['day_of_' . $suffix] ){
            
            return  trim(filter_input($this->type, 'year_of_'  . $suffix, FILTER_SANITIZE_STRING) ) . "-" .
                    trim(filter_input($this->type, 'month_of_' . $suffix, FILTER_SANITIZE_STRING) ) . "-" .
                    trim(filter_input($this->type, 'day_of_'   . $suffix, FILTER_SANITIZE_STRING) );
          
            
        } else{
            
            return 0;
            
        }
        
        
        
    }
    
    
}