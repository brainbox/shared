<?php


Class Model_Feedback {

    public $id;
    public $full_name; 
    public $mobile_number;
    public $email;
    public $customer_number;
    public $property_code;
    public $subject;
    public $subject_2;
    public $message;
   
    public $create_date;
    
    
    public function __construct($dbPDO){
        
        $this->dbPDO = $dbPDO;
    }
    
    /**
     * takes the Application object and uses it to update the database
     */ 
    
    public function update($feedback){
    
    }
    
        /**
     * takes the Feedack object and uses it to create a new Feedback record in the database
     *
     *
     * 
     */ 
    
    public function create($feedback){
	
        #echo 'Creating in model';
        
	$this->feedback=$feedback;


        $sql = "INSERT INTO feedback_submissions(
                        
                        `full_name`, 
                        `mobile_number`, 
                        `email`, 
                        `customer_number`, 
                        `property_code`, 
                        `subject`, 
                        `subject_2`, 
                        `message`
                        
                ) VALUES (
                        
                    '" . $feedback->full_name           . "', 
                    '" . $feedback->mobile_number       . "', 
                    '" . $feedback->email               . "',
                    '" . $feedback->customer_number     . "', 
                    '" . $feedback->property_code       . "', 
                    '" . $feedback->subject             . "',
                    '" . $feedback->subject_2           . "',
                    '" . $feedback->message             . "'
					
                )";

        #NOTE: make sure to check the NUMBER validations above
        #echo $sql;
    
        
       
	$stmnt=$this->dbPDO->prepare($sql);
	$stmnt->execute();
        
        #Get the id for the Applicaiton object
        
        $insertID = $this->dbPDO->lastInsertId();
        
        #echo 'insertedID = ' . $insertID;
        
        $feedback->id = $insertID;
	
        $feedback->create_date = date('Y-m-d H:i:s'); #Will come from the database for other calls.
        
        
	#print_r($stmnt->errorInfo() );
	
	#For testing;
    /*
	echo '<p>reading';
	$sql = "SELECT * from feedback";
	
	$stmnt=$this->dbPDO->prepare($sql);
	$stmnt->execute();
	
	$results = $stmnt->fetchAll();
	
	print_r($results);
       
    */
    }
    
     public function getItems(){
        
        $sql='SELECT * from feedback_submissions';
        #echo $sql;
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
    
    }
    
    
    public function fetchAll(){
        
        return $this->stmnt->fetchAll();
        
    }
    
    public function fetch(){
        
        return $this->stmnt->fetch();
        
        
    }

    
    
}