<?php
Class webPage{

    function webPage($baseURL,& $db, & $pathVars){
		    
				//grab the info
				$this->baseURL=$baseURL;
				$this->db = & $db;
				$this->pathVars = & $pathVars;
				
				
				//sent string
				$this->section='';
				$this->topNavString='';
				$this->bodyString='';
				$this->title='';
				$this->navString='';
			
					
				//get the pageID
				$this->_getPageID();
				//get the page details
				$this->_getPageDetails();
				//get the Top Nav
			  $this->_getTopNav();
				//get the Nav
			  $this->_getNavigation();
				
				
				
		     
				
		}
		
		function setTitle($title){
		   
			 $this->title=$title;
		
		}
		
		function _getPageID(){
		   
			  echo 'in the webpage class';
			  print_r($this->pathVars);
			 
			 
			  if ($this->pathVars->fetchByIndex(0) =='') {
             echo '<h1>It is the HOME PAGE</h1>';
						 
						 $this->pageID=1;
        }
        else{
				     //check that the section AND the ID - if present - and get the ID... all in a oner
						 
						 $sql  = 'SELECT id, published, depublish FROM articles ';
						 $sql .= ' WHERE section= "'.  $this->pathVars->fetchByIndex(0) . '"';
						
						 
						 //extend the SQL depending on whether there's a subpage
						 if ($this->pathVars->fetchByIndex(1) == ''){ 
						 	$sql .= ' AND parent_id= 1'; 
						 }
						 else{
								$sql .= ' AND id= '.  $this->pathVars->fetchByIndex(1); 
								
								//Need to know if subpages are active
								$time=time();
								$sql .= ' AND published <= '.$time.' AND depublish > '.$time ;			
						 }		 
								 
						 echo 'ffffffff'. $sql;
						 
						
						 $result = $this->db->query($sql);
          	 if(($result->size())!=0){
						 			 //echo ' FOUND ONE!!';
									 $this->section=$this->pathVars->fetchByIndex(0);
									 //echo 'so the section is ' . $this->section;
									 $resultArray=$result->fetch();
						       $this->pageID= $resultArray['id'];
						 }else{
			       			// echo '404 time';
									//should we be
									header("HTTP/1.0 404 Not Found");
    					 		 $this->pageID=0; //need to make sure that 0 is the 404 page
											      
						 }

        }
		 }
		 function _getPageDetails(){  
		   
		      echo '<h1>Getting details for '.$this->pageID .'</h1>'; 
					
					
    		  $sql = 'SELECT title, body, isHTML FROM articles where id= '.   $this->pageID ;
    		  $result = $this->db->query($sql);
    		  $resultArray=$result->fetch();
			
			  
		      $this->title= $resultArray['title'];
  				$this->pageHeading= '<h1>' . $resultArray['title'] . '</h1>';
					$this->bodyString= $resultArray['body'];
    		  
					//Apply BBCode if necessary
					if($resultArray['isHTML'] == FALSE){
					    
							 $this->_applyBBCode();
					
					}
				
				
				
		}
		function _getTopNav(){
		
					$sql = 'SELECT id, title, section FROM articles where parent_id=1 ORDER BY publish DESC sortorder' ;
    		  $result = $this->db->query($sql);
    		  		
			    while ( $row = $result->fetch() ) {
             //print_r($row);
						 $this->topNavString .= '<dd><a href="' . $this->baseURL . $row['section'] . '/"';
						    if($row['section'] == $this->section){
								     $this->topNavString .= ' class="current" ';
								}
						 $this->topNavString .=		'>' . $row['title']. '</a></dd>';
          }  
			    
		   
		}
		function _getNavigation(){
		
							require_once ('UI/Menu.php');
							require_once ('UI/ContextMenu.php');

              //echo 'gootttt heeeeeeeeeeeeeeeee';
              $menu=& new ContextMenu($this->db,$this->pageID); // A menu in context with location
              
						//	echo '<pre>';        
							//print_r($menu);							
														
														
              // Loop through the items
              while ( $item = $menu->fetch() ) {
							    
									//Only what the non-sections
									//echo 'gootttt heeeeeeeeeee555555555eeeeee';
									
							    if($item->parent_id() != 1){
							   
                      if ( $item->isStart() ) {
                          // This is a Marker to show the START of a submenu
                  				$this->navString .='<ul>';
                      } else if ( $item->isEnd() ) {
                          // This is a Marker to show the END of a submenu
                  				$this->navString .= '</ul>';
                      } else {
                          if ( $item->isCurrent() ) {
                              // This is the menu item corrsponding to the current location
                          }
                  				
                          $item->id(); // The menu_id from table menu (should not be needed)
                          $item->parent_id(); // The parent_id from table menu ( not needed)
                          $item->name(); // The name field from table menu
                          $item->description(); // The description field from table menu
                          $item->location(); // The location field from table menu
                  				
                  				$this->navString .= '<li><a href="'.$this->baseURL. $this->section .'/'. $item->location().'"';
                  				if ($item->location() == $this->pageID){
                  				   //echo "current";
                  					 $this->navString .= ' id="navselected"';
                  				}
                  				$this->navString .= '">' .$item->name().'</a></li>';				
                      }
									}		
              }
       
		
		}
		
		function _applyBBCode() {
		
		   	    /* require PEAR and the parser */
            require_once('PEAR.php');
            require_once('HTML/BBCodeParser.php');
            				
    				$tempString = $this->bodyString;		
    		    //$tempString = htmlspecialchars($tempString);
    				$tempString = htmlentities($tempString, ENT_QUOTES);//an experiment 
    				$parser = new HTML_BBCodeParser(
    				            array(  'quotestyle'    => 'single',
                                'quotewhat'     => 'all',
                                'open'          => '[',
                                'close'         => ']',
                                'xmlclose'      => true,
                                'filters'       => 'Basic,Extended,Links,Images,Lists,Email'
                             )
    												 );
            $parser->setText($tempString);
            $parser->parse();
						
            $parsed = $parser->getParsed();
    				
						
									
    				//Replace double linebreaks with double breaks
    				$parsed = str_replace("\r\n\r\n","</p>\n<p>",$parsed);
    				$parsed = str_replace("</ul>","</ul>\n<p>",$parsed);
    				$parsed = str_replace("</ol>","</ol>\n<p>",$parsed);
    				
    				//top and tail it
    				$parsed ="\n<p>".$parsed ."</p>\n";
    				
						   //echo $parsed;
							 
							 //EXPERIMENT
							 
							
							 $lines = explode("\r\n",$parsed);

						
						     
                  //print_r($lines);
                  
                  
                  $newlines = array();
                  
                  foreach($lines as $name=>$value){
                  
									 // echo "<p>Lenth of line is" .strlen($value);
                    if (strlen($value)>1){
									       
											//	 echo "<p>first charcter is ".$value{1};
												// echo "<p>last charcter is ".$value{strlen($value)-2};
											//	 	
										   
                  	 
                        	if ($value{0}!='<'){
                        	    //$value=  '<p>'.$value;
															
															$value='<p>'.$value;
                        	}
                        	
                        	if($value{strlen($value)-1}!='>'){
                        	    $value=  $value.'</p>';
                        	} 
                  	       
                  				  $newlines[]=  $value;
                  	}
									}	
                  $parsed = implode("\r\n",$newlines);						
						
						
						$this->bodyString= $parsed;
				
				
		}
		
		
		
		
	  function addToPage($element){
		   
			 $this->bodyString .= $element;
		
		}
}



Class QueensgateWebpage extends WebPage{

    function QueensgateWebPage($baseURL,& $db, & $pathVars){
		    
				//call the parent constructor
				parent::WebPage($baseURL,$db,$pathVars);
				
				$this->eventsString='';
				
		}
		
		function getPage(){
		
		  $pageString=          '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
      $pageString .=        '<html>';
			$pageString .=    		'<head>';
			$pageString .=    		'<title>Queen\'s Gate School PTA | '.$this->title.'</title>';
			$pageString .=    		'<meta name="author" content="Gary Straughan | Brainbox" />';
			$pageString .=    		'<meta http-equiv="content-language" content="en" />';
			$pageString .=    		'<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />';
			$pageString .=    		'<link href="'.$this->baseURL.'css/s.css" type="text/css" rel="stylesheet" />';
			$pageString .=    		'</head>';
			$pageString .=    		'<body>';
			
			
			
			
		
       
			 						
      $pageString .=    	  '<div id="header">';
			$pageString .=    	  '<a href="'.$this->baseURL.'" title="Click to return to the Home Page"><img src="'.$this->baseURL.'images/logo.png" alt="Click to return to the Home Page" width="111"  height="81" /></a>';
    //  $pageString .=    	  '<div class="spacer">&nbsp;</div>';
      $pageString .=    	  '</div>';
    //  $pageString .=    	  '<table summary="layout" class="layouthome">';
    //  $pageString .=    	  '<tr>';
    //  $pageString .=    	  '<td class="col1">';
     
		 
		 
		  $pageString .=    	  '<div id="bigbox1">';
			$pageString .=        '<dl id="topnav">';
			$pageString .=        $this->topNavString;
			$pageString .=        '</dl>';
		 
      $pageString .=    	  '<div id="centercontent1">';
			
			
      $pageString .=       $this->pageHeading;
      $pageString .=        $this->bodyString;
      
     
		  if($this->pageID==169){
			     					 
					 $this->_getEventsList();
			     $pageString .=       $this->eventsString;
			}
		 
		 
		 
    //  $pageString .=    	  '</td>';

     // $pageString .=    	  '<td class="col2"><img src="' . $this->baseURL . 'images/salad.jpg" alt="A delicious bowl of salad" width="200" height="130" /><img src="' . $this->baseURL . 'images/oil.jpg" alt="Oil for dipping" width="200" height="130" /></td>';
    ////  $pageString .=    	  '</tr>';
    //  $pageString .=    	  '</table>';

      $pageString .=    	  '</div>';
       
			 If ($this->navString !=''){
			 
  			 $pageString .=    	 '<div id="rightcontent1">';
				 
   			 $pageString .=    	 '<!-- START navigation -->';
   			 $pageString .=    	 '<div id="nav">';
				 $pageString .=    	 'In this section...';
  			 $pageString .=    	 '<ul>';
   			 $pageString .=    	  $this->navString;
  			 $pageString .=    	 '</ul>';
   			 $pageString .=    	 '</div>';
   			 $pageString .=    	 '<!-- END navigation -->';
   			 $pageString .=    	 '</div>';
       }
			
			 $pageString .=        '<div id="footer1">';
       //$pageString .=        '      <p><a href="site-map.htm" title="Site Map">Site Map</a> | <a href="contact-us.htm" title="Contact Us">Contact Us</a></p>';
       $pageString .=        '      <p>Copyright &#169; 2004-2005 Queen\'s Gate School. All rights reserved.</p>';
       $pageString .=        '      <p>Website sponsored by <a href="http://www.filebusters.co.uk/" title="FileBusters: Filing~Sorted">FileBusters</a> | ';
       $pageString .=        '     Powered by <a href="http://www.brainboxweb.co.uk/" title="BRAINBOX Web Development"><img src="'.$this->baseURL.'images/brainbox.gif" width="77" height="14"  alt="BRAINBOX Web Development and Content Management" /></a></p>';
       $pageString .=        '     </div>';
						
				$pageString .=    		'</div>';			
			$pageString .=    		'</body>';
			$pageString .=    		'</html>';

			
			
			return $pageString;
    }
		
		
		function _getEventsList(){
		
        
        require_once('articles/Diary.php');
        	
        $params=array('active'=>true);
        	
        $diary=new diary($this->db, $params);
        		
        		 
        		 $diaryArray=$diary->getSummary();
        		 
        		 
        		 $this->eventsString .= '<table summary="Diary of Events" class="diary" >';
        
        		 
        		 $counter=0;
        		 foreach($diaryArray as $name=>$value){
        				    
        						
        						
        		        $classArray=array();
        						
        						$alt = fmod($counter,2);
        						
        						switch($value['countDown']){
        						
        						
        						   Case 0:
        							 		 $classArray[]='today';	
        						        break;
        						   Case ($value['countDown']>0 && $alt==1):
        						       $classArray[]='future';
        									 break;
        							  Case ($value['countDown']>0 && $alt==0):
        						       $classArray[]='future_alt';
        									 break;		 
        									 
        						   Case ($value['countDown']<0 && $alt==1) :
        						       $classArray[]='past';
        									 break;
        							 Case ($value['countDown']<0  && $alt==0) :
        						       $classArray[]='past_alt';
        									 break;		 
        									 
        									 
        							 
        
        						}
        										
        						if ($value['highlight']==true){
        						   $classArray[]='highlight';
        						}
        						
        						
        						//col variations
        					  $col1 = $classArray;
        						$col2 = $classArray;
        						
        						$col1[]='col1';
        						$col2[]='col2';
        						
        												
        						 $this->eventsString .= '<tr>';
        						 $this->eventsString .= '<td ' . $this->_getClassString($col1) . '>';
        						
        					
        						
        						switch($value['countDown']){
        						
        						   Case ($value['countDown']>1 && $value['highlight'] == true):
        						        $countdownString = '<span> - '.$value['countDown'].' days to go</span>';
        						        break;
        							  Case ($value['countDown']=1 && $value['highlight'] == true):
        						        $countdownString = '<span> - Tomorrow!</span>';
        						        break;
        								 Case ($value['countDown']=0 && $value['highlight'] == true):
        						        $countdownString = '<span> - Today!</span>';
        						        break;					
        						    Default: 
        						         $countdownString = '';
        						}				 
        						
        						 $this->eventsString .=  '<h3>' . $value['title'] . $countdownString . '</h3>' ;
        						
        						if( $value['intro'] !=''){
        						       $this->eventsString .= '<p>' . $value['intro'] . '</p>';
        						}
        						if ($value['body'] != ''){
        						
        						    $this->eventsString .=  '<p><a href="#' . $value['id'] . '">' . $value['title'] . ' - More details</a></p>';
        						}
        						
        						
        						 $this->eventsString .=  '</td>'; 
        						 $this->eventsString .=  '<td ' . $this->_getClassString($col2) . '>' . $value['displayDate'] . '</td>';
        						
        												
        						 $this->eventsString .=  '</tr>';
        						
        						
        				    $counter=$counter + 1;
        		 }
        		  $this->eventsString .=  '</table>';
        		 
        		 
        		 
        		  foreach($diaryArray as $name=>$value){
        				    
        						
        									
        						
        						if ($value['body'] != ''){
        						
        							  $this->eventsString .=  '<h2 id="'.$value['id'].'">' . $value['title'] . '</h2>';
        						    $this->eventsString .=  $value['body'];
        
        						}
        						
              }
        		 
        }
        function _getClassString($classArray){
                
        				 $classString='';
                       
        				 foreach($classArray as $name=>$value){
          
        						 $classString .= $value . ' ';
        				 
        				 }
                 $classString= ' class="' . trim($classString) . '" ';
        				 
        				 return $classString;
        }			
        	
		
		
}














Class JohnHallettWebpage extends WebPage{

    function JohnHallettWebPage($baseURL){
		
		    
		}
		
		function getPage(){
		
		  $pageString=          '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">';
      $pageString .=        '<html>';
			$pageString .=    		'<head>';
			$pageString .=    		'<title>'.$this->title.'</title>';
			$pageString .=    		'<meta name="author" content="Gary Straughan | Brainbox" />';
			$pageString .=    		'<meta http-equiv="content-language" content="en" />';
			$pageString .=    		'<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />';
			$pageString .=    		'<link href="'.$this->baseURL.'css/s.css" type="text/css" rel="stylesheet" />';
			$pageString .=    		'</head>';
			$pageString .=    		'<body>';
			$pageString .=    	  '<div class="centercontent">';


      $pageString .=    	  '<div id="header">';
      $pageString .=    	  '<a href="' . $this->baseURL . 'index.php"><img src="' . $this->baseURL . 'images/logo.gif" alt="London Gastros Logo" width="239" height="41" /></a>';
      $pageString .=    	  '<a href="' . $this->baseURL . 'contact/">contact</a>';
      $pageString .=    	  '<div class="spacer">&nbsp;</div>';
      $pageString .=    	  '</div>';
      $pageString .=    	  '<table summary="layout" class="layouthome">';
      $pageString .=    	  '<tr>';
      $pageString .=    	  '<td class="col1">';
     

			
  
      $pageString .=        $this->bodyString;
      
     
      $pageString .=    	  '</td>';

      $pageString .=    	  '<td class="col2"><img src="' . $this->baseURL . 'images/salad.jpg" alt="A delicious bowl of salad" width="200" height="130" /><img src="' . $this->baseURL . 'images/oil.jpg" alt="Oil for dipping" width="200" height="130" /></td>';
      $pageString .=    	  '</tr>';
      $pageString .=    	  '</table>';

      $pageString .=    	  '</div>';

			//$pageString .=    		'</div>';
			
			$pageString .=        '<div id="footer">Copyright &#169; 2005 London Gastros. All rights reserved.</div>';
			$pageString .=    		'</body>';
			$pageString .=    		'</html>';

			
			
			return $pageString;
    }
}





