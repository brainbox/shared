<?php

class Application
{
    var $menu;
    
    function Application()
    {
    }
    
    function run()
    {
        $this->loadMenu();
        $page =& $this->loadPage($this->getCurrent());
        $page->show();
    }
    
    function loadMenu()
    {
        $this->menu =  array();
        $data       =& new DataFile('menu.dat', new DataFileReader);
        for ($it =& new DataFileIterator($data); $it->isValid(); $it->next())
        {
            $current =& $it->getCurrent();
            $this->menu[$current['name']] = $current;
        }
    }
    
    function loadPage(&$item)
    {
        $class = $item['class'];
        require_once('classes/' . $class . '.php');
        return new $class($this, $item['name'], $item['arguments']);
        
    }
    
    function &getMenu()
    {
        return $this->menu;
    }
    
    function &getCurrent()
    {
        if (isset($_GET['page']) && isset($this->menu[$_GET['page']]))
        {
            return $this->menu[$_GET['page']];
        }
        reset($this->menu);
        return current($this->menu);
    }
}
?> 