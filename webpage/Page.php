<?php
/*
This class needs to provide the following:

$this->contentString
$this->title 
$this->shortTitle 
$this->metaTitle
$this->metaDescription

*/


Class Page{

    public $contentString;
    public $title;
    public $shortTitle;
    public $metaTitle;
    public $metaDescription;
    public $code;
    
    public $noIndex=false; //The page is to be indexed... unless otherwise stated.
    
    public $basePath;
    
    public $breadcrumbArray;
    public $breadcrumbString;
    
    
    //public $navigationString;
    public $navString;            
    
    public $image;
    public $width;
    public $height;
    public $alt;
    
    public $topNavArray;
    
   
    public $cssArray;
    public $jsArray;
    
    
    public $lastUpdateDate;

    function __construct($dbPDO, $pathVars, $basePath){
	
	
	
        $this->dbPDO = $dbPDO;
        $this->pathVars = $pathVars;
        $this->basePath = $basePath;
	
	
	
        
        require_once('UI/Menu.php');
        
                
        $this->getContent();
        
        $this->getNavigation();
        $this->renderNavigation();
        
        $this->getTopNav();
        
        $this->getBreadcrumb();
        $this->renderBreadcrumb();
	
	
    }
    
    
    protected function getContent(){}
        
    
    function getTopNav(){
	       
	
        $sql="SELECT id, title, path from articles
            WHERE parent_id=1
            AND isOrphan = '0'
            AND published < NOW()
            AND (depublish > NOW() or depublish IS NULL)
            ORDER by sortorder ASC
            ";
            
        #echo $sql;
        
        $stmnt=$this->dbPDO->prepare($sql);
        
        $stmnt->execute();
        
        $this->topNavArray = $stmnt->fetchAll(PDO::FETCH_ASSOC);
        
      
    
    } 
     
    function getBreadcrumb(){
	       
        #echo 'HERERERERER';
        // Include BreadCrumb class
        
        require_once('UI/BreadCrumb.php');
        $crumbs = new BreadCrumb($this->dbPDO,$this->pathVars->getLocation(),true);
        
        $breadcrumbArray = array();
        
        // Display the breadcrumbs   
        while ($crumb = $crumbs->fetch()) {
            
             $breadcrumbArray[] = array('title'=>$crumb->item['title'], 'path'=>$crumb->item['path'] );
          
        }
       
        $this->breadcrumbArray = $breadcrumbArray;

    }
    
    public function renderBreadcrumb(){
	
        //If - and only if - there is a breadcrumbArray, overwite the bread ccrumstring
        if($this->breadcrumbArray){     
             
            $count=count($this->breadcrumbArray);
             
            $breadcrumbString = '<li><a href="' .$this->basePath . '">Home</a></li>';                 
             
            for($i=1;$i<$count-1;$i++){
              
                $breadcrumbString .= '<li><a href="' .$this->basePath . $this->breadcrumbArray[$i]['path'] . '">' . $this->truncateFromLeft($this->breadcrumbArray[$i]['title']) . '</a></li>'; 
            }
            
            //This is the one that's most likely to be very long
            $breadcrumbString .= '<li>' . $this->truncateFromMiddle($this->breadcrumbArray[$count-1]['title']) . '</li>'; 
             
             
            $this->breadcrumbString = $breadcrumbString; 
        }   

	
	
        
    }
     
    protected function truncateFromLeft($string){
        
         
        $maxLength = 20;
        
        
        
        $stringLength = strlen($string);
        
        if( $stringLength < ($maxLength+5) ){  //The 10 is there to make sure we don't get silly trucations  
            return $string;
        }
        
        
        
        $tempString = substr($string, 0, $maxLength) . '&#133;';
        return  $tempString;
        
        
    }
     
     
     
    protected function truncateFromMiddle($string){
        
        
        $maxLength = 45;
     
        
        $stringLength = strlen($string);
        
        if( $stringLength < ($maxLength+5) ){  //The 10 is there to make sure we don't get silly trucations  
            return $string;
        }
        
        //Want the three dots in the middle
        $left = floor($maxLength/2);
        
        $tempString = substr($string,0,$left) . ' &#133; ' . substr($string,$stringLength-$left,$left) ;
        return  $tempString;
        
        
    }
     
     
     
    protected function getNavigation(){
	
		    
        // Include CollapsingTree menu
        require_once('UI/CollapsingTree.php');
        
	#echo $this->pathVars->getLocation();
	#exit;
				
	$isActive=true;
	$this->menu = new CollapsingTree($this->dbPDO,$this->pathVars->getLocation(), $isActive);
	
	
    }
    
    protected function renderNavigation(){
                
               
                
        $navString='';
		$depth=0;
		
        //	Display the collapsing tree menu
        while ( $item = $this->menu->fetch() ) {

            //	echo '<hr>' . $item->type .'--'. $item->name();
			
			//	If this is the start of a new menu branch, increase the depth
	    	if ( $item->isStart() ) {
				$depth++;
				if($depth >1 && $depth <4){	
					//	$navString .= "\nSTART\n";				
					$navString .= "\r<ul>\r<li>";
				}
		
			//	If this is the end of a menu branch, decrease the depth
			} else if ( $item->isEnd() ) {
	    
				if($depth >1 && $depth <4){
					//	$navString .= "\nEND\n";			
					$navString .= "</li>\r</ul>\r</li>";
				}
				$depth--;
			
			//	Display a menu item
	    	} else {
	    
				//	Display the menu item with bullets
				if($depth >1 && $depth <4){	
			
					//	$navString .="</li>\n";   
					$navString .=  '<li><a href="' . $this->basePath . $item->location() .'"';
				
					if ($item->location() == $this->pathVars->getLocation() ){
						$navString .= ' class="select" ';
					}
				
					$navString .= '>'.$item->name() ."</a>" ;
					// $navString .= "</li>\n";
				}
	    	}
		}

        //	Double "<li>"s are also turning up
        $navString = str_replace( "<li><li>", "<li>" , $navString);
        
        //	Gets siblings wrong... in a different way to the sitemap!!!
        $navString = str_replace( "</a><li><a", "</a></li>\r<li><a" , $navString);
        
        $navString = str_replace( '</li></li>', "</li>" , $navString);
       
       
        $navString = trim($navString,'</li>');

        $pattern='/<li>?/i';      
        $navString = preg_replace($pattern,'<li class="top">',$navString,1);
        
        $this->navigationString = $navString;
		#echo $navString;
	
        #echo  $this->navigationString;
        #exit;
    }
    
    public function setTitle($title)
    {
	
	$this->title=$title;
	
    }

    //limit strings to X number of words
    public function word_limiter($str, $length){
        
        #echo '<p>string is ' . $str . '|';
        #echo $length;
        
        if(strlen($str)<$length){
               return $str;
        }
        
        $wordArray = explode(" ", str_replace("  ", " ", $str));
        
        $wordCount=count($wordArray);
        
        $strNew='';
        for ($i = 0; $i < $wordCount; $i++){
          
            if(strlen($strNew) > ($length-3) ){
                 break;
            }
            $strNew .= $wordArray[$i] . ' ';
        }
        
        $strNew = trim($strNew). '...';
        # echo 'it is ' . $strNew;
        #exit;
        return $strNew;
            
        /*
          $str = 
          //	$total_words = count($str);
          $mod_str = '';
        
          for ($word_count = 0; $word_count < $length; $word_count++){
                  $mod_str .= $str[$word_count];
                  if ($word_count < $length){
                          $mod_str .= " ";
                  }
          }
        
          //	remove trailing spaces and check to see if string ends with a full stop.  if it doesn't add "..."
          $mod_str = trim($mod_str);
          if ($mod_str{strlen($mod_str) -1} != ".") {
                  $mod_str .= "...";
          }
          return $mod_str;
        */
    }
    
    public function cssArray()
    {
	return false;
    }
    
    
}