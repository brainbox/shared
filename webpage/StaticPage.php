<?php
class StaticPage extends Page
{
    function parseValue($field, $value)
    {
        if ($field == 'files')
        {
            return array_map('trim', explode(',', $value));
        }
        return false;
    }
    
    function showContents()
    {
        $it =& new ArrayIterator($this->getArgument('files'));
        for ( ; $it->isValid(); $it->next())
        {
            $this->showFile($it->getCurrent());
        }
    }
}
?> 