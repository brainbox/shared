<?php

require_once "phing/Task.php";

class PhingTaskCleanBuilds extends Task {

   
    private $maxBuildCount = 10;

    /**
     * The setter for the attribute "message"
     */
    public function setMessage($str) {
        $this->message = $str;
    }

    /**
     * The init method: Do init steps.
     */
    public function init() {
      // nothing to do here
    }

    /**
     * The main entry point method.
     */
    public function main() {
        
        
        
        $directory =  dirname(__FILE__) . '/../../../../build' ;

        
        
        $directory = realpath($directory);
        
        
        
        $filenames = array();
        $iterator = new DirectoryIterator($directory);
        foreach ($iterator as $fileinfo) {
            if (!$fileinfo->isDot() && $fileinfo->isDir()) {
                $filenames[$fileinfo->getMTime()] = $fileinfo->getPath() . '/' . $fileinfo->getFilename();
            }
        }
        ksort($filenames);
        
       
        //Any to remove?
        $removeArray = array_slice($filenames,0, $dirCount-$this->maxBuildCount);
       
        
        if($removeArray){
            foreach($removeArray as $k=>$dir){
                
                echo 'Removing ' . $dir;
                exec( 'sudo rm -rf ' . $dir );
                
            }
        }
        
    }
}
