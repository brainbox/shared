<?php
/**
 * Speaker class
 *
 * + Uses GETers to access speaker properties
 * + Data comes from the corresponding Speaker model
 *
 *
 */


Class GalleryImage{
    
    public $id;
    public $forename;
    public $surname;
    public $biog;
    public $company;
    public $role;
    
    public $filename;

    function __construct(){
        
    }
    
    /**
     * Populate the object
     *
     * @param array
     *
     */
    
    public function populate($data){
        
        //print_r($data);
        //exit;
        
        $this->id=          $data['id'];
        $this->name=        $data['name'];
        $this->position=    $data['position'];
        $this->biog=        $data['biog'];
        $this->isActive=    $data['is_active'];
    
        
        $this->width=       $data['width'];
        $this->height       =$data['height'];
        //$this->role=$data['role'];
        
        $this->_applyBusinessRules();
        
        
        //echo 'BIOG... ' . $this->biog;
        //exit;
        
    }
    
    
    private function _applyBusinessRules(){
        
        //$this->name=htmlspecialchars($this->name);
        
        $this->name=htmlspecialchars($this->name);
        $this->position=htmlspecialchars($this->position);
        
        if($this->biog){
            require_once('Markdown/markdown.php');
            $this->biog = markdown( $this->biog);
        }
        
  
        
        
    }
    
    /**
     * Takes the Biog - in HTML - and returns a TRIMMED and STRIPPED version
     *
     *
     *
     */
    
    public function summary($words){
        
        require_once('string/StringFunctions.php');

        $stringObject = new StringFunctions($this->biog);
        return strip_tags($stringObject->trimToWords($words) );
         
    }
    
    
    
}