<?php
/**
 * Speaker class
 *
 * + Uses GETers to access speaker properties
 * + Data comes from the corresponding Speaker model
 *
 *
 */


Class Gallery{
    
    public $id;
    public $name;
    public $filename;
    public $isActive; 
    public $description;
    public $keyImage; //an Image Object
    

    function __construct($dbPDO){
        
        $this->dbPDO = $dbPDO;
    }
    
    /**
     * Populate the object
     *
     * @param array
     *
     */
    
    public function populate($data){
        
        //print_r($data);
        //exit;
        
        $this->id=          $data['id'];
        $this->name=        $data['name'];
        $this->filename=        $data['filename'];
        $this->isActive=    $data['is_active'];
    
        
        $this->description =       $data['description'];
        
        $this->_applyBusinessRules();
        
        
        
    }
    
    
    private function _applyBusinessRules(){
        
        //$this->name=htmlspecialchars($this->name);
        
        $this->name=htmlspecialchars($this->name);
        
        $this->getKeyImage();

    }
    
    /**
     * Grab a key image for 
     *
     *
     *
     */
    function getKeyImage()
    {
        
        require_once('Gallery/Model/GalleryImage.php');
        require_once('Gallery/GalleryImage.php');
        
        $mImage = new Model_GalleryImage($this->dbPDO);
        $data = $mImage->getRandomImage($this);
        
        $this->keyImage = new GalleryImage;
        
        $this->keyImage->populate($data);
        
        //print_r($this->keyImage);
        
    }
    
}