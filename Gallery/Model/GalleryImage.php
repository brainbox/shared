<?php
/**
 *
 *
 *
 *
 */


Class Model_GalleryImage{
    
    
    protected $dbPDO;
    public $isActive = true; // Return published items ONLY by default
    public $id;
    
    function __construct($dbPDO){
        
        $this->dbPDO = $dbPDO;
        
    }
    
    /**
     * Get list of Committee Members
     *
     * @param $type
     * @return array
     *
     */
    
    function getByGalleryId($galleryId){
        
      
         
        $sql = "SELECT
                    id
                    ,   is_active
                    ,   filename
                    ,   caption
                    ,   width
                    ,   height
                    ,   height
                    ,   mime
                FROM
                    gall_image
                WHERE
                    1=1";
    
        if($this->isActive){
            
            $sql .= " AND is_active = '1' ";
        
        }
        
        $sql .= " AND gallery_id='$galleryId'";
        
        
        // $sql .= " ORDER BY
        //                is_active DESC
        //                ,   sort_order ASC";
        
        //echo $sql; 
        
        $stmnt=$this->dbPDO->prepare($sql);
        
        try{
                            
            $stmnt->execute();
            return $stmnt->fetchALL(PDO::FETCH_ASSOC);
                        
        }
        catch(Exception $e){
                 echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
    /**
     * Get individual image
     *
     * @param $type
     * @return array
     *
     */
    
    function getById($id){
        
       
        $sql = "SELECT
                    id
                    ,   is_active
                    ,   filename
                    ,   caption
                    ,   width
                    ,   height
                    ,   height
                    ,   mime
                FROM
                    gall_image
                WHERE
                    1=1";
    
        if($this->isActive){
            $sql .= " AND is_active = '1' ";
        }
        
        
        $sql .= " AND id=$id";
      
        //echo $sql; 
        
        $stmnt=$this->dbPDO->prepare($sql);
        
        try{
        
            $stmnt->execute();
            return $stmnt->fetch(PDO::FETCH_ASSOC);
           
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
    /**
     * Get individual image
     *
     * @param $type
     * @return array
     *
     */
    
    function getByFilename($filename){
        
       
        $sql = "SELECT
                    id
                    ,   is_active
                    ,   filename
                    ,   caption
                    ,   width
                    ,   height
                    ,   height
                    ,   mime
                FROM
                    gall_image
                WHERE
                    1=1";
    
        if($this->isActive){
            $sql .= " AND is_active = '1' ";
        }
        
        $sql .= " AND filename=$filename";
      
        //echo $sql; 
        
        $stmnt=$this->dbPDO->prepare($sql);
        
        try{
        
            $stmnt->execute();
            return $stmnt->fetch(PDO::FETCH_ASSOC);
           
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
   
     /**
     * Get individual image
     *
     * @param $type
     * @return array
     *
     */
    
    function getRandomImage($gallery){
        
       
        $sql = "SELECT
                    id
                    ,   is_active
                    ,   filename
                    ,   caption
                    ,   width
                    ,   height
                    ,   height
                    ,   mime
                FROM
                    gall_image
                WHERE
                    1=1";
    
        if($this->isActive){
            $sql .= " AND is_active = '1' ";
        }
        
        
        $sql .= " AND gallery_id=" . $gallery->id ."
        
                ORDER BY RAND()
                LIMIT 1;" ;
      
        //echo $sql; 
        
        $stmnt=$this->dbPDO->prepare($sql);
        
        try{
        
            $stmnt->execute();
            return $stmnt->fetch(PDO::FETCH_ASSOC);
           
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
     function update(){
          
        /*  
        //print_r($_POST);
        //Grab the id - needed for photo uploads
        $this->id = $_POST['id'];
        
          
        $isActive = '0';  
          
        if(isset($_POST['isActive']) ){
            $isActive = '1';
      
        }    
        
       
        $sql = "UPDATE committee
                SET
                    name =      :name,
                    position =  :position,
                    is_active = :is_active,
                    sort_order =:sort_order,
                    biog =      :biog
                
                WHERE id =  :id ";
        
         $stmnt=$this->dbPDO->prepare($sql);
        
        $stmnt->bindParam(':name',      $_POST['name']);
        $stmnt->bindParam(':position',  $_POST['position']);
        $stmnt->bindParam(':is_active', $isActive);
        $stmnt->bindParam(':sort_order',$_POST['sortOrder']);
        $stmnt->bindParam(':biog',      $_POST['biog']);
       
        $stmnt->bindParam(':id',      $_POST['id']);
        
        try{
        
            $stmnt->execute();
            return true;
        
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return false;
        }
        */
    }
    
    function updatePhoto(){
        
        /*
	
	## HANDLE IMAGE ##
	if($_FILES['image']['type'] ){
		
            move_uploaded_file(
                    $_FILES['image']['tmp_name'],
                    '../images/committee/' . $this->id . '.jpg'
            );
           
            require_once('Images/Thumbnail.php');
            
            $tn=new ThumbNail( self::thumbWidth, self::thumbHeight);
            
            // Load an image into a string (this could be from a database)
            $image=file_get_contents('../images/committee/' . $this->id . '.jpg');
                                                                                            
            // Load the image data
            $tn->loadData($image,'image/jpeg');
            
            // Build the thumbnail and store as a file
            $tn->buildThumb('../images/committee/thumb_' . $this->id . '.jpg');
                
            return true;
                
		
	} else {
            
            return false;
         
	}
        */
    }
    
    
      /**
     * Want this to return gallery id
     *
     *
     */ 
     
    function add($galleryId, $filename){
        
        
        $this->isActive = false;
        if($result = $this->getByFilename($filename) ){
            
            return false;
        }
      
       
        $sql = "INSERT INTO gall_image
                (gallery_id, filename)
                     VALUES ($galleryId,'$filename')";
        //echo $sql;
        //exit;
     
        $stmnt=$this->dbPDO->prepare($sql);
        
        try{
            $stmnt->execute();
            //print_r( $this->dbPDO->errorInfo() );    
            return $this->dbPDO->lastInsertId();           
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    

    
}