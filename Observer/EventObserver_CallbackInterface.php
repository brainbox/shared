<?php
/**
 * The Event Callback interface
 * 
 * If you do not wish to use a closure
 * you can define a class that extends
 * this instead. The run method will be
 * called when the event is triggered.
 *
 *
 *  RACKSPACE DOESN'T SUPPORT CLOSURES
 * 
 */
 
 
interface EventObserver_CallbackInterface {
    
  public function run($data);

  
}