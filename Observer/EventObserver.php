<?php
/**
 * The EventObserver Class
 * 
 * With this class you can register callbacks that will
 * be called (FIFO) for a given event.
 */
class EventObserver {
  /**
   * @var array A multi-dimentional array of events => callbacks
   */
  static protected $callbacks = array();
  
  /**
   * Register a callback
   * 
   * @param string $eventName Name of the triggering event
   * @param mixed $callback An instance of Event_Callback or a Closure
   */
  static public function registerCallback($eventName, $callback)
  {
      
    if (!($callback instanceof EventObserver_CallbackInterface) && !($callback instanceof Closure)) {
      throw new Exception("Invalid callback!");
    }
    
    $eventName = strtolower($eventName);
    
    self::$callbacks[$eventName][] = $callback;
  }
  
  /**
   * Trigger an event
   * 
   * @param string $eventName Name of the event to be triggered
   * @param mixed $data The data to be sent to the callback
   */
  static public function trigger($eventName, $data)
  {
    $eventName = strtolower($eventName);
    
    if (isset(self::$callbacks[$eventName])) {
      foreach (self::$callbacks[$eventName] as $callback) {
        self::callback($callback, $data);
      }
    }
  }
  
  /**
   * Perform the callback
   * 
   * @param mixed $callback An instance of Event_Callback or a Closure
   * @param mixed $data The data sent to the callback
   */
  static protected function callback($callback, $data)
  {
    if ($callback instanceof Closure) {
      $callback($data);
    } else {  
      $callback->run($data);
    }
  }
}
