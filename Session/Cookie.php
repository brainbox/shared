<?php
/**
* @package SPLIB
* @version $Id: Session.php,v 1.6 2003/08/17 22:13:17 harry Exp $
*/
/**
* A wrapper around PHP's session functions
* <code>
* $session = new Session();
* $session->set('message','Hello World!');
* echo ( $session->get('message'); // Displays 'Hello World!'
* </code>
* @package SPLIB
* @access public
*/
class Cookie {
    /**
    * Session constructor<br />
    * Starts the session with session_start()
    * <b>Note:</b> that if the session has already started, session_start()
    * does nothing
    * @access public
    */
    function Cookie () {
      
			 
			  // echo 'made it to cookie';
    }

    /**
    * Sets a session variable
    * @param string name of variable
    * @param mixed value of variable
    * @return void
    * @access public
    */
    function set ($name,$value) {
		  
			 //Safest bet is to always set when loggin in
		   //echo 'make it to set cookie:' . $name.  '/' . $value;
			 
			 setcookie($name,$value, time() + 60*60*24*120,'/');
			 
			// echo 'COOKIE SET';
			//exit;		
											
    }

    /**
    * Fetches a session variable
    * @param string name of variable
    * @return mixed value of session varaible
    * @access public
    */
		 function get ($name) {
        if ( isset ( $_COOKIE[$name] ) )
            return $_COOKIE[$name];
        else
            return false;
    }

		
		
		
		
    function check ($name,$value) {
		
        if ( isset ( $_COOKIE[$name] ) && $_COOKIE[$name]==$value  )
            return true;
        else
            return false;
    }

    /**
    * Deletes a session variable
    * @param string name of variable
    * @return boolean
    * @access public
    */
    function del($name) {
		    
				//echo '<p>in del. Name is ' . $name;
				
				//"/~rasmus/", ".example.com", 1
				
        setcookie($name,"",time()-60*60*24*7,'/');
				//echo 'in del';
				
    }

    /**
    * Destroys the whole session
    * @return void
    * @access public
    */
    
}
?>