<?php
/**
 * Groups and Group Classes
 *
 * Remember that the database table is COLLECTION (as Group is a protected term)
 */
 
Class AdminPermissions{
    
    protected $items; //Array store of Groups data - used by the factory classes.
        
        
    function __construct($dbPDO){

        $this->dbPDO=$dbPDO;
        $this->_initialise();       

    }

    /**
    * Read the collections from the database
    */
    function _initialise(){
        
        $sql="Select * from permission";
        
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //print_r($row);
            $this->items[]=$row;
        }
        //print_r($this->items);
        //exit;
        
    }
    
    /**
    * Factory class
    */
    function fetch(){
    
	if(isset($this->items)){
    		
	    $row=each($this->items);
	    if ( $row ) {
	    
		return new AdminPermission($this->dbPDO,$row['value']);
	    
	    }
	    else {
		reset ( $this->items);
		return false;
	    }
	}
	else{
	    return false;
	}		 
    
    }
    
    /**
    * factory Class
    */
    function getByID($id){
    
        foreach($this->items as $name=>$value){
	
	    if($value['id']==$id){

		return new AdminPermission($this->dbPDO,$value);																			 
	    }
	
	}
	//only gets this far if article not found
	reset($this->items);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
	
	return false;
    
    }
    function create($name){
    
        $sql="INSERT INTO permission (name) VALUES ('$name')";
	
	try
	{
	    $stmt = $this->dbPDO->prepare($sql);
	    $stmt->execute();
	    
	}
	
	catch (PDOException $e) {
	   
	    print "Error!: " . $e->getMessage() . "<br/>";
	    die();
	}
    
    }
    
}
       
Class AdminPermission{
	
    protected $id;
    protected $name;

        
        
    function __construct($dbPDO, $data){
        
        //print_r($data);
        $this->dbPDO=$dbPDO;
        $this->id=$data['permission_id'];
        $this->name=$data['name'];
        
        
            
    }
    
    public function __get($nm){
        
        
        if (isset($this->$nm)) {
            return ($this->$nm);
        } else {
            return false;
        }
     }
    
    public function __set($nm, $val){
            
          
        if($this->$nm !=$val){
       
            $this->$nm = $val;
        }
       
  
    }
    
    
    function assignPermissionToGroup($permission){
    
    
    }
    
    function assignUserToGroup($user){
    
    }
    
}



?>

