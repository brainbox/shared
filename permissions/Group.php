<?php
/**
 * Groups and Group Classes
 *
 * Remember that the database table name is COLLECTION (GROUP is a SQL protected term)
 */
 
 
 
 
Class AdminGroups{
    
    protected $items; //Array store of Groups data - used by the factory classes.
        
        
    function __construct($dbPDO){

        $this->dbPDO=$dbPDO;
        $this->_initialise();       

    }

    /**
    * Read the groups from the database
    */
    function _initialise(){
        
        $sql="Select * from collection";
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //print_r($row);
           $this->items[]=$row;
        }
        //print_r($this->items);
        //exit;
        
    }
    
    /**
    * Factory class
    */
    function fetch(){
    
	if(isset($this->items)){
    		
	    $row=each($this->items);
	    if ( $row ) {
	    
		return new AdminGroup($this->dbPDO,$row['value']);
	    
	    }
	    else {
		reset ( $this->items);
		return false;
	    }
	}
	else{
	    return false;
	}		 
    
    }
    
    /**
    * factory Class
    */
    function getByID($id){
    
        foreach($this->items as $name=>$value){
	
	    if($value['id']==$id){

		return new Group($this->dbPDO,$value);																			 
	    }
	
	}
	//only gets this far if article not found
	reset($this->items);   //Reset the pointer
	
	return false;
    
    }
    
    /**
    * Creates a new GROUP
    */
    function create($name){
    
        $sql="INSERT INTO collection (name) VALUES ('$name')";
	
	try
	{
	    $stmt = $this->dbPDO->prepare($sql);
	    $stmt->execute();
            return true;
	    
	}
	
	catch (PDOException $e) {
	   
	    print "Error!: " . $e->getMessage() . "<br/>";
            return true;
	    die();
	}
    
    }
    
}
   
   
/**
* GROUP Class - Typically created via the Groups class factory method
*/       
Class AdminGroup{
	
    /**
    * Constructor
    */    
    function __construct($dbPDO, $data){
        
        $this->dbPDO=$dbPDO;
        $this->id=$data['collection_id'];
        $this->name=$data['name'];

    }
    
    /**
    * Magic Method
    */
    public function __get($nm){
        
        if (isset($this->$nm)) {
            return ($this->$nm);
        } else {
            return false;
        }
    }
    
    /**
    * Magic Method
    */
    public function __set($nm, $val){

        if($this->$nm !=$val){
       
            $this->$nm = $val;
        }
       
  
    }
    
    /**
    * Checks that the current Group has a speficified Permission
    * Takes the Permisssion (class) as an argument
    */
    function hasPermission($permission){
        
        $sql="Select count(*) from collection2permission WHERE permission_id=" . $permission->id . " AND collection_id=" . $this->id;
        
        //echo $sql;
        //exit;
        
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        //print_r($result);
        //exit;
        if ($result[0]==1)
        {
            return true;    
        }
        else{
            return false;
        }
        
        
    }
    
    
}
