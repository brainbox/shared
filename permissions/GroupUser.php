<?php
/**
 * an "xref" Class - something I've never tried before!
 *
 * Remember that the database table is COLLECTION (as Group is a protected term)
 */
 
Class AdminGroupUser{
    
    protected $groups; //Array store of Groups data - used by the factory classes.
    protected $users; //Array store of Groups data - used by the factory classes.
        
        
    function __construct($dbPDO){

        $this->dbPDO=$dbPDO;


    }
    function assignUserToGroups($userID,$groupIDarray){
	
	//print_r($groupIDarray);
	//exit;

	try {
	    
	    $this->dbPDO->beginTransaction(); //Delete and Add - so transaction safest

	    $sql="DELETE FROM user2collection WHERE user_id=" . $userID;
	    $stmt = $this->dbPDO->prepare($sql);
	    $stmt->execute();

	    if(!empty($groupIDarray)){
		
		//Let's have a shot at binding
		$sql="INSERT INTO user2collection (collection_id, user_id) VALUES (:collection_id,:user_id)";
		$stmt = $this->dbPDO->prepare($sql);
		
		$stmt->bindParam(':collection_id', $collection_id);
		$stmt->bindParam(':user_id', $user_id);
		
		foreach ($groupIDarray as $name=>$value){
		    
		    $user_id=$userID;
		    $collection_id=$name; // May need to be fixed
		    $stmt->execute();

		}
	    }
	    $this->dbPDO->commit();

	}   
	catch (PDOException $e) {
	    $this->dbPDO->rollback();
	    print "Error!: " . $e->getMessage() . "<br/>";
	    die();
	}
	$stmt=null;
	
    }
        
    
    
    
    
    /**
    * Gets an array of permissions
    *
    */
    function getUsersByGroup($group){
        
	$this->users= array(); //Make sure we're starting afresh.
	
        $sql="Select user.user_id, user.login, user.email from user, user2collection
	    WHERE
	    user.user_id=user2collection.user_id
	    AND
	    user2collection.collection_id=" . $group->id;
        
	//echo $sql;
	//exit;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //print_r($row);
           $this->users[]=$row;
        }
        //print_r($this->users);
        //exit;
        
    }
    /**
    * Factory class
    */
    function fetchUsers(){
    
	if(isset($this->users)){
    		
	    $row=each($this->users);
	    if ( $row ) {
	    
		return new AdminUser($this->dbPDO,$row['value']);
	    
	    }
	    else {
		reset ( $this->users);
		return false;
	    }
	}
	else{
	    return false;
	}		 
    
    }
   
}
?>