<?php
/**
 * Groups and Group Classes
 *
 * Remember that the database table is COLLECTION (as Group is a protected term)
 */
 
Class AdminUsers{
    
    protected $items; //Array store of Groups data - used by the factory classes.
        
        
    function __construct($dbPDO){

        $this->dbPDO=$dbPDO;
        $this->_initialise();       

    }

    /**
    * Read the collections from the database
    */
    function _initialise(){
        
        $sql="Select * from user";
        
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //print_r($row);
           $this->items[]=$row;
        }
        //print_r($this->items);
        //exit;
        
    }
    
    /**
    * Factory class
    */
    function fetch(){
    
	if(isset($this->items)){
    		
	    $row=each($this->items);
	    if ( $row ) {
	    
		return new AdminUser($this->dbPDO,$row['value']);
	    
	    }
	    else {
		reset ( $this->items);
		return false;
	    }
	}
	else{
	    return false;
	}		 
    
    }
    
    /**
    * factory Class
    */
    function getByID($id){
    
        foreach($this->items as $name=>$value){
	
	    if($value['user_id']==$id){

		return new AdminUser($this->dbPDO,$value);																			 
	    }
	
	}
	//only gets this far if article not found
	reset($this->items);   //Without this line, it just doesn't work... because the above jumps out of a loop!!!
	
	return false;
    
    }
     /**
    * factory Class
    */
    function create($name){
    
        $sql="INSERT INTO user (login, active) VALUES ('$name','1')";
	try
	{
	     $stmt = $this->dbPDO->prepare($sql);
	    $stmt->execute();
	    
	}
	
	catch (PDOException $e) {
	   
	    print "Error!: " . $e->getMessage() . "<br/>";
	    die();
	}
    
    }
}
       
Class AdminUser{
	
    protected $id;
    protected $login;
    protected $email;
        
        
    function __construct($dbPDO, $data){
        
        //print_r($data);
        $this->dbPDO=$dbPDO;
        $this->id=$data['user_id'];
        $this->login=$data['login'];
        $this->email=$data['email'];
        
            
    }
    
    public function __get($nm){
        
        
        if (isset($this->$nm)) {
            return ($this->$nm);
        } else {
            return false;
        }
     }
    
    public function __set($nm, $val){
            
          
        if($this->$nm !=$val){
       
            $this->$nm = $val;
        }
       
  
    }
    /**
     * Test whether a User belongs to a specified Group
     *
     */
    
    
    function belongsTo($group){
        
        $sql="Select count(*) from user2collection WHERE collection_id=" . $group->id . " AND user_id=" . $this->id;
        
        //echo $sql;
        //exit;
        
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        //print_r($result);
        //exit;
        if ($result[0]==1)
        {
            return true;    
        }
        else{
            return false;
        }
    }
    
    
    
    function assignUserToGroup($groupIDarray){
	
	
	
	
    }
    
    //Special treatement for password change
    
    function setPassword($password){
        
        $sql="UPDATE user SET password=MD5('" . $password . "')
        WHERE user_id=" . $this->id;
        //echo $sql;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        
    }
    
    function finalise(){
        
        $sql="UPDATE user SET login='" . $this->login . "' 
        WHERE user_id=" . $this->id;
        //echo $sql;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        
    }
    
}
