<?php
/**
 * an "xref" Class - something I've never tried before!
 *
 * Remember that the database table is COLLECTION (as Group is a protected term)
 */
 
Class AdminGroupPermission{
    
    protected $groups; //Array store of Groups data - used by the factory classes.
    protected $permissions; //Array store of Groups data - used by the factory classes.
        
        
    function __construct($dbPDO){

        $this->dbPDO=$dbPDO;
              

    }
    
    function assignGroupToPermissions($groupID,$permissionIDarray){
	
	//print_r($groupIDarray);
	//exit;

	try {
	    
	    $this->dbPDO->beginTransaction(); //Delete and Add - so transaction safest

	    $sql="DELETE FROM collection2permission WHERE collection_id=" . $groupID;
	    $stmt = $this->dbPDO->prepare($sql);
	    $stmt->execute();

	    if(!empty($permissionIDarray)){
		
		//Let's have a shot at binding
		$sql="INSERT INTO collection2permission (collection_id, permission_id) VALUES (:collection_id,:permission_id)";
		$stmt = $this->dbPDO->prepare($sql);
		
		$stmt->bindParam(':collection_id', $collection_id);
		$stmt->bindParam(':permission_id', $permission_id);
		
		foreach ($permissionIDarray as $name=>$value){
		    
		    $collection_id=$groupID;
		    $permission_id=$name; // May need to be fixed
		    $stmt->execute();

		}
	    }
	    $this->dbPDO->commit();

	}   
	catch (PDOException $e) {
	    $this->dbPDO->rollback();
	    print "Error!: " . $e->getMessage() . "<br/>";
	    die();
	}
	$stmt=null;
	
    }
    
    
    /**
    * Gets an array of permissions
    *
    */
    function getPermissionByGroup($group){
        
	$this->permissions= array(); //Make sure we're starting afresh.
	
        $sql="Select permission.permission_id, permission.name from permission, collection2permission
	    WHERE
	    permission.permission_id=collection2permission.permission_id
	    AND
	    collection2permission.collection_id=" . $group->id;
        
	//echo $sql;
	//exit;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //print_r($row);
           $this->permissions[]=$row;
        }
        //print_r($this->items);
        //exit;
        
    }
    /**
    * Factory class
    */
    function fetchPermissions(){
    
	if(isset($this->permissions)){
    		
	    $row=each($this->permissions);
	    if ( $row ) {
	    
		return new AdminPermission($this->dbPDO,$row['value']);
	    
	    }
	    else {
		reset ( $this->permissions);
		return false;
	    }
	}
	else{
	    return false;
	}		 
    
    }
    

    /**
    * Gets an array of groups
    *
    */
    function getGroupsByPermission($permission){
	
	$this->groups= array(); //Make sure we're starting afresh.

        
        $sql="Select * from collection, collection2permission
	    WHERE
	    collection.collection_id=collection2permission.collection_id
	    AND
	    collection_id=" . $permission->id;
        

	
	
	//secho $sql;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //print_r($row);
           $this->groups[]=$row;
        }
        //print_r($this->items);
        //exit;
        
    }
    
    /**
    * Factory class
    */
    function fetchGroups(){
    
	if(isset($this->groups)){
    		
	    $row=each($this->groups);
	    if ( $row ) {
	    
		return new AdminGroup($this->dbPDO,$row['value']);
	    
	    }
	    else {
		reset ( $this->groups);
		return false;
	    }
	}
	else{
	    return false;
	}		 
    
    }
    
    
}
      
?>