<?php
/**
 * Session MODEL - provides data to the Session class
 *
 * 
 *
 */


Class Model_AccredQuestion{
    
    
    protected $dbPDO;
    //public $published=true; // Return published items ONLY by default
    
    
    /**
     * Constructor - provides a db object to the class
     *
     * @param $dbPDO database object
     *
     */
    
    function __construct($dbPDO){
        
        $this->dbPDO = $dbPDO;
        
    }
    
    
    /**
     * Get questions by Section ID
     * 
     * @param $sessionID
     * @return array
     *
     */    
    
     function getBySectionId($sectionId){
        
        $sql = "SELECT * FROM accred_question
                WHERE section_id = :id ";
                
        //echo $sql;
        
        $stmnt=$this->dbPDO->prepare($sql);
        $stmnt->bindParam(':id', $sectionId);
        
        try{
         
            $stmnt->execute();
    
            return $stmnt->fetchAll(PDO::FETCH_ASSOC);
            
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
 
    
    
}