<?php
/**
 * Session MODEL - provides data to the Session class
 *
 * 
 *
 */


Class Model_AccredSection{
    
    
    protected $dbPDO;
    //public $published=true; // Return published items ONLY by default
    
    
    /**
     * Constructor - provides a db object to the class
     *
     * @param $dbPDO database object
     *
     */
    
    function __construct($dbPDO){
        
        $this->dbPDO = $dbPDO;
        
    }
    
    
    /**
     * Get All sections
     * 
     * @return array
     *
     */    
    
     function getSections(){
        
        $sql = "SELECT 
                    id
                    ,   name
                    ,   description
                    ,   passmark
                FROM
                    accred_section
                ";
                
       
        
        $stmnt=$this->dbPDO->prepare($sql);
       
        
        try{
         
            $stmnt->execute();
    
            return $stmnt->fetchAll(PDO::FETCH_ASSOC);
            
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
    
     /**
     * Get section By ID
     * 
     * @param int $sectionID
     * @return array
     *
     */    
    
     function getById($id){
        
        $sql = "SELECT
                    id
                    ,   name
                    ,   description
                    ,   passmark
                    
                FROM
                    accred_section
                WHERE
                    id = $id";
                
       
        
        $stmnt=$this->dbPDO->prepare($sql);
       
        
        try{
         
            $stmnt->execute();
            
            $results=$stmnt->fetchAll(PDO::FETCH_ASSOC);
            return $results[0];
            
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
 
    
    
}