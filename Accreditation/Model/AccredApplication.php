<?php
/**
 * Session MODEL - provides data to the Session class
 *
 * 
 *
 */
//echo '<pre>';
//print_r($_POST);
//echo '</pre>';

Class Model_AccredApplication{
    
    
    protected $dbPDO;
    //public $published=true; // Return published items ONLY by default
    
    
    /**
     * Constructor - provides a db object to the class
     *
     * @param $dbPDO database object
     *
     */
    
    function __construct($dbPDO){
        
        $this->dbPDO = $dbPDO;
        
    }
    
    
    /**
     * Get application getByMemberId
     * 
     * @param $sessionID
     * @return array Sngel result
     *
     * @todo - work out how to get right application (where there is more than one)
     */    
    
     function getApplications(){
        
      
        $sql = "SELECT
                    app.id
                    ,   u.firstName
                    ,   u.lastName
                    ,   u.member_no
                    ,   app.years_in_profession
                    ,   app.qualifications
                    ,   app.telephone
                    ,   app.start_date
                    ,   app.last_update
                    ,   app.submit_date
                    ,   app.cv
                    ,   app.cv_mime
                    ,   app.cv_size
                    ,   app.cv_date
                    ,   app.reference
                    ,   app.reference_mime
                    ,   app.reference_size
                    ,   app.reference_date
                    
                    
                    FROM
                        accred_application AS app INNER JOIN user_new as u

                            ON app.member_id = u.user_id                       
                        
                    ORDER BY
                        app.start_date DESC
                    
                ";
                
        //echo $sql;
        
        $stmnt=$this->dbPDO->prepare($sql);
       
        
        try{
            $stmnt->execute();
            $results = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return  $results;
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
    
    /**
     * Get application getByMemberId
     * 
     * @param $sessionID
     * @return array Sngel result
     *
     * @todo - work out how to get right application (where there is more than one)
     */    
    
     function getById($id){
       
        $sql = "SELECT
                    app.id
                   
                    ,   u.firstName
                    ,   u.lastName
                     ,   u.member_no
                    ,   app.member_id
                    ,   app.years_in_profession
                    ,   app.qualifications
                    ,   app.telephone
                    ,   app.start_date
                    ,   app.last_update
                    ,   app.submit_date
                    ,   app.cv
                    ,   app.cv_mime
                    ,   app.cv_size
                    ,   app.cv_date
                    ,   app.reference
                    ,   app.reference_mime
                    ,   app.reference_size
                    ,   app.reference_date
                    
                    
                FROM
                        accred_application AS app INNER JOIN user_new as u

                            ON app.member_id = u.user_id                 
                WHERE
                        id = $id
                ORDER BY
                        app.start_date DESC
                LIMIT 1
                ";
                
        //echo $sql;
        
        $stmnt=$this->dbPDO->prepare($sql);
       
        
        try{
            $stmnt->execute();
            
            $results = $stmnt->fetchAll(PDO::FETCH_ASSOC);
            return  $results[0];
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
    
    /**
     * Get application getByMemberId
     * 
     * @param $sessionID
     * @return array Sngel result
     *
     * @todo - work out how to get right application (where there is more than one)
     */    
    
    function getByMemberId($memberId){
        
        //check if there is one
        $sql = "  SELECT count(*)
                FROM 
                     accred_application
                     WHERE
                        member_id = :memberId";
        //echo $sql; 
        $stmnt=$this->dbPDO->prepare($sql);
        $stmnt->bindParam(':memberId',  $memberId);
        
        $stmnt->execute();
        //print_r($stmnt->errorInfo());
        $result = $stmnt->fetchAll(PDO::FETCH_ASSOC);
        
        //print_r($result);
        //exit;
        //Add where necessary
        if($result[0]['count(*)'] == 0 ){
            
             $sql = " INSERT INTO accred_application
                        (member_id, start_date, last_update)
                        
                     VALUES
                        (:memberId, NOW(), NOW() ) ;";
                
                $stmnt=$this->dbPDO->prepare($sql);
                $stmnt->bindParam(':memberId',  $memberId);
                $stmnt->execute();
                //print_r($stmnt->errorInfo());
        }
        $sql = "SELECT
                    app.id
                    ,   app.years_in_profession
                    ,   app.qualifications
                    ,   app.telephone
                    ,   app.start_date
                    ,   app.last_update
                    ,   app.submit_date
                    
                    ,   app.cv
                    ,   app.cv_mime
                    ,   app.cv_size
                    ,   app.cv_date
                     
                    ,   app.reference
                    ,   app.reference_mime
                    ,   app.reference_size
                    ,   app.reference_date
                    
                    
                    
                    
                    FROM
                        accred_application AS app
                    WHERE
                        member_id = :memberId
                    ORDER BY
                        app.start_date DESC
                    LIMIT 1
                ";
                
        //echo $sql;
        
        $stmnt=$this->dbPDO->prepare($sql);
        $stmnt->bindParam(':memberId',  $memberId);
       
        
        try{
         
            $stmnt->execute();
            //print_r($stmnt->errorInfo());
            $results =  $stmnt->fetchAll(PDO::FETCH_ASSOC);
            
            return $results[0];
            
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
    /**
     * Save
     * 
     * @global $_POST Assumed that values will be provided by POST
     * @return void
     *
     * @todo - OK to use the member_id for the update... or should I be using the ID?
     *
     */    
    
     function save(){
        
        $sql = "UPDATE accred_application
                SET
                    years_in_profession = :years
                    ,   qualifications  = :qualifications
                    ,   telephone = :telephone
                    ,   last_update = NOW()
                    
                WHERE
                    id = :id
                ";
        //print_r($_POST); 
        //echo $sql;
        
        $stmnt=$this->dbPDO->prepare($sql);
        
        // echo '<p>' . $_POST['years'];
        //echo  '<p>' . $_POST['memberID'];
        
        $stmnt->bindParam(':years',         $_POST['years']);
         $stmnt->bindParam(':qualifications', $_POST['qualifications']);
        $stmnt->bindParam(':telephone',     $_POST['telephone']);
        $stmnt->bindParam(':id',            $_POST['applicationId']);
        
        
        try{
            $stmnt->execute();
            //print_r( $stmnt->errorInfo());
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
 
 
 /**
     * Save
     * 
     * @global $_FILES Assumed that values will be provided by FILES
     * @return void
     *
     * @todo - OK to use the member_id for the update... or should I be using the ID?
     *
     */    
    
    function saveCV($id, $baseFilePath)
    {
        
        //print($_FILES);
        //exit;
        //echo '<p>' . $_FILES['cv']['tmp_name'];
        //echo '<p>' . $baseFilePath.'/cv/'.$id;

        if (move_uploaded_file($_FILES['cv']['tmp_name'], $baseFilePath.'/cv/'.$id)) {
            
              
            $sql = "UPDATE accred_application
                    SET
                        cv = :filename
                        ,   cv_mime = :mime
                        ,   cv_size = :size
                        ,   cv_date = NOW()
                        ,   last_update = NOW()
                        
                    WHERE
                        id = :id
                    ";
            //print_r($_POST); 
           // echo $sql;
            
            $stmnt=$this->dbPDO->prepare($sql);
            
            $stmnt->bindParam(':filename',  $_FILES['cv']['name']);
            $stmnt->bindParam(':mime',      $_FILES['cv']['type']);
            $stmnt->bindParam(':size',      $_FILES['cv']['size']);
            $stmnt->bindParam(':id',        $id);
            
            $stmnt->execute();
           // print_r( $stmnt->errorInfo());
            
            
            
           return true;
            
            
        } else {
            return false;
        }
       
        
    }
 
   
 
     /**
     * Save
     * 
     * @global $_FILES Assumed that values will be provided by FILES
     * @return void
     *
     * @todo - OK to use the member_id for the update... or should I be using the ID?
     *
     */    
    
    function saveReference($id, $baseFilePath)
    {
        
        echo $id;
        //echo $this->id;
        //exit;
        
        //print($_FILES);
        //exit;
        //echo '<p>' . $_FILES['cv']['tmp_name'];
        //echo '<p>' . $baseFilePath.'/cv/'.$id;

        if (move_uploaded_file($_FILES['reference']['tmp_name'], $baseFilePath.'/reference/'.$id)) {
            
              
            $sql = "UPDATE accred_application
                    SET
                        reference = :filename
                        ,   reference_mime = :mime
                        ,   reference_size = :size
                        ,   reference_date = NOW()
                        
                        ,   last_update = NOW()
                        
                    WHERE
                        id = :id
                    ";
            //print_r($_POST); 
           // echo $sql;
            
            $stmnt=$this->dbPDO->prepare($sql);
            
            $stmnt->bindParam(':filename',  $_FILES['reference']['name']);
            $stmnt->bindParam(':mime',      $_FILES['reference']['type']);
            $stmnt->bindParam(':size',      $_FILES['reference']['size']);
            $stmnt->bindParam(':id',        $id);
            
            $stmnt->execute();
           // print_r( $stmnt->errorInfo());
            
            
            
           return true;
            
            
        } else {
            return false;
        }
       
        
    }
 
    
    
    
}