<?php
/**
 * Session MODEL - provides data to the Session class
 *
 * 
 *
 */


Class Model_AccredResponse{
    
    
    protected $dbPDO;
    //public $published=true; // Return published items ONLY by default
    
    
    /**
     * Constructor - provides a db object to the class
     *
     * @param $dbPDO database object
     *
     */
    
    function __construct($dbPDO){
        
        $this->dbPDO = $dbPDO;
        
    }
    
    
    /**
     * Get questions by application ID
     * 
     * @param $applicationId
     * @return array
     *
     */    
    
     function getByApplicationId($applicationId){
        
        $sql = "SELECT 
                id
                ,   score
                ,   freeform
                FROM accred_response
                WHERE application_id = :id ";
           
        //echo $applicationId;     
        //echo $sql;
        
        $stmnt=$this->dbPDO->prepare($sql);
        $stmnt->bindParam(':id', $applicationId);
        
        try{
         
            $stmnt->execute();
    
            return $stmnt->fetchAll(PDO::FETCH_ASSOC);
            //print_r($stmnt->errorInfo() );
            
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    /**
     * Get questions by application ID
     * 
     * @param $applicationId
     * @return array
     *
     */    
    
     function get($applicationId,$sectionId=null){
        
        if($sectionId){
            
            
            //Watch out: RESPONSE has a COMPOUND primary key
             $sql = "SELECT 
                    accred_response.question_id
                    ,   accred_response.score
                    ,   accred_response.freeform
                  
                    FROM
                    
                        accred_section INNER JOIN accred_question
                            ON   accred_section.id  = accred_question.section_id
                    
                      INNER JOIN accred_response
                            ON  accred_question.id = accred_response.question_id
                            
                  
                        INNER JOIN accred_application
                            ON  accred_response.application_id = accred_application.id

                            
                    WHERE 1
                        AND accred_application.id = :application_id
                        AND accred_section.id = :section_id
                    ";
               
            //echo $sql;
            
            $stmnt=$this->dbPDO->prepare($sql);
            $stmnt->bindParam(':application_id',    $applicationId);
            $stmnt->bindParam(':section_id',        $sectionId);
            
            
        } else {
        
            $sql = "SELECT 
                    accred_response.question_id
                    ,   accred_response.score
                    ,   accred_response.freeform
                    FROM accred_response
                        INNER JOIN accred_application
                            ON  accred_application.id = accred_response.application_id
                            
                    WHERE accred_application.id = :application_id        
                    ";
               
            //echo $sql;
            
            $stmnt=$this->dbPDO->prepare($sql);
            $stmnt->bindParam(':application_id', $applicationId);
        
        }
        
        
        
        try{
         
            $stmnt->execute();
            //print_r($stmnt->errorInfo() );
            return $stmnt->fetchAll(PDO::FETCH_ASSOC);
           
            
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
    function saveResponses($applicationId){
        
       
        
        $this->dbPDO->beginTransaction();
        
        try{
            
            
            //echo '<pre>';
            //print_r($_POST);
            
            foreach ($_POST['question'] as $name=>$value ){
               
                 $sql = "REPLACE  accred_response (application_id, question_id, score, freeform)
                        VALUES (:application_id,  :question_id, :score, :freeform)";
    
                //echo '<p>'. $sql;
    
                $stmnt=$this->dbPDO->prepare($sql);
                
                $stmnt->bindParam(':application_id',    $applicationId);
                $stmnt->bindParam(':question_id',       $name);
                $stmnt->bindParam(':score',             $value['score']);
                $stmnt->bindParam(':freeform',          $value['freeform']);
    
                $stmnt->execute();
              
                //print_r($stmnt->errorInfo() );
                                
            }
            
            $this->dbPDO->commit();
            
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            
             $this->dbPDO->rollBack();
        }
        
    
        
    }
    
    
}