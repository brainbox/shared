<?php
/**
 * Session object
 *
 *
 *
 */


Class AccredSection{
    
      
    public $id;
    public $name;
    public $description;
    public $passmark;
    
    public $totalScore=0;
    
    //public $isComplete;   
       
    public function __construct(){
        
        
        
    }
    
    /**
     * Populates the object
     *
     * @param array 
     */
    
    public function populate($data){
        
        //print_r($data);
        
        $this->id =             $data['id'];
        $this->name =           $data['name'];
        $this->description =    $data['description'];
        $this->passmark =       $data['passmark'];
     
    }
    
    
    /**
     * Get questions for the current section
     *
     * Need to ask the model for the approprite questions
     *
     * @param object $dbPDO
     * @return array An array of Question Objects
     */
    
    public function getQuestions($dbPDO){
        
        //print_r($data);
        
        require_once('Accreditation/Model/AccredQuestion.php');
        require_once('Accreditation/AccredQuestion.php');

        $mQuestion = new Model_AccredQuestion($dbPDO);
        
        $data = $mQuestion->getBySectionID($this->id);
        
        
        //get ready to store the objects
        $questionArray=array();
        
        foreach($data as $name=>$value){
        
            $question = new AccredQuestion();
            $question->populate($value);
            
            $questionArray[] = $question;
            
            
        }
        
        return  $questionArray;
     
    }
    
    
    /**
     * Would be good if the function could return the summary information for a
     * paricular APPLICANT.
     *
     * Perhaps an instance of the applicant could be passed in?
     *
     */
    
    public function sectionScore($dbPDO, $applicationId){
        
        //print_r($data);
        
        require_once('Accreditation/Model/AccredResponse.php');
        require_once('Accreditation/AccredResponse.php');

        $mResponse = new Model_AccredResponse($dbPDO);
        
        $data = $mResponse->get($applicationId, $this->id);
        
        //print_r($data);
        //exit;
        
        //get ready to store the objects
        $questionArray=array();
        $totalScore=0;
        foreach($data as $name=>$value){
        
            $response = new AccredResponse();
            $response->populate($value);
            //echo '<hr>';
            //print_r($response);
            ///$responseArray[] = $response;
            
            $totalScore = ($totalScore + $response->adjustedScore);
           
            
        }
        //echo 'total score is ' . $totalScore;
        return   $totalScore;
     
    }
    
    public function getStatus($dbPDO, $applicationId)
    {

        $score = $this->sectionScore($dbPDO, $applicationId);
        
        if ($score >= $this->passmark){
            return true;
        } else {
            return false;
        }
        
        
    }
    
    
}