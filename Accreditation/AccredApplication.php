<?php

/**
 * The "control centre of the ship. Thing it'll be calling other other functions
 *
 *
 *
 */


Class AccredApplication{
    
      
    public $id;
    public $memberNo;
    public $firstName;
    public $lastName;
    
    public $memberNumber;
    public $yearsInProfession;
    public $qualifications;
    public $startDate;
    public $lastUpdate;
    public $applicationDate;
    public $cv;
    public $cvMime;
    public $cvSize;
    public $cvDate;
    
    
    public $reference;
    public $referenceMime;
    public $referenceSize;
    public $referenceDate;
    
    
    public function populate($data)
    {
        //echo '<hr>';
        //print_r($data);
        //exit;
        
        $this->id =                 $data['id'];
        
        if(isset( $data['firstName'])){
             $this->firstName =       $data['firstName'];
        }
         if(isset( $data['lastName'])){
             $this->lastName =       $data['lastName'];
        }
          if(isset( $data['member_no'])){
             $this->memberNo =       $data['member_no'];
        }
        
        
        //$this->memberNumber =       $data['member_id'];
        $this->yearsInProfession =  $data['years_in_profession'];
        $this->qualifications =     $data['qualifications'];
        $this->telephone =          $data['telephone'];
        $this->startDate =          $data['start_date'];
        $this->lastUpdate =         $data['last_update'];
        
        $this->cv =                 $data['cv'];
        $this->cvMime =             $data['cv_mime'];
        $this->cvSize =             $data['cv_size'];
        $this->cvDate =             $data['cv_date'];
        
        $this->reference =         $data['reference'];
        $this->referenceMime =     $data['reference_mime'];
        $this->referenceSize =     $data['reference_size'];
        $this->referenceDate =     $data['reference_date'];
        
    }
    
    public function __construct()
    {
        
        
        $this->personalInformation();
        $this->qualification();
        $this->experience();
        $this->sections();
        $this->CV();
        
    }
    
    
    function personalInformation()
    {
        
        //get the personalInformation SUMMARY
        
        
    }
    function qualification()
    {
        
            //get the qaulification SUMMARY
        
    }
    function experience()
    {}
    function sections()
    {}
    function CV()
    {}
    
    
    
    public function experienceStatus()
    {
        
        if($this->yearsInProfession >= 5){
            
            return true;
        
        }
        elseif($this->yearsInProfession >= 3 && strlen($this->qualifications) > 10 ){
            
            return true;
            
        }  else {
            
            return false;
        }
        

    }
    
}