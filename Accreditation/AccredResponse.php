<?php
/**
 * Session object
 *
 *
 *
 */


Class AccredResponse{
    
      
    
    public $questionId;
    public $score;
    public $freeform;
    
    public $adjustedScore=0;
    public $minimumWordCount=20;
    public $errorMessage;
           
    public function __construct(){
        
        
        
    }
    
    /**
     * Populates the object
     *
     * @param array 
     */
    
    public function populate($data){
        
        //print_r($data);
        
        
        //$this->id=          $data['id']; NOTE - does NOT have it's own prmiary key
        $this->questionId= $data['question_id'];
        $this->score= $data['score']; // raw "radio button" score
        $this->freeform= $data['freeform'];
       
        $this->_applyBusinessRules();
    }
    
    
    private function _applyBusinessRules()
    {
        //Determine the word count
        
        //echo $this->freeform;
        $this->wordCount = str_word_count($this->freeform); 
        
        //if score is greater than 0... 
        if($this->score){
            
            //Sufficient words
            if($this->wordCount >= $this->minimumWordCount){
            
                $this->adjustedScore=$this->score;
            
            }
            
            switch ($this->wordCount){
                
                case(0):
                
                    $this->errorMessage = 'Please provide a description';
                    break;
                
                case($this->wordCount < $this->minimumWordCount):
                
                    $this->errorMessage = "Please provide a fuller description - at least $this->minimumWordCount words.";
                    break;
                
            }
        } 
        
        //echo '<p>'. $this->adjustedScore;
        
    }
    
    
    
}