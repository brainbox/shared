<?php
/**
* @package SPLIB
* @version $Id: PathVars.php,v 1.2 2003/07/15 22:58:48 harry Exp $
*/
/**
* PathVars Class
* Extracts scalar variables from URL like<br />
* http://localhost/index.php/these/are/variables/
* <code>
* // Url is http://localhost/index.php/forum/view/1
* $pathVars = new PathVars('index.php');
* echo ( $pathVars->fetchByIndex(1) ); // Displays 'view'
* </code>
* @access public
* @package SPLIB
*/

require_once('Url/PathVars.php');

class PathVarsAChiPPP extends PathVars{
  
  
    public $pathVars;
  
    function __construct($baseUrl) {
        //Kick the parent's constructor
	parent::__construct($baseUrl);
    }



    /**
    * Parses the fragment into variables
    * @return void
    * @access private
    */
    function parseFragment () {
		    
        //Extended to handle by "pseudo querystring"
        //Two types   /asdasdasd/asdasd/1 - number in last place taken to be a "quesrtsing"
        //            /asdfasdfasdf/asdfasdf/-/xxx   - xxx taken to be a quesry string
        
        
        
        //step 1 - get rid of any real query string
        
        if ( strstr ($this->fragment,'?') ){
                                
            $this->fragment=substr($this->fragment,0,strpos($this->fragment,'?') );

                        
        }
                     
                     
                      
        if ( strstr ($this->fragment,'/') ) {
            $vars=explode('/',$this->fragment);
                                                
            //print_r($vars);
                                                
            foreach ($vars as $var) {
       
                if ( $var == '' )
                    continue;
                if ( strstr ($var,'=') )
                    continue;
                if ( strstr ($var,'&') )
                    continue;
               
                                                                
                $this->pathVars[]=$var;
            }
        }
        else{
            //If there's no '/' in the string
            $this->pathVars[]=$this->fragment;
        
        }
        
        //print_r($this->pathVars);
        
        $count=count($this->pathVars);
        if($count>1){
        
            //check for NUMBER at the end
            if(is_numeric($this->pathVars[$count-1] )  ){
                
                $this->queryString=$this->pathVars[$count-1];
                //echo 'ends with a number';
                array_pop($this->pathVars);
                //print_r($this->pathVars);
            }
            else{    
    
                                    
                //Check for a DASH
                //if an element is "-", treat the following element as a quesry string
                $position= array_search('-',$this->pathVars);
                //exit;
               // echo 'posn is ' . $position;
                
                if($position){
                        
                        //echo $this->pathVars[($position+1)];
                        $this->queryString=$this->pathVars[($position+1)];
                        
                        //Pop the last two off
                        array_pop($this->pathVars);
                        array_pop($this->pathVars);
                        
               
                }
            }
        }
        
        //echo '<hr>Querystring is ' . $this->queryString . '<pre>';
        //print_r($this->pathVars);
        //exit;
        
    }    
    function section(){
        
        if($this->pathVars[0]) {
            return $this->pathVars[0];	
        }
        else {
            return '';	
        }
        	
 
    }
    function location(){
        
        /*
         $location='';
        
        
        for($i=0; $i<count($this->pathVars)-1; $i+=1){
        
        $location .= $this->pathVars[$i]. '/';
        
        }
        //add the last element
        
        
        $location .= $this->pathVars[$i];
        
        
        //echo 'THE LOCATION IS ' . $location;
        */
        if($this->pathVars){
	    return implode('/',$this->pathVars);
	} else {
	    return '';
	}
    
                                 
    
    }
    function queryString(){
                
                            if(isset($this->queryString) ){
                            
                                             return $this->queryString;
                      }
                            else{
                                 return false;
                            }		 
                     
                                             
            
            }

    
}
?>