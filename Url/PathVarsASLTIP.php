<?php
/**
* @package SPLIB
* @version $Id: PathVars.php,v 1.2 2003/07/15 22:58:48 harry Exp $
*/
/**
* PathVars Class
* Extracts scalar variables from URL like<br />
* http://localhost/index.php/these/are/variables/
* <code>
* // Url is http://localhost/index.php/forum/view/1
* $pathVars = new PathVars('index.php');
* echo ( $pathVars->fetchByIndex(1) ); // Displays 'view'
* </code>
* @access public
* @package SPLIB
*/
class PathVarsASLTIP extends PathVars{
  
    function __construct ($baseUrl) {
        //Kick the parent's constructor
	
	
				parent::__construct($baseUrl);
    }



    /**
    * Parses the fragment into variables
    * @return void
    * @access private
    */
    function parseFragment () {
		    
				//Extended to handle by "pseudo querystring
			  
				//step 1 - get rid of any real query string
			 
				if ( strstr ($this->fragment,'?') ){
								
								     $this->fragment=substr($this->fragment,0,strpos($this->fragment,'?') );
								     	         
							
         }
			 
			 
			  
        if ( strstr ($this->fragment,'/') ) {
            $vars=explode('/',$this->fragment);
						
	//print_r($vars);
						
            foreach ($vars as $var) {
						    
						
						
                if ( $var == '' )
                    continue;
                if ( strstr ($var,'=') )
                    continue;
                if ( strstr ($var,'&') )
                    continue;
               
								
								$this->pathArray[]=$var;
            }
        }
				else{
				       //If there's no '/' in the string;
				       
				       
				       
				       
						   $this->pathArray[]=$this->fragment;
				
				}
				
				$newPathVars=array();
				//Find the number - that's the query string
				foreach ($this->pathArray as $name=>$value){
					
					
					
					if(is_numeric($value) ){
							
						//echo '<hr />it is a number';
						$this->queryString=$value;
						//ignore evything else'
						
						break;
					}
					$newPathVars[]=$value;
				}
				//echo '<hr />';
				//print_r($newPathVars);
				
				//pass it back
				
				$this->pathArray=$newPathVars;
				
				//echo '<hr>kkk';
				//print_r($this->pathArray);
				//echo 'kkk<hr>';
				
				
				/*
				//if an element is "-", treat the following element as a quesry string
				$position= array_search('-',$this->pathArray);
				//exit;
				//echo 'posn is ' . $position;
				
				if($position){
					
					//echo $this->pathArray[($position+1)];
					$this->queryString=$this->pathArray[($position+1)];
					
					//Pop the last two off
					array_pop($this->pathArray);
					array_pop($this->pathArray);
					
				}
				
				//echo 'QS is ' . $this->queryString;
				//exit;
				
				*/
				/*
				//print_r($this->pathArray);
				$count=count($this->pathArray);
				$last=$this->pathArray[$count];
				//echo $last;
				
        if (is_numeric(substr($last,0,1))){
           //Grab the query string, then pop if off the end
           $this->queryString=$last;
					 array_pop($this->pathArray);
					 
        
        }
	*/
				
				//echo '<hr>GGGGGGGGGGGGGGGGG';
				//print_r($this->pathArray);
				
				
				
				
				
    }
    
    function section(){
	if(isset($this->pathArray[0])){
	    return $this->pathArray[0];
	} else {
	    return false;
	}
    }
	
    function location(){
			
	$location='';
	
	for($i=0; $i<count($this->pathArray)-1; $i+=1){
	
	    $location .= $this->pathArray[$i]. '/';
	    
	}
	//add the last element
	if(isset($this->pathArray[$i]) ){
	 
	 $location .= $this->pathArray[$i];
	}
	      
	//echo 'THE LOCATION IS ' . $location;
	return $location;

		
    }
    function queryString(){
    
	if(isset($this->queryString) ){
	
	    return $this->queryString;
	}
	else{
	    return false;
	}		 
    
    
    
    }

    
}
