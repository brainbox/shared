<?php
/**
* @package SPLIB
* @version $Id: pathArray.php,v 1.2 2003/07/15 22:58:48 harry Exp $
*/
/**
* pathArray Class
* Extracts scalar variables from URL like<br />
* http://localhost/index.php/these/are/variables/
* <code>
* // Url is http://localhost/index.php/forum/view/1
* $pathArray = new pathArray('index.php');
* echo ( $pathArray->fetchByIndex(1) ); // Displays 'view'
* </code>
* @access public
* @package SPLIB
*/
class pathVars {
    /**
    * The baseUrl of the script receiving the URL
    * @access private
    * @var string
    */
    var $baseUrl;

    /**
    * The fragment of the URL after the baseUrl
    * @access private
    * @var string
    */
    public $fragment;

    /**
    * The variables extracted from the fragment
    * @access private
    * @var string
    */
    var $pathArray=array();

    /**
    * pathArray constructor
    * @param string receiving script url
    * @access public
    */
    function __construct($baseUrl) {
	
        $this->baseUrl=$baseUrl;
        $this->pathArray=array();
        $this->fetchFragment();
        $this->parseFragment();
    }

		
		
    function getBaseURL(){
       return $this->baseUrl;
    }
		
		
    /**
    * Strips out $this->baseUrl from $_SERVER['REQUEST_URI']
    * @return void
    * @access private
    */
    private function fetchFragment () {
        if ( !strstr($_SERVER['REQUEST_URI'],$this->baseUrl) )
            trigger_error ('$baseUrl is invalid: '.$this->baseUrl );
        if ( $this->baseUrl != '/' )
            $this->fragment=str_replace($this->baseUrl,'',$_SERVER['REQUEST_URI']);
        else
            $this->fragment=$_SERVER['REQUEST_URI'];
						
						
	//Get rid of any quesrystring (added NOV 06);
	if ( strstr ($this->fragment,'?') ){
	
	    $this->fragment=substr($this->fragment,0,strpos($this->fragment,'?') );

	}		
	
	//echo 'FRAGMENT IS ' . $this->fragment;
    }

    /**
    * Parses the fragment into variables
    * @return void
    * @access private
    */
    function parseFragment () {
        
        #ECHO 'got here';
        #EXIT;
        
       # if ( strstr ($this->fragment,'/') ) {
            $vars=explode('/',$this->fragment);
						
						//echo '<hr>';
						//print_r($vars);
						
            foreach ($vars as $var) {
                if ( $var == '' )
                    continue;
                if ( strstr ($var,'=') )
                    continue;
                if ( strstr ($var,'&') )
                    continue;
                if ( strstr ($var,'?') )
                    continue;
                $this->pathArray[]=$var;
            }
       # }
				
        #echo '<hr>';
       # print_r( $this->pathArray);
        #EXIT;
				
    }

    /**
    * Iterator for path vars
    * @return mixed
    * @access public
    */
    function fetch () {
        $var = each ( $this->pathArray );
        if ( $var ) {
            return $var['value'];
        } else {
            reset ( $this->pathArray );
            return false;
        }
    }

    /**
    * Returns $this->pathArray
    * @return array
    * @access public
    */
    function fetchAll () {
        return $this->pathArray;
    }

    /**
    * Return a value from $this->pathArray given it's index
    * @param int the index of this->pathArray to return
    * @return string
    * @access public
    */
    function fetchByIndex ($index) {
	
	#echo '<hr>THE FETCH BY INDEX BIT IS';
	#print_r($this->pathArray);
	#exit;
	
	
        if ( isset ($this->pathArray[$index]) )
            return $this->pathArray[$index];
        else
            return false;
    }
    /**
    * Returns the number of variables found
    * @return int
    * @access public
    */
    function size () {
        return count ( $this->pathArray );
    }
}
?>