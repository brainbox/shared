<?php

class PathVarsSimple extends PathVars{
  
  
    public $section;
  
    function __construct($baseUrl) {
        //Kick the parent's constructor
	parent::__construct($baseUrl);
        
        $this->section = $this->fetchByIndex(0); // Would be null for the Home Page
        
        
    }
    /*
    function section(){
        
        print_r($this->pathVars);
        
        if($this->pathVars[0]) {
            return $this->pathVars[0];	
        }
        else {
            return '';	
        }
        	
 
    }
    */
    function getLocation(){
     
        return implode('/',$this->pathArray);
    
    
    }
    
    function trimToIndex($index){
	
	#print_r($this->pathArray);
	
	$this->pathArray = array_slice($this->pathArray,0,$index);
	
	#Need to parse it again
	#$this->parseFragment ();
	
	#echo '<hr>';
	#print_r($this->pathArray);
	#echo count($this->pathArray);
	#exit;
	
    }
 
}