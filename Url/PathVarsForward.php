<?php
/**
* @package SPLIB
* @version $Id: PathVars.php,v 1.2 2003/07/15 22:58:48 harry Exp $
*/
/**
* PathVars Class
* Extracts scalar variables from URL like<br />
* http://localhost/index.php/these/are/variables/
* <code>
* // Url is http://localhost/index.php/forum/view/1
* $pathVars = new PathVars('index.php');
* echo ( $pathVars->fetchByIndex(1) ); // Displays 'view'
* </code>
* @access public
* @package SPLIB
*/
class PathVarsForward extends PathVars{
  
    function PathVarsForward ($baseUrl) {
        //Kick the parent's constructor
				parent::__construct($baseUrl);
    }



    /**
    * Parses the fragment into variables
    * @return void
    * @access private
    */
    function parseFragment () {
		    
				//Extended to handle by "pseudo querystring
			  
				//step 1 - get rid of any real query string
			 
				if ( strstr ($this->fragment,'?') ){
								
								     $this->fragment=substr($this->fragment,0,strpos($this->fragment,'?') );
								     	         
							
         }
			 
			 
			  
        if ( strstr ($this->fragment,'/') ) {
            $vars=explode('/',$this->fragment);
						
					    //print_r($vars);
						
            foreach ($vars as $var) {
						    
						
						
                if ( $var == '' )
                    continue;
                if ( strstr ($var,'=') )
                    continue;
                if ( strstr ($var,'&') )
                    continue;
               
								
								$this->pathArray[]=$var;
            }
        }
				else{
				       //If there's no '/' in the string;
						   $this->pathArray[]=$this->fragment;
				
				}
				//echo '<hr>kkk';
				//print_r($this->pathArray);
				//echo 'kkk<hr>';
				//If the last element starts with a number, treat as quesry string
				//print_r($this->pathArray);
				$count=count($this->pathArray);
				$last=$this->pathArray[$count-1];
				//echo $last;
				
        if (is_numeric(substr($last,0,1))){
           //Grab the query string, then pop if off the end
           $this->queryString=$last;
					 array_pop($this->pathArray);
					 
        
        }
				
				//echo '<hr>GGGGGGGGGGGGGGGGG';
				//print_r($this->pathArray);
				
				
				
				
				
    }
		function section(){
		    
			return $this->pathArray[0];	
				
				
		}
		function location(){
			
			$location='';
		   
			   for($i=0; $i<count($this->pathArray)-1; $i+=1){
				    
						$location .= $this->pathArray[$i]. '/';
				 
				 }
			   //add the last element
				
				 
				 $location .= $this->pathArray[$i];
				 
				 
				 //echo 'THE LOCATION IS ' . $location;
				 
				 return $location;
		
		 				 
		
		}
		function queryString(){
		    
				if(isset($this->queryString) ){
				
						 return $this->queryString;
			  }
				else{
				     return false;
				}		 
			 
		 				 
		
		}

    
}
?>