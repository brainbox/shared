<?php
/**
 *
 *
 *
 *
 */


Class Model_Committee{
    
    
    protected $dbPDO;
    public $isActive = true; // Return published items ONLY by default
    public $id;
    const thumbWidth = 100;
    const thumbHeight = 150;
    
    function __construct($dbPDO){
        
        $this->dbPDO = $dbPDO;
        
    }
    
    /**
     * Get list of Committee Members
     *
     * @param $type
     * @return array
     *
     */
    
    function get(){
        
      
         
        $sql = "SELECT
                    id
                    ,   name
                    ,   position
                    ,   biog
                    ,   is_active
                    ,   sort_order
                    ,   width
                    ,   height
                      
                FROM
                    committee
                WHERE
                    1=1";
    
        if($this->isActive){
            
            $sql .= " AND is_active = '1' ";
        
        }
        
         $sql .= " ORDER BY
                        is_active DESC
                        ,   sort_order ASC";
        
        //echo $sql; 
        
        $stmnt=$this->dbPDO->prepare($sql);
        
        try{
                            
            $stmnt->execute();
            
            $progString =''; 
            //echo '<pre>';
            return $stmnt->fetchALL(PDO::FETCH_ASSOC);
                        
        }
        catch(Exception $e){
                 echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
    /**
     * Get list of Committee Members
     *
     * @param $type
     * @return array
     *
     */
    
    function getById($id){
        
       
        $sql = "SELECT
                    id
                    ,   name
                    ,   position
                    ,   biog
                    ,   is_active
                    ,   sort_order
                    ,   width
                    ,   height
                  
                FROM
                    committee
                WHERE
                    1=1";
    
        if($this->isActive){
            $sql .= " AND is_active = '1' ";
        }
        
        
        $sql .= " AND id=$id";
      
        //echo $sql; 
        
        $stmnt=$this->dbPDO->prepare($sql);
        
        try{
                            
            $stmnt->execute();
            
            $progString =''; 
            //echo '<pre>';
            return $stmnt->fetch(PDO::FETCH_ASSOC);
                        
        }
        catch(Exception $e){
                 echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
   
   
    
     function update(){
          
        //print_r($_POST);
        //Grab the id - needed for photo uploads
        $this->id = $_POST['id'];
        
          
        $isActive = '0';  
          
        if(isset($_POST['isActive']) ){
            $isActive = '1';
      
        }    
        
       
        $sql = "UPDATE committee
                SET
                    name =      :name,
                    position =  :position,
                    is_active = :is_active,
                    sort_order =:sort_order,
                    biog =      :biog
                
                WHERE id =  :id ";
        
         $stmnt=$this->dbPDO->prepare($sql);
        
        $stmnt->bindParam(':name',      $_POST['name']);
        $stmnt->bindParam(':position',  $_POST['position']);
        $stmnt->bindParam(':is_active', $isActive);
        $stmnt->bindParam(':sort_order',$_POST['sortOrder']);
        $stmnt->bindParam(':biog',      $_POST['biog']);
       
        $stmnt->bindParam(':id',      $_POST['id']);
        
        try{
        
            $stmnt->execute();
            return true;
        
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return false;
        }
        
    }
    
    function updatePhoto(){
        

	
	## HANDLE IMAGE ##
	if($_FILES['image']['type'] ){
		
            move_uploaded_file(
                    $_FILES['image']['tmp_name'],
                    '../images/committee/' . $this->id . '.jpg'
            );
           
            require_once('Images/Thumbnail.php');
            
            $tn=new ThumbNail( self::thumbWidth, self::thumbHeight);
            
            // Load an image into a string (this could be from a database)
            $image=file_get_contents('../images/committee/' . $this->id . '.jpg');
                                                                                            
            // Load the image data
            $tn->loadData($image,'image/jpeg');
            
            // Build the thumbnail and store as a file
            $tn->buildThumb('../images/committee/thumb_' . $this->id . '.jpg');
                
            return true;
                
		
	} else {
            
            return false;
         
	}
        
    }
    
    
    function add(){
        
       
        $sql = "INSERT INTO committee
                (name)
                     VALUES ('*** New ***')";
        //echo $sql;
     
        $stmnt=$this->dbPDO->prepare($sql);
        
        try{
            $stmnt->execute();
                  
        }
        catch(Exception $e){
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
        
    }
    
    
    /**
     * Assumes the phote details are availabel through $_FILES
     *
     */
    
    function addImage()
    {
        pritn_r($_FILES);
        
        
        
        
    }
    
}