<?php

/**
 * 27 Apr - Slight change of emphasis: going to make this class more "front-end" oriented: less "raw" database
 * values, more business rules. A consequence is that the Admin site will now talk drectly to the Model
 *
 */

require_once('articles/Item.php');

Class Event extends Item{
    
    public $id;
    public $title;
    public $description;
    
    public $onHold;
    
    private $subtitle; #Delimited string... Dangerous!!!
    
    public $path;
    #public $pathOld;            # Original path to the Event item on the old website
    
    public $published;
    public $depublish;        # Now a mysql datetime value
    #public $theDateUNIX;       # UNIX equivalent
    
    public $startDate;
    public $endDate;
    
    
    public $place;
    public $body;
                 
    public $image;
    public $alt;
    public $height;
    public $width;
    public $lastUpdate;         # Now a mysql datetime value
    #public $lastUpdateUNIX;    # UNIX equivalent
    public $intro;              # Derived from body. Not updated
    public $bodyWithoutIntro;   # Derived from body. Not updated
  
    public $notes;
     
     
    //Derived 
    public $metaTitle;
    public $metaDescription;
    public $subtitleArray;
    
    public $teaser;
    
    public $isActive;

    function __construct(){
        
        
      
    }
    
    
    
    
    function getTitle($minimumCharacters = false){
        
        $string=$this->title;
        
        //Truncate if required
        if($minimumCharacters){
            $string = $this->truncate($string, $minimumCharacters);
        }
        
        return $string;
        
    }
    
    /**
     * Populates the class with values from the Model
     *
     */
    

    public function populate($data){
        
        //echo '<br />------------------------------ppp';
        //print_r($data);
        //exit;
        
        
        if(isset($data['id']))
            $this->id = $data['id'];
        
        if(isset($data['title']))
            $this->title = $data['title'];
            
        if(isset($data['teaser']))
            $this->teaser = $data['teaser'];
            
            
            
        if(isset($data['subtitle']))
            $this->subtitle = $data['subtitle'];    
            
        if(isset($data['path']))
            $this->path = $data['path'];
            
        if(isset($data['path_raw']))
            $this->pathOld = $data['path_raw'];    
            
        if(isset($data['body']))
            $this->body = $data['body'];
        
        
        if(isset($data['published']))
            $this->published = $data['published'];
            
        if(isset($data['depublish']))
            $this->depublish = $data['depublish'];
            
            
        if(isset($data['start_date']))
            $this->startDate = $data['start_date'];
            
        if(isset($data['end_date']))
            $this->endDate = $data['end_date'];    
        
        if(isset($data['place']))
            $this->place = $data['place'];
            
        if(isset($data['image']))
            $this->image = $data['image'];
            
        if(isset($data['alt']))
            $this->alt = $data['alt'];
        
        if(isset($data['width']))
            $this->width = $data['width'];
         
        if(isset($data['height']))
            $this->height = $data['height'];
        
        if(isset($data['last_update']))
            $this->lastUpdate = $data['last_update'];
           
        
        
        if(isset($data['notes']))
            $this->notes = $data['notes'];
        
        
          if(isset($data['meta_title']))
            $this->metaTitle = $data['meta_title'];
        
        
         if(isset($data['meta_description']))
            $this->metaDescription = $data['meta_description'];
        
      
       
      
        $this->applyBusinessRules();
        
        //echo $this->metaDescription;
        //exit;
       
        
        
        
        
    }
    
    /**
     *
     *
     */
    
    protected function applyBusinessRules(){
        
        #echo $this->body;
        //Clean up
        $pattern = '/&(?!amp;)/';
        $replace = '&amp;';
        $this->title =      preg_replace($pattern,  $replace, $this->title);
        $this->body =       preg_replace($pattern,  $replace, $this->body);
        $this->alt =        preg_replace($pattern,  $replace, $this->alt);

         #$this->subtitle =   preg_replace('/&(?!amp;)/', '&amp;', $this->subtitle);
        
        //Derived parameters
        require_once('Markdown/markdown.php');
        $this->bodyRaw =  $this->body;
        
        $this->body=Markdown($this->body);
        $this->teaser=Markdown($this->teaser);
        
        if(!$this->metaTitle){
            $this->metaTitle = $this->truncate($this->title, 60);
            $this->metaTitle = htmlentities(strip_tags(($this->metaTitle) ),ENT_QUOTES,'UTF-8' );
        }
        
        if($this->subtitle){
            $this->subtitleArray=explode('|',$this->subtitle);
            $this->subtitleArray = preg_replace($pattern,  $replace, $this->subtitleArray);
        }
        
        $this->_createMetaDescription();
      
       
      
        
        if(strtotime($this->published)<time() ){
            
            $this->isActive=true;
        } else {
            $this->isActive=false;
        }
        
    }
    
    
    private function _createMetaDescription(){
        
        
        if($this->metaDescription){
            
            return true;
        }
        
        if(count($this->subtitleArray) ){
          
            $this->metaDescription = trim(implode('. ', $this->subtitleArray), '. ');
            
        } elseif(trim($this->body) ) {
            
            #Grab the first paragraph
            $pattern = '/^.*?\<p>.*?\/p>/is'; #firs two paras
            preg_match($pattern, $this->body, $matches);
            
            #print_r($matches);
           # exit;
            
            $this->metaDescription = htmlentities(strip_tags(($matches[0]) ),ENT_QUOTES,'UTF-8'  );
            #echo 'MD in Event is ' . $this->metaDescription;
           # exit;
        }
    
        
    }
    
    private function splitBody(){
        
       
       
        $pattern = '/^.*?\<p>.*?\/p>.*?\/p>/is'; #firs two paras
        
        preg_match($pattern, $this->body, $matches); 
	
        #print_r($matches);
        
        $this->intro = $matches[0];
        
        $this->bodyWithoutIntro = str_replace($this->intro,'',$this->body);
        
        
    }
    
    /*
    public function update($dbPDO){
        
        #echo 'llllllllllllllll' . $this->raw;
        #exit;
        
        #echo $this->place;
        #exit;
        
        $model = new Model_Event($dbPDO);
        
        $model->update($this);
        
        
        
    }
    */
    
      /**
      * Added the date formatting for Shack's benefit. Only applies to one day events at present
      *
      */
     
     function dateRange(){
        
        
        $startDateUNIX=strtotime($this->startDate);
        $endDateUNIX=strtotime($this->endDate);
        
        if( $endDateUNIX=='' ||  $endDateUNIX < $startDateUNIX ||  $startDateUNIX == $endDateUNIX){ //Ignore endDate if it's before the start date
            
            return date('j M Y', $startDateUNIX);
           
        }

        //Eveything is different!
        if( date('Y', $startDateUNIX)!=date('Y',  $endDateUNIX)  ){
            
            return date('j M Y', $startDateUNIX) . ' - ' . date('j M Y',  $endDateUNIX);
            
        }
        
        //Different months
        if(date('m', $startDateUNIX)!=date('m',$endDateUNIX) ){
            
            return date('j M', $startDateUNIX) . ' - ' . date('j M Y',  $endDateUNIX);
            
        }
        
        
        //Just days different
        return date('j', $startDateUNIX) . '-' .  date('j M Y',  $endDateUNIX);
        
     }
     
     
    function dateRangeUSA(){
          
          //easy case first: one-day event;
          //echo '<hr>' . $this->startDate;
          //echo '<hr>' . $this->endDate;
          // echo '<hr>' . date('d M Y', $this->startdate);
          //exit;
          
        $startDateUNIX=strtotime($this->startDate);
        $endDateUNIX=strtotime($this->endDate);
        
        //easy case first: one-day event;
        #echo '<hr>' . $this->startDate;
        #echo '<hr>' . $this->endDate;
        
        
        if( $endDateUNIX=='' ||  $endDateUNIX < $startDateUNIX ||  $startDateUNIX== $endDateUNIX){ //Ignore endDate if it's before the start date
             
             
          return date('jS M Y', $startDateUNIX);
        
        
        }
        
        
        
        //more tricky!
        else {
               //Eveything is different!
               if( date('Y', $startDateUNIX)!=date('Y',  $endDateUNIX)  ){
                    return date('jS M Y', $startDateUNIX) . ' - ' . date('jS M Y',  $endDateUNIX);
               
               }
               //Differnet months
               elseif(date('m', $startDateUNIX)!=date('m',$endDateUNIX) ){
                    return date('jS M', $startDateUNIX) . ' - ' . date('jS M Y',  $endDateUNIX);
               
               }
               else{
                    return  date('jS', $startDateUNIX) . ' - ' . date('jS M Y',  $endDateUNIX);
               }
          }
     } 
     
    
    function daysToGo(){
        
        $days =  floor( ( strtotime($this->startDate) - mktime(0,0,0,date('m'),date('d'),date('y') ) )/(60*60*24));
        
        if($days < 0){
            $days=0;
        }
        
        return $days;
    }
        
    
    
    
    

}