<?php

/**
 * 17 Apr (year ?)
 *
 * Updated Jan 2012
 *
 */

require_once ('articles/Model/Item.php');

Class Model_Event extends Model_Item{

    
   
    public $top;
    public $isActive =  true; //Means NOT awkward AND the_date (published) is in the past. Override for admin
    

    
    public function __construct($dbPDO){
       
        $this->dbPDO=$dbPDO;
        
    }
    
    
    
    public function getByPath($path){
        
       # print_r($this->dbPDO);
        
        
        $sql = "SELECT * from event
                WHERE 1 = 1                 
                AND path = '" .  $path . "' ";
                
                
        if($this->isActive){        
            $sql .= "
                    AND published < NOW()
                    AND (depublish IS NULL OR depublish > NOW() ) ";        
        }
        
        $sql .= " LIMIT 1 ";
        
        
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
    
        
    }
    
    public function getRecent($count,$imageRequired=false){
        
        $sql = "SELECT id, title, subtitle, the_date, path FROM event
                WHERE 1=1 ";
        
        if($this->isActive){        
            $sql .= " AND awkward = 0            
                    AND the_date < NOW() ";        
        }
        
        if($imageRequired){
            
            $sql .= " AND image IS NOT NULL ";
        }
        
        $sql .= "ORDER BY the_date DESC LIMIT " . $count;
        
        #echo $sql;
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
    
    }
    
    
    public function getItems($count=null){
        
        $sql = "SELECT *
        
                    ,   IF( published > CURDATE() AND (depublish IS NULL || depublish > published   ), 1, 0) AS is_embargoed
                    ,   IF( published <= CURDATE() AND (depublish IS NULL || depublish > CURDATE()   ), 1, 0) AS is_published 
                    ,   IF( depublish <  CURDATE()   , 1, 0) AS is_expired
                    
                    
                    -- no end_date means 1 day event
                    
                    ,   IF(  start_date > CURDATE(), 1, 0) AS is_future 
                    ,   IF(  start_date = CURDATE() || (start_date < CURDATE() AND end_date >= CURDATE() ), 1, 0) AS is_on_now
                    ,   IF( (start_date < CURDATE() AND end_date IS NULL) ||  end_date   <  CURDATE()  , 1, 0) AS is_past 
        
        
        
        
        from event
                WHERE 1=1 ";
        
        if($this->isActive){        
            $sql .= "             
                    AND published < NOW()
                    AND ( depublish >NOW() OR depublish IS NULL ) ";        
        }
        
        $sql .= " ORDER BY start_date DESC ";
        
        if($count){
            
             $sql .= "  LIMIT " . $count;
            
        }
       
        
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
        
    }
    
    
    public function getFuture($count=null){
        
        $sql = "SELECT * from event
                WHERE 1=1 ";
        
        if($this->isActive){        
            $sql .= "             
                    AND published < NOW()
                    AND ( depublish > NOW() OR depublish IS NULL ) ";        
        }
        
        //Get TODAY's event PLUS  future events 
        $sql .= " AND ((end_date IS NOT NULL AND DATE_ADD(end_date, INTERVAL 1 DAY) > NOW() ) OR (DATE_ADD(start_date, INTERVAL 1 DAY) > NOW() ) ) ";
        
        $sql .= " ORDER BY start_date ASC ";
        
        if($count){
            
            $sql .= " LIMIT $count ";
            
        }
        
        
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
       
        
    }
    
    
        
    public function getItemsByYear($year = null){
        
        
        if(!$year){
            
            $year=date('Y');
        }
        
        $startDate = $year .'-1-1';
        #echo date('d M Y H:s',$startDate);
        
        $endDate = $year+1 .'-1-1';;
        #echo date('d M Y H:s',$endDate);
        
        
        $sql = "SELECT * from event
                WHERE
                    1=1
                    AND start_date >= '$startDate'
                    AND start_date < '$endDate' ";
                    
        if($this->isActive){        
            $sql .= "             
                    AND published < NOW()
                    AND ( depublish > NOW() OR depublish IS NULL ) ";        
        }
                    
                    
         $sql .= "        ORDER BY start_date DESC
                ";
        //echo $sql;
        # exit;
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        try{
            $this->stmnt->execute();
            
        
        } catch (Exception $e) {
           
            echo "Failed: " . $e->getMessage();
        }
        
    }
    
    
    
    
    /**
     * @todo - this one returns the result. Inconsistent with the others
     *
     *
     *
     */
    
    
    public function getById($id){
        
        #var_dump($this->dbPDO);
       #exit;
        $sql = "SELECT
                    *
                    ,   IF( published > CURDATE() AND (depublish IS NULL || depublish > published   ), 1, 0) AS is_embargoed
                    ,   IF( published <= CURDATE() AND (depublish IS NULL || depublish > CURDATE()   ), 1, 0) AS is_published 
                    ,   IF( depublish <  CURDATE()   , 1, 0) AS is_expired
                    
                    
                    -- no end_date means 1 day event
                    
                    ,   IF(  start_date > CURDATE(), 1, 0) AS is_future 
                    ,   IF(  start_date = CURDATE() || (start_date < CURDATE() AND end_date >= CURDATE() ), 1, 0) AS is_on_now
                    ,   IF( (start_date < CURDATE() AND end_date IS NULL) ||  end_date   <  CURDATE()  , 1, 0) AS is_past 
        
                FROM
                    event
                WHERE
                    1=1 ";
        
        if($this->isActive){        
            $sql .= "       
                    AND published < NOW()
                    AND (depublish > NOW() || depublish IS NULL)
                    ";        
        } 
        
        $sql .= " AND id=$id";
        //echo $sql;
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
        
        return $this->stmnt->fetch();
    
        
    }
   
   
    
    public function fetchAll(){
        
        return $this->stmnt->fetchAll();
        
        
    }
    
    public function fetch(){
        
        return $this->stmnt->fetch();
        
        
    }
    
    /**
     * 27 Apr 08 - changing so that it's updated with POSTED data... not with the event object
     * 19 May 08 - changing to use FILTER
     * 17 June 08 - changed to use the radio controls for start/end and pub/depub
     *
     * 2012-02 - Refactor to split out allow dependey injection / the writing to the database
     *
     */
    
    public function updateFromPOST($data = null){
        
        //Backwards compatibility
        if(!$data){
            
            $data = $_POST;
            
        }
       
        $data = $this->prepareData($data);
        
        $this->updateDatabase($data);
        
    }
    
    
    public function prepareData($data){
        
        
        $today = mktime( 0,0,0,date('m'),date('d'),date('y') );
        
        $dataSkeleton = array();
            
        $dataSkeleton['id'] = '';
        
        $dataSkeleton['title'] = '';
        
        $dataSkeleton['body'] = '';
        
        $dataSkeleton['metaTitle'] = '';
        $dataSkeleton['metaDescription'] = '';
        
        $dataSkeleton['startDate'] = '';
        $dataSkeleton['endDate'] = '';   
        
        
        $dataSkeleton['published'] = '';
        $dataSkeleton['depublish'] = '';
        
        $dataSkeleton['expire'] = '';
        $dataSkeleton['oneday'] = '';
        
        
        $dataSkeleton['isPublished'] = '';
        $dataSkeleton['usesIsPublished'] = '';
        

        
        $data = array_merge($dataSkeleton, $data);
        
        
        
        
        
        //Convert the dates
        $data['published'] = strtotime($data['published']);
        $data['depublish'] = strtotime($data['depublish']);
        
        $data['startDate'] = strtotime($data['startDate']);
        $data['endDate'] =   strtotime($data['endDate']  );
        
        
        //Fix
        
        if( !empty($data['isPublished']) ){
            
            $data['depublish'] = null;
            
            //Set published to today IF it's not set OR if it's in the future
            if(!$data['published'] || $data['published'] > $today ){
                
                $data['published'] = $today;
                
            }
            
        }
        
        //If it's not ticked AND it's a for that uses isPublished 
        if ( empty($data['isPublished']) && $data['usesIsPublished'] ){
            
            //Set expire to today IF it's not already set, and it's not in the past
            if( empty($data['depublish']) || $data['depublish'] > $today ){
                                                
                $data['depublish'] = $today;
                
            }
            
        }
        
        
        
        
    
        
        //DEPUBLISH - New logic for depublish: "expire" (the radio control) may may be "never" or "date"
        if($data['expire']=='never'){
            
            $data['depublish'] = NULL;
            
        }
        
        
        //END DATE
        //New logic for endDate: "oneday" (the radio control) may may be "true" or "false" (NB strings)
        if($data['oneday']=="true"){
            
            $data['endDate'] = NULL;
            
        }
        
        
        //If start and end data are the same... fix it!
        
        if($data['startDate']==$data['endDate']){
            
            $data['endDate'] = NULL;
            
        }
        
        
       
        
        
        //Dates to SQL
        
        if($data['published']){
            $data['published'] = date('Y-m-d',  $data['published']);
        }
        if($data['depublish']){
            $data['depublish'] = date('Y-m-d',  $data['depublish']);
        }
         if($data['startDate']){
            $data['startDate'] = date('Y-m-d',  $data['startDate']);
        }
        if($data['endDate']){
            $data['endDate'] = date('Y-m-d',  $data['endDate']);
        }
        
        
        
        
        
        //echo "<hr><pre>";
        //var_dump($data);
        //echo "<hr></pre>";
        
        return $data;
        
        
        
    }
    
    
    
    protected function updateDatabase($data){

        
        $sql = "UPDATE event
                
                SET
                title = :title,
                body = :body,
                
                meta_title = :metaTitle,
                meta_description = :metaDescription,
               
               
                start_date =    :startDate,
                end_date =      :endDate,
                published =     :published,
                depublish =     :depublish
                

                WHERE id=:id";
       
        
        $stmnt = $this->dbPDO->prepare($sql);
        
        
        $stmnt->bindParam(':id',                $data['id']);
        
        $stmnt->bindParam(':title',             $data['title']);
                
        $stmnt->bindParam(':body',              $data['body']);

        $stmnt->bindParam(':metaTitle',         $data['metaTitle']);
        $stmnt->bindParam(':metaDescription',   $data['metaDescription']);    
        
        $stmnt->bindParam(':startDate',         $data['startDate']);
        $stmnt->bindParam(':endDate',           $data['endDate']);    
        
        $stmnt->bindParam(':published',         $data['published']);
        $stmnt->bindParam(':depublish',         $data['depublish']);    
        
        
       
        
        
        $this->dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try
        {
            
            $stmnt->execute();
            
        }
        catch (PDOException $e)
        {
            #echo 'hasdasdas here';
            print ("The statement failed.\n");
            print ("getCode: ". $e->getCode() . "\n");
            print ("getMessage: ". $e->getMessage() . "\n");
        }
        //print_r( $stmnt->ErrorInfo() ) ;
        #exit;
        
        
    }
    
    
    
    
    
    /**
     * Assumes that the image will be updated using $_POST and $_FILES data
     *
     */
    
    
    public function updateImage($basePath){
        
        
        //Store the image
        #echo '<hr>';
        #print_r($config);
        #echo '<hr>';
        
        //Get image site
        $dims = getimagesize($_FILES['key_image']['tmp_name']);
        
        $width=$dims[0];
        $height=$dims[1];
        
        
        
        
        
        
        
        
        // Load the source image
        $original = imagecreatefromjpeg($_FILES['key_image']['tmp_name']);
        // Create a blank thumbnail (note slightly reduced height)
        $thumb = imagecreatetruecolor(303,162);
        //Resize it
        imagecopyresampled( $thumb, $original, 0, 0, 0, 0, 303, 162, $dims[0], $dims[1] );
        //Save the thumbnail
        $path= $_SERVER['DOCUMENT_ROOT'] . $basePath . 'images/homepage/event/article_' . $_GET['id'] . '.jpg';
        
        //Fix possible double-forward slashes
        $path=str_replace('//','/',$path);
        
        #echo $path;
        #exit;
        imagejpeg($thumb,$path);
       
       
        //Store the main image
        $filename='main-'. $_GET['id'] .'.jpg';
        $path= $_SERVER['DOCUMENT_ROOT'] . $basePath . 'images/event/articles/';
          //Fix possible double-forward slashes
        $path=str_replace('//','/',$path);
        
        move_uploaded_file($_FILES['key_image']['tmp_name'],$path . $filename);

        //tell the database 
       
        
        
        
        
          
        $sql = "UPDATE event
                
                SET
               
                image = :image,
                
                width = :width,
                height = :height
                
           
                WHERE id=:id";
        #echo $sql;
        #exit;
        
        $stmnt = $this->dbPDO->prepare($sql);
        
        $stmnt->bindParam(':id', $id);
      
        $stmnt->bindParam(':image', $image);
       
        $stmnt->bindParam(':width', $width);
        $stmnt->bindParam(':height', $height);
        
        $id = $_POST['id'];
        $image = $filename;
        $width = (int) $dims[0];
        $height = (int) $dims[1];
        
        #echo '<hr>';
        try
        {
            $stmnt->execute ();
        }
        catch (PDOException $e)
        {
          print ("The statement failed.\n");
          print ("getCode: ". $e->getCode () . "\n");
          print ("getMessage: ". $e->getMessage () . "\n");
        }
        
        #print_r($stmnt->errorInfo()) ;
     
        
        
    }
     /**
     * Tricky this here is that the path is a mandatory field... and it must be unique
     *
     * 
     */
    
    
    public function addItem($title = 'New Item'){
        
        
        $path = $this->createPathfromTitle($title);
        
        //Check the path is OK
        
        $sql = "SELECT count(*) from event where title = '$path'";
        $stmnt = $this->dbPDO->prepare($sql);
        $stmnt->execute();
        $result = $stmnt->fetch();
        
        //Path already taken? append the date
        if($result['count(*)'] !=0){
            $path = $path . '-' . time();
        }
        
        // Use "NOW()" rather than an generated date to preserve the order of multiple items added as
        // part of the same session
        
        $sql = "INSERT INTO event (
                    title,
                    path,
                    published,
                    depublish,
                    start_date
                    
                    )
                VALUES (
                    :title,
                   :path,
                   NOW(),
                   :depublish,
                   :startDate
                

                    );";

        $stmnt = $this->dbPDO->prepare($sql);
       
        $stmnt->bindParam(':title', $title);
        $stmnt->bindParam(':path', $path);
        $stmnt->bindParam(':depublish', $depublish);
        $stmnt->bindParam(':startDate', $startDate);
        
        //Create the item in the expired state
         $depublish = date('Y',mktime(0,0,0,date('m'),date('d')-1,date('y'))) . '-' . date('m',mktime(0,0,0,date('m'),date('d')-1,date('y'))). '-' . date('d',mktime(0,0,0,date('m'),date('d')-1,date('y')));
         
        //Stick in a dummy date - a month from now
        $startDate = date('Y-m-d',mktime(0,0,0,date('m')+1,date('d'),date('y'))) ;

        try
        {
            $stmnt->execute ();
        }
        catch (PDOException $e)
        {
            print ("The statement failed.\n");
            print ("getCode: ". $e->getCode () . "\n");
            print ("getMessage: ". $e->getMessage () . "\n");
        }
        
        #print_r($stmnt->errorInfo()) ;
        
        
    }
    
    /*
    public function addItem($title){
        
        
        #echo '<hr>here';
        #exit;
        
        $sql = "INSERT INTO event (
                    title,
                    path,
                    the_date,
                    year
                    
                    )
                VALUES (
                    :title,
                   :path,
                    :theDate,
                    :year
                

                    );";
        #echo $sql;
       # exit;
        
        $stmnt = $this->dbPDO->prepare($sql);
        
       
        $stmnt->bindParam(':title', $title);
        $stmnt->bindParam(':path', $path);
        $stmnt->bindParam(':theDate', $theDate);
        $stmnt->bindParam(':year', $year);

        #Title is provided
        $path = str_replace(' ','_',$title);
        #$path= $path . '/';
        $theDate=        date('Y-m-d');
        $year=        date('Y');

        
       
        
        #echo '<hr>';
        try
        {
            $stmnt->execute ();
        }
        catch (PDOException $e)
        {
          print ("The statement failed.\n");
          print ("getCode: ". $e->getCode () . "\n");
          print ("getMessage: ". $e->getMessage () . "\n");
        }
        
        #print_r($stmnt->errorInfo()) ;
        
        
    }
    */
      
    
    /**
     * Usually called statically for the admin site
     */

    function status($published, $depublish){
        
        
        $today=mktime(0,0,0,date('m'),date('d'),date('y') );
        
        if($published > $today ){
            return 'pending';
        }
        
        
        if($depublish && $depublish <= $today){
            return 'expired';
        }
        
        
        return 'active';
        			

    }
    
     public function assignFromPOST(){
        
        #print_r($_POST);
        $id=$_POST['id'];
        #echo 'the id is ' . $id;
        
        $sql = "DELETE FROM development_event_xref WHERE event_id=$id";
        
        #echo $sql;
         $stmnt = $this->dbPDO->prepare($sql);
           $stmnt->execute();
         #  exit;
        
        if(isset($_POST['development'])){
            
            $developmentIDarray=array_keys($_POST['development']);
            
            #print_r($developmentIDarray);
            
            
            $sql = "INSERT INTO development_event_xref
                   (event_id, development_id)
                   VALUES";
                   
            foreach($developmentIDarray as $developmentID){
              
               $sql .= "($id, $developmentID),";
               
           }
           $sql = trim($sql,',');
           
           #echo $sql;
           $stmnt = $this->dbPDO->prepare($sql);
           $stmnt->execute();
        }
        
        
    }
    
    
}