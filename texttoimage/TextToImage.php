<?php
Class textToImage {
			
    //constructor
    function textToImage($text,$font,$color, $background, $fontSize, $width, $height, $folder){
        
             
        $this->text = $text;
        $this->font = $font;
        $this->color = $color;
        $this->background = $background;
        $this->fontSize = $fontSize;
        $this->folder = $folder;
        #$this->width = $width;
        #$this->height = $height;
        
        $this->createFilename();
        
        $this->buildImage();
        
        
        //save the image
        imagepng($this->image,$this->folder . '/' . $this->filename . '.png');
        
        imagedestroy($this->image);
   
    }
    
    function createFilename(){
                            
        $this->filename=$this->text;
        
        
        //$this->filename=urlencode(strtolower($this->filename));
        $this->filename=str_replace("'",'-', $this->filename);
        $this->filename=str_replace(" ",'-', $this->filename);
        $this->filename=str_replace('?','', $this->filename);
        
        //The filename could be blank, so add a prefix
        
        $this->filename = 'pre' . $this->filename;
        
        //echo '<textarea>' . $this->filename . '</textarea>';
        //$this->filename=str_replace("'",'-', $this->filename);
                            
    }
    
    //function getFilename(){
     //                 return $this->filename .'.png';      
                            
    //}
                
    function getImagePath(){
    
         return  $this->folder . '/' . $this->filename .'.png';
             
        
    
    }
    Function getImageLink($alt){
    
                //Note that "text" may havefunny charcters in it
                
         return  '<img src="' . $this->folder . '/' . $this->filename .'.png" width="'.$this->width.'" height="'.$this->height.'" alt="'.$alt.'" />';
        
    
    }
    
    Function buildImage(){
    
        //echo $this->color;
            ### Convert HTML backgound color to RGB
            if( eregi( "([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})", $this->background, $bgrgb ) )
            {$bgred = hexdec( $bgrgb[1] );  $bggreen = hexdec( $bgrgb[2] );  $bgblue = hexdec( $bgrgb[3] );}
            
            ### Convert HTML text color to RGB
            if( eregi( "([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})", $this->color, $textrgb ) )
                {$textred = hexdec( $textrgb[1] );  $textgreen = hexdec( $textrgb[2] );  $textblue = hexdec( $textrgb[3] );}
            
            
             //Needs to be centered - find the dimensions first
             //array imagettfbbox ( float size, float angle, string fontfile, string text )		
        
             //GET DIMENSIONS
                                 #echo '<p>===' . $this->font;
                                 
             $size = imagettfbbox($this->fontSize, 0, $this->font, $this->text); 
             
             
            //echo '<pre>'; 															 
             #print_r($size);

             
             $this->width = $size[2] + $size[0] + 10;
             $this->height = abs($size[1]) + abs($size[7]);
    
                
                
             $this->image = imagecreatetruecolor($this->width, $this->height);


             $this->bgcolor = imagecolorallocate($this->image, $bgred,$bggreen,$bgblue);
            
            ### Declare image's text color
            $this->fontcolor = imagecolorallocate($this->image, $textred,$textgreen,$textblue);
    
            //set colours
                //$this->setColor();
                //$this->setBackground();
                
                imagefill($this->image,0,0,$this->bgcolor);	
                
                imagettftext($this->image, $this->fontSize, 0, 0, abs($size[5]), $this->fontcolor, $this->font, $this->text  );
             
             
                            
                                            
    }
    
    

}
?>