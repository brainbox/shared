<?php

class YearPager {
    /**
    * Total number of pages
    * @access private
    * @var int
    */
    var $totalPages;

    /**
    * The row MySQL should start it's select with
    * @access private
    * @var int
    */
    var $startRow;

    /**
    * SimplePager Constructor
    * @param int number of rows per page
    * @param int total number of rows available
    * @param int current page being viewed
    * @access public
    */
    function __construct($startYear,$currentYear=null) {
        
	//echo '<hr>HEREEEE';
	
	
	if(!$currentYear){
	    $currentYear=date('Y');
			//echo 'THE CURRENT YEAR IS ' . $currentYear;
	}
	
	// Calculate the total number of pages
	
	$endYear=date('Y');
				
        $this->totalPages=$endYear-$startYear+1;
				
        // Check that a valid year has been provided
        if ( $currentYear > $endYear )
	{
	
            $currentYear=$endYear;
	}					
        else if ( $currentYear < $startYear )
        { 
	   $currentYear=$startYear;
	}
	$this->startYear=$startYear;
	$this->endYear=$endYear;
	$this->currentYear=$currentYear;
	
	//echo 'THE CURRENT YEAR IS ' . $currentYear;
				
				
    }
    
    
    
    function getLinkArray(){
    
	$linkArray=array();
    
	for($i=$this->startYear;$i< ($this->startYear + $this->totalPages);$i++){
	     
	    if($i==$this->currentYear)
	    {
		$linkArray[$i]=1;
	    }
	    else
	    {
		$linkArray[$i]=0;
	    }
	     
	}
	return array_reverse($linkArray,true);
    
    
    }
		
		function getStartDate(){
		      
		    return mktime(0,0,0,1,1,$this->currentYear);
		
		}
		function getEndDate(){
		   
			  $endDate=mktime(0,0,0,1,0,($this->currentYear)+1);
				
				//echo ';;;;;;;;;;;;;;;;;' . $endDate;
				
				if($endDate>time() ){
				     return time();
				}
				else{
			 			 return $endDate;
						 
				}
		}
		
    /**
    * Returns the total number of pages available
    * @return int
    * @access public
    */
    function getTotalPages () {
        return $this->totalPages;
    }

    /**
    * Returns the row to start the select with
    * @return int
    * @access public
    */
    function getStartRow() {
        return $this->startRow;
    }
}
