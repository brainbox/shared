<?php
//error_reporting('E_ALL');

define ('SHELF_LIFE', (18*60*60)  );//18 hours


Class Whitepaper{
	
	
	public $errors=array();
	
	
	function __construct($dbPDO, $id){
		$this->dbPDO=$dbPDO;
		$this->id=$id;
		$this->_initialise();
	}
	
	function _initialise(){
		
		$sql="SELECT name, filename, description FROM whitepaper WHERE id=" . $this->id;
		
		//echo $sql;
		//exit;
		
		
		$stmnt=$this->dbPDO->prepare($sql);
		
		
		
		try {
			$stmnt->execute();
			//echo 'tryyyy';
		}
		catch (PDOException $e) {
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
		
		//exit;
		$matches= $stmnt->fetchAll(PDO::FETCH_ASSOC);
		//print_r($matches);
		//exit;
		if(count($matches)!=0){
			//print_r($matches);
			$this->name=$matches[0]['name'];
			$this->filename=$matches[0]['filename'];
			$this->description=$matches[0]['description'];
			return true;
		}
		else{
			
			//echo 'YES!';
			return false;
		}
		
		
		
	}
	function id(){
		return $this->id;
		
	}
	
	function name(){
		return $this->name;
		
	}
	function filename(){
		return $this->filename;
	}
	function description(){
		return $this->description;
	}
	
	
}


Class Subscription{
	
	function __construct($dbPDO){
		$this->dbPDO=$dbPDO;
	}
	
	
	function getSubscription($whitepaper,$subscriber){
			
		$sql="SELECT id, hash, the_date FROM whitepaper_subscriber_xref WHERE whitepaper_id=" . $whitepaper->id() . " AND subscriber_id=" . $subscriber->id();
		
		//echo $sql;
		$stmnt=$this->dbPDO->prepare($sql);
		//echo 'lll';
		try {
			$stmnt->execute();
			//echo 'good';
			//exit;
			
		}
		catch (PDOException $e) {
			//echo 'bad';
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
		
		$matches= $stmnt->fetchAll(PDO::FETCH_ASSOC);
		//print_r($matches);
		//echo 'good';
		
		if(count($matches)!=0){
			
			$this->hash=$matches[0]['hash'];
			$this->theDate=$matches[0]['the_date'];
			return true;
		
		}
		else{
			//$this->errors[]='No such subscription';
			return false;
		}
		
	}
	
	function hash(){
		return $this->hash;
	}
	function theDate(){
		return $this->theDate;
	}
	
	function createSubscription($whitepaper,$subscriber){
		
		
		//check if it already exists:
		$sql="SELECT count(*) FROM whitepaper_subscriber_xref WHERE whitepaper_id=" . $whitepaper->id() . " AND subscriber_id=" . $subscriber->id();
		//echo '============';
		//echo $sql;
		$stmnt=$this->dbPDO->prepare($sql);
		//echo 'lll';
		try {
			$stmnt->execute();
			//echo 'good';
			
			
		}
		catch (PDOException $e) {
			
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
		$matches= $stmnt->fetchAll(PDO::FETCH_ASSOC);
		//print_r($matches);
		//exit;
		
		
		
		$the_time=time();
		
		//echo '<p>=====' . $the_time;
		
		//make a hash for verification purposes
		$hash= $whitepaper->id() . $subscriber->id() .  $the_time;
		//echo '<p>' . $hash;
		
		$hash = MD5($hash);
		//echo '<p>' . $hash;
		$this->hash=$hash;	
		
		
		
		$sql="INSERT INTO whitepaper_subscriber_xref (whitepaper_id, subscriber_id, hash, the_date) VALUES (" . $whitepaper->id() . ", " . $subscriber->id() . ", '$hash', " . $the_time . ")";
		//echo '============';
		//echo $sql;
		$stmnt=$this->dbPDO->prepare($sql);
		//echo 'lll';
		try {
			$stmnt->execute();
			//echo 'good';
			$this->id= $this->dbPDO->lastInsertId();
			//echo $this->id;
			//echo 'good';
			
		}
		catch (PDOException $e) {
			//echo 'bad';
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
	}
	
	function id(){
		return $this->id;	
	}
	
	function getSubscriptionByID($id, $hash=null){
		
		//echo $hash;
		
		$sql="SELECT id, whitepaper_id, subscriber_id, hash, the_date FROM whitepaper_subscriber_xref WHERE id=$id;";
		//echo $sql;
		
		$stmnt=$this->dbPDO->prepare($sql);
		//echo 'lll';
		try {
			$stmnt->execute();
			//echo 'good';
			//exit;
			
		}
		catch (PDOException $e) {
			//echo 'bad';
			print "Error!: " . $e->getMessage() . "<br/>";
			die();
		}
		
		$matches= $stmnt->fetchAll(PDO::FETCH_ASSOC);
		//print_r($matches);
		//echo count($matches);
		/*
		echo '<p>' . time();
		echo  '<p>' .( time() - 5*60*60 );
		echo  '<p>' . $matches[0]['the_date'];
		echo  '<p>' . (( time() - 1*60*60 )    -  $matches[0]['the_date']   );
		//exit;
		echo 'good';
		*/
		
		//print_r($matches);
		//exit;
		
		if(count($matches)!=0){
			
			//echo 'aaaa';
			
			$this->whitepaperID=$matches[0]['whitepaper_id'];
		
			//echo '<p>dd' . $matches[0]['hash'];
			//echo '<p>dd' . $hash;
			
			
			//Run through the error conditions
			
			//echo '<p>' . $matches[0]['hash'];
			//echo '<p>' . $hash;
			
			
			if($matches[0]['hash']!=$hash){
				//echo 'ssssssssssssssss';
				//$this->errors[]='No such subscription (hash)';
				//exit;
				return false;
			}
			elseif($matches[0]['the_date'] < (time() - SHELF_LIFE ) ){
				$this->errors[]='expired';
				//echo 'hererere';
				//return true;//There IS a subscription... it's just out of date.
			}
			
			
			//print_r($this->errors);
			return true;
		
		}
		else{
			//$this->errors[]='No such subscription';
			//sdecho 'zzz';
			//exit;
			return false;
		}
		
	}
	
	function whitepaperID(){
		return $this->whitepaperID;
	}
	
	function getErrors(){
		
		if(count($this->errors)==0){
			//no errors
			return false;
		}
		else{
			return $this->errors;
		}
	}
	
}
?>