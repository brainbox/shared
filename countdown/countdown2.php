<?php

/**
 *   Days remaining works such that
 *   ZERO days remaining is the final day
 *   
 *
 *
 */



class Countdown {

	// initialize
    
    /**
     * Expects date in "MYSQL" date format: y:m:d h:m:s
     *
     *
     */
    
    
    public $theDate; //The date we're counting down to
    public $now;
    public $daysRemaining; //This is the tricky one. For this version, 1 day to go means it's tomorrow. 0 days means today.
    
    
    
	function __construct($timestamp){
		
		$this->theDate=$timestamp;
		$this->now = time();	
			
		$this->calculate();
		
		
	}
    
    /**
     * Used for testing purposes. $now will typically be time()
     *
     *
     */
    function setNow($theDate){
        
        
        if (($timestamp = strtotime($theDate)) === false) {
            return false;
        } else {
            $this->now=$timestamp;
			$this->calculate();
            return true;
        }
		
		
        
    }
    

    public function setDate($theDate){
        
        $timestamp = strtotime($theDate);
        #echo 'TIMESTAMP IS ' . $timeStamp;
        
        if ($timestamp  === false) {
            return false;
        } else {
            #echo $timestamp;
            $this->theDate=$timestamp;
            return true;
        }
				
        
    }
    function daysLeft(){
	  	
		$this->calculate();		 
		return $this->daysRemaining;
	 
	}
    
	function calculate(){
        
        
        //Seconds
		
		//echo '<p>' . $this->theDate . ' - ' .  $this->now . '( ' . date('d m Y',$this->theDate)  , ' / ' . date('d m Y',$this->now)   . ')';
		
		
        $difference = ( $this->theDate - $this->now );
        
        
        //echo '<p>' .  $difference;
        
        //echo '<p>' .  $difference/(24*60*60);
	
        //Days
        $this->daysRemaining = ceil( $difference/(24*60*60) );
        
        
		
	 
	}
	function countBlock_old(){
		if ($this->num_days_left > 0){
	 		 echo '<div style="font-size: 10em; color: red; background: none blue; width: 1em; float: left; margin-right: 0.25em">';
	 		 echo $this->num_days_left;
	  	 echo '</div>';
	  }
	}
	function countBlock($suffix){
		if ($this->num_days_left > 0){
	 		 echo '<p>';
	 		 echo '<span>' . $this->num_days_left . '</span>';
			 if ($this->num_days_left == 1){
			 		echo ' days ';
			 }
			 else{
			 	  echo ' days ';
			 }
			 echo $suffix;
	  	 echo '</p>';
	  }
	}
	function futureText($fText){
		if ($this->num_days_left > 0){
	 		 echo $fText;
	  }
	}
	function pastText($fText){
		if ($this->num_days_left < 0){
	 		 echo $fText;
	  }
	}
	function todayText($fText){
		if ($this->num_days_left == 0){
	 		 echo $fText;
	  }
	}
	
	
	function daysText(){
		
		
		
		switch($this->daysRemaining){
			
			case 0:
				
				return 'today';
				break;
			
			case 1:
				
				return 'tomorrow';
				break;
			
			default:
			
				return 'in ' .$this->daysRemaining . ' days';
				
		}
		
	}
	
	
}


?>

