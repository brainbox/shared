<?php
require_once('../countdown2.php');

require_once ('PHPUnit/Framework.php');

class CountdownTest extends PHPUnit_Framework_TestCase
{
    
    protected function setUp(){
            
            
            
            $this->countdown= new Countdown();
            #print_r($this->countdown);
            #exit;
        
    }
    
    /**
     * test of good and bad date formats
     *
     */ 
    public function testStringToTime(){
        
        $theDate='07 jan 2008';
        $setting = $this->countdown->setDate($theDate);
        $this->assertTrue($setting);
        
        $theDate='2008-5-16';
        $setting = $this->countdown->setDate($theDate);
        $this->assertTrue($setting);
        
        $theDate='2009-31-2'; //Wrong! No 31st month!
        $setting = $this->countdown->setDate($theDate);
        $this->assertFalse($setting);
        
        
        
        
    }
    
    /**
     * Dates set correctly?
     *
     *
     */ 
    public function testCanSetTime()
    {
        #real date (wrong dates tested above)
        $day=15;
        $month=4;
        $year=2005;
        
        
        $setting = $this->countdown->setDate($year . '-' . $month . '-' . $day);
        
    
        // Assert that the size of the Array fixture is 0.
        
        $this->assertEquals(mktime(0,0,0,$month,$day, $year),$this->countdown->theDate);
    }
 
 
    /**
     * Can it count?
     *
     *
     */ 
    public function testDaysRemaining()
    {
        
        $this->countdown->setNow( '2008-1-1');
        $this->countdown->setDate('2008-1-5');
        $this->countdown->calculate();
        $this->assertEquals(4,$this->countdown->daysRemaining );
        
        $this->countdown->setNow( '2008-1-1 12:0:0');
        $this->countdown->setDate('2008-1-5');
        $this->countdown->calculate();
        $this->assertEquals(4,$this->countdown->daysRemaining );
        
        $this->countdown->setNow( '2008-1-1 23:55:55');
        $this->countdown->setDate('2008-1-5');
        $this->countdown->calculate();
        $this->assertEquals(4,$this->countdown->daysRemaining );
        
        $this->countdown->setNow( '2008-1-2 00:00:01');
        $this->countdown->setDate('2008-1-5');
        $this->countdown->calculate();
        $this->assertEquals(3,$this->countdown->daysRemaining );
        
        
         #Tomorrow = one day remaining
        $this->countdown->setNow( '2008-1-4');
        $this->countdown->setDate('2008-1-5');
        $this->countdown->calculate();
        $this->assertEquals(1,$this->countdown->daysRemaining );
        
        $this->countdown->setNow( '2008-1-4 12:00:00');
        $this->countdown->setDate('2008-1-5');
        $this->countdown->calculate();
        $this->assertEquals(1,$this->countdown->daysRemaining );
        
        $this->countdown->setNow( '2008-1-4 23:30:00');
        $this->countdown->setDate('2008-1-5');
        $this->countdown->calculate();
        $this->assertEquals(1,$this->countdown->daysRemaining );
           
        
        
        
        
        #Today = zero days remaining
        $this->countdown->setNow( '2008-1-5 00:00:00');
        $this->countdown->setDate('2008-1-5');
        $this->countdown->calculate();
        $this->assertEquals(0,$this->countdown->daysRemaining );
        
        $this->countdown->setNow( '2008-1-5 12:00:00');
        $this->countdown->setDate('2008-1-5');
        $this->countdown->calculate();
        $this->assertEquals(0,$this->countdown->daysRemaining );
        
        $this->countdown->setNow( '2008-1-5 23:30:00');
        $this->countdown->setDate('2008-1-5');
        $this->countdown->calculate();
        $this->assertEquals(0,$this->countdown->daysRemaining );
           
    }
    
}
