<?php
/**
 * The idea is that this will...
 * 
 * ... hecklinks!
 * 
 * 
 *  
 */





Class LinkChecker
{
	
    private $_ch;
    private $_httpCode;
 
    
    public function initialise()
    {
       $agent = "Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)";
       $this->_ch=curl_init();
      
       curl_setopt($this->_ch, CURLOPT_USERAGENT, $agent);
       curl_setopt($this->_ch, CURLOPT_RETURNTRANSFER, 1);
       curl_setopt($this->_ch, CURLOPT_VERBOSE,false);
       curl_setopt($this->_ch, CURLOPT_TIMEOUT, 5);
       curl_setopt($this->_ch, CURLOPT_SSL_VERIFYPEER, FALSE);
       curl_setopt($this->_ch, CURLOPT_SSLVERSION,3);
       curl_setopt($this->_ch, CURLOPT_SSL_VERIFYHOST, FALSE);
         
    }
    
    
    public function check($url)
    {
        
       $url = filter_var($url, FILTER_SANITIZE_URL);

        
       curl_setopt($this->_ch, CURLOPT_URL,$url );
          
       curl_exec($this->_ch);
   
       $this->_httpCode = curl_getinfo($this->_ch, CURLINFO_HTTP_CODE);
      
       
       
       if($this->_httpCode>=200 && $this->_httpCode<400){
           
           return true;
       
       }else{
                      
           return false;
           
       }
       
       
       
     
	}
    
    
    public function getHTTPCode()
    {
        return $this->_httpCode;
        
        
    }
    
    
    
    function  __destruct()
    {
        
         curl_close($this->_ch);
        
    }
    
    
    
}
