<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
<title><?php echo $metaTitle  ?> | BRAINBOX</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="en" />
<meta name="author" content="Gary Straughan | BRAINBOX" />



<link href="<?php echo $adminBasePath ?>css/s.css" type="text/css" rel="stylesheet" />

<?php 
if(isset($css)){
     echo '<style type="text/css">';
     echo '<!--';
     echo $css;
     echo '-->';
     echo '</style>';
}

if($cssArray){
     foreach ($cssArray as $name=>$value){
          echo '<link href="' . $adminBasePath   .$value  . '" type="text/css" rel="stylesheet"  />';
     }
}
if($javaScriptArray){
     foreach ($javaScriptArray as $name=>$value){
          echo '<script type="text/javascript" src="' . $adminBasePath .$value  . '" ></script>';
     }
}
?>
<link rel="Shortcut Icon" href="<?php echo $baseURL ?>favicon.ico" />
</head>
<body>
<div id="logo"><img src="images/logo.gif" /></div>


<div id="breadcrumb">
<?php
if(isset($breadcrumbString)){
 echo $breadcrumbString;
}
else{
  echo '&nbsp;';
}
?>
</div>
     
<!--
<div id="quicklinks">
   <a href="<?php echo $baseURL ?>">Home</a> |	<a href="<?php echo $baseURL ?>help">Help</a> 	| <a href="<?php echo $baseURL ?>contact">Contact BRAINBOX</a>
	
</div>     
-->

<!-- START topnavigation -->
<dl id="menu">
<?php
if (isset($topNavString)){
     echo $topNavString;
}
 ?>
</dl>


<?php
if (isset($subMenu)){
     //echo '<div id="submenu">';
     echo $subMenu;
     //echo '</div>';
}
?>
<!-- END topnavigation -->









<?php



if( isset($navString) &&  $navString   != '' ){
     echo '<div id="nav">';
   //  echo '<p style="font-size: 80%">In this section:</p>';
     echo $navString;
		 echo '</div>';
} 
?>

<!-- END navigation -->



     







<!-- START centercontent -->
<div id="centercontent">


<?php 
if(isset($contentString)){
     
    if(strtolower($title) != ''){ 
        //echo '<hr>';
        //print_r($pageTitleArray);
        echo '<h1>' . $title. '</h1>';
    }
    // echo '<textarea rows=100 cols=100>' . $contentString . '</textarea>';
    //There's no evaling for the Admin site!
    echo $contentString;
}
?>

<div class="spacer">&nbsp;</div>
<div id="bottomlinks">
<a href="#">Top</a> | 
 <a href="<?php echo $adminBasePath ?>">Home</a> 
 
</div>


</div>
<!-- END centercontent -->





<!--start ABOLSUTELY positioned stuff -->







<!-- START footer -->
<div id="footer">
<!--
<img style="float:right" src="<?php echo $baseURL ?>images/bb-logo-0845.gif" width="156" height="42"  alt="Call BRAINBOX on 0845 003 0025" />
-->
<strong>BRAINBOX Intelligent Web Development</strong><br />
Oxford Road South, Chiswick, London W4&nbsp;3DD United Kingdom<br />
Copyright &#169; 2002-<?php echo date('Y') ?> BRAINBOX. All rights reserved
<div class="spacer">&nbsp;</div>
</div>
<!-- END footer -->




</div>
<!--END Center Container-->

</body>
</html>

