				

Dear <?php echo $firstName ?>

You have successfully reset your password for the Member Area. Your access details are now:

      Email address: <?php echo $email ?>
			Password: <?php echo $newPassword ?>


(Note that both email address and password are case sensitive.)

The following link will take you directly to the ASLTIP Member Area:

http://www.helpwithtalking.com/member


Regards
ASLTIP

