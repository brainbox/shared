<?php

require_once 'AdminItemPage.php';

Class EventItemPage extends AdminItemPage  implements AdminItem{
        

	
        
    
        
    
    function _initialise(){
        
        
		$this->handleSession();
		
        $this->handlePOSTs();
        
        $this->handleErrors();
		
        $this->getItemData();
	

        
        $this->title .= $this->data['title'] ;
                

		
		
		$section = "events";
		$downloadArray = $this->getDownloads();
		$imageArray = $this->getImages();   
		  
		  
		ob_start();
                
            require  'views/event-item-form.php';
				
            $this->contentString .=  ob_get_contents();
                
        ob_end_clean();
        
        
	}
	
	
	
	function getItemData(){
      
        require_once ('Event/Model/Event.php');
            
		$eventsModel = new Model_Event($this->dbPDO);
		$eventsModel->isActive=false;
          
        $this->data = $eventsModel->getById($_GET['id']);
       
    }   
	
	function getDownloads(){
		
		require_once 'Download/Model/Download.php';
		
		return Model_Download::getByArticleId($this->dbPDO, $_GET['id'], 'events' );
		
	}
	
	function getImages(){
		
        require_once 'Image/Model/Image.php';
                
        return  Model_Image::getByArticleId($this->dbPDO, $_GET['id'],'events');
		
	}
	
	
	
	 function handleMainPOST(){
        
		
		require_once('forms/validation.php');
		
		$validator=new FormValidator();
		
		$validator->isEmpty('title', 'Please enter a Title for the page');
		
		$validator->isEmpty('published', 'Please enter a Publish date');
		
		if($validator->isError() ){
				
			//Store details of the form and its errors in the session
            $_SESSION['referringTime'] = time();
            $_SESSION['formVars'] = $_POST;
            $_SESSION['errors'] = $validator->getErrorList();
			
			
		} else {
				//echo 'goood';
			//exit;
			require_once ('Event/Model/Event.php');
			$eventModel = new Model_Event($this->dbPDO);
			$eventModel->updateFromPOST();
			
            //TRIGGER the event... IF it is available.
            require_once 'Observer/EventObserver.php';
            EventObserver::trigger('saveEventItem',array('NOT SURE')); 
            
            
            
			// Clear the session 
            if(isset($_SESSION['errors']) ){
                unset($_SESSION['errors']);
            }
            unset($_SESSION['formVars']);
			   
		}

        
        //Avoind "reposting" probs by redirecting
        header("Location: ".$_SERVER['REQUEST_URI']);
        die;    
        
        
    }


	
	
	
	function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'news','title'=>'Events');
		$breadcrumbArray[]=array('fullPath'=>'news?id=' . $_GET['id'] ,'title'=>$this->title );
		return $breadcrumbArray;
		
    }
       
       
	function cssArray(){
			  
		$cssArray=array();
		
		$cssArray[]='css/flexi.css'; 
		$cssArray[]='css/datepicker.css';
		return $cssArray;
                
        
	}
		
		
	function javaScriptArray(){
			
			$jsArray=array();
			$jsArray[]='js/validate_form.js';              
			$jsArray[]='js/tooltips.js';
			//$jsArray[]='js/jquery.js';
			//$jsArray[]='js/ui.datepicker.js';
			$jsArray[]='js/calendar-stuff-event.js'; // set up for the jquery calendar
		  
			$jsArray[]='js/events.js';
			
			
			return $jsArray;
	}
   
}

