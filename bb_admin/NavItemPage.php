<?php

require_once 'AdminItemPage.php';

Class NavItemPage extends AdminItemPage implements AdminItem{


  
    

    /**
     * Initialise now needs to be about CHOOSING the right from for the selected page
     *
     *
     */
    function _initialise(){
        $this->handleSession();

        $this->handlePOSTs();
        
        $this->handleErrors();

        $this->getItemData();
    
        
        
       
           
    }
       
    function getItemData(){
      
        require_once('articles/Model/Item.php');
    
        $model=new Model_Item($this->dbPDO);
       
    
        //New process
        $model->id = $_GET['id'];
        $model->active=false; //All items - not just the active ones
        $model->navOnly=false;
       
        ##This is where the checking should go
        
        

        $model->getItems();
        $this->data = $model->fetch();
        
      
		 $this->title .= $this->data['title'] ;
        
        
        
        //Convert the dates...
        //Published is easy
        $this->data['published'] = date('d-m-Y',strtotime($this->data['published']) );
       
        
        //Depublish is more complicated
        if(!$this->data['depublish'] || $this->data['depublish']=='0000-00-00 00:00:00'){

            $this->data['depublish'] = '';
                
        } else {
           
            $this->data['depublish'] = date('d-m-Y',strtotime($this->data['depublish']));
        }
        
        //Set the status here. Don't want it to change for an un-successful form submit.
        $this->data['status'] =  Model_Item::status(strtotime($this->data['published']),strtotime($this->data['depublish']) );
        

        
       
        //Download list and form
        require_once 'Download/Model/Download.php';
        
        $downloadArray =  Model_Download::getByArticleId($this->dbPDO, $_GET['id'] );
        
        
        //Get images list and form
        require_once 'Image/Model/Image.php';
        
        $section = '';
          
        $imageArray =  Model_Image::getByArticleId( $this->dbPDO, $_GET['id'], $section );
        
        $this->getPathParts();
        
        ob_start();
                
            require  'views/nav-item-form.php';
            
            $this->contentString .=  ob_get_contents();
                
        ob_end_clean();
       
    }   
    
    
    
    
    
    
    
    
    
    function handleMainPOST(){
        
        //print_r($_POST);
        require_once('forms/validation.php');
        $validator=new FormValidator();
       
        
        
        switch($_GET['type']){
                
            case 'homepage':
                
                //No validation... yet??
                break;
            
            case 'section':
                $validator->isEmpty('title', 'Please enter a Title for the page');
                //No filename for a section page    
                break;
                
            default: 
                $validator->isEmpty('title', 'Please enter a Title for the page');
                $validator->isEmpty('filename', 'Please enter a Filename');
                $validator->isAlphaNumbericDashSlash('filename', 'Numbers, letters and dashes and forward slashes only for the Filename');
        
            
            
        }
       
       
        
        if($validator->isError() ){
        
            //Store details of the form and its errors in the session
           
            $_SESSION['referringTime'] = time();
            $_SESSION['formVars'] = $_POST;
            $_SESSION['errors'] = $validator->getErrorList();
    
        } else {
            
            require_once('articles/Model/NavItem.php');
            
            $model=new Model_NavItem($this->dbPDO);
            $model->updateFromPOST();
            
            // Clear the session 
            if(isset($_SESSION['errors']) ){
                unset($_SESSION['errors']);
            }
            unset($_SESSION['formVars']);
        
        }
        
        //Avoind "reposting" probs by redirecting
        header("Location: ".$_SERVER['REQUEST_URI']);
        die;    
        
        
    }

    
    
    
    
    ///  Standard stuff   
       
    function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'navigation', 'title'=>'Navigation');
		$breadcrumbArray[]=array('fullPath'=>'',        'title'=>$this->title );
		return $breadcrumbArray;

    }
       
       
   function cssArray(){
            
        $cssArray=array();
        
        $cssArray[]='css/flexi.css'; 
        $cssArray[]='css/datepicker.css';

        return $cssArray;
    
    }
    
    
    function javaScriptArray(){
            
        $jsArray=array();
        $jsArray[]='js/validate_form.js';              //calendar
        $jsArray[]='js/tooltips.js';
        
        return $jsArray;
    }
    
    
    function getPathParts(){
       
        
        //Break up the path into "first" and "last"
        
        $path = $this->data['path'];
        $partsArray = explode( '/', $path  );
        
        if(count($partsArray)==1){

            $this->first='';
            $this->last=$path;

        } else {                                 
            
            $this->last=array_pop($partsArray);
            
            if(count($partsArray)>1){
                $this->first=implode('/' , $partsArray);
            } else {
                $this->first=$partsArray[0];
            }
        }
        
        
    }
     
     
    
    
    
    
    
    
}