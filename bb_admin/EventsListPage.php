<?php
//header('Expires: Mon, 26 July 1997 05:00:00 GMT' );
//header('Pragma: nocache');

//$this->contentString .=  '<pre>';
//print_r($_POST);
//$this->contentString .=  '</pre>';

/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description

*/

Class EventsListPage extends Page{
        
        var $section='events';
         var $sectionName='Events';//Not sure this is really needed

        function __construct($db,$basePath, $dbPDO){
               
                             
                $this->db=$db;
                $this->dbPDO=$dbPDO;
                $this->_initialise();
                
                
                $this->title =  'Events';
                $this->metaTitle= 'Events';
                
               // $this->contentString='';
               
               //Need to do this properly later
               $this->type='news';
       
       
        }
        
    
        
       // A function to apply mysql_real_escape_string   //may not need this anymore
       function escapeValue($value) {
               return mysql_real_escape_string($value);
       }
       
       
       function _initialise(){
                
                
                
                
                require_once('articles/EventItems3.php');
                
                //require_once('HTML/Table.php');
                                                         
                // Include the QuickForm class
                //require_once ("HTML/QuickForm.php");
                
                 
                // A function to apply mysql_real_escape_string
                function escapeValue($value) {
                        return mysql_real_escape_string($value);
                }
                
                
               
                
                //setup the array of SECTIONS:  section=>sectionName
                
                
                //Posted values?
                if (!empty($_POST)) {
                        
                        $params=array(	 
                        );
                        
                        //Params are ( $db, $active, $params='')
                        $flexiItems=new EventItems($this->db,false,$params);	
                        $flexiItems->createNewItem();
                                        
                }									

                //title
                
                
                
                
                //Add Item form
                $this->contentString .=  '<form  name="addNew" method="post" action="'.$_SERVER['REQUEST_URI'].'">';
                $this->contentString .=  '<input type="hidden" name="section" value="'.$this->section.'" />';
                //$this->contentString .=  '<input type="hidden" name="sectionName" value="'.$this->sectionName.'" />';
                $this->contentString .=  '<input type="submit" value="Add new item" />';
                $this->contentString .=  '</form>';
                
                
                //RETURN the list of flexiItems
                
                $params=array(	
                
                );
                
                 //Params are ( $db, $active, $params='')
                $flexiItems=new EventItems($this->db,false,$params);		
                	
                
                
                
                /*
                $table = new HTML_Table(array('summary'=>'List of Items','id'=>'itemlist'));
                
                $classArray=array( 
                array('class'=>'col1'),
                        array('class'=>'col2'),
                        array('class'=>'col3'),
        
                        array('class'=>'status')		
                );	 						 
                
                
                $table->addRow(array('Item','Publish','Depublish', 'Status'),$classArray,'TH');
                */
                
                
                $this->contentString .='<table id="itemlist">';
                while ( $flexiItem=$flexiItems->fetch() ) {
                
                        
                        
                        $this->contentString .= '<tr>';
                        $this->contentString .= '<td class="col1 '.  $flexiItem->status() .'"><a href="' . $this->type  . '?id='. $flexiItem->id() .'&section='.$this->section.'&sectionName='.$this->sectionName.'">'.$flexiItem->title.'</a></td>';
                        $this->contentString .= '<td>'. $flexiItem->dateRange().'</td>';
                        $this->contentString .= '</tr>';
                        /*
                        $table->addRow(array(
                        '<a href="' . $this->type  . '?id='. $flexiItem->id() .'&section='.$section.'&sectionName='.$sectionName.'">'.$flexiItem->title.'</a>',
                        date('d M y',$flexiItem->published()),
                        date('d M y',$flexiItem->depublish()),
                        $flexiItem->status()
                                                                                          ),
                        $classArray												
                        );				
                        
                        $table->updateRowAttributes($table->getRowCount()-1, 'class='.$flexiItem->status(), 1);
                        */
                }
                 $this->contentString .='</table>';
                //$this->contentString .=  $table->toHtml();
		//echo $this->contentString;

       }
        function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'events','title'=>'Events');
		return $breadcrumbArray;

        }
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/flexi.css'; 
                
                //$cssArray[]='build/fonts/fonts.css';                    //calendar
                //$cssArray[]='build/reset/reset.css';                    //calendar
                //$cssArray[]='build/calendar/assets/calendar.css';       //calendar
                
                //$cssArray[]='build/fonts/fonts-min.css';                //tabs
                //$cssArray[]='build/tabview/assets/skins/sam/tabview.css';//tabs
		
                return $cssArray;
                

        
        }
        function javaScriptArray(){
                
                $jsArray=array();
                $jsArray[]='js/validate_form.js';              //calendar
                $jsArray[]='js/tooltips.js';
               
                
                return $jsArray;
        }
       
}

?>

