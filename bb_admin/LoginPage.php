<?php
/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description


*/

Class LoginPage extends Page{


     function __construct($baseURL){
		      
            
    
        // Include QuickForm class
       // require_once ("HTML/QuickForm.php");
        
        // If $_GET['from'] comes from the Auth class
        if ( isset ( $_GET['from'] ) ) {
            $target=$_GET['from'];
        } else {
            // Default URL: usually index.php
            $target=$baseURL;
        }
        
		
	
	
	  
	
	/*
        // Instantiate the QuickForm class
        $form = new HTML_QuickForm('loginForm', 'POST', $target);
        
        // Add a header to the form
        //$form->addHeader('Please Login');
        
        // Add a field for the login name
        $form->addElement('text','login','Email Address');
        $form->addRule('login','Enter your email address','required',false,'client');
        
        // Add a field for the password
        $form->addElement('password','password','Password');
        $form->addRule('password','Enter your password','required',false,'client');
	
        
        // Add a submit button
        $form->addElement('submit','submit',' Login ');
        */
        
        $this->contentString  = '<p>Please log in to access the Administration Website</p>';
	$this->contentString  .=  '<form name="loginForm" action="'.$target.'" method="post">
	
	     <label for="login">Email address</label><br />
	     <input name="login" type="text" /><br />
	     
	     
	     <label for="password">Password</label><br /><input name="password" type="password" />
	     
	     <input type="submit" value="Login">
	     </form>
	';
	
        #$this->contentString .= $form->toHTML();
        
        $this->shortTitle = 'Login';
         $this->titleNote  = '';
         $this->title  = 'Login';
         $this->description = 'BRAINBOX Login Page';
        
        
              
     }			
	  
    

     function breadcrumbArray(){
          
          
          
     }
      function cssArray(){
          
          
          
     }
        function javascriptArray(){
          
          
          
     }
    

}



?>