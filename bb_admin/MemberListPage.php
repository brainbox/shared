<?php
//header('Expires: Mon, 26 July 1997 05:00:00 GMT' );
//header('Pragma: nocache');

//$this->contentString .=  '<pre>';
//print_r($_POST);
//$this->contentString .=  '</pre>';

/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description

*/

Class MemberListPage extends Page{
    
    
       

        function __construct($db,$basePath, $dbPDO){
               
                 
                $this->title =  'Members';
                
                if(isset($_GET['alpha']) ){
                        
                        $this->title .= ' - ' . $_GET['alpha'];
                }
                
                $this->metaTitle= 'Members';
                             
                $this->db=$db;
                $this->dbPDO=$dbPDO;
                $this->_initialise();
                
       
       
        }
        
    
        
       // A function to apply mysql_real_escape_string   //may not need this anymore
       function escapeValue($value) {
               return mysql_real_escape_string($value);
       }
       
       
    function _initialise(){
                
                
       
        

        //require_once('articles/NewsItems.php');
        
        //Has a letter been requested?
        
        if (!isset($_GET['alpha'])){
            
            $this->contentString .=  '<p style="margin-bottom: 20em">Please select a menu option.</p>';
          
        }
        else {

            $sql = 'SELECT id, email, forename, surname, active, available, webuser FROM member WHERE substring(surname,1,1) = "'.$_GET['alpha'].'" ORDER BY surname ASC'; 
            //only pull results where the first letter of name equals the current letter 
        
            
            //$this->contentString.=  $sql;
        
        
            $result=$this->db->query($sql);
            
            // print_r($result);
            
            if(($result->size())==0){
         
         
         
           $this->contentString.=  '<p class="error">No matching records</p>';
         
         
         
         
         }		
         else{
             
             $this->contentString.=  '<table id="itemlist">';
             $this->contentString.=  '<tr><th>ID</th><th>Member</th><th>Email</th><th>Active?</th><th>Available?</th><th>Web?</th></tr>';
         
              while($row=$result->fetch()){
                  
                      // print_r($row);
                    
                       $classString='';
                    if($row['active']!=1){
                         $classString=' class="inactive"';
                    }
                    
                    $this->contentString.=  '<tr' . $classString. '>';
                    $this->contentString.=  '<td>'.$row['id'].'</td>';
                    $this->contentString.=  '<td><a href="' . $this->basepath .'members?id='.$row['id'].'">' .$row['surname'] .', '. $row['forename'] . '</a></td>';
                    $this->contentString.=  '<td>'. $row['email'] . '</td>';
                    
                    
                    $active= 	$row['active']?'YES':'-';
                    $available= 	$row['available']?'YES':'-';
                    $webuser= 	$row['webuser']?'YES':'-';
                    
                    $this->contentString.=  '<td>' . $active . '</td>';
                    $this->contentString.=  '<td>' . $available . '</td>';
                    $this->contentString.=  '<td>' . $webuser  . '</td>';
                    $this->contentString.=  '</tr>';
                    
             }
             $this->contentString.=  '</table>';
            
         }
	
	}
	


       }
        function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'members','title'=>'Members' );
        
        if (isset($_GET['alpha'])){
            $breadcrumbArray[]=array('fullPath'=>'','title'=>$_GET['alpha']);
            
        }
        
		return $breadcrumbArray;

        }
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/flexi.css'; 
                
                //$cssArray[]='build/fonts/fonts.css';                    //calendar
                //$cssArray[]='build/reset/reset.css';                    //calendar
                //$cssArray[]='build/calendar/assets/calendar.css';       //calendar
                
                //$cssArray[]='build/fonts/fonts-min.css';                //tabs
                //$cssArray[]='build/tabview/assets/skins/sam/tabview.css';//tabs
		
                return $cssArray;
                

        
        }
        function javaScriptArray(){
                
                //$jsArray=array();
                //$jsArray[]='js/validate_form.js';              //calendar
                //$jsArray[]='js/tooltips.js';
               
                
                return false;
        }
        
        function subMenu(){
        
        $tempString='<dl id="submenu">';
        
        for($i=65; $i<=90; $i++) { //loop through the alphabet 
        
            $x = chr($i); //$x holds   
            
            $tempString .=  '<dd><a href="' . $this->basepath .'members?alpha='.$x.'">'.$x.'</a></dd>';
        }
        $tempString.= '<dd><a href="new-members">New</a></dd>';
        $tempString.= '</dl>';
        
        
        return $tempString;
        
    }
       
}

?>

