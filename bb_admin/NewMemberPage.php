<?php
//header('Expires: Mon, 26 July 1997 05:00:00 GMT' );
//header('Pragma: nocache');

//$this->contentString .=  '<pre>';
//print_r($_POST);
//$this->contentString .=  '</pre>';

/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description

*/



Class NewMemberPage extends Page{

        function __construct($db,$basePath, $dbPDO){
               
                 
                
                             
                $this->db=$db;
                $this->dbPDO=$dbPDO;
                $this->_initialise();
                
       
       
        }
        
    
        
       // A function to apply mysql_real_escape_string   //may not need this anymore
       function escapeValue($value) {
               return mysql_real_escape_string($value);
       }
     
        
       
    function _initialise(){
        
        
              
        require_once('members/MemberManagement.php');
        
        require_once('achippp/ACHIPPPmembers.php');
        require_once('HTML/Table.php');
                                 
        // Include the QuickForm class
        require_once ("HTML/QuickForm.php");
        
         
        // A function to apply mysql_real_escape_string
        function escapeValue($value) {
            return mysql_real_escape_string($value);
        }			 
        
        //print_r($_POST);
        //exit;
        
        if(isset($_POST['newuser'])  ){
            
            //echo 'JERERERE';
            $members=new ACHIPPPmembers($this->dbPDO);
            $members->createNewMember();
            
        }
        
        $tempString='';
        
        $tempString .= '<h1>New Member</h1>';
        $tempString .=  '<form method="post">';
        $tempString .=  '<input type="hidden" name="newuser" />';
        
        $tempString .=  '<input type="submit" value="Create New Member" />';
        $tempString .=  '</form>';
        
        $tempString .=  '<h2>Recently Created</h2>';
        
        
        
        $sql='SELECT id, forename, surname, email, create_date, active, available, webuser from member ORDER BY id DESC LIMIT 20';;
          
        //echo $sql;
        //exit;
        
            
        try{
            
            $stmnt=$this->dbPDO->prepare($sql);
            $stmnt->execute();
            $table='<table summary="Recently added members"  id="itemlist">';
            $table .= '	<thead>';
            $table .= '	<tr><th>Number</th><th>Name</th><th>Create Date</th><th>Active?</th><th>Available?</th><th>Web user?</th></tr>';
            $table .= '	</thead>';
            $table .= '	<tbody>';
                
            
            while ($row = $stmnt->fetch(PDO::FETCH_ASSOC  )) {
            
                // print_r ($row);
                //echo $row['create_date'];
                
                $active= 	$row['active']?'YES':'-';
                $available= 	$row['available']?'YES':'-';
                $webuser= 	$row['webuser']?'YES':'-';
                        
                            
                 $classString='';
                        if($row['active']!=1){
                             $classString=' class="inactive"';
                        }
                        
                $table .= '<tr' . $classString. '>';
                $table .= '<td>' . $row['id'] . '</td><td><a href="members?id=' . $row['id'] . '">' . $row['forename'] . ' '  . $row['surname'] . '</a></td>';
                //$table .= '<td>' . $row['email'] . '</td>';
                $table .= '<td>' . date('H:i d-M-Y',0+$row['create_date']) . '</td>';
                $table .= '<td>' . $active . '</td>';
                $table .= '<td>' . $available . '</td>';
                $table .= '<td>' . $webuser  . '</td>';
                
                
                $table .=  '</tr>';
            
         
            }
            $table .= '</tbody>';
            $table .= '</table>';
                
            $this->contentString .= $tempString . $table;
                
            
        }
        catch(Exception $e){
            $updateMessage='<span class="error">Error</span> (Please report the following error: "' . $e->getMessage() . '")';
            echo $updateMessage;
        }
        
        

        
                
        //$this->contentString .=$userForm->toHTML();
        
   
         
	
	}
	
    function maxwords($element_value, $element_name) {
        
            ////echo '<hr>here';
            
            $max=100;
            $count = count(array_filter(explode(" ", $element_value)));
            
            
            if ($count<$max) {
                return true;
            }
            else {
            
            return false;
        
            } 
        
    } 


        function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'members','title'=>'Members');
                $breadcrumbArray[]=array('fullPath'=>'','title'=>'New Members');
		return $breadcrumbArray;

        }
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/flexi.css'; 
                
                //$cssArray[]='build/fonts/fonts.css';                    //calendar
                //$cssArray[]='build/reset/reset.css';                    //calendar
                //$cssArray[]='build/calendar/assets/calendar.css';       //calendar
                
                //$cssArray[]='build/fonts/fonts-min.css';                //tabs
                //$cssArray[]='build/tabview/assets/skins/sam/tabview.css';//tabs
		
                return $cssArray;
                

        
        }
        function javaScriptArray(){
                
                $jsArray=array();
                $jsArray[]='js/validate_form.js';              //calendar
                //$jsArray[]='js/tooltips.js';
               
                
                return $jsArray;
        }
        
        
    function subMenu(){
        
        $tempString='<dl id="submenu">';
        
        for($i=65; $i<=90; $i++) { //loop through the alphabet 
        
            $x = chr($i); //$x holds   
            
            $tempString .=  '<dd><a href="' . $this->basepath .'members?alpha='.$x.'">'.$x.'</a></dd>';
        }
        $tempString.= '<dd><a href="new-members">New</a></dd>';
        $tempString.= '</dl>';
        
        
        return $tempString;
        
    }
       
}

?>

