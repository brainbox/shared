<?php

/*
echo '<pre>';
print_r($_SERVER);


print_r($_SESSION);
echo '</pre>';

echo '<p>Request is ' . $_SERVER['REQUEST_URI'];
echo '<p>Referrer is ' . $_SERVER['HTTP_REFERER'];

echo $_SESSION['referringTime'];
*/

//Kill the formVars if (a) no referrer, (b) the referrer is a different page, (c) the referring happened a while ago. 

if( !isset($_SERVER['HTTP_REFERER'])  ||
    !strstr($_SERVER['HTTP_REFERER'], $_SERVER['REQUEST_URI']) ||
    (isset($_SESSION['referringTime'])  && time() - $_SESSION['referringTime'] > 30 )
     ) {
    
    unset($_SESSION['errors']);
    unset($_SESSION['formVars']);
    
}


    

if(isset($_POST['main'])  ){
    
    //print_r($_POST);
    require_once('forms/validation.php');
    $validator=new FormValidator();
    
    
   
    $validator->isEmpty('title', 'Please enter a Title for the page');
    $validator->isEmpty('filename', 'Please enter a Filename');
    $validator->isAlphaNumbericDashSlash('filename', 'Numbers, letters and dashes and forward slashes only for the Filename');
   
    $validator->isEmpty('published', 'Please enter a Publish date');
    #  $validator->isEmpty('depublish', 'Please enter an Expiry date');
    
    if($validator->isError() ){
        
        //Store details of the form and its errors in the session
       
        $_SESSION['referringTime'] = time();
        $_SESSION['formVars'] = $_POST;
        $_SESSION['errors'] = $validator->getErrorList();

    }
    else{
        
        
        $model->updateFromPOST();
        
        // Clear the session 
        if(isset($_SESSION['errors']) ){
            unset($_SESSION['errors']);
        }
        unset($_SESSION['formVars']);
      

        
    }
    
    //Avoind "reposting" probs by redirecting
    header("Location: ".$_SERVER['REQUEST_URI']);
    die;

    
    
}














      //Check for errors - held in session.
    
    if(isset($_SESSION['errors']) ){
        
        //Need to pass the posted values back to the form
        $data['title'] =            $_SESSION['formVars']['title'];
        $last =                     $_SESSION['formVars']['filename'];
        $data['status'] =           $_SESSION['formVars']['status'];
        $data['published'] =        $_SESSION['formVars']['published'];
        $data['depublish'] =        $_SESSION['formVars']['depublish'];
        $data['meta_title'] =       $_SESSION['formVars']['metaTitle'];
        $data['meta_description'] = $_SESSION['formVars']['metaDescription'];
        $data['body'] =             $_SESSION['formVars']['body'];
        
        $this->contentString .= '<p class="error">Please correct the following error(s):</p><ul>';
        foreach ( $_SESSION['errors'] as $name=>$value){
            
            $this->contentString .= '<li>' . $value['msg'] .'</li>';
            
        }
        $this->contentString .= '</ul>';
    }
    
    
    //Try a hadn-coded form
      //Try a hadn-coded form
            $this->contentString .= '
    
            
            <form action="'.$_SERVER['REQUEST_URI'] . '" method="post" name="updateflexiItem" id="updateflexiItem">
            <input name="main" id="main" type="hidden" value="1" />
            
            <table class="form" border=0>
          
            
            
        <tr>
            <td class="col1">';
                            
                            
                       
                        
                                 
                      //echo '<hr>' . $data['parentID;
                     // exit;
                        

                     
                                
                                $this->contentString .= '
                                
                                <input name="submit1" id="submit1" value="Save" type="submit" style="float:right"/>
                       
                                
                                
                                <div class="container">
                                <h3><label for="title">Title</label></h3>
                                <input name="title" id="title" type="text" value="'.$data['title'].'" class="long checkRequired" />
                                </div>
                                
                               
                                <div class="container">
                                <h3><label for="shortTitle">Short Navigation Title</label><span> (if different)</span></h3>
                                <input name="shortTitle" id="shortTitle" type="text" value="'.$data['short_title'].'"  class="medium" />
                                </div>
                               
                                <div class="optionalContainer">
                                    <!-- Hide the Meta info -->
                                    <span class="hastooltip" >More...</span>
									
	                        <div class="optional">
                                
                                    <div class="container">
                                            <h3><label for="metaTitle">Meta Title</label></h3>
                                            <input name="metaTitle" id="metaTitle" type="text" value="' . $data['meta_title'] . '" class="long" />
                                    </div>
                                </div>
                                    
                               
                        			</div>
                        		</div>
                               
                               
                               
                               
                                <div class="container">';
                                
                                //Filepath is a tricky on
                                #echo '<pre>';
                                $path = $data['path'];
                                #echo $path;
                                ##need to split this into two bits;
                                
                                
                                $partsArray = explode( '/', $path  );
                                
                                var_dump($partsArray);
                                exit;
                                
                                if(count($partsArray)==1){

                                    $first='';
                                    $last=$path;


                                } else {                                 
                                    #echo '<p>Parts array';
                                    #print_r($partsArray);
                                    
                                    
                                    $last=array_pop($partsArray);
                                    #echo '<p>Parts array after POP';
                                    #print_r($partsArray);
                                    
                                    if(count($partsArray)>1){
                                        $first=implode('/' , $partsArray);
                                    } else {
                                        $first=$partsArray[0];
                                        
                                    }
                                }
                                #echo '<p>FIRST PArt ' . $first;
                                #echo '<P>LAST PART ' . $last;
                                
                                #exit;
                                
                                
                                
                                
                                
                                $this->contentString .= '
                                <h3><label for="path[1]">Path/Filename</label></h3>
                                <input name="filepath" id="filepath" type="hidden" value="' . $first . '"/>
                               <span class="mono">' . $first . '/</span><input name="filename" id="filename" type="text" value="' . $last . '" class="medium checkRequired mono" style="display: inline"/>
                                </div>
                                
                                
                               

                                ';
                       
                        
                       
                 $this->contentString .= '
                 
                 
                 
                    <div class="container">
                        <h3><label for="metaDescription">Meta Description</label> <span>For search engine results</span></h3>
                        <textarea rows="5" cols="80" name="metaDescription" id="metaDescription" class="description">' . $data['meta_description'] . '</textarea>
                    </div>
                 
                
                 
                 
                  <h3><label for="body">Body</label></h3>
                       
                        <div class="container">
                        <textarea rows="20" cols="80" name="body" id="body" >' . $data['body'] . '</textarea>
                        </div>
                 
                 
                 </td>
                
               ';
                
                      
          
								
                $status=Model_Item::status(strtotime($data['published']),strtotime($data['depublish']));
               
                $published = date('d-m-Y',strtotime($data['published'])); //Always a $published?
				
                $expireString = '';
			    $neverExpireString = '';
                
                if(strlen($data['depublish']) > 0){
					$depublish = date('d-m-Y',strtotime($data['depublish']));
                     $expireString = ' checked="checked" ';
				} else {
                    //If no expiry date stored, provide a default a 31 days ahead of the publish date
                    $depublish = date('d-m-Y',(strtotime($data['published']) + 31*24*60*60)); // 31 days in the future
                    $neverExpireString = ' checked="checked" ';
                }
                                
                  $this->contentString .= '
						<td>
							<div class="calender_container ' . $status . '">
                                 <h3><label for="published">Publish Date</label></h3>
                                <input type="text" id="published" name="published" class="date" value="' . $published . '" />

                              
                                <p>&nbsp;</p>
                                <h3>Expiry Date</h3>
                               
                                <input type="radio" id="expire_never" name="expire"  ' . $neverExpireString . 'value="never" />
                                <label for="expire_never">Never expire</label>
                                
                                <br />

                                <input type="radio" id="expire_date" name="expire"  ' . $expireString . ' value="date" />
                                <label for="expire_date">
                                <input type="text" id="depublish" name="depublish" class="date"  value="' . $depublish . '" />
                                </label>
                            
                            
							</div>';
                            
                            
                      
                       
                
                 $this->contentString .= '</td>
	</tr>
        
      
      
        </table>

        </form>';
       
       
       
       

