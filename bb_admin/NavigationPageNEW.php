<?php

/**
 * This class needs to provide the following to the parent class:
 * 
 *    $this->contentString	
 *    $this->shortTitle 
 *    $this->titleNote 
 *    $this->title 
 *    $this->description
 */




require_once('articles/Model/Item.php');  //Used for a statc call to get status

Class NavigationPage {
     
     var $contentString=''; 
     
     
     
     function __construct($dbPDO,$basePath){  
            
          $this->dbPDO=$dbPDO;
          
          $this->title='Navigation Pages';
          $this->_initialise();

     }
        
    
       
       
     function _initialise(){
               

          if(!empty($_POST['action']) ){

              # require_once('cms/CMS2.php');
                
               #$CMS=new CMS($this->dbPDO);
               

               
               require_once('forms/validation.php');
               
               $validator=new FormValidator;
               
               $validator->isEmpty('title', 'Please enter a title for the page');
               #$validator->isEmpty('filename', 'Please enter a filename for the page');
               #$validator->isAlphaNumbericAndDash('filename', 'The filename may contain letters, numbers and dashes only');
               
               if($validator->isError()  ){
                     
                    echo '<p class="error">Please correct the following:</p>';
                    echo '<ul>';
                    
     
                    $errors=$validator->getErrorList();
                    foreach ($errors as $name=>$value){
                         echo '<li>' . $value['msg'] . '</li>';
                    }
                    echo '</ul>';
               }
               else{
                    //echo 'adding';
                    
                    require_once('articles/Item.php');
                    $itemModel = new Model_Item($this->dbPDO);
                    $itemModel->addChild(
                         $_POST['menuID'],
                         $_POST['menuLocation'],
                         $_POST['title']) ;
                    
                    #{
                         
                         $this->contentString .= '<p>New page added successfully</p>';
                    #}
                    #else{
                        # $this->contentString .= '<p class="error">Filename already exists</p>';
                    #}
               }
            
          }
     
          //Display the TREE
        
          // Include Menu class
          require_once('UI/Menu.php');
          
          
          // Location is always the Home Page location
          $location='';
          
          require_once('UI/FullTree.php');
          
          // require_once('cms/CMS.php');
          // $CMS=new CMS($this->db);
          
          // Instantiate the FullTree class
          
          
          $isActive=false;//Need to see them regardless of state
          $includeOrphans=false;
          $menu=new FullTree($this->dbPDO, $isActive, $includeOrphans);
          
          
          #print_r($menu);
          //exit;
          $contentString='';
		  
		  //	toggle status
		  //	$contentString .= '<p class="hide_navigation_pages">Hide navigation pages that are: <a href="#" id="toggle_expired">Expired</a> | <a href="#" id="toggle_pending">Pending</a></p><br />';
		  
          // Display the FullTree menu
          $level = 0;
          
          while ( $item = $menu->fetch() ) {
                  
             
               # exit;
               
               // If this is the start of a new menu branch, increase the depth
               if ( $item->isStart() ) {
                    $level++;
                    $contentString .= '<ul>';
                    // If this is the end of a menu branch, decrease the depth
               } else if ( $item->isEnd() ) {
                    
                     $level--;
                    $contentString .= '</ul>';
                    
                    // Display a menu item
               } else {
                    // Display the menu item with bullets
                    
               #echo '<pre>';  
               #print_r($item);
               #echo '</pre>';
                    
                    /**
                     * This is the point to use the PATH of the item to determine which
                     * "form" will be used.
                     */
                    
                    #echo '<p>The kevek is ' . $level;
                   
                    //echo $item->location();
               
                    switch($item->location()){
                         
                         case(''):
                              
                              $type='homepage';
                              break;
                         
                         //Level 1 pages "does not contain a slashsa
                         case(preg_match('/^(?:(?!\/).)*$/i',$item->location() ) ? $item->location():!$item->location()) :
                              
                              $type='section';
                              break;
                         
                         
                         default:
                         
                              $type='default';
                         
                         
                    }
                    
                    //echo $type;
                    //exit;
                    
                    
                    
                    $contentString .= '<li class="' . Model_Item::status(strtotime($item->published() ) , strtotime($item->depublish() ) )  .  '"><a class="thepage" href="navigationNEW?id=' . $item->id() . '&type=' . $type . '">';
               
               
               
               
                    if(	 $item->name() ){
                         $contentString .= $item->name() ;
                    }
                    else{
                         $contentString .= 'Home Page' ;
                    }
                         
                         $contentString .= '</a>';
     
                             
                         //print_r($item);
                         //echo '<hr>' . $item->published();
                         //exit;
                         
                         //No path-changing for the Home page
                         if($item->location() !='' && $level <5){
                         
                         $contentString .= ' &nbsp;&nbsp;<a class="hastooltip"' . $item->id() . '" >More...</a>';
                         
                         
                         $contentString .= '<div class="optional">';
                         $contentString .= '<form class="addchild" id="addchild' . $item->id() . '" method="post">';
                         $contentString .= '<input type="hidden" name="action" value="addChild" />';
                         $contentString .= '<input type="hidden" name="menuID" value="' . $item->id() . '" />';
                         $contentString .= '<input type="hidden" name="menuLocation" value="' . $item->location() . '" />';
                         
                         $contentString .= '
                         <fieldset>
                         <legend>ADD a Child Page</legend>
                         <table class="form">
                         <tr><td class="labels">Page Title</td><td class="inputs"><input type="text" name="title" class="text checkRequired" /></td><td class="tips">Can be changed later. Include one or more keywords if possible</td></tr>
                         
                        
                         
                         <tr><td class="labels">&nbsp;</td><td ><input type="submit" name="submit" value="Add Child" /></td></tr>
                         </table>
                         
                         </fieldset>
                                 ';    
                                 
                         $contentString .= '</form>
                                                </div>';
                                
                       
                        }
                        
                        
                        $contentString .= '</li>' ;
                }
            }
          //Pass it back to the Class level thing
          $this->contentString .= $contentString ;

   
     }
     
     function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'navigation','title'=>'Navigation');
		return $breadcrumbArray;

     }
       
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/tree.css';
                
                
             
		
                return $cssArray;
                

        
     }
     function javaScriptArray(){
                
                $jsArray=array();
                $jsArray[]='js/tooltips.js';
                $jsArray[]='js/validation.js';
                
                return $jsArray;
        }
       
}
