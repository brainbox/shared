<?php
/*
echo '<pre>';
print_r($_POST);
print_r($_FILES);
echo '</pre>';
*/

#exit;
/**
 *
 *  This class needs to provide the following to the parent class:
 *   $this->contentString	
 *   $this->shortTitle 
 *   $this->titleNote 
 *   $this->title 
 *   $this->description
 *
 *
 *  May 08 - Changed to use Item MODEL
 *
 *  June 08 - Attempting to make this more flexible, so that different forms - and form handling - can be presented
 *  for differnet pages:
 *      + Home Page
 *      + Landing Page
 *      + "Special" pages
 *
 *
 *  Jan 11 - Now refactored to an abtract class: images and downloads are common
 *
 *
 *  
 */


interface AdminItem{
    
    function _initialise();
    
    function getItemData();
    
}


Abstract Class AdminItemPage {

    function __construct($dbPDO,$pathToUserFiles){
        
        
        $this->dbPDO=$dbPDO;
        $this->pathToUserFiles = $pathToUserFiles;
        
        
        $this->_initialise();
    
    }
    
    
    function handleSession(){
        
        if( !isset($_SERVER['HTTP_REFERER'])  ||
            !strstr($_SERVER['HTTP_REFERER'], $_SERVER['REQUEST_URI']) ||
            (isset($_SESSION['referringTime'])  && time() - $_SESSION['referringTime'] > 30 )
             ) {
            
            
            if(isset($_SESSION['errors'])){
                unset($_SESSION['errors']);
            }
            if(isset($_SESSION['formVars'])){
                unset($_SESSION['formVars']);
            }
            
            
        }

    }
    
    function handleErrors(){
        
        //Check for errors - held in session.
        
        if(isset($_SESSION['errors']) ){
          
            //Need to pass the posted values back to the form
            $data['title'] =            $_SESSION['formVars']['title'];
            $last =                     $_SESSION['formVars']['filename'];
            $data['status'] =           $_SESSION['formVars']['status'];
            $data['published'] =        $_SESSION['formVars']['published'];
            $data['depublish'] =        $_SESSION['formVars']['depublish'];
            $data['meta_title'] =       $_SESSION['formVars']['metaTitle'];
            $data['meta_description'] = $_SESSION['formVars']['metaDescription'];
            $data['body'] =             $_SESSION['formVars']['body'];
          
        }
    }
    
    
    
    
    function handlePOSTs(){
        
        
        if( !empty($_POST['main'])  ){
            
            $this->handleMainPOST();
        }
        
        
        if(!empty($_POST['addImage'])  && !empty($_FILES)  ){
            
            $this->handleImagePOST();
            
        }
        
        if( !empty($_POST['deleteImage']) ){
                
            $this->handleRemoveImagePOST();
                
        }

       
        //Handle new file
        if(!empty($_POST['addDownload'])  && !empty($_FILES)  ){
        
            $this->handleDownloadPOST();
            
        }
        
        //Handle removal
        if( !empty($_POST['deleteDownload']) ){
               
            $this->handleRemoveDownloadPOST();  
                
        }

    }
    
    

    
    
    function handleImagePOST(){
        
        
        require_once 'Image/Model/Image.php';
        
        $image = Model_Image::addFromPOST($this->dbPDO, $this->pathToUserFiles); 
        
        //Avoid "reposting" probs by redirecting
        header("Location: ".$_SERVER['REQUEST_URI']);
        die;
    
    }
    
    function handleRemoveImagePOST(){
        
        require_once 'Image/Model/Image.php';
        Model_Image::deleteFromPOST($this->dbPDO, $this->pathToUserFiles);
        
        //Avoid "reposting" probs by redirecting
        header("Location: ".$_SERVER['REQUEST_URI']);
        die;
    }
    
    function handleDownloadPOST(){
        
        require_once 'Download/Model/Download.php';
        Model_Download::addFromPOST($this->dbPDO, $this->pathToUserFiles);
        
        //Avoid "reposting" probs by redirecting
        header("Location: ".$_SERVER['REQUEST_URI']);
        die;
        
    }
    
    
    function handleRemoveDownloadPOST(){
     
        require_once 'Download/Model/Download.php';
        Model_Download::deleteFromPOST($this->dbPDO,$this->pathToUserFiles );
        
        //Avoid "reposting" probs by redirecting
        header("Location: ".$_SERVER['REQUEST_URI']);
        die;
        
    }
    
    
    
}