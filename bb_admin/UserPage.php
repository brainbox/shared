<?php
//error_reporting(E_ALL);
//echo '<pre>';
//print_r($_POST);
//echo '</pre>';

require_once('permissions/User.php');
require_once('permissions/Group.php');
require_once('permissions/Permission.php');
require_once('permissions/GroupPermission.php');
require_once('permissions/GroupUser.php');

Class UserPage extends Page{

    function __construct($dbPDO,$basePath){
               
        $this->title =  'User Management';        
        $this->dbPDO=$dbPDO;
        
        if (!empty($_POST)){
            $this->_process();
        }
        else {
        $this->_initialise();
        }
    }
        
    /**
     * A function to apply mysql_real_escape_string   //may not need this anymore
     *
     */

    function escapeValue($value) {
        return mysql_real_escape_string($value);
    }
       
       
    private function _initialise(){

       
        //Users
        $this->contentString .= '<h2>Users</h2>';
        $this->contentString .= '<table border=1>';
        $this->contentString .= '<tr>';
        $this->contentString .= '<th>id</th><th>login</th><th>--</th><th>password</th><th>--</th></tr>';
       
        $users =new AdminUsers($this->dbPDO);
        
        while ($user=$users->fetch() ){
                
             $this->contentString .= '<tr>';    
            //Form for general updates    
            $this->contentString .= '<form method="post" >
                                    <input type="hidden" name="action" value="updateUser" />
                                    <input type="hidden" name="userID" value="'. $user->id . '" />';
           
            $this->contentString .= '<td>' . $user->id  . '</td>';
            $this->contentString .= '<td><input type="text" name="login" value="' . $user->login  . '" style="width: 20em" /></td>';
            $this->contentString .= '<td><input type="submit" value="Update" /></td>';
            $this->contentString .= '</form>';
              
            //Form for password changes  
             $this->contentString .= '<form method="post" >
                                    <input type="hidden" name="action" value="changePassword" />
                                    <input type="hidden" name="userID" value="'. $user->id . '" />';
                
              $this->contentString .= '<td><input type="text" name="password"style="width: 10em" /></td>';
             $this->contentString .= '<td><input type="submit" value="Change Password" /></td>';
            
            
            $this->contentString .= '</form>';
            
            $this->contentString .= '</tr>';
                
        }
        
        $this->contentString .= '</table>';
        
        
         $this->contentString .= '<form method="post" >
                                    <input type="hidden" name="action" value="createUser" />
                                    <input type="text" name="name"  />
                                    <input type="submit"  value="Create New User" />
                                    </form>';
                                    
        
        
        
        
        
        
        
        //Groups and Users
        $this->contentString .= '<h2>Groups</h2>';
        $this->contentString .= '<table border=1>';
        $this->contentString .= '<tr>';
        $this->contentString .= '<th>--</th>';
       
        $groups =new AdminGroups($this->dbPDO);
       
        
        //Table header
        while ($group=$groups->fetch() )
        {
            $this->contentString .= '<th>'.$group->name.'</th>';
        }
        //Now need the users - a table row for each
        $this->contentString .= '<th>--</th>';
        $this->contentString .= '</tr>';

        while ($user=$users->fetch() ){
                
            $this->contentString .= '<form method="post" >
                                    <input type="hidden" name="action" value="updateUserGroup" />
                                    <input type="hidden" name="userID" value="'. $user->id . '" />';
            $this->contentString .= '<tr>';
            $this->contentString .= '<td>' . $user->login  . '</td>';
            
            //Need the Groups they belog to
            
            while ($group=$groups->fetch() )
            {
                if ($user->belongsTo($group) ){
                    $this->contentString .= '<td><input type="checkbox" name="group[' . $group->id . ']" checked="checked" /></td>';
                }
                else{
                    $this->contentString .= '<td><input type="checkbox" name="group[' . $group->id . ']" /></td>';
                }
            }
             $this->contentString .= '<td><input type="submit" value="Update" /></td>';
            $this->contentString .= '</tr>';
             $this->contentString .= '</form>';
            //Find the groups belonged to.
                
        }
        
        $this->contentString .= '</table>';
        
        $this->contentString .= '<form method="post" >
                                    <input type="hidden" name="action" value="createGroup" />
                                    <input type="text" name="name"  />
                                    <input type="submit"  value="Create New Group" />
                                    </form>';
        
  

        
        
        
        
        
        
        //Groups and Permissions
        $this->contentString .= '<h2>Permissions</h2>';
        $this->contentString .= '<table border=1>';
        $this->contentString .= '<tr>';
        $this->contentString .= '<th>--</th>';
       
        $perms =new AdminPermissions($this->dbPDO);
       
        
        //Table header
        while ($perm=$perms->fetch() )
        {
            $this->contentString .= '<th>'.$perm->name.'</th>';
        }
         $this->contentString .= '<th>--</th>';
        
        
        
        //Now need the groups - a table row for each

        while ($group=$groups->fetch() ){
                
            $this->contentString .= '<tr>';
            $this->contentString .= '<td>' . $group->name  . '</td>';
            
            //Need the Permissions they have they belog to
            while ($perm=$perms->fetch() )
            {
                
                $this->contentString .= '<form method="post" >
                                    <input type="hidden" name="action" value="updateGroupPermission" />
                                    <input type="hidden" name="groupID" value="'. $group->id . '" />';
                
                if ($group->hasPermission($perm)){
                    $this->contentString .= '<td><input type="checkbox" name="perm[' . $perm->id . ']" checked="checked" /></td>';
                }
                else{
                    $this->contentString .= '<td><input type="checkbox" name="perm[' . $perm->id . ']" /></td>';
                }
            }
              $this->contentString .= '<td><input type="submit" value="Update" /></td>';
            $this->contentString .= '</tr>';
             $this->contentString .= '</form>';
            
           
                
        }
        $this->contentString .= '</table>';
        
        $this->contentString .= '<form method="post" >
                                    <input type="hidden" name="action" value="createPermission" />
                                     <input type="text" name="name"  />
                                    <input type="submit"  value="Create New Permission" />
                                    </form>';
        
        
        
     
     
	
    }
	
    private function _process(){
        
        if (isset($_POST['action'])){
            switch ($_POST['action']){
                
                case('updateUserGroup'):            
                
                
                    //require_once('permissions/GroupUser.php');
                    //echo '<h1>Update the User/group associations</h1>';
                    //Kill 'em then start again. Do that in the class - no SQL HERERE
                    $groupUser= new AdminGroupUser($this->dbPDO);
                    
                    $groupUser->assignUserToGroups($_POST['userID'],$_POST['group']);
                
                
                
                
                break;
            
                case('updateGroupPermission'):
                
                    //require_once('permissions/GroupPermission.php');
                    
                    //Kill 'em then start again. Do that in the class - no SQL HERERE
                    $groupPerm= new AdminGroupPermission($this->dbPDO);
                    
                    $groupPerm->assignGroupToPermissions($_POST['groupID'],$_POST['perm']);
                
                
                
                
                break;
            
              
            
                case('updateUser'):
                    
                    $users= new AdminUsers($this->dbPDO);
                    
                    $user=$users->getByID($_POST['userID']); 
                    
                    $user->login=$_POST['login'];
                    
                    
                   
                    $user->finalise();
                    
                break;
            
                case('changePassword'):
                    
                    $users= new AdminUsers($this->dbPDO);
                    $user=$users->getByID($_POST['userID']);    
                    $user->setPassword($_POST['password']);
                    
                break;    
                    
                case('createUser'):
    
                    $users= new AdminUsers($this->dbPDO);
                    $users->create($_POST['name']);
    
                break;
            
                case('createGroup'):
    
                    $users= new AdminGroups($this->dbPDO);
                    $users->create($_POST['name']);
    
                break;
            
                case('createPermission'):
    
                    $users= new AdminPermissions($this->dbPDO);
                    $users->create($_POST['name']);
    
                break;    
            }
        }

        
        $this->_initialise();
        
    }
       function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'dummy','title'=>'Users');
		return $breadcrumbArray;

     }
       
       

  function cssArray(){
                
              
            

        
        }
        function javaScriptArray(){
                
            
        }
      
}

?>

