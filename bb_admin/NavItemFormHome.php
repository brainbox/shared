<?php

/**
 * Chopped-down version for the Home Page
 *
 *
 */


//Kill the formVars if (a) no referrer, (b) the referrer is a different page, (c) the referring happened a while ago. 

if( !isset($_SERVER['HTTP_REFERER'])  ||
    !strstr($_SERVER['HTTP_REFERER'], $_SERVER['REQUEST_URI']) ||
    (isset($_SESSION['referringTime'])  && time() - $_SESSION['referringTime'] > 30  )
    ) {
    
    unset($_SESSION['errors']);
    unset($_SESSION['formVars']);
    
}


    

if(isset($_POST['main'])  ){
    
    //print_r($_POST);
    require_once('forms/validation.php');
    $validator=new FormValidator();
    
    
   
    #$validator->isEmpty('title', 'Please enter a Title for the page');
    #$validator->isEmpty('filename', 'Please enter a Filename');
    #$validator->isAlphaNumbericDashSlash('filename', 'Numbers, letters and dashes and forward slashes only for the Filename');
   
    #$validator->isEmpty('published', 'Please enter a Publish date');
    #  $validator->isEmpty('depublish', 'Please enter an Expiry date');
    
    if($validator->isError() ){
        
        //Store details of the form and its errors in the session
       
        $_SESSION['referringTime'] = time();
        $_SESSION['formVars'] = $_POST;
        $_SESSION['errors'] = $validator->getErrorList();

    }
    else{
        
        
        $model->updateFromPOST();
        
        // Clear the session 
        if(isset($_SESSION['errors']) ){
            unset($_SESSION['errors']);
        }
        unset($_SESSION['formVars']);
      

        
    }
   
    //Avoind "reposting" probs by redirecting
    header("Location: ".$_SERVER['REQUEST_URI']);
    die;

    
    
}

      //Check for errors - held in session.
    
    if(isset($_SESSION['errors']) ){
        
        //Need to pass the posted values back to the form
        #$data['title'] =            $_SESSION['formVars']['title'];
        #$last =                     $_SESSION['formVars']['filename'];
        #$data['status'] =           $_SESSION['formVars']['status'];
        #$data['published'] =        $_SESSION['formVars']['published'];
       # $data['depublish'] =        $_SESSION['formVars']['depublish'];
        $data['meta_title'] =       $_SESSION['formVars']['metaTitle'];
        $data['meta_description'] = $_SESSION['formVars']['metaDescription'];
        $data['body'] =             $_SESSION['formVars']['body'];
        
        $this->contentString .= '<p class="error">Please correct the following error(s):</p><ul>';
        foreach ( $_SESSION['errors'] as $name=>$value){
            
            $this->contentString .= '<li>' . $value['msg'] .'</li>';
            
        }
        $this->contentString .= '</ul>';
    }
    
    
    //Try a hadn-coded form
    $this->contentString .= '

    <h1>Homedd</h1>
    
    <form action="'.$_SERVER['REQUEST_URI'] . '" method="post" name="updateflexiItem" id="updateflexiItem">
    
    <input name="main" id="main" type="hidden" value="1" />
    <input name="published"  type="hidden" value="01-01-2000" />
    <input name="depublish"  type="hidden" value="" />
    <input name="expire"  type="hidden" value="never" />
    
    
    
    <table class="form" border=0>
  
 
<tr>
    <td class="col1">
   
        
       
        <div class="container">
        <h3><label for="metaTitle">Meta Title</label>  <span>for search engine results</span></h3>
        <input name="metaTitle" id="metaTitle" type="text" value="' . $data['meta_title'] . '" class="long" />
        </div>
       
       
        
        
        <input name="submit" value="Save" type="submit" style="float:right"/>
        
        <div class="container">
        <h3><label for="metaDescription">Meta Description</label> <span>for search engine results</span><h3>
        <textarea rows="5" cols="80" name="metaDescription" id="metaDescription" class="description">' . $data['meta_description'] . '</textarea>
        </div>
         

                <h3><label for="body">Body</label></h3>
               
                <div class="container">
                <textarea rows="20" cols="80" name="body" id="body" >' . $data['body'] . '</textarea>
                </div>

        </td>
        
       
        
        <td>
            <!-- NO DATES -->
            
        </td>
            
        
        
    
</tr>

</table>

</form>


';