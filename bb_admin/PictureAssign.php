<?php
/**
 * This page isn't access protected: if you have access to (say) news, you need to be able to assign images
 *
 *
 *
 *
 *
 */



Class PictureAssign extends Page{

    public $dbPDO;
    public $pathToUserFiles;
    
    function __construct(){
           
           
            
    }
   
   
   function initialise(){
            
        if(!$this->dbPDO ){
               
               throw Exception('No db');
               
        }
       
        $this->handlePOST();
       
       
        if( isset($_GET['referrerEncoded']) ){
            
           $this->getListForAssignment();
            
           //return;
            
        }
            
            
           
            
    }
    
    /**
     *
     *
     *
     *
     */
    
   
    function handlePOST(){
        
        
        
        if(!isset($_POST['action'])){
            
            return;
            
        }
        
       
        
        
        switch($_POST['action']){
            
            
            
            case 'assign':
                $this->imageAssign();
                break;
            
            case 'unassign':
                $this->imageUnassign();
                break;
            
            default:
            
                //no default - intentional
            
            
        }
    }
   

    
    function imageAssign(){
        
        require_once 'Image/Model/Image.php';
        
        Model_Image::assignFromPOST($this->dbPDO);
        
        $fullPath = 'http://' . $_SERVER['SERVER_NAME'] . urldecode($_POST['referrerEncoded']);
        
        header("Location: $fullPath" );
        
    }
    
    function imageUnassign(){
        
        
        
        require_once 'Image/Model/Image.php';
        
        Model_Image::unassignFromPOST($this->dbPDO);
        
        //Redirect!
        $fullPath = 'http://' . $_SERVER['SERVER_NAME']  . urldecode($_POST['referrerEncoded']);
        
        header("Location: $fullPath" );
        
    }

        
    
    /**
     * Referrer is urlencoded. Need to pick the bones out of it to get an itm
     * 
     *
     *
     */
    function getListForAssignment(){
            

                    
            $id = $this->getIdFromReferrer();            
            
            $itemType= $_GET['itemType'];
           
            
            require_once 'Image/Model/Image.php';
            
            $imageArray = Model_Image::getAll($this->dbPDO);
            
            ob_start();
            
                    require  'views/image-list-for-assignment.php';
                        
                    $this->contentString .=  ob_get_contents();
                        
            ob_end_clean();
            

            
    }
    
    
    
    function getIdFromReferrer(){
        
        $referrer = urldecode($_GET['referrerEncoded']);
            
        $querystring = explode('?', $referrer);
         
        $querystring = $querystring[1]; 
         
        //echo $querystring;
         
        parse_str($querystring, $output);

        //var_dump($output);
        
        return $output['id'];
        
    }
    
    
    
    
    
        
     function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'news','title'=>'News');
		return $breadcrumbArray;

        }    
        
    function javaScriptArray(){
           
           $jsArray=array();
        
           return $jsArray;
    }
      
}

