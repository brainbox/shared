<?php

//Process image upload
if( isset($_FILES['image']) && $_FILES['image']['name']  ){
            
    $model->updateImage($_SERVER['DOCUMENT_ROOT'] . $this->basePath . 'html/images/pages/');//parameter is the folder to which the image is to be stored
    
    //Avoid re-submit problems
    header("Location: ".$_SERVER['REQUEST_URI']);
    die;

}

$this->contentString .='

 <!-- images -->
<!-- Image names are changed, but there is no image manipulation -->


<form action="'.$_SERVER['REQUEST_URI'] . '" method="post" enctype="multipart/form-data" name="addImage" id="addImage">

    <input name="id" id="id" type="hidden" value="' . $_GET['id'] . '" />
    <input type=hidden name="MAX_FILE_SIZE" value="1048576" />
    

    <!--<label for="image[0]">Choose Image</label></td><td><input type="file" name="image"  />
    <input type="submit" value="Add/Update Image(s)" />-->
                    
    <h2>Image</h2>
    
    <table summary="layout" class="images">
        <tr>
                <td class="label"><label for="image">Main</label></td>
                <td class="path"><input type="file" name="image" size="60" /></td>
                <td class="view">';
                    
    if($data['image']){
            $this->contentString .= '<a href="' . $this->basePath . 'html/images/pages/' .$data['image'] . '">View main image</a>';
    }
                    
    $this->contentString .= '</td>
            </tr>
            <tr>
                    <td class="submit" colspan="3"><input type="submit" value="Add/Update Image" /></td>
            </tr>
    </table>
</form>';