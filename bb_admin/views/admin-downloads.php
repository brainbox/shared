<h2>Downloads</h2>
<table class="form">
    <?php foreach($downloadArray as $download): ?>
    <?php $download = (object) $download ?>
        <tr>
            <td style="width: 600px">
                <strong><?php echo $download->filename ?></strong>
                (Uploaded <?php echo date('j F Y',strtotime($download->create_date)) ?>)
            </td>
            <td>
                <form action="" method="post" >
                    <input type="hidden" name="deleteDownload" value="1" />
                    <input type="hidden" name="section" value="<?php echo $section ?>" />
                    <input type="hidden" name="id" value="<?php echo $download->id ?>" />
                     <input type="hidden" name="filename" value="<?php echo $download->filename ?>" />
                    <input name="submit" value="delete" type="submit" />
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
    
    <tr>
        <td colspan="2">
            <form action="" method="post" name="upload" id="upload" enctype="multipart/form-data" >
                <fieldset>
                     <input type="hidden" name="addDownload" value="1" />
                      <input type="hidden" name="articleId" value="<?php echo $_GET['id'] ?>" />
                     <input type="hidden" name="section" value="<?php echo $section ?>" />
                    <input name="MAX_FILE_SIZE" type="hidden" value="<?php echo defined('MAX_UPLOAD_SIZE_IN_BYTES') ? MAX_UPLOAD_SIZE_IN_BYTES : '10485760' ?>" />
                    <ul>
                    <li>
                        <label for="file"><span style="color: #ff0000">*</span>Select file:</label>
                        <input name="file" type="file" />
                    </li>
                        
                    <li>
                        <input name="submit" value="Upload" type="submit" />
                    </li>
                    </ul>
                <fieldset>
            </form>
        </td>
    </tr>
    
</table>



