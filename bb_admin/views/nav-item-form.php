<div id="tabs">
    
    
    <ul>
        <li><a href="#fragment-1"><span>Main</span></a></li>
        
        <?php if($_GET['type'] != 'homepage'): ?>
            <li><a href="#fragment-2"><span>Images</span></a></li>
            <li><a href="#fragment-3"><span>Downloads</span></a></li>
        <?php endif ?>
        
        <li><a href="#fragment-4"><span>Cheatsheet</span></a></li>
    </ul>
   
    
    <div id="fragment-1">
         
        <?php require 'nav-item-form-main.php';  ?>
        
    </div>
    
    
    <?php if($_GET['type'] != 'homepage'): ?>
    
        <div id="fragment-2">
            
            <?php require 'bb_admin/views/admin-image.php' ?>
            
        </div>
        
        
        <div id="fragment-3">
            
            <?php require 'admin-downloads.php' ?>
        
        </div>
        
    <?php endif ?>
    
    
    <div id="fragment-4">
        
        <?php require 'markdown-cheatsheet.php';  ?>
        
    </div>
    
</div>
