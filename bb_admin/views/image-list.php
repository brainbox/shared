
<form action="" method="post" enctype="multipart/form-data" class="add_form" >
    <fieldset>
        <input type="hidden" name="action" value="add">
        <input name="MAX_FILE_SIZE" type="hidden" value="10485760">
        <ul>
            <li>
                <label for="file"><span style="color: #ff0000">*</span>Select image:</label>
                <input name="image" type="file">
            </li>
            <li>
                <input name="submit" value="Upload" type="submit">
            </li>
        </ul>
    </fieldset>
</form>            
           
             

<div class="pictures table">
    <?php $i=1; ?>
    
    
    <?php foreach($imageArray as $image): ?>
        <?php echo $i%2?'<div class="table-row">':''; ?>

        <div class="table-cell picture">
                <img src="/usr/images/<?php echo $image['filename'] ?>"   width="<?php echo $image['width'] ?>" height="<?php echo $image['height'] ?>"  />
        </div>
        
        <?php echo $i%2?'':'</div>'; ?>
        <?php  $i++ ?>
    <?php endforeach ?>
</div>