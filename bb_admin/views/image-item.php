<img style="float:right; margin: 0 0 20px 20px" src="/usr/images/<?php echo $image['filename'] ?>"   width="<?php echo $image['width'] ?>" height="<?php echo $image['height'] ?>"  />


<p>Image added/updated: <?php echo date('j F Y, H:i' , strtotime($image['create_date']) )  ?></p>


<h3>Link to the image:</h3>
<p>Caution! Cut and pasted links will NOT be updated automatically.</p>


<p class="cutandpaste">&lt;img src="/usr/images/<?php echo $image['filename'] ?>"   width="<?php echo $image['width'] ?>" height="<?php echo $image['height'] ?>" alt="<?php echo $image['alt'] ?>" /&gt;</p>
 

<h3>Add/Update Alt text</h3>

<p>Describe the image. (This text won't be visible on the web page.)</p>

<form action="" method="post" >
    <fieldset>
        <input type="hidden" name="action" value="alt">
        <input type="hidden" name="id" value="<?php echo $image['id'] ?>">
        <ul>
            <li>
                <textarea name="alt"><?php echo $image['alt'] ?></textarea>
            </li>
            <li>
                <input type="submit" value="Submit" />
            </li>
        </ul>
    </fieldset>
</form>            
           




<h3>Replace image</h3>
<p>Caution! The image may be assigned and (worse) its link may have been cut and pasted.</p>

<form action="" method="post" enctype="multipart/form-data" class="add_form" >
    <fieldset>
        <input type="hidden" name="action" value="replace">
        <input name="MAX_FILE_SIZE" type="hidden" value="10485760">
        <input type="hidden" name="id" value="<?php echo $image['id'] ?>">
        <input type="hidden" name="filename" value="<?php echo $image['filename'] ?>">
        <ul>
            <li>
                <label for="file"><span style="color: #ff0000">*</span>Select image:</label>
                <input name="image" type="file">
            </li>
            <li>
                <input name="submit" value="Upload" type="submit">
            </li>
        </ul>
    </fieldset>
</form>            
