<div id="tabs">
    
    
    <ul>
        <li><a href="#fragment-1"><span>Main</span></a></li>
        <li><a href="#fragment-2"><span>Images</span></a></li>
        <li><a href="#fragment-3"><span>Downloads</span></a></li>
        <li><a href="#fragment-4"><span>Cheatsheet</span></a></li>
    </ul>
    
    
    <div id="fragment-1">
        
        <?php require 'news-item-form-main.php';  ?>
        
    </div>
    
    
    
    <div id="fragment-2">
        
        <?php require 'bb_admin/views/admin-image.php' ?>
        
        <?php if(CLIENT_SHORT_NAME == 'forward2'): ?>
            <p>Recommended image widths:</p>
            <ul>
                <li><strong>300px</strong>  - to align with text</li>
                <li><strong>400px</strong>  - to align with the edge of the page</li>
            </ul>
            <p>Need more help? Watch this <a href="http://www.youtube.com/watch?v=ZZ-ngbwvAMY">how-to video</a> on YouTube.</p>
        <?php endif ?>
    </div>
    
    
    <div id="fragment-3">
        
        <?php require 'admin-downloads.php' ?>
        
    </div>
    
    <div id="fragment-4">
        
        <?php require 'markdown-cheatsheet.php';  ?>
        
     </div>
    
</div>
