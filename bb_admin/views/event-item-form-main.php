
    
<?php  if(!empty($_SESSION['errors']) ): ?>
   
    <p class="error">Please correct the following error(s):</p>
    <ul>
        
        <?php foreach ( $_SESSION['errors'] as $name=>$value): ?>
            
            <li><?php echo $value['msg'] ?></li>
            
         <?php endforeach; ?>
         
    </ul>
    
<?php endif; ?>
    
    
    


            
<form action="" method="post" name="updateflexiItem" id="updateflexiItem">
    
    <input name="main" type="hidden" value="1" />
    <input name="id"  type="hidden" value="<?php echo $_GET['id'] ?>" />
    
    <table class="form" border=0>

        <tr>
            <td class="col1">
                            
                               
            <input name="submit1" id="submit1" value="Save" type="submit" style="float:right"/>
         
                <div class="container">
                <h3><label for="title">Title</label></h3>
                <input name="title" id="title" type="text" value="<?php echo htmlspecialchars($this->data['title']) ?>" class="long checkRequired" />
                </div>
           
                <div class="optionalContainer">
                    <!-- Hide the Meta info -->
                    <span class="hastooltip" >More...</span>
                                                        
                    <div class="optional">
                    
                        <div class="container">
                                <h3><label for="metaTitle">Meta Title</label></h3>
                                <input name="metaTitle" id="metaTitle" type="text" value="<?php echo htmlspecialchars($this->data['meta_title']) ?>" class="long" />
                        </div>
                    </div>
                </div>
                               
                               
                               
            <div class="container">            
            
             
            <div class="container">
                <h3><label for="metaDescription">Meta Description</label> <span>For search engine results</span></h3>
                <textarea rows="5" cols="80" name="metaDescription" id="metaDescription" class="description checkRequired"><?php echo  $this->data['meta_description'] ?></textarea>
            </div>
         
                
                 
                 
        <h3><label for="body">Body</label></h3>
             
              <div class="container">
              <textarea rows="20" cols="80" name="body" id="body" class="checkRequired" ><?php echo  $this->data['body'] ?></textarea>
              </div>
       
       
       </td>
                
       
            <td>

           
                <div class="calender_container <?php echo $status ?>">
                
                    <h3><label for="published">Publish Date</label></h3>
                    <input type="text" id="published" name="published" class="date" value="<?php echo $published ?>" />
 
                  
                    <p>&nbsp;</p>
                    <h3>Expiry Date</h3>
                   
                    <input type="radio" id="expire_never" name="expire"  <?php echo $neverExpireString ?> value="never" />
                    <label for="expire_never">Never expire</label>
                    
                    <br />
 
                    <input type="radio" id="expire_date" name="expire"  <?php echo $expireString ?> value="date" />
                    <label for="expire_date">
                    <input type="text" id="depublish" name="depublish" class="date"  value="<?php echo $depublish ?>" />
                    </label>
                            
                            
		</div>
            
            
                <div class="calender_container startend">
                    
                    <h3><label for="start_date">Event Start Date</label></h3>
                    <input type="text" id="startDate" name="startDate" class="date" value="<?php echo $startDate ?>" />
                    
                    <p>&nbsp;</p>
                    <h3>Event End Date</h3>
                    
                    <input type="radio" id="oneday_true" name="oneday"  <?php echo $noEndDateString ?> value="true" />
                    <label for="oneday_true">One day event</label>
                    
                    <br />
                    
                    <input type="radio" id="oneday_false" name="oneday"  <?php echo $endDateString ?> value="false" />
                    <label for="oneday_false">
                    <input type="text" id="endDate" name="endDate" class="date"  value="<?php echo $endDate ?>" />
                    </label>
                            
                </div>

                

	    </td>
        

	</tr>
        
      
      
    </table>

</form>

