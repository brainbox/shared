<?php

    switch( $section ){
        
        case 'news';
            $imageFolder = '/usr/images/news/';
            break;
            
        case 'events';
            $imageFolder = '/usr/images/events/';
            break;
        
        default:
            $imageFolder = '/usr/images/articles/';
        
    }

?>



<h2>Images</h2>
<table class="form">
    
    <?php foreach($imageArray as $image): ?>
    <?php $image = (object) $image ?>
        <tr>
            <td style="width: 600px">
                 <img src="<?php echo $imageFolder ?><?php echo $image->filename ?>" width="50" height="50" >
               
                &lt;img src="<?php echo $imageFolder ?><?php echo $image->filename ?>" width="<?php echo $image->width ?>" height="<?php echo $image->height ?>" alt="" /&gt;
                
            </td>
            <td>
                <form action="" method="post" >
                    <input type="hidden" name="deleteImage" value="1" />
                    <input type="hidden" name="section" value="<?php echo $section ?>" />
                    <input type="hidden" name="id" value="<?php echo $image->id ?>" />
                    <input type="hidden" name="filename" value="<?php echo $image->filename ?>" />
                    <input name="submit" value="Delete" type="submit" />
                </form>
            </td>
        </tr>
    <?php endforeach; ?>
    
        <tr>
            <td colspan="2">
                <form action="" method="post"  enctype="multipart/form-data" >
                    <fieldset>
                        <input type="hidden" name="addImage" value="1" />
                        <input type="hidden" name="articleId" value="<?php echo $_GET['id']  ?>" />
                        <input type="hidden" name="section" value="<?php echo $section ?>" />
                      
                        
                        
                        <input name="MAX_FILE_SIZE" type="hidden" value="10485760" />
                        <ul>
                            <li>
                                <label for="file"><span style="color: #ff0000">*</span>Select image:</label>
                                <input name="image" type="file" />
                            </li>
                                
                            <li>
                                <input name="submit" value="Upload" type="submit" />
                            </li>
                        </ul>
                    <fieldset>
                </form>
            </td>
        </tr>
    
</table>



