
    
<?php  if(!empty($_SESSION['errors']) ): ?>
   
    <p class="error">Please correct the following error(s):</p>
    <ul>
        
        <?php foreach ( $_SESSION['errors'] as $name=>$value): ?>
            
            <li><?php echo $value['msg'] ?></li>
            
         <?php endforeach; ?>
         
    </ul>
    
<?php endif; ?>
    
    

            
<form action="" method="post" name="updateflexiItem" id="updateflexiItem">
    
    <input name="main" type="hidden" value="1" />
    <input name="id"  type="hidden" value="<?php echo $_GET['id'] ?>" />
    
    <table class="form" border=0>

        <tr>
            <td class="col1">
                            
                               
            <input name="submit1" id="submit1" value="Save" type="submit" style="float:right"/>
   
            
            <?php if($_GET['type'] != 'homepage'): ?>  
                <div class="container">
                <h3><label for="title">Title</label></h3>
                <input name="title" id="title" type="text" value="<?php echo htmlspecialchars($this->data['title']) ?>" class="long checkRequired" />
                </div>
            <?php else: ?> 
                
                <input name="title" type="hidden" value="<?php echo htmlspecialchars($this->data['title']) ?>"  />
                
            <?php endif;?>
            
            <?php if($_GET['type']== 'default'): ?>
                <div class="container">
                <h3><label for="shortTitle">Short Navigation Title</label><span> (if different)</span></h3>
                <input name="shortTitle" id="shortTitle" type="text" value="<?php echo htmlspecialchars($this->data['short_title']) ?>"  class="medium" />
                </div>
            <?php endif; ?>
           
           
           
           
           
            <?php if($_GET['type']== 'homepage'): ?>
                <div class="container">
                    <h3><label for="metaTitle">Meta Title</label></h3>
                    <input name="metaTitle" id="metaTitle" type="text" value="<?php echo htmlspecialchars($this->data['meta_title']) ?>" class="long checkRequired" />
                </div>
            
             <?php else: ?>  
                <div class="optionalContainer">
                    <!-- Hide the Meta info -->
                    <span class="hastooltip" >More...</span>
                                                        
                    <div class="optional">
                    
                        <div class="container">
                                <h3><label for="metaTitle">Meta Title</label></h3>
                                <input name="metaTitle" id="metaTitle" type="text" value="<?php echo htmlspecialchars($this->data['meta_title']) ?>" class="long maxlength" />
                        </div>
                    </div>
                </div>
            <?php endif; ?>  
           

                               
                               
                               
            <div class="container">
            
            
            <?php if($_GET['type'] == 'default'): ?>  
                <h3><label for="path[1]">Path/Filename</label></h3>
                <input name="filepath" id="filepath" type="hidden" value="<?php echo  $this->first ?>"/>
                        
                    <span class="mono"><?php echo  $this->first ?>/</span>
                    <input name="filename" id="filename" type="text" value="<?php echo  $this->last ?>" class="medium checkRequired mono" style="display: inline"   />
                
                </div>
            <?php endif; ?>
            
            
             
            <div class="container">
                <h3><label for="metaDescription">Meta Description</label> <span>For search engine results</span></h3>
                <textarea rows="5" cols="80" name="metaDescription" id="metaDescription" class="description checkRequired"><?php echo $this->data['meta_description'] ?></textarea>
            </div>
         
                
                 
                 
        <h3><label for="body">Body</label></h3>
             
              <div class="container">
              <textarea rows="20" cols="80" name="body" id="body" class="checkRequired" ><?php echo  $this->data['body'] ?></textarea>
              </div>
       
       
       </td>
                
       
	<td>

            <?php if($_GET['type']== 'default'): ?>
            
            <div class="<?php echo  $this->data['status'] ?>">
                <h3><label for="isActive">Published</label></h3>
                    
                  <input type="checkbox"  name="isActive" class="date" "<?php if($this->data['status']=='active') echo ' checked="checked"' ?>" />
                    
	    </div>
            
            <?php else: ?>
            
                <input type="hidden"  name="isActive" value="1" />
            
             <?php endif; ?>
            
            </td>
	</tr>
        
      
      
    </table>

</form>

