<?php





?>


               
<table id="itemlist" class="eventList">
    <thead>
        <tr>     
            <th class="date first">Published?</th>
            <th class="eventDate">Event Date</th>
            <th class="title">Title</td>                    
        </tr>
    </thead>
    <tbody>
                
    <?php  while($row=$eventModel->fetch() ): ?>
            
        <?php  
               
            if($row['is_embargoed']){
                $publishedString = 'pending' ;
            }
            
            if($row['is_published']){
                $publishedString = 'active';
            } else {
                $publishedString = 'expired';
            }
            
            if($row['is_expired']){
                $publishedString = 'expired';
            }


            //EVENT DATE
            
            if($row['is_on_now']){
                $eventStatusString = 'on_now';
            }
            
            if($row['is_past'] ){
                $eventStatusString = 'past';
            }
             
            if($row['is_future'] ){
                $eventStatusString = 'future>';
            }
            
        ?>

        <tr class=" <?php echo $publishedString ?> <?php echo $eventStatusString ?> ">
                        
            <td class="date first"></td>
            
            <?php
                $dateString = date('l j M Y',strtotime($row['start_date']) );
                
                #echo '<p>' . $row['end_date'];
                if( !($row['end_date'] =='' ||  $row['end_date'] == '0000-00-00 00:00:00')  ){
                          $dateString .= ' - ' . date('l j M Y',strtotime($row['end_date']) );
                } 
            ?>
            <td class="eventDate"><?php echo $dateString ?></td>
            

            <td><a href="?id=<?php echo $row['id'] ?>"><?php echo htmlentities($row['title'],ENT_QUOTES,'UTF-8')  ?></a></td>

        </tr>

    <?php  endwhile; ?>
    </tbody>
</table>
            
