<div id="tabs">
    
    
    <ul>
        <li><a href="#fragment-1"><span>Main</span></a></li>
        <li><a href="#fragment-2"><span>Images</span></a></li>
        <li><a href="#fragment-3"><span>Files</span></a></li>
    </ul>
    
    
    <div id="fragment-1">
        
        <?php require 'nav-item-form.php'; //this will be a variable ?>
        <?php require 'news-item-form.php'; ?>
        <?php require 'markdown-cheatsheet.php' ?>
        
    </div>
    
    
    
    <div id="fragment-2">
        
        <?php require 'bb_admin/views/admin-image.php' ?>
    
    </div>
    
    
    <div id="fragment-3">
        
        <?php require 'admin-downloads.php' ?>
    
    </div>
    
</div>



<link type="text/css" href="http://jqueryui.com/latest/themes/base/ui.all.css" rel="stylesheet" />
<script type="text/javascript" src="http://jqueryui.com/latest/jquery-1.3.2.js"></script>
<script type="text/javascript" src="js/jquery-cookie.js"></script>
<script type="text/javascript" src="http://jqueryui.com/latest/ui/ui.core.js"></script>
<script type="text/javascript" src="http://jqueryui.com/latest/ui/ui.tabs.js"></script>
<script type="text/javascript">
$(document).ready(function(){

    $("#tabs").tabs({ cookie: { expires: 1 } });

});
</script>