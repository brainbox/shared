<?php
/**
 * The idea is to provide a super-simple form for those that don't need the
 * complexity...
 *
 * ... with a view to adding the complexity back in later
 *
 *
 *
 */


//PUBLISHED

if($this->data['is_embargoed']){
    $publishedString = 'This is Event <em>item</em> is <strong>embargoed</strong> until ' . date('d F Y',strtotime($this->data['start_date']) );
}

if($this->data['is_published']){
    $publishedString = 'This Event <em>item</em>  is <strong>published</strong>';
} else {
    $publishedString = 'This Event <em>item</em>  is <strong>not</strong> published';
}

if($this->data['is_expired']){
    $publishedString = 'This Event <em>item</em>  has <strong>expired</strong>';
}


//EVENT DATE

if($this->data['is_on_now']){
    $eventStatusString = 'This Event is <strong>on now</strong>';
}

if($this->data['is_past'] ){
    $eventStatusString = 'This Event is <strong>in the past</strong>';
}
 
if($this->data['is_future'] ){
    $eventStatusString = 'This Event is <strong>in the future</strong>';
}


?>


    
<?php  if(!empty($_SESSION['errors']) ): ?>
   
    <p class="error">Please correct the following error(s):</p>
    <ul>
        
        <?php foreach ( $_SESSION['errors'] as $name=>$value): ?>
            
            <li><?php echo $value['msg'] ?></li>
            
         <?php endforeach; ?>
         
    </ul>
    
<?php endif; ?>

<div id="growl">
    <div class="publishedStatus">
        <?php echo $publishedString ?>
    </div>
    
    <div class="itemStatus">
        <?php echo $eventStatusString  ?>
    </div>
</div>




<form id="updateflexiItem" name="updateflexiItem" method="post" action="">

    <fieldset class="save">
        <input type="hidden" name="main" value="1" >
        <input type="hidden"  name="id"  value="<?php echo $_GET['id'] ?>">
			
        <input type="hidden"  name="usesIsPublished"  value="1">   
        
        <!-- SIMPLIFICATION: hiding the published/expire dates -->
		<input type="hidden" name="published" value="<?php echo $this->data['published'] ?>"
        <input type="hidden" name="depublish" value="<?php echo $this->data['depublish'] ?>">

        <!-- SIMPLIFICATION: one day event -->            
        <input type="hidden" value="true" name="oneday">
     
		
        <ul>
            <li>  
                <input type="submit" value="SAVE" id="submit1" name="submit1">
            </li>
        </ul>
    </fieldset>
    
    <fieldset id="pubDepub">
        <ul>
            <li>
                <fieldset>
                    <ul>
                        <li>
                            <input type="checkbox" <?php echo $this->data['is_published']?' checked="checked" ':'' ?> value="1" id="isPublished" name="isPublished">
                            <label for="isPublished">Published?</label>
                        </li>
                    </ul>
                </fieldset>
            </li>
        </ul>
    </fieldset>
    
    <fieldset id="eventDates">
        <ul>    
            <li>     
                <label for="start_date">Event Date</label>
                <input type="text" value="<?php echo date('d-m-Y',strtotime($this->data['start_date']) ) ?>" class="date" name="startDate" id="startDate">
            </li>
		</ul>
    </fieldset>
    
    <fieldset>
        <ul>
            <li>
                <label for="title">Title</label>
                <input type="text" class="long checkRequired" value="<?php echo htmlspecialchars($this->title) ?>" id="title" name="title">    
            </li>
            <li>
                <label for="body">Body</label>
                <textarea class="checkRequired" id="body" name="body" cols="60" rows="20"><?php echo  $this->data['body'] ?></textarea>
            </li>
        </ul>
    </fieldset>
</form>







<script>




</script>


