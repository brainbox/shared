<div id="tabs">
    
    <!--
    <ul>
        <li><a href="#fragment-1"><span>Main</span></a></li>
        <li><a href="#fragment-2"><span>Images</span></a></li>
        <li><a href="#fragment-3"><span>Downloads</span></a></li>
        <li><a href="#fragment-4"><span>Cheatsheet</span></a></li>
    </ul>
    -->
    
    <div id="fragment-1">
                
        <?php if( defined('CLIENT_SHORT_NAME') && CLIENT_SHORT_NAME == 'mizu'): ?>
            <?php require 'event-item-form-main-simple.php';  ?>
        <?php else: ?>
            <?php require 'event-item-form-main.php';  ?>
        <?php endif ?>
           
        
        
        
    </div>
    
    
    
    <div id="fragment-2">
        
        <?php require 'bb_admin/views/admin-image.php' ?>
        
          <?php if( defined('CLIENT_SHORT_NAME') && CLIENT_SHORT_NAME == 'forward'): ?>
               <p>Need more help? Watch this <a href="http://www.youtube.com/watch?v=2JzWdjBXBY8">how-to video</a> on YouTube.</p>
          <?php endif ?>
    </div>
    
    
    <div id="fragment-3">
        
        <?php require 'admin-downloads.php' ?>
    
    </div>
    
    
     <div id="fragment-4">
          
          <?php require 'markdown-cheatsheet.php';  ?>
    
     </div>
     
</div>
