<?php
//echo '<pre>';
//print_r($_POST);
//echo '</pre>';

/**
 * This class needs to provide the following to the parent class:
* 
 * $this->contentString					
 * $this->shortTitle 
 * $this->titleNote 
 * $this->title 
 * $this->description
 */



Class ImagePage extends Page{
    
    protected $contentString='';					
    protected $shortTitle = 'Images';
    protected $titleNote =  'Images';
    protected $title =      'Images';
    protected $description='';
    protected $message='<p>&nbsp;</p>';

    function __construct($dbPDO,$basePath){
    
        
        $this->dbPDO=$dbPDO;
        $this->basePath=$basePath;
        
        require_once('images/Images.php');
        
        
        $this->_initialise();       

    }
    

    
   // A function to apply mysql_real_escape_string   //may not need this anymore
   function escapeValue($value) {
    
        return mysql_real_escape_string($value);
    
    }
   
   
    private function _initialise(){
        
        //Set the form       
        $this->_setForm();
        
        //Process the form (if necessary)
        if($this->form->validate()){

            $this->_processForm();

        }
        
        //Container for messages
        $this->contentString .= '<div>';
        $this->contentString .= $this->message;
        $this->contentString .= '</div >';
        
        
        //New Image 
        //Container for the FORM
        $this->contentString .= '<div class="document new" >';
        $this->contentString .= '<div><a class="hasForm" id="addNew">Add New Image...</a></div>';
        $this->contentString .= '</div >';
        
        
        
        //List the docs
        $images=new Images($this->dbPDO, $this->basePath);
        
        while ($image=$images->fetch() ){
            
            //print_r($image);
            //exit;
            
            $this->contentString .= '<div class="document" style="width: ' . ($image->width+10) . 'px" >';
            
            
            
            $this->contentString .= '<h2>' . $image->filename . ' ' . $image->width . 'x' . $image->height . '</h2>';
            $this->contentString .= '<img src="' . BASE_PATH . 'images/bank/' . $image->filename . '" />';
            
            
            $dateString = 'Created on ' . date( 'd M Y H:i:s',$image->create_date);
            
            if ($image->upload_date > $image->create_date)
            {
                
                switch($image->upload_date){
                    
                    //TODAY
                    case( $image->upload_date > mktime(0,0,0,date('m'),date('d'),date('y') ) ):
                        
                    $dateString .= '<br />Updated TODAY at ' . date('H:i:s',$image->upload_date);
                    
                    break;
                
                    //YESTERDAY
                    case( $image->upload_date > mktime(0,0,0,date('m'),date('d')-1,date('y') ) ):
                        
                    $dateString .= '<br />Updated YESTERDAY at ' . date('H:i:s',$image->upload_date);
                    
                    break;
                    
                    default:
                
                    $dateString .= '<br />Updated on ' . date('d M Y H:i:s',$image->upload_date);
                
                }
                
            }
            
            $this->contentString .= '<p>' . $dateString . '</p>';
            
            
            
            //Container for the FORM
            $this->contentString .= '<div id="' . $image->id . '" >';
            $this->contentString .= '<a class="hasForm">Add new version...</a>';
            $this->contentString .= '</div >';
            
            //Complete the image wrapper
            $this->contentString .= '</div>';
            
        }
        
        
        //Stick the form at the bottom of the page
        $this->contentString .= '<div class="hidden">';
        $this->contentString .= $this->form->toHTML();
        $this->contentString .= '</div>';
    

	}
    
   
    
    private function _setForm(){
        
        require_once ("HTML/QuickForm.php");
        $this->form = new HTML_QuickForm('fileUpload', 'post', $_SERVER['REQUEST_URI'],null,null, true);
        
        $this->form->removeAttribute('name'); //for XHTML compliance
        
        //For testing - will later be added by JS
        //$this->form->addElement('hidden','action','newImage');

        
        //The file upload:
        $this->form->addElement('file','file','Select a file',array('class'=>'text'));
        $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
       
        $this->form->addRule('file',    'Please select a file',               'required','' ,'client');
        $this->form->addRule('file',    'The file must .gif, .jpg or .png', 'mimetype', array('image/gif','image/jpeg','image/png'));
        $this->form->addRule('file', 	'The maximum file size is 1 MB', 'maxfilesize', (1048570) );
        //$this->form->addRule('file', 	'Punctuation not allowwed in the filename', 'nopunctuation');

    }
    
    private function _processForm(){
        
        $this->form->applyFilter('__ALL__','escapeValue');			//Dounbt this works				
          
        //$formData=$this->form->getSubmitValues();
        //print_r  ($formData);
        //exit;
        
        $images=new Images($this->dbPDO, $this->basePath);
        
        if ($this->form->getSubmitValue('action')=='addDocument' ) //Note that the JS is shared with the Document pages
        {
            
            
            if (  $images->addImage($this->form->_submitFiles['file'])  )
            {
                $this->message = '<p>New image added successfully</p>';
            }
            else
            {
               $this->message = '<p class="error">File Exists</p>';
            }
        
        }
        
         if ($this->form->getSubmitValue('action')=='addVersion' )
        {
            //echo '<p>The ID is ' . $this->form->getSubmitValue('id');

            $image=$images->getByID($this->form->getSubmitValue('id'));
            
            //print_r($image);
            
            if (  $image->addVersion($this->form->_submitFiles['file'], BASE_FILE_PATH . '/images/bank/')  )
            {
                $this->message = '<p>New version added successfully</p>';
            }
            else
            {
                $this->message = '<p class="error">Filename in use by another image</p>';
            }
        
        }
        
    }
	
	function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'dummy','title'=>'Images');
		
		return $breadcrumbArray;

        }
       
       
        function cssArray(){
                
            $cssArray=array();
            $cssArray[]='css/imagebank.css'; 
            return $cssArray;
 
        }
        function javaScriptArray(){
                
            $jsArray=array();
            //$jsArray[]='js/validate_form.js';              //calendar
            $jsArray[]='js/showForm.js';
            
            return $jsArray;
        }
       
}

?>

