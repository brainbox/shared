<?php
//header('Expires: Mon, 26 July 1997 05:00:00 GMT' );
//header('Pragma: nocache');

//$this->contentString .=  '<pre>';
//print_r($_POST);
//$this->contentString .=  '</pre>';

/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description

*/
         
@define ('USER_TABLE','member');
@define ('MEMBER_TABLE','member');

@define ('USER_TABLE_ID','id');
@define ('USER_TABLE_LOGIN','email');
@define ('USER_TABLE_FIRST','forename');
@define ('USER_TABLE_LAST','surname');

@define ('MEMBER_AGE_XREF_TABLE','member_age_xref');
@define ('MEMBER_DISORDER_XREF_TABLE','member_specialty_xref');

Class MemberItemPage extends Page{

        function __construct($db,$basePath, $dbPDO){
               
                 
                
                             
                $this->db=$db;
                $this->dbPDO=$dbPDO;
                $this->_initialise();
                
       
       
        }
        
    
        
       // A function to apply mysql_real_escape_string   //may not need this anymore
       function escapeValue($value) {
               return mysql_real_escape_string($value);
       }
     
        
       
    function _initialise(){
                
        $messageString='';
      
        
        require_once('achippp/ACHIPPPmembers.php');
        
        require_once('HTML/Table.php');
                                 
        // Include the QuickForm class
        require_once ("HTML/QuickForm.php");
        
      
        
        
        //Count first - CHECK THAT THE MEMBER EXISTS
        $sql = 'SELECT COUNT(*) FROM ' . MEMBER_TABLE  . ' WHERE id='.$_GET['id'];
        //echo $sql;
        
        $stmnt=$this->dbPDO->prepare($sql);
        $stmnt->execute();
        $result=$stmnt->fetchAll(PDO::FETCH_ASSOC) ;
        //print_r($result);
        if( count($result) == 0){
            //echo 'No such user';	
            exit;
            //Crash out!!!!!
        }
        
        
        
        
        ////echo 'CREATING';
        $member=new AChiPPPmember($this->dbPDO, $_GET['id']);
        $member->attach(new MemberLogger($this->dbPDO, FALSE));
        
        
        //echo '<hr><hr>' . $member->forename;
        //echo '<hr><hr>' . $member->surname;
        
        // A function to apply mysql_real_escape_string
        
        
        
        
        //instantiate the form
        $userForm= new HTML_QuickForm('updateUser', 'POST',$_SERVER['REQUEST_URI']);
        $userForm->addElement('hidden','memberdetails');
            
        $userForm->addElement('checkbox','active','Active?');	
        $userForm->addElement('checkbox','available','Available?');	
        $userForm->addElement('checkbox','webuser','Website Member?');
        $userForm->addElement('checkbox','search_parent','Parent Search?');
        $userForm->addElement('checkbox','search_medico','Medico-Legal Search?');	
        
        $userForm->addElement('submit','submit','Save');
        
        $classArray=array('class'=>'text');
        
        $userForm->addElement('text','title','Title', $classArray);
        $userForm->addElement('text','forename','Forename', $classArray);
        $userForm->addElement('text','surname','Surname', $classArray);
        
        $userForm->addElement('text','position','Position', $classArray);
        $userForm->addElement('text','limitedco','Ltd Co. Name', $classArray);
        
        //Roles
        $roleArray=$member->getAllRoles();
        //print_r($roleArray);
        $userForm->addElement('radio','role', 'AChiPPP Role','None',0);	///DOUBLE CHECK  THIS
        
        foreach ($roleArray as $name=>$value) {
            $userForm->addElement('radio','role','' ,$value['name'], $value['id']);
        }
        
        $userForm->addElement('text','member_no','Member No.', $classArray);
        $userForm->addElement('text','bps_no','BPS No.', $classArray);
        
        $userForm->addElement('text','email','Contact Email Address (hidden)', $classArray);
        $userForm->addElement('text','email_display','Display Email Address', $classArray);
        
        $userForm->addElement('submit','submit','Save');
        
        $attribs=array('style'=>'width: 300px; height: 100px');
        $userForm->addElement('textarea','address','Postal Address',$attribs);	
        
        //$userForm->addElement('text','address','Address', $classArray);
        $userForm->addElement('text','address2','', $classArray);
        $userForm->addElement('text','address3','', $classArray);
        $userForm->addElement('town','address3','', $classArray);
        $userForm->addElement('text','town','Town', $classArray);
        $userForm->addElement('text','county','County', $classArray);
        $userForm->addElement('text','postcode','Postcode', $classArray);
        $userForm->addElement('text','country','Country', $classArray);
        
        
        $userForm->addElement('submit','submit','Save');
        
        
        $userForm->addElement('text','postcodebase','Base Postcode', $classArray);	
        $userForm->addElement('text','range','Range (0-1000 miles)', $classArray);	
        
        $userForm->addElement('submit','submit','Save');
        
        //$userForm->addElement('text','home','Home Phone');
        $userForm->addElement('text','work','Work Phone', $classArray);
        $userForm->addElement('text','mobile','Mobile', $classArray);
        $userForm->addElement('text','fax','Fax', $classArray);
        $userForm->addElement('text','website','Website', $classArray);
        
        //$attribs=array('disabled'=>'disabled', 'style'=>'width: 600px; height: 50px');
        //$userForm->addElement('textarea','oldnotes','Notes (locked)',$attribs);	
        
        
        $attribs=array('style'=>'width: 600px; height: 170px');
        $userForm->addElement('textarea','blurb','Blurb',$attribs);	
        
        //$userForm->addElement('html','<tr><td><strong>Preview</strong></td><td>' . strip_tags($member->blurb) . '</td></tr>');	
        
        
        
        
        $userForm->addElement('submit','submit','Save');
        
        
        
        //AREAS (Regions
        $areaArray=$member->getMasterAreaArray();
        //echo '<hr>Master area:';
        //print_r($areaArray);
        
        
        $counter=0;
        foreach($areaArray as $name=>$value){
            if($counter==0){		
                $userForm->addElement('checkbox','area['.$value['id'] . ']','Areas Covered',$value['name']);
            }
            else{	
                $userForm->addElement('checkbox','area['.$value['id'] . ']','',$value['name']);
                }
            $counter++;
        }
        
        
        $userForm->addElement('submit','submit','Save');
        
        
        
        
        //SPECIALITIES
        $specialtyArray=$member->getMasterSpecialtyArray();	
        //print_r($disorderArray);
        
        
        $counter=0;
        foreach($specialtyArray as $name=>$value){
            if($counter==0){		
                $userForm->addElement('checkbox','specialty['.$value['id'] . ']','Specialties',$value['specialty']);
            }
            else{	
                $userForm->addElement('checkbox','specialty['.$value['id'] . ']','',$value['specialty']);
                }
            $counter++;
        }
        
        
        $attribs=array('style'=>'width: 300px; height: 100px');
        $userForm->addElement('textarea','specialist_other','Other Specialties - one per line', $attribs);
        
        $userForm->addElement('submit','submit','Save');
        
        
        
        
        
        //BPS Types
        $BPSarray=$member->getAlltypes();
        //echo '<pre>';
        //print_r($BPSarray);
        
        
        $counter=0;
        foreach($BPSarray as $name=>$value){
            if($counter==0){		
                $userForm->addElement('checkbox','bps['.$value['id'] . ']','BPS Chartered Types',$value['name']);
            }
            else{	
                $userForm->addElement('checkbox','bps['.$value['id'] . ']','',$value['name']);
                }
            $counter++;
        }
        $userForm->addElement('submit','submit','Save');
        
        
        $userForm->addElement('checkbox','neuro','Additionally...', 'Division of Neuropsychology (Full Practitioner Member)');	
        
        
        
        
        
        
        
        $userForm->addElement('submit','submit','Save');
        //BPS Types
        $associationArray=$member->getAllAssociations();
        
        
        $counter=0;
        foreach($associationArray as $name=>$value){
            if($counter==0){		
                $userForm->addElement('checkbox','association['.$value['id'] . ']','Associations',$value['name']);
            }
            else{	
                $userForm->addElement('checkbox','association['.$value['id'] . ']','',$value['name']);
                }
            $counter++;
        }
        
        $attribs=array('style'=>'width: 300px; height: 100px');
        $userForm->addElement('textarea','associations_other','Other Associations - one per line' , $attribs);
        
        
        
        
        
        $userForm->addRule('forename','Forename required', 'required', NULL, 'client');
        $userForm->addRule('surname','Surname required', 'required', NULL, 'client');
        
        
        $userForm->addRule('email','Valid email address required', 'email', NULL, 'client');
        //$userForm->addRule('address','Address required', 'required', NULL, 'client');
        //$userForm->addRule('town','Town required', 'required', NULL, 'client');
        //$userForm->addRule('postcode','Postcode required', 'required', NULL, 'client');
        $userForm->addRule('range','Range must be numeric', 'numeric', NULL, 'client');
        
        
        $userForm->addRule('home','Phone numbers must not contain punctuation', 'nopunctuation', NULL, 'client');
        $userForm->addRule('work','Phone numbers must not contain punctuation', 'nopunctuation', NULL, 'client');
        $userForm->addRule('mobile','Phone numbers must not contain punctuation', 'nopunctuation', NULL, 'client');
        $userForm->addRule('fax','Fax number must not contain punctuation', 'nopunctuation', NULL, 'client');
        
        //$userForm->addRule('login','Username must be at least 6 characters long', 'minlength', 6, 'client');
        //$userForm->addRule('login','Valid email address required', 'email', NULL, 'client');
        
        
        
        ///DEFAULTS time
                    
                                
        //echo $member->specialist_other;      
             
        //echo  nl2br($member->specialist_other);
        //$poop=explode("\n",$member->specialist_other);
        //print_r($poop);
             
                                
                                        
        $defaultsArray=array(
            'memberdetails'=>	'true',	
            'active'=>   		$member->active,
            'available'=>   	$member->available,
            'webuser'=>   		$member->webuser,
                
                'search_parent'=>   	$member->search_parent,
            'search_medico'=>   	$member->search_medico,
            
            'title'=>		$member->title,
            'forename'=>		$member->forename,
            'surname'=>		$member->surname,
                
                'position'=>		$member->position,
            'limitedco'=>		$member->limitedco,
                
                 'role'=>   		$member->role,
                
                'member_no'=>		$member->member_no,
                'bps_no'=>		$member->bps_no,
                
            'email'=>		$member->email,
            'email_display'=>	$member->email_display,
            
            'address'=>		$member->address,
            'address2'=>		$member->address2,
            'address3'=>		$member->address3,
            'town'=>		$member->town,
            'county'=>		$member->county,
            'postcode'=>		$member->postcode,
            'country'=>		$member->country,
            
            'postcodebase'=>        $member->postcodebase,
            'range'=>		$member->range,
            
            'home'=>		$member->home,
            'work'=>		$member->work,
            'mobile'=>		$member->mobile,
            'fax'=>			$member->fax,
            'website'=>		$member->website,
            
            'oldnotes'=>		$member->oldnotes,
            'blurb'=>		$member->blurb,
                
                'neuro'=>	$member->neuro,
                
                
                'specialist_other'=>	$member->specialist_other,
                'associations_other'=>	$member->associations_other
        
        
            );
            //echo '<pre>';
        //print_r($defaultsArray);
        
        $userForm->setDefaults($defaultsArray);					
        
        
        
        //DisorderDefalts	
        $specialtyIDarray=$member->getSpecialtyIDarray();
        //echo '<hr>';
        //print_r($specialtyIDarray);
        //exit;
        
        if($specialtyIDarray){
            foreach($specialtyIDarray as $name=>$value) {
                ////print_r($treatArray );
                $userForm->setDefaults(array('specialty['.$name . ']'=>1) );
            }
        }	
        
        
        //BPS TYPE
        $typeIDarray=$member->getTypeIDarray();
        //echo '<p>... and the types are...';
        //print_r($typeIDarray);
        
        
        if($typeIDarray){
            foreach($typeIDarray as $name=>$value) {
                ////print_r($treatArray );
                $userForm->setDefaults(array('bps['.$name . ']'=>1) );
            }
        }	
        
        
        
        //AREA (Region)
        $areaIDarray=$member->getAreaIDarray();
        //echo '<p>... and the areas are...';
        //print_r($areaIDarray);
        //exit;
        
        if($areaIDarray){
            foreach($areaIDarray as $name=>$value) {
                ////print_r($treatArray );
                $userForm->setDefaults(array('area['.$name . ']'=>1) );
            }
        }	
        
        
        
        //Associations defaults	
        $associationIDarray=$member->getAssociationIDarray();
        //echo '<hr>';
        //echo '<hr>ASSS';
        //print_r($associationIDarray);
        
        if($associationIDarray){
            foreach($associationIDarray as $name=>$value) {   
                        
                        //echo 'association['.$name . ']';
                        
                        $userForm->setDefaults(array('association['.$name . ']'=>1) );
            }
        }	
        
        //exit;
        
        
        
        
        
        
        //Page title
        
        $this->title =   $member->forename	.' '.		$member->surname . ' - ' . $_GET['id'];
        $this->metaTitle=$this->title;
        
        
        
        
            
        if($userForm->validate()  ){
                            
            ////echo '<hr>PASSED CVALIDATION';
            
            
            //The following is messing up the textarea... but doesn't on the main site for some reason
            $userForm->applyFilter('__ALL__','escapeValue'); 
            
                
            $member->active= 		$userForm->getSubmitValue('active');
            $member->available= 		$userForm->getSubmitValue('available');
            $member->webuser= 		$userForm->getSubmitValue('webuser');
                
                $member->search_parent= 	$userForm->getSubmitValue('search_parent');
            $member->search_medico= 	$userForm->getSubmitValue('search_medico');
                
                
            $member->title=			$userForm->getSubmitValue('title');
            $member->forename= 		$userForm->getSubmitValue('forename');
            $member->surname= 		$userForm->getSubmitValue('surname');
                
                $member->position= 		$userForm->getSubmitValue('position');
            $member->limitedco= 		$userForm->getSubmitValue('limitedco');
                
                
                $member->role= 		        $userForm->getSubmitValue('role');
                
                $member->member_no= 		$userForm->getSubmitValue('member_no');
                $member->bps_no= 		$userForm->getSubmitValue('bps_no');
            
            
            //SPECIAL treatment for email: needs to go in as NULL if blank
            $member->setEmail($userForm->getSubmitValue('email') ) ;
                
                
                $member->email_display=         $userForm->getSubmitValue('email_display') ;
            
            
            $member->address= 		$userForm->getSubmitValue('address');
            $member->address2= 		$userForm->getSubmitValue('address2');
            $member->address3= 		$userForm->getSubmitValue('address3');
            $member->town=  		$userForm->getSubmitValue('town');
            $member->county=  		$userForm->getSubmitValue('county');
            $member->postcode=  		$userForm->getSubmitValue('postcode');
            $member->country=  		$userForm->getSubmitValue('country');
            
            $member->postcodebase=  	$userForm->getSubmitValue('postcodebase');
            $member->range=  		$userForm->getSubmitValue('range');
            
            $member->home=  		$userForm->getSubmitValue('home');
            $member->work=  		$userForm->getSubmitValue('work');
            $member->mobile=  		$userForm->getSubmitValue('mobile');
            $member->fax=  		 	$userForm->getSubmitValue('fax');
            $member->website=  		$userForm->getSubmitValue('website');
            
            
            //Blurb has its own method
            $member->setBlurb($userForm->getSubmitValue('blurb') );
            
            //AREAS (Regions)
                
                //print_r($userForm->getSubmitValue('area')  );
                $member->setAreaIDarray($userForm->getSubmitValue('area'));
            
            //ages
            //print_r($userForm->getSubmitValue('age'));
            //$member->setAgeIDarray($userForm->getSubmitValue('age'));
            
            //disorders
            //print_r($userForm->getSubmitValue('disorder'));
            $member->setSpecialtyIDarray($userForm->getSubmitValue('specialty'));
                
                $member->specialist_other=  	$userForm->getSubmitValue('specialist_other');
                
                //BPS Types
            //print_r($userForm->getSubmitValue('bps'));
                $member->setTypeIDarray($userForm->getSubmitValue('bps'));
                //exit;
            //$member->setSpecialtyIDarray($userForm->getSubmitValue('specialty'));
                
                
            
                $member->neuro= 		$userForm->getSubmitValue('neuro');
        
                
                
            
            //$member->finalise();
                
                
                
               
                //ASSSOCIATIONS
                
                //print_r($userForm->getSubmitValue('association')  );
                //exit;
                $member->setAssociationIDarray($userForm->getSubmitValue('association'));
            $member->associations_other=  		$userForm->getSubmitValue('associations_other');
            
            
            $member->finalise();
            
            
            
            //Messages (if any). Messages can come from different places!!!!
            
            $classErrors=$member->getErrors();
            
            $messageString='';
            if($classErrors){
                //echo '<h2>Errors</h2>';
                foreach($classErrors as $name=>$value){
                    $messageString .= '<p class="error">' . $value . '</p>';
                }
            }
            elseif($classMessages=$member->getMessages()  ){
                foreach($classMessages as $name=>$value){
                    $messageString .= '<p class="success">' . $value . '</p>';
                }
                
            }
            
            
                
        }
        
        /*
        echo '<div style="float: right; width: 120px; border: 1px solid #ccc; padding: 0 1em 1em 1em">';
        
        echo '<p><a href="adminUserSummary.php?id=' . $_GET['id'] . '">Printer-friendly</a></p>';
        
        
        if(trim($member->email) !=''   &&  $member->active=='1'){
            echo '<p><a href="adminUserMessages.php?id=' . $_GET['id'] . '">Send Message</a></p>';
        }
        echo '</div>';
        */
                
        
        
        /*	
            
        }
        else{
            if(count($_POST)!=0){	
                $errorMessage='<span class="error">Validation Failed (see below for details). User details NOT SAVED.</span>';
            }
        }
        */
        
        
        
        //function for quickform
        //function maxwords($element_value, $element_name) {
        
        
                
        
                
        $this->contentString .= $messageString . $userForm->toHTML();
        
   
         
	
	}
	
        function maxwords($element_value, $element_name) {
        
            ////echo '<hr>here';
            
            $max=100;
            $count = count(array_filter(explode(" ", $element_value)));
            
            
            if ($count<$max) {
                return true;
            }
            else {
            
            return false;
        
            } 
        
    } 


        function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'members','title'=>'Members');
                $breadcrumbArray[]=array('fullPath'=>'','title'=>$this->title);
		return $breadcrumbArray;

        }
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/flexi.css'; 
                
                //$cssArray[]='build/fonts/fonts.css';                    //calendar
                //$cssArray[]='build/reset/reset.css';                    //calendar
                //$cssArray[]='build/calendar/assets/calendar.css';       //calendar
                
                //$cssArray[]='build/fonts/fonts-min.css';                //tabs
                //$cssArray[]='build/tabview/assets/skins/sam/tabview.css';//tabs
		
                return $cssArray;
                

        
        }
        function javaScriptArray(){
                
                $jsArray=array();
                $jsArray[]='js/validate_form.js';              //calendar
            
               
                
                return $jsArray;
        }
        
        
    function subMenu(){
        
        $tempString='<dl id="submenu">';
        
        for($i=65; $i<=90; $i++) { //loop through the alphabet 
        
            $x = chr($i); //$x holds   
            
            $tempString .=  '<dd><a href="' . $this->basepath .'members?alpha='.$x.'">'.$x.'</a></dd>';
        }
        $tempString.= '<dd><a href="new-members">New</a></dd>';
        $tempString.= '</dl>';
        
        
        return $tempString;
        
    }
       
}

?>

