<?php


require_once 'AdminItemPage.php';

Class NewsItemPage extends AdminItemPage  implements AdminItem{
        

    
       
    function _initialise(){
        
        
		$this->handleSession();
		
        $this->handlePOSTs();
        
        $this->handleErrors();
		
        $this->getItemData();
	
				
				
		$this->title .= $this->data['title'] ;
			
		//Get the status of the item
		$status=Model_News::status(strtotime($this->data['published']),strtotime($this->data['depublish']));
			
		$published = date('d-m-Y',strtotime($this->data['published'])); //Always a $published?
			
		$expireString = '';
		$neverExpireString = '';
			
		if(strlen($this->data['depublish']) > 0){
			$depublish = date('d-m-Y',strtotime($this->data['depublish']));
			 $expireString = ' checked="checked" ';
		} else {
			//If no expiry date stored, provide a default a 31 days ahead of the publish date
			$depublish = date('d-m-Y',(strtotime($this->data['published']) + 31*24*60*60)); // 31 days in the future
			$neverExpireString = ' checked="checked" ';
		}
			
           
		$section = 'news';
        $downloadArray = $this->getDownloads();
		$imageArray = $this->getImages();   
		  
		
		
		
	

		 ob_start();
                
            require  'views/news-item-form.php';
            
            $this->contentString .=  ob_get_contents();
                
        ob_end_clean();
        
		
	}
	
	
	function getItemData(){
      
        require_once ('News/Model/News.php');
            
		$newsModel = new Model_News($this->dbPDO);
		$newsModel->isActive=false;
          
        $this->data = $newsModel->getById($_GET['id']);
       
    }   
	
	function getDownloads(){
		
		require_once 'Download/Model/Download.php';
		
		return Model_Download::getByArticleId($this->dbPDO, $_GET['id'], 'news' );
		
	}
	
	function getImages(){
		
        require_once 'Image/Model/Image.php';
        
		$resultArray = Model_Image::getByArticleId($this->dbPDO,  $_GET['id'],'news');        
		
        return  $resultArray;
		
	}
	
	
	
    
    function handleMainPOST(){
        
		
		require_once('forms/validation.php');
		
		$validator=new FormValidator();
		
		$validator->isEmpty('title', 'Please enter a Title');
		$validator->isEmpty('metaDescription', 'Please enter a Meta Description');
		$validator->isEmpty('body', 'Please enter body text');
		
		if($validator->isError() ){
				
			//Store details of the form and its errors in the session
            $_SESSION['referringTime'] = time();
            $_SESSION['formVars'] = $_POST;
            $_SESSION['errors'] = $validator->getErrorList();
			
		} else {
				
			require_once ('News/Model/News.php');
			$newsModel = new Model_News($this->dbPDO);
			$newsModel->updateFromPOST();
            
            
            //TRIGGER the event... IF it is available.
            require_once 'Observer/EventObserver.php';
            EventObserver::trigger('saveNewsItem',array('NOT SURE')); 
            
			
			// Clear the session 
            if(isset($_SESSION['errors']) ){
                unset($_SESSION['errors']);
            }
            unset($_SESSION['formVars']);
			   
		}

        
        //Avoind "reposting" probs by redirecting
        header("Location: ".$_SERVER['REQUEST_URI']);
        die;    
        
        
    }

    
    
   
	
	
	
	
	
	
	function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'news','title'=>'News');
		$breadcrumbArray[]=array('fullPath'=>'news?id=' . $_GET['id'] ,'title'=>$this->title );
		return $breadcrumbArray;

        }
       
       
        function cssArray(){
		$cssArray=array();
		$cssArray[]='css/flexi.css';
        $cssArray[]='css/datepicker.css';
        
        
        
		return $cssArray;
	}

	function javaScriptArray(){
		$jsArray=array();
		$jsArray[]='js/validate_form.js';              
        $jsArray[]='js/tooltips.js';
        //$jsArray[]='js/jquery.js';
        //$jsArray[]='js/ui.datepicker.js';
        $jsArray[]='js/calendar-stuff.js'; // set up for the jquery calendar
        
        
		return $jsArray;
	}
}