<?php
/**
 * Chopped-down version for the News and similar: main diffs:
 *
 *     + no dates
 *     + no body!
 *
 *
 */


//Kill the formVars if (a) no referrer, (b) the referrer is a different page, (c) the referring happened a while ago. 

if( !isset($_SERVER['HTTP_REFERER'])  ||
    !strstr($_SERVER['HTTP_REFERER'], $_SERVER['REQUEST_URI']) ||
    time() - $_SESSION['referringTime'] > 30 ) {
    
    unset($_SESSION['errors']);
    unset($_SESSION['formVars']);
    
}


    

if(isset($_POST['main'])  ){
    
    //print_r($_POST);
    require_once('forms/validation.php');
    $validator=new FormValidator();
    
    
   
    $validator->isEmpty('title', 'Please enter a Title for the page');
    #$validator->isEmpty('filename', 'Please enter a Filename');
    #$validator->isAlphaNumbericDashSlash('filename', 'Numbers, letters and dashes and forward slashes only for the Filename');
   
    #$validator->isEmpty('published', 'Please enter a Publish date');
    #  $validator->isEmpty('depublish', 'Please enter an Expiry date');
    
    if($validator->isError() ){
        
        //Store details of the form and its errors in the session
       
        $_SESSION['referringTime'] = time();
        $_SESSION['formVars'] = $_POST;
        $_SESSION['errors'] = $validator->getErrorList();

    }
    else{
        
        
        $model->updateFromPOST();
        
        // Clear the session 
        if(isset($_SESSION['errors']) ){
            unset($_SESSION['errors']);
        }
        unset($_SESSION['formVars']);
      

        
    }
    
    //Avoind "reposting" probs by redirecting
    header("Location: ".$_SERVER['REQUEST_URI']);
    die;

    
    
}

      //Check for errors - held in session.
    
    if(isset($_SESSION['errors']) ){
        
        //Need to pass the posted values back to the form
        $data['title'] =            $_SESSION['formVars']['title'];
        $last =                     $_SESSION['formVars']['filename'];
        #$data['status'] =           $_SESSION['formVars']['status'];
        #$data['published'] =        $_SESSION['formVars']['published'];
        #$data['depublish'] =        $_SESSION['formVars']['depublish'];
        $data['meta_title'] =       $_SESSION['formVars']['metaTitle'];
        $data['meta_description'] = $_SESSION['formVars']['metaDescription'];
        #$data['body'] =             $_SESSION['formVars']['body'];
        
        $this->contentString .= '<p class="error">Please correct the following error(s):</p><ul>';
        foreach ( $_SESSION['errors'] as $name=>$value){
            
            $this->contentString .= '<li>' . $value['msg'] .'</li>';
            
        }
        $this->contentString .= '</ul>';
    }
    
    
    //Try a hadn-coded form
    $this->contentString .= '

    
    
    <form action="'.$_SERVER['REQUEST_URI'] . '" method="post" name="updateflexiItem" id="updateflexiItem">
    <input name="main" id="main" type="hidden" value="1" />
    
    <table class="form" border=0>
  
    
    
<tr>
    <td class="col1">
        <div class="container">
                        <label for="title">Title</label>
                        <input name="title" id="title" type="text" value="'.$data['title'].'" class="long checkRequired" />
                        </div>
                       
                     
                        <div class="container">
                        <label for="path[1]">Path/Filename</label>
                        <input name="filepath" id="filepath" type="hidden" value="' . $first . '"/>
                        <br /><span class="mono">' . $first . '/</span><input name="filename" id="filename" type="text" value="' . $last . '" class="medium checkRequired mono" style="display: inline"/>
                        </div>
                      
                        
                        <div class="optionalContainer">
                        <!-- Hide the Meta info -->
                        <span class="hastooltip" >More...</span>
                        
                        <div class="optional">
                        
                        
                        <div class="container">
                        <label for="metaTitle">Meta Title</label>
                        <input name="metaTitle" id="metaTitle" type="text" value="' . $data['meta_title'] . '" class="long" />
                        </div>
                        <div class="container">
                        <label for="metaDescription">Meta Description (or Teaser)</label>
                        <textarea rows="5" cols="80" name="metaDescription" id="metaDescription" class="description">' . $data['meta_description'] . '</textarea>
                        </div>
                        </div>
                        </div>
        </td>
        
        <td>
         
          <!-- SAVE FOR EVENT OBJECTS -->
         
        </td>
        
        <td>
        
        <!-- NO DATES FOR SECTIONS -->
        
        </td>
        
        <!--
        <td class="' . $data['status'] . '">
                                                        
                        <script type="text/javascript">					
                                window.addEvent("domready", function() { 
                                        cal1 = new Calendar({
                                                published: "d-m-Y" ,
                                                depublish: "d-m-Y"
                                        })
                                });
                        </script>
                                                        
                                                   <div class="container">
                                                   
                                                   
                        <input type="hidden" name="status" value="' . $data['status'] . '" />
                                                   
                       <label for="published">Publish</label> (dd-mm-yyyy)
                       <input name="published" id="published" type="text" value="'. $data['published'] .'"  class="short checkRequired" />
                       </div>
                       
                       <div class="container">
                       <label for="depublish">Expire</label> (dd-mm-yyyy) 
                       <input name="depublish"  id="depublish" type="text" value="'.  $data['depublish'] .'"  class="short" />
                       Leave <strong>Expire</strong> blank for pages that never expire
                       </div>
                       
    </td>
    -->
</tr>


<tr>
        <td colspan="3" class="body">
                
                <input name="submit" value="Save" type="submit" style="float:right"/>
                 <br /> 

                <label for="body">Body</label>
               
                <div class="container">
                <textarea rows="20" cols="80" name="body" id="body" >' . $data['body'] . '</textarea>
                </div>


      
        
        </td>
</tr>
<tr>
<td colspan="3" class="submit" >
  <input name="submit" value="Save" type="submit" />
</td>
</tr>
</table>

</form>


';