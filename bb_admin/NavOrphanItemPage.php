<?php

require_once 'AdminItemPage.php';

Class NavOrphanItemPage extends AdminItemPage  implements AdminItem{



    /**
     * Initialise now needs to be about CHOOSING the right from for the selected page
     *
     *
     */
    function _initialise(){
                
        $this->handleSession();

        $this->handlePOSTs();
        
        $this->handleErrors();

        $this->getItemData();
    
        
        
           
    }
       
    function getItemData(){
      
        require_once('articles/Model/Item.php');
    
        $model=new Model_Item($this->dbPDO);
       
    
        //New process
        $model->id =        $_GET['id'];
        $model->active =    false; //All items - not just the active ones
        $model->navOnly =   false;
        $model->isOphan =   true;
        ##This is where the checking should go
        
        

        $model->getItems();
        $this->data = $model->fetch();
        
        
        
        $this->title .= $this->data['title'] ;
        
        
        
        //Convert the dates...
        //Published is easy
        $this->data['published'] = date('d-m-Y',strtotime($this->data['published']) );
       
        
        //Depublish is more complicated
        if(!$this->data['depublish'] || $this->data['depublish']=='0000-00-00 00:00:00'){

            $this->data['depublish'] = '';
                
        } else {
           
            $this->data['depublish'] = date('d-m-Y',strtotime($this->data['depublish']));
        }
        
        //Set the status here. Don't want it to change for an un-successful form submit.
        $this->data['status'] =  Model_Item::status(strtotime($this->data['published']),strtotime($this->data['depublish']) );
        
        
       
        //Download list and form
        require_once 'Download/Model/Download.php';
        
        $download=new Model_Download($this->dbPDO);
        
        $downloadArray =  $download->getByArticleId( $this->dbPDO, $_GET['id'] );
        
        
        //Get images list and form
        require_once 'Image/Model/Image.php';
        
        $image=new Model_Image($this->dbPDO, $this->pathToUserFiles );
        
        $section = '';
        $imageArray =  $image->getByArticleId($this->dbPDO, $_GET['id'], $section );
        
        
        $section = '';
        
        ob_start();
                
            require  'views/orphan-item-form.php';
            
            $this->contentString .=  ob_get_contents();
                
        ob_end_clean();
       
    }   
    
   
    
    
    function handleMainPOST(){
        
        //print_r($_POST);
        require_once('forms/validation.php');
        $validator=new FormValidator();
       
        if($_GET['id'] != 1){ //Not for the home page
            $validator->isEmpty('title', 'Please enter a Title for the page');
            $validator->isEmpty('filename', 'Please enter a Filename');
            $validator->isAlphaNumbericDashSlash('filename', 'Numbers, letters and dashes and forward slashes only for the Filename');
        }
       
        //$validator->isEmpty('published', 'Please enter a Publish date');
        #  $validator->isEmpty('depublish', 'Please enter an Expiry date');
        
        if($validator->isError() ){
        
            //Store details of the form and its errors in the session
           
            $_SESSION['referringTime'] = time();
            $_SESSION['formVars'] = $_POST;
            $_SESSION['errors'] = $validator->getErrorList();
    
        } else {
            
            require_once('articles/Model/NavItem.php');
            
            $model=new Model_NavItem($this->dbPDO);
            $model->updateFromPOST();
            
            // Clear the session 
            if(isset($_SESSION['errors']) ){
                unset($_SESSION['errors']);
            }
            unset($_SESSION['formVars']);
        
        }
        
        //Avoind "reposting" probs by redirecting
        header("Location: ".$_SERVER['REQUEST_URI']);
        die;    
        
        
    }

    
    
    function breadcrumbArray(){

		$breadcrumbArray=array();
        
        $request = $_SERVER['REDIRECT_URL'];
        $pathArray=explode('/',$request);
        
        $segment = array_pop($pathArray);
        
        #echo $segment;
        if($segment == 'navigation'){
            
            $section = 'Navigation';
            
        } else {
            
            $section = 'Orphans';
        }
        #exit;
        
		$breadcrumbArray[]=array('fullPath'=>$segment, 'title'=>$section);
		$breadcrumbArray[]=array('fullPath'=>'',        'title'=>$this->title );
		return $breadcrumbArray;

    }
   
   
    function cssArray(){
            
        $cssArray=array();
        
        $cssArray[]='css/flexi.css'; 
        $cssArray[]='css/datepicker.css';

        return $cssArray;
    
    }
    
    
    function javaScriptArray(){
            
        $jsArray=array();
        $jsArray[]='js/validate_form.js';              //calendar
        $jsArray[]='js/tooltips.js';
        
        //$jsArray[]='js/jquery.js';
        $jsArray[]='js/ui.datepicker.js';
        $jsArray[]='js/calendar-stuff.js'; // set up for the jquery calendar

        return $jsArray;
    }
   
}
