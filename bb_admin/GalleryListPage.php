<?php
#print_r($_POST);
/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description

*/

Class GalleryListPage extends Page{

	function __construct($dbPDO){
		   
		$this->dbPDO=$dbPDO;
		$this->_initialise();
			
	}
       
       
    function _initialise(){
                
		$this->title = 'poop';
		
	
				
		#require_once ('News/News.php');
		require_once ('Gallery/Model/Gallery.php');
		
		$mGallery = new Model_Gallery($this->dbPDO);	
		 //Add Item form
		 
		if(isset($_POST['title']) ){
                        
			#print_r($_POST);
			#exit;
	   
			require_once('forms/validation.php');
			
			$validator=new FormValidator();
			
			
			
			
			$validator->isEmpty('title', 'Please enter a Title');
			//$validator->isEmpty('filename', 'Please Filename for the page'); NOT REQUIRED FOR THIS TYPE
			//$validator->isAlphaNumbericAndDash('filename', 'Numbers, letters and dashes only for the filename');
			
			
			if($validator->isError() ){
					
					 $this->contentString .= '<p class="error">Please correct the following error(s):</p><ul>';
					 foreach ($validator->getErrorList() as $name=>$value){
							
							$this->contentString .= '<li>' . $value['msg'] .'</li>';
							
							
					 }
					$this->contentString .= '</ul>';
					 
					 
			}
			else{

	
					$mGallery->addItem($_POST['title']);
					
			}
                
		}

                $newsString = '';
                
                $newsString .=  '<form class="add_form" name="addNew" method="post" action="'.$_SERVER['REQUEST_URI'].'">';
                $newsString .=  '<label for="title">Title</label><input type="text" name="title" value="" style="width: 30em" />';
                $newsString .=  '<input type="submit" value="Add new item" />';
                $newsString .=  '</form>';
                
                
                
                
             
                $mGallery->isActive=false; //Ove-ride the default - the 
                #echo $mGallery->isActive;
                #exit;
                
              
                
                
                
                $data = $mGallery->get();
                
                
                
                #$zebra = 1; #Used for the zebra "tables"
                $counter=0;
                $activeCount=0;
                
                $newsString .= '<table id="itemlist">';
                $newsString .= '<thead>';
                $newsString .= '<tr>
                                        <th class="date first">Publish Date</th>
                                        <th class="date">Expiry Date</th>
                                        <th class="title">Title</th>
                                        
                                </tr>';
                $newsString .= '</thead>';
                $newsString .= '<tbody>';
                
                foreach($data as $row){
                
               
                       //print_r($row);
                       // $newsString .= "\n" . '<tr class="' . $status . '">';
                        
                       // $newsString .= "\n" . '<td class="date first">'  . date('d M Y',strtotime($row['published'])) . '</td>';
                        
                        #echo $row['depublish'];
                        #exit;
                      //  if( !$row['depublish'] || $row['depublish'] == '0000-00-00 00:00:00'){
                                
                      //         $dateString = 'never';
                                
                      //  } else {
                     //             $dateString = date('d M Y',strtotime($row['depublish']));
                                
                      //  }
                        
                    //    $newsString .= "\n" . '<td class="date">'  . $dateString . '</td>';
                        $newsString .= '<td><a href="?id=' . $row['id'] . '">' . htmlentities($row['name'],ENT_QUOTES,'UTF-8')  . '</a></td>';
                        
                        
                        
                        $newsString .= "\n" . '</tr>';
                        
                        
                        
                      
                        $counter++;
                
                }
                 $newsString .= '</tbody>';
                 $newsString .= "\n" . '</table>';
                #echo '<p>' . $activeCount .' active out of ' . $counter . ' ('  . round(($activeCount/$counter)*100,1) . '%)';
                
                //echo $newsString;
                
                $this->contentString .= $newsString;


			


       }
        function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'news','title'=>'News');
		return $breadcrumbArray;

        }
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/flexi.css'; 
                
                //$cssArray[]='build/fonts/fonts.css';                    //calendar
                //$cssArray[]='build/reset/reset.css';                    //calendar
                //$cssArray[]='build/calendar/assets/calendar.css';       //calendar
                
                //$cssArray[]='build/fonts/fonts-min.css';                //tabs
                //$cssArray[]='build/tabview/assets/skins/sam/tabview.css';//tabs
		
                return $cssArray;
                

        
        }
        function javaScriptArray(){
                
                $jsArray=array();
                $jsArray[]='js/validate_form.js';              //calendar
                $jsArray[]='js/tooltips.js';
               
                
                return $jsArray;
        }
       
}

