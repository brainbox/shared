<?php
#print_r($_POST);
/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description

*/

Class OrphanPage extends Page{

     function __construct($dbPDO){
            
          $this->dbPDO=$dbPDO;
          $this->_initialise();
             
     }
       
       
       function _initialise(){
                
                #require_once ('Orphan/Orphan.php');
                require_once ('articles/Model/Item.php');
                
                $orphanModel = new Model_Item($this->dbPDO);
                
                
                #print_r($orphanModel);
                 //Add Item form
                 
                if(isset($_POST['title']) ){
                        
                        #print_r($_POST);
                        #exit;
                   
                        require_once('forms/validation.php');
                        
                        $validator=new FormValidator();

                        $validator->isEmpty('title', 'Please enter a Title');
                        //$validator->isEmpty('filename', 'Please Filename for the page'); NOT REQUIRED FOR THIS TYPE
                        //$validator->isAlphaNumbericAndDash('filename', 'Numbers, letters and dashes only for the filename');
                        
                        
                        if($validator->isError() ){
                                
                                 $this->contentString .= '<p class="error">Please correct the following error(s):</p><ul>';
                                 foreach ($validator->getErrorList() as $name=>$value){
                                        
                                        $this->contentString .= '<li>' . $value['msg'] .'</li>';
                                        
                                        
                                 }
                                $this->contentString .= '</ul>';
                                 
                                 
                        }
                        else{

                
                                $orphanModel->addOrphan($_POST['title']);
                                
                        }
                
                }
               							
 
                 
                 
                 
                 
                $orphanString = '';
                
                $orphanString .=  '<form class="add_form" name="addNew" method="post" action="'.$_SERVER['REQUEST_URI'].'">';
               #$orphanString .=  '<input type="hidden" name="action" value="add" />';
                $orphanString .=  '<label for="title">Title</label><input type="text" name="title" value="" style="width: 30em" />';
                $orphanString .=  '<input type="submit" value="Add new item" />';
                $orphanString .=  '</form>';
                
                
                
                
             
               $orphanModel->active = false; //Ove-ride the default - the
               $orphanModel->orphanOnly = true;
               $orphanModel->navOnly = false;
               $orphanModel->getItems();
               
                #echo $orphanModel->isActive;
                #exit;
                
              
                
                
                
               # $orphanModel->getItems();
                
                
                
                #$zebra = 1; #Used for the zebra "tables"
                $counter=0;
                $activeCount=0;
                
                $orphanString .= '<table id="itemlist">';
                $orphanString .= '<thead>';
                $orphanString .= '<tr>
                                        <th class="date first">Publish Date</th>
                                        <th class="date">Expiry Date</th>
                                        <th class="title">Title</th>
                                        <th >Path</th>
                                        
                                </tr>';
                $orphanString .= '</thead>';
                $orphanString .= '<tbody>';
                
                while($row = $orphanModel->fetch() ){
                    
                    #print_r($row);
               
                      
                        $status=Model_Item::status(strtotime($row['published']),strtotime($row['depublish']));
                        
                       
                        $orphanString .= "\n" . '<tr class="' . $status . '">';
                        
                        $orphanString .= "\n" . '<td class="date first">'  . date('d M Y',strtotime($row['published'])) . '</td>';
                        
                        #echo $row['depublish'];
                        #exit;
                        if(!$row['depublish'] || $row['depublish'] == '0000-00-00 00:00:00'){
                                
                                 $dateString = 'never';
                                 
                                
                        } else {
                                
                                $dateString = date('d M Y',strtotime($row['depublish']));
                        }
                        
                        $orphanString .= "\n" . '<td class="date">'  . $dateString . '</td>';
                        $orphanString .= '<td><a href="?id=' . $row['id'] . '">' . htmlentities($row['title'],ENT_QUOTES,'UTF-8')  . '</a></td>';
                         $orphanString .= '<td>' . htmlentities($row['path'],ENT_QUOTES,'UTF-8')  . '</td>';

                        #if($row['notes']){
                        #$orphanString .= '<br />' . $row['notes'];
                        #}
                        
                        #echo '<hr>' . $row['lastUpdate'];
                        
                        /*
                        $elapsed = (time() - strtotime($row['lastUpdate']));
                        
                        switch($elapsed){
                        case( $elapsed < (60*60) ):
                        
                        $orphanString .= ' -  ' . round($elapsed/60) . ' minutes ago';
                        
                        break;
                        
                        case($elapsed < (60*60*24) ):
                        
                        $orphanString .= ' -  ' . round($elapsed/(60*60) ) . ' hours ago';
                        
                        break;
                        
                        default:
                        $orphanString .= ' -  ' . round($elapsed/(60*60*24)  ) . ' days ago';
                        
                        }
                        */
                        
                        #if($row['isActive){
                        
                        #     $orphanString .= ' - <a href="http://rotslon01/wwwNakheelCom/www/orphan/orphan_detail.php?' . str_replace('/','', $row['path) . '">preview</a>';
                        
                        # }
                        
                        
                        
                        $orphanString .= "\n" . '</tr>';
                        
                        
                        
                      
                        $counter++;
                
                }
                 $orphanString .= '</tbody>';
                 $orphanString .= "\n" . '</table>';
                #echo '<p>' . $activeCount .' active out of ' . $counter . ' ('  . round(($activeCount/$counter)*100,1) . '%)';
                
                #echo $orphanString;
                
                $this->contentString .= $orphanString;


       }
        function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'orphans','title'=>'Orphans');
		return $breadcrumbArray;

        }
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/flexi.css'; 
                
                //$cssArray[]='build/fonts/fonts.css';                    //calendar
                //$cssArray[]='build/reset/reset.css';                    //calendar
                //$cssArray[]='build/calendar/assets/calendar.css';       //calendar
                
                //$cssArray[]='build/fonts/fonts-min.css';                //tabs
                //$cssArray[]='build/tabview/assets/skins/sam/tabview.css';//tabs
		
                return $cssArray;
                

        
        }
        function javaScriptArray(){
                
                $jsArray=array();
                $jsArray[]='js/validate_form.js';              //calendar
                $jsArray[]='js/tooltips.js';
               
                
                return $jsArray;
        }
       
}

?>

