<?php
#print_r($_POST);
/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description

*/

Class PicturesPage extends Page{

    public $dbPDO;
    public $pathToUserFiles;
    
    function __construct(){
           
           
            
    }
   
   
    function initialise(){
                
        if(!$this->dbPDO ){
               
               throw Exception('No db');
               
        }
       
        $this->handlePOST();
       
        if( isset($_GET['referrerEncoded']) ){

            $this->getListForAssignment();

           return;
        }
       
       
        if( isset($_GET['id']) ){
               
               $this->getItem();
               
               
        } else {
               
               $this->getList();
        }
               
               
       return;   
       
           
          
           
           
    }
        
    /**
     *
     *
     *
     *
     */
    
   
    function handlePOST(){
        
        
        if(!isset($_POST['action'])){
            
            return;
            
        }
        
        
        switch($_POST['action']){
            
            
            case 'add':
                
                $this->imageAdd();
                break;
            
            case 'delete':
                $this->imageDelete();
                break;
            
             case 'alt':
                $this->imageAlt();
                break;
                
            case 'replace':
                
                $this->imageReplace();
                break;
            
            
            default:
            
                //no default
            
            
        }
        
        
        //Avoid "reposting" problems by redirecting
        header("Location: ".$_SERVER['REQUEST_URI']);
        die;    
        
        
    }
       
    function imageAdd(){
        
                
            require_once 'Image/Model/Image.php';
            
            if(Model_Image::addFromPOST($this->dbPDO, $this->pathToUserFiles) ){ 
                    
                //Cool
                    
            } else {
                
                $this->contentString .= 'Upload failed. Image ' . $_FILES['image']['name'] . ' already exists';
                
            }
        
        
    }
    
    
    
    function imageReplace(){
                
        require_once 'Image/Model/Image.php';
        
        Model_Image::replaceFromPOST($this->dbPDO, $this->pathToUserFiles);
        
    }
    
    
    
    function imageAlt(){
        
                
        require_once 'Image/Model/Image.php';
        
        Model_Image::updateAltFromPOST($this->dbPDO) ;
             
        
        
    }
       
    function imageDelete(){}
        
        
        
        

    /**
     *
     * 
     *
     *
     */
    function getList(){
            
        require_once 'Image/Model/Image.php';
        
        $imageArray = Model_Image::getAll($this->dbPDO);
        
        ob_start();
        
                require  'views/image-list.php';
                    
                $this->contentString .=  ob_get_contents();
                    
        ob_end_clean();
            

            
    }
   
   
    /**
     *
     * @todo - add view
     *
     *
     */
    function getItem(){
            
               
        require_once 'Image/Model/Image.php';
        
        $image = Model_Image::getById($this->dbPDO, $_GET['id']);
        
        //var_dump($image);
        
        
        ob_start();
        
                require  'views/image-item.php';
                    
                $this->contentString .=  ob_get_contents();
                    
        ob_end_clean();
            
            
            
    }
   
   
   
   
    function breadcrumbArray(){

    $breadcrumbArray=array();
    $breadcrumbArray[]=array('fullPath'=>'news','title'=>'News');
    return $breadcrumbArray;

    }
   
    function cssArray(){
            
          
            $cssArray=array();
            
            $cssArray[]='css/flexi.css'; 
            
            //$cssArray[]='build/fonts/fonts.css';                    //calendar
            //$cssArray[]='build/reset/reset.css';                    //calendar
            //$cssArray[]='build/calendar/assets/calendar.css';       //calendar
            
            //$cssArray[]='build/fonts/fonts-min.css';                //tabs
            //$cssArray[]='build/tabview/assets/skins/sam/tabview.css';//tabs
    
            return $cssArray;
            

    
    }
    
    
    
    
    function javaScriptArray(){
            
            $jsArray=array();
         
            return $jsArray;
    }
  
       
}

