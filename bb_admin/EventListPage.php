<?php
#print_r($_POST);
/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description

*/

Class EventListPage extends Page{

        function __construct($dbPDO){
               
                $this->dbPDO=$dbPDO;
                $this->_initialise();
                
        }
       
       
       function _initialise(){
                
                #require_once ('event/event.php');
                require_once ('Event/Model/Event.php');
                
                $eventModel = new Model_Event($this->dbPDO);	
                 //Add Item form
                 
                if(isset($_POST['title']) ){  
                        
                        #print_r($_POST);
                        #exit;
                   
                        require_once('forms/validation.php');
                        
                        $validator=new FormValidator();
                        
                        
                        
                        
                        $validator->isEmpty('title', 'Please enter a Title');
                        //$validator->isEmpty('filename', 'Please Filename for the page'); NOT REQUIRED FOR THIS TYPE
                        //$validator->isAlphaNumbericAndDash('filename', 'Numbers, letters and dashes only for the filename');
                        
                        
                        if($validator->isError() ){
                                
                                 $this->contentString .= '<p class="error">Please correct the following error(s):</p><ul>';
                                 foreach ($validator->getErrorList() as $name=>$value){
                                        
                                        $this->contentString .= '<li>' . $value['msg'] .'</li>';
                                        
                                        
                                 }
                                $this->contentString .= '</ul>';
                                 
                                 
                        }
                        else{

                
                                $eventModel->addItem($_POST['title']);
                                
                        }
                
                }
               							
 
                 
                 
                 
                 
                $eventString = '';
                
                $eventString .=  '<form class="add_form" name="addNew" method="post" action="'.$_SERVER['REQUEST_URI'].'">';
                $eventString .=  '<label for="title">Title</label><input type="text" name="title" value="" style="width: 30em" />';
                $eventString .=  '<input type="submit" value="Add new item" />';
                $eventString .=  '</form>';
                
                
                
                
             
                $eventModel->isActive=false; //Ove-ride the default - the 
                #echo $eventModel->isActive;
                #exit;
                

                
                $eventModel->getItems();
                
                ob_start();
                
                    require  'views/event-list.php';
                            
                    $this->contentString .=  ob_get_contents();
                            
                ob_end_clean();
                    
                
              

       }
        function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'events','title'=>'Events');
		return $breadcrumbArray;

        }
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/flexi.css'; 
                
                //$cssArray[]='build/fonts/fonts.css';                    //calendar
                //$cssArray[]='build/reset/reset.css';                    //calendar
                //$cssArray[]='build/calendar/assets/calendar.css';       //calendar
                
                //$cssArray[]='build/fonts/fonts-min.css';                //tabs
                //$cssArray[]='build/tabview/assets/skins/sam/tabview.css';//tabs
		
                return $cssArray;
                

        
        }
        function javaScriptArray(){
                
                $jsArray=array();
                $jsArray[]='js/validate_form.js';              //calendar
                $jsArray[]='js/tooltips.js';
               
                
                return $jsArray;
        }
       
}

?>

