<?php
//echo '<pre>';
//print_r($_POST);
//echo '</pre>';

/**
 * This class expects the following constants
 * BASE_PATH
 * BASE_FILE_PATH
 *
 *
 * Also... the page REQUIRES JavScript to work
 *
 * 
 * This class needs to provide the following to the parent class:
* 
 * $this->contentString					
 * $this->shortTitle 
 * $this->titleNote 
 * $this->title 
 * $this->description
 */

@ DEFINE(DOCUMENT_SERVING_PAGE,'documents');

require_once('documents/Documents.php');

Class DocumentPage extends Page{
    
    public $contentString='';					
    public $shortTitle = 'Documents';
    protected $titleNote =  'Documents';
    public $title =      'Documents';
    protected $description='';
    protected $message='<p>&nbsp;</p>';

    function __construct($dbPDO){
    
        $this->dbPDO=$dbPDO;
        //$this->basePath=$basePath;
        
        $this->_initialise();       

    }
    

    
    // A function to apply mysql_real_escape_string   //may not need this anymore
    function escapeValue($value) {
    
        return mysql_real_escape_string($value);
    
    }
   
   
    private function _initialise(){
        
        //Set the form       
        $this->_setForm();
        
        //Process the form (if necessary)
        if($this->form->validate()){

            $this->_processForm();

        }
        
        //Container for messages
        $this->contentString .= '<div>';
        $this->contentString .= $this->message;
        $this->contentString .= '</div >';
        
        
        //New Document 
        //Container for the FORM
        $this->contentString .= '<div class="document new" >';
        $this->contentString .= '<div><a class="hasForm" id="addNew">Add New Document...</a></div>';
        $this->contentString .= '</div >';
        
        
        
        //List the docs
        $documents=new Documents($this->dbPDO);
        
        while ($document=$documents->fetch() ){
            
            $this->contentString .= '<div class="document">';
            $this->contentString .= '<h2><a href="' . BASE_PATH . DOCUMENT_SERVING_PAGE . '/' . $document->id . '">' . $document->filename . ' (' . $document->fileSizeHuman() . ')</a></h2>';
            
            $dateString = 'Created on ' . date( 'd M Y H:i:s',$document->create_date);
            
            if ($document->upload_date > $document->create_date)
            {
                
                switch($document->upload_date){
                    
                    //TODAY
                    case( $document->upload_date > mktime(0,0,0,date('m'),date('d'),date('y') ) ):
                        
                    $dateString .= ' | Updated TODAY at ' . date('H:i:s',$document->upload_date);
                    
                    break;
                
                    //YESTERDAY
                    case( $document->upload_date > mktime(0,0,0,date('m'),date('d')-1,date('y') ) ):
                        
                    $dateString .= ' | Updated YESTERDAY at ' . date('H:i:s',$document->upload_date);
                    
                    break;
                    
                    default:
                
                    $dateString .= ' | Updated on ' . date('d M Y H:i:s',$document->upload_date);
                
                }
                
            }
            
            $this->contentString .= '<p>' . $dateString . '</p>';
            
            
            
            //Container for the FORM
            $this->contentString .= '<div id="' . $document->id . '" >';
            $this->contentString .= '<a class="hasForm">Add new version...</a>';
            $this->contentString .= '</div >';
            
            //Complete the document wrapper
            $this->contentString .= '</div>';
            
        }
        
        
        //Stick the form at the bottom of the page
        $this->contentString .= '<div class="hidden">';
        $this->contentString .= $this->form->toHTML();
        $this->contentString .= '</div>';
    

	}
    
   
    
    private function _setForm(){
        
        require_once ("HTML/QuickForm.php");
        $this->form = new HTML_QuickForm('fileUpload', 'post', $_SERVER['REQUEST_URI'],null,null, true);
        
        $this->form->removeAttribute('name'); //for XHTML compliance
        
        //For testing - will later be added by JS
        //$this->form->addElement('hidden','action','newDocument');

        
        //The file upload:
        $this->form->addElement('file','file','Select a file',array('class'=>'text'));
        $this->form->addElement('submit','submit','Submit', array('class'=>'button'));
       
        $this->form->addRule('file',    'Please select a file',               'required','' ,'client');
        $this->form->addRule('file',    'The file must .doc, .docx, .xls, .ppt or .pdf', 'mimetype', array('application/msword','application/vnd.openxmlformats-officedocument.wordprocessingml.document','application/excel','applicaton/pdf','application/pdf','application/powerpoint'));
        $this->form->addRule('file', 	'The maximum file size is 5 MB', 'maxfilesize', (5*1048570) );
        //$this->form->addRule('file', 	'Punctuation not allowwed in the filename', 'nopunctuation');

    }
    
    private function _processForm(){
        
        $this->form->applyFilter('__ALL__','escapeValue');			//Dounbt this works				
          
        $formData=$this->form->getSubmitValues();
        //print_r  ($formData);
        //exit;
        
        $documents=new Documents($this->dbPDO);
        
        if ($this->form->getSubmitValue('action')=='addDocument' )
        {
            
            
            if (  $documents->addDocument($this->form->_submitFiles['file'])  )
            {
                $this->message = '<p>New document added successfully</p>';
            }
            else
            {
               $this->message = '<p class="error">File Exists</p>';
            }
        
        }
        
         if ($this->form->getSubmitValue('action')=='addVersion' )
        {
            //echo '<p>The ID is ' . $formData['id'];

            $document=$documents->getByID($formData['id']);
            
            //print_r($document);
            
            if (  $document->addVersion($this->form->_submitFiles['file'], BASE_FILE_PATH . '/documents/')  )
            {
                $this->message = '<p>New version added successfully</p>';
            }
            else
            {
                $this->message = '<p class="error">Filename in use by another document</p>';
            }
        
        }
        //echo $this->message;
        
    }
	
	function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'dummy','title'=>'Documents');
		return $breadcrumbArray;

        }
       
       
        function cssArray(){
                
              
            $cssArray=array();
            $cssArray[]='css/documents.css'; 
            return $cssArray;
                

        
        }
        function javaScriptArray(){
                
            $jsArray=array();
            //$jsArray[]='js/validate_form.js';              //calendar
            $jsArray[]='js/showForm.js';
            return $jsArray;
        }
       
}

?>

