<?php
//header('Expires: Mon, 26 July 1997 05:00:00 GMT' );
//header('Pragma: nocache');

//echo '<pre>';
//print_r($_POST);
//echo '</pre>';

/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description


// Oct 2007 - This WAS the page used for just about everything. Now want to strip it down for the simplest of items: NEWS (may later extend it to other items
*/

Class FlexiItemPage extends Page{
        
        var $className;

        function __construct($db,$basePath, $dbPDO, $type){
		
		
                $this->db=$db;
	        $this->dbPDO=$dbPDO;
		$this->type=$type;
		//echo '---------------';

		$this->_initialise();       

        }
        
    
        
       // A function to apply mysql_real_escape_string   //may not need this anymore
       function escapeValue($value) {
        
               return mysql_real_escape_string($value);
        
        }
       
       
       function _initialise(){
                require_once('articles/Items.php');
		//echo '---------------';
		//echo $this->type;
		
                require_once('articles/'.$this->type.'Items.php');
		//echo '---------------';
                                                
                // Include the QuickForm class
                //require_once ("HTML/QuickForm.php");
                // require_once 'HTML/QuickForm/element.php';  
    
	//echo 'HELLLO OUT THERE';
        
        
        
        
        $params=array('section'=>'news','active'=>false);
        
        //Params( $db, $table, $params='', $limit='') {
	
	//echo $this->type;
        $tempString= (string) $this->type;
        //echo $tempString;
        
	$tempString= $tempString.'Items';
        
        //Generates a error... bu I don't know why
        $$className= $tempString;
	
	//print_r($$className);
	//Generates a error... bu I don't know why
        $flexiItems=new $$className($this->db, 'news', $params);
        
        $theFlexiItem=$flexiItems->getItemByID($_GET['id']);
        
        
        
        if(isset($_POST['main'])  ){
                
                //print_r($_POST);
           
           
                require_once('forms/validation.php');
                
                $validator=new FormValidator();
                
                
                
                
                $validator->isEmpty('title', 'Please enter a Title for the page');
                //$validator->isEmpty('filename', 'Please Filename for the page'); NOT REQUIRED FOR THIS TYPE
                //$validator->isAlphaNumbericAndDash('filename', 'Numbers, letters and dashes only for the filename');
                
                $validator->isEmpty('published', 'Please enter a Publish date');
                $validator->isEmpty('depublish', 'Please enter an Expiry date');
                
                if($validator->isError() ){
                        
                         $this->contentString .= '<p class="error">Please correct the following error(s):</p><ul>';
                         foreach ($validator->getErrorList() as $name=>$value){
                                
                                $this->contentString .= '<li>' . $value['msg'] .'</li>';
                                
                                
                         }
                        $this->contentString .= '</ul>';
                         
                         
                }
                else{
           
                        $theFlexiItem->title=           $_POST['title'] ;
                        $theFlexiItem->shortTitle=      $_POST['shortTitle'] ;
                        $theFlexiItem->metaTitle=       $_POST['metaTitle'] ;
                        
                        
                        //$theFlexiItem->filename=       $_POST['filename'];
                       
                        
                        
                        //dates to UNIX timestamps
                        $dateArray=explode("-", $_POST['published'] );
                        //print_r($dateArray);
                        $published=  mktime(0,0,0,$dateArray[1],$dateArray[0],$dateArray[2]);
        
                        $dateArray=explode("-", $_POST['depublish'] );
                        $depublish=  mktime(0,0,0,$dateArray[1],$dateArray[0],$dateArray[2]);
                        
                        $theFlexiItem->published=       $published;
                        $theFlexiItem->depublish=       $depublish;
                        
                        
                        $theFlexiItem->metaDescription= $_POST['metaDescription'] ;
                                  
                       
                        if (isset($_POST['isHTML']) ){          
                                $theFlexiItem->isHTML=       1;
                        }
                        else{
                                $theFlexiItem->isHTML=       0;
                        }
                        $theFlexiItem->body=            $_POST['body'] ;
                        
                        
                        $theFlexiItem->finalise();
                }
        }
      
        
        //echo $theFlexiItem->code;
        //e//xit;
        
        //Header for the page				 
        if( $theFlexiItem->title ){

        $this->title .= $theFlexiItem->title ;
        
        }
        else{
                
                  $this->title .= 'Home' ;
        }
        
       
        //Try a hadn-coded form
        $this->contentString .= '

        
        
        <form action="'.$_SERVER['REQUEST_URI'] . '" method="post" name="updateflexiItem" id="updateflexiItem">
        <input name="main" id="main" type="hidden" value="1" />
        
        <table class="form" border=0>
      
        
        
	<tr>
		<td class="col1">';
                        
                        
                       
                        
                                 
                        //echo '<hr>' . $theFlexiItem->parentID;
                       // exit;
                        

                         //No FILE PATH STUFF thing to do for the Home page
                    
                                
                        $this->contentString .= '<div class="container">
                        <label for="title">Title</label>
                        <input name="title" id="title" type="text" value="'.$theFlexiItem->title.'" class="long checkRequired" />
                        </div>
                        
                        
                        <div class="container">
                        <label for="shortTitle">Short (Navigation) Title</label>
                        <input name="shortTitle" id="shortTitle" type="text" value="'.$theFlexiItem->shortTitle.'"  class="medium" />
                        </div>
                        
                        <div class="optionalContainer">
                        <!-- Hide the Meta info -->
                        <span class="hastooltip" >More...</span>
                        
                        <div class="optional">
                        
                        <!--  NO NEED FOR A FILENAME FOR THIS KIND OF ITEM
                        <div class="container">
                        <label for="shortTitle">Filename</label>
                        <input name="path" id="filepath" type="hidden" value="' . $theFlexiItem->filepath . '"/>
                        <br />' . $theFlexiItem->filepath . '/<input name="filename" id="filename" type="text" value="' . $theFlexiItem->filename . '" class="medium" style="display: inline"/>
                        </div>
                        -->
                       
                        <div class="container">
                        <label for="metaTitle">Meta Title</label>
                        <input name="metaTitle" id="metaTitle" type="text" value="' . $theFlexiItem->metaTitle . '" class="long" />
                        
                        <div class="container">
                        <label for="metaDescription">Meta Description (or Teaser)</label>
                        <textarea rows="5" cols="80" name="metaDescription" id="metaDescription" class="description">' . $theFlexiItem->metaDescription . '</textarea>
                        </div>
                        
                        </div>
                        
                        
                        
                </td>
                
                <td>
                 
                  <!-- SAVE FOR EVENT OBJECTS -->
                 
                </td>
                
                
                <td class="' . $theFlexiItem->status() . '">';
                      
                      
                        
                        $this->contentString .= '<div class="container">
                        <label for="published">Publish</label> (dd-mm-yyyy)
                        <input name="published" id="published" type="text" value="'. date('d-m-Y',$theFlexiItem->published) .'"  class="short checkRequired" />
                        </div>
                        
                        <div class="container">
                        <label for="depublish">Expire</label> (dd-mm-yyyy) 
                        <input name="depublish"  id="depublish" type="text" value="'. date('d-m-Y',$theFlexiItem->depublish) .'"  class="short checkRequired" />
                        </div>';
                       
                
                 $this->contentString .= '</td>
	</tr>
        
      
        <tr>
		<td colspan="3" class="body">
                
                        <input name="submit" value="Save" type="submit" style="float:right"/>
               

                        <label for="body">Body</label>&nbsp;&nbsp;&nbsp;&nbsp;
                        <input name="isHTML"  id="isHTML" type="checkbox" ';
                        
                        if($theFlexiItem->isHTML){
                                $this->contentString .=  ' "checked"="checked" ';
                        }
                        
                        
                         $this->contentString .=  ' />
                        <label for="isHTML">is HTML</label>
                        
                        
                        <div class="container">
                        <textarea rows="20" cols="80" name="body" id="body" >' . $theFlexiItem->body . '</textarea>
                        </div>
      

              
                
                </td>
	</tr>
        <tr>
        <td colspan="3" class="submit" >
          <input name="submit" value="Save" type="submit" />
        </td>
        </tr>
        </table>

        </form>
        ';
        
        //Get some BBCode help... if necessary
        if(!$theFlexiItem->isHTML){
                
                ob_start();
                
                require('template/help-bbcode.htm');
                
                $helpBBCode=ob_get_clean();

                $this->contentString .= $helpBBCode;
                
                
        
        }
        
        
     
        
        
        //SEPARATE FORM FOR CODE... just for me to see.
        

	}
	
	function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'news','title'=>'News');
		$breadcrumbArray[]=array('fullPath'=>'news?id=' . $_GET['id'] ,'title'=>$this->title );
		return $breadcrumbArray;

        }
       
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/flexi.css'; 
                
		
                return $cssArray;
                

        
        }
        function javaScriptArray(){
                
                $jsArray=array();
                $jsArray[]='js/validate_form.js';              
                $jsArray[]='js/tooltips.js';
                
              
                
                return $jsArray;
        }
       
}

?>

