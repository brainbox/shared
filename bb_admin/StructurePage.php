<?php

/**
 * This class needs to provide the following to the parent class:
 * 
 *    $this->contentString	
 *    $this->shortTitle 
 *    $this->titleNote 
 *    $this->title 
 *    $this->description
 *
 *
 *  New addition to the Admin site, designed to "set up" the main sections
 *  of the website, control their order, and allow the addition of
 *  banners
 *    
 */




require_once('articles/Model/Item.php');  //Used for a statc call to get status

Class StructurePage {
     
     var $contentString=''; 
     
     
     
     function __construct($dbPDO,$baseFilePath){  
      
          $this->dbPDO=$dbPDO;
          $this->baseFilePath=$baseFilePath;
          $this->title='Website Structure';
          $this->_initialise();

     }
        
    
       
       
     function _initialise(){
                
          print_r($_POST);
          //Handle image upload (if any)
          if(!empty($_POST['addImage'])){
               
               echo '<pre>';
               print_r($_FILES);
               print_r($_POST);
               echo '</pre>';
          
               $file=$_FILES['image'];
               
               if($file['error'] == 0 && $file['size'] > 0){
                   
                    $newimage = $this->baseFilePath . 'images/section/' . $_POST['id'] . '.jpg';
                    #echo '<p>NEW ismage is ' . $newimage;
                    
                    $result = move_uploaded_file($file['tmp_name'], $newimage);
                    
                    print_r($result);
                    
               }
               if(isset($result)){
                    
                    $this->contentString .= 'Image added';
               } else{
                    
                    $this->contentString .= 'Image upload failed';
               }
          }
         

          /* 
               require_once('forms/validation.php');
               
               $validator=new FormValidator;
               
               $validator->isEmpty('title', 'Please enter a title for the page');
               #$validator->isEmpty('filename', 'Please enter a filename for the page');
               #$validator->isAlphaNumbericAndDash('filename', 'The filename may contain letters, numbers and dashes only');
               
               if($validator->isError()  ){
                     
                    echo '<p class="error">Please correct the following:</p>';
                    echo '<ul>';
                    
     
                    $errors=$validator->getErrorList();
                    foreach ($errors as $name=>$value){
                         echo '<li>' . $value['msg'] . '</li>';
                    }
                    echo '</ul>';
               }
               else{
                    //echo 'adding';
                    
                    require_once('articles/Item.php');
                    $itemModel = new Model_Item($this->dbPDO);
                    $itemModel->addChild(
                         $_POST['menuID'],
                         $_POST['menuLocation'],
                         $_POST['title']) ;
                    
                    #{
                         
                         $this->contentString .= '<p>New page added successfully</p>';
                    #}
                    #else{
                        # $this->contentString .= '<p class="error">Filename already exists</p>';
                    #}
               }
            
          }
          */
          $this->contentString .= '<h2>Home Page</h2>
           <form action="" method="post" enctype="multipart/form-data" name="addImage" id="addImage">
               <input name="addImage" type="hidden" value="1" />
               <input name="id" id="id" type="hidden" value="1" />
               <input type=hidden name="MAX_FILE_SIZE" value="1048576" />
               <input type="file" name="image" size="60" /><input type="submit" value="Add/Update Banner" />
               
               <p>&nbsp;</p>';
     
          
     
          $model= new Model_Item($this->dbPDO);
          
          $model->getSections();
          
          
          while ($row = $model->fetch()){
               
               #print_r($row);
               
               $this->contentString .= '<h2>' . $row['title'] . '</h2>
               <form action="" method="post" enctype="multipart/form-data" name="addImage" id="addImage">
               <input name="id" id="id" type="hidden" value="' . $row['id'] . '" />
               <input type=hidden name="MAX_FILE_SIZE" value="1048576" />
               <input type="file" name="image" size="60" /><input type="submit" value="Add/Update Banner" />
               
               
                </form>';


               
          }
         
     

   
     }
     
     function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'navigation','title'=>'Navigation');
		return $breadcrumbArray;

     }
       
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/tree.css';
                
                
             
		
                return $cssArray;
                

        
     }
     function javaScriptArray(){
                
                $jsArray=array();
                $jsArray[]='js/tooltips.js';
                $jsArray[]='js/validation.js';
                
                return $jsArray;
        }
       
}
