<?php
#print_r($_POST);
/*
This class needs to provide the following to the parent class:

$this->contentString					
$this->shortTitle 
$this->titleNote 
$this->title 
$this->description

*/

Class GalleryPage extends Page{

	function __construct($dbPDO){
		   
		$this->dbPDO=$dbPDO;
		$this->_initialise();
			
	}
       
       
    function _initialise(){
                
		
	
				
		#require_once ('News/News.php');
		require_once ('Gallery/Model/Gallery.php');
		
		$mGallery = new Model_Gallery($this->dbPDO);	
		 //Add Item form
		 
		 
		$mGallery->isActive=false;	
                
		$data = $mGallery->getById($_GET['id']);		
		
		//print_r($data);
		
		$this->title = $data['name'];
		$galleryName =  $data['name'];
		
		///Get the images
		require_once ('Gallery/Model/GalleryImage.php');
		
		$mImage = new Model_GalleryImage($this->dbPDO);	
		 //Add Item form
		 
		 
		$mImage->isActive=false;	
		
		$data = $mImage->getByGalleryId($_GET['id']);
		
		
		//print_r($data);
		
		$tempString = '';
		
		foreach($data as $image){
			
			$tempString .= '<img src="../../galleries/' . $galleryName . '/160x160/' . $image['filename'] . '" />';
			$tempString .= $image['is_active'];
		}
		
		
		
		$this->contentString = $tempString;
		
		//exit;
		
		
			


    }
    
	function breadcrumbArray(){

		$breadcrumbArray=array();
		$breadcrumbArray[]=array('fullPath'=>'gallery','title'=>'Gallery');
		return $breadcrumbArray;

    }
       
        function cssArray(){
                
              
                $cssArray=array();
                
                $cssArray[]='css/flexi.css'; 
                
                //$cssArray[]='build/fonts/fonts.css';                    //calendar
                //$cssArray[]='build/reset/reset.css';                    //calendar
                //$cssArray[]='build/calendar/assets/calendar.css';       //calendar
                
                //$cssArray[]='build/fonts/fonts-min.css';                //tabs
                //$cssArray[]='build/tabview/assets/skins/sam/tabview.css';//tabs
		
                return $cssArray;
                

        
        }
        function javaScriptArray(){
                
                $jsArray=array();
                $jsArray[]='js/validate_form.js';              //calendar
                $jsArray[]='js/tooltips.js';
               
                
                return $jsArray;
        }
       
}

