<?php
/**
* @package SPLIB
* @version $Id: AccountMaintenance.php,v 1.6 2003/11/13 05:37:28 kevin Exp $
*/
/**
* Constants which define table and column names
*/
# Modify this constant to reflect session variable name
// Name to use for login variable used in Auth class
@define ( 'USER_LOGIN_VAR','login');

# Modify these constants to match your user login table
// Name of users table
@define ( 'USER_TABLE','user');
// Name of user_id column in table
@define ( 'USER_TABLE_ID','user_id');
// Name of login column in table
@define ( 'USER_TABLE_LOGIN','login');
// Name of password column in table
@define ( 'USER_TABLE_PASSW','password');
// Name of email column in table
@define ( 'USER_TABLE_EMAIL','email');
// Name of firstname column in table
@define ( 'USER_TABLE_FIRST','firstName');
// Name of lastname column in table
@define ( 'USER_TABLE_LAST','lastName');
// Name of signature column in table
@define ( 'USER_TABLE_ACTIVE','active');


/*
Main stuff - password 



/**
* AccountMaintenance Class<br />
* Provides functionality for users to manage their own accounts
* @access public
* @package SPLIB
*/
class AccountMaintenanceASLTIP extends AccountMaintenance {
   
    var $db;
    var $errorArray;
		
    /**
    * AccountMaintenance constructor
    * @param object instance of database connection
    * @access public
    */
    function AccountMaintenanceASLTIP (&$db) {
        $this->db=& $db;
				
    }


  function fetchLoginbyLogin($login) {
        $email=mysql_escape_string($login);
        $sql="SELECT * 
              FROM
                  ".USER_TABLE."
              WHERE
                  ".USER_TABLE_LOGIN."='".$login."'";

		 // echo $sql;
        $result=$this->db->query($sql);
        if ( $result->size() == 1 )
            return $result->fetch();
        else
            return false;
    }
    
    
    //As long as the email address exisits, this works. Check the Activeness/Webuserness elsewhere
    function resetPassword($email) {
	     
	$login=mysql_escape_string($login);
        
	
	if($details=$this->fetchLoginbyLogin($email) ){
		
		
		$password = $this->generatePassword();
		
		$sql="UPDATE
			  ".USER_TABLE."
		      SET
			  ".USER_TABLE_PASSW."='".md5($password)."'
		      WHERE
			  ".USER_TABLE_ID."='".$details['id']."'";
			  
			  
		$result=$this->db->query($sql);
          	

		return $password;
			

	}
	else{
	   return false;	
	}
	
	
	
    }
		

    
		
			function createUser($login,$password, $email, $first, $last,$sig        ) {
        
				  //create only if login and email don't already exisit
					//NOTE email may be blank!!!!!
					
					if ($this->existsLogin($login)){
						     $this->addError('Username');
								 return false;	 												 
					}
					
					
					
					if (   $email !=''    && $emailStatus=$this->existsEmail($email)){
					       $this->addError('Email');
								 return false;	 	
					
					}
					
					//Does it always get here????
					
					
		      //create the user
		
            $sql="INSERT INTO ".USER_TABLE." SET
                    ".USER_TABLE_LOGIN."='".$login."',
                    ".USER_TABLE_PASSW."='".md5($password)."',";
										
						if($email !=''){				
										
                    $sql .= USER_TABLE_EMAIL."='".$email."',";
						}				
										
										
            $sql .= USER_TABLE_FIRST."='".$first."',
                    ".USER_TABLE_LAST."='".$last."'";
            
						//echo $sql;
						//exit;
						
            $result=$this->db->query($sql);
            if ( $result->isError() ) {
               return FALSE;
           
				
				    }
						else{ 
	
  					    //User successfully created - get the ID
  							$sql='SELECT user_id FROM '.USER_TABLE.' WHERE '.USER_TABLE_LOGIN.'="'.$login.'"';
			
  							//echo $sql;
  							
                $result=$this->db->query($sql);
  							$row=$result->fetch();
  							
  							$this->newUserID=$row['user_id'];
			
						    return true;
		
						}						 
						
									  
      }
		  
}
?>