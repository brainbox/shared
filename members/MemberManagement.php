<?php
/**
* @package SPLIB
* @version $Id: AccountMaintenance.php,v 1.6 2003/11/13 05:37:28 kevin Exp $
*/
/**
* Constants which define table and column names
*/
# Modify this constant to reflect session variable name
// Name to use for login variable used in Auth class
@define ( 'USER_LOGIN_VAR','login');

# Modify these constants to match your user login table
// Name of users table
@define ( 'USER_TABLE','user');
// Name of user_id column in table
@define ( 'USER_TABLE_ID','user_id');
// Name of login column in table
@define ( 'USER_TABLE_LOGIN','login');
// Name of password column in table
@define ( 'USER_TABLE_PASSW','password');
// Name of email column in table
@define ( 'USER_TABLE_EMAIL','email');
// Name of firstname column in table
@define ( 'USER_TABLE_FIRST','firstName');
// Name of lastname column in table
@define ( 'USER_TABLE_LAST','lastName');
// Name of signature column in table
@define ( 'USER_TABLE_ACTIVE','active');





/**
* AccountMaintenance Class<br />
* Provides functionality for users to manage their own accounts
* @access public
* @package SPLIB
*/
class AccountMaintenance {
    /**
    * Database connection
    * @access private
    * @var object
    */
    var $db;

    /**
    * A list of words to use in generating passwords
    * @access private
    * @var array
    */
    var $words;

		
		var $errorArray;
		
		
		
    /**
    * AccountMaintenance constructor
    * @param object instance of database connection
    * @access public
    */
    function AccountMaintenance (&$db) {
        $this->db=& $db;
				
    }

    /**
    * Given an email address, returns the user details
    * that account. Useful is password is not encrpyted
    * @param string email address
    * @return array user details
    * @access public
    */
    function fetchLogin($email) {
        $email=mysql_escape_string($email);
        $sql="SELECT
                  ".USER_TABLE_LOGIN.", 
									".USER_TABLE_PASSW.",
                  ".USER_TABLE_FIRST.", 
									".USER_TABLE_LAST.",
									".USER_TABLE_ACTIVE."
              FROM
                  ".USER_TABLE."
              WHERE
                  ".USER_TABLE_EMAIL."='".$email."'";
        $result=$this->db->query($sql);
        if ( $result->size() == 1 )
            return $result->fetch();
        else
            return false;
    }
		
		 function fetchLoginbyLogin($login) {
        $email=mysql_escape_string($login);
        $sql="SELECT
                  ".USER_TABLE_LOGIN.", 
									".USER_TABLE_PASSW.",
                  ".USER_TABLE_FIRST.", 
									".USER_TABLE_LAST.",
									".USER_TABLE_ACTIVE."
              FROM
                  ".USER_TABLE."
              WHERE
                  ".USER_TABLE_LOGIN."='".$login."'";
        $result=$this->db->query($sql);
        if ( $result->size() == 1 )
            return $result->fetch();
        else
            return false;
    }
			 function fetchLoginByID($ID) {
        $ID=mysql_escape_string($ID);
        $sql="SELECT
                  ".USER_TABLE_LOGIN.", ".USER_TABLE_PASSW.",
                  ".USER_TABLE_FIRST.", ".USER_TABLE_LAST."
              FROM
                  ".USER_TABLE."
              WHERE
                  ".USER_TABLE_ID."='".$ID."'";
									
				//echo $sql;					
									
        $result=$this->db->query($sql);
        if ( $result->size() == 1 )
            return $result->fetch();
        else
            return false;
    }

    /**
    * Given a username / email combination, resets the password
    * for that user and returns the new password.
    * @param string login name
    * @param string email address
    * @return array of user details or FALSE if failed
    * @access public
    */
    function resetPassword($login,$email=null) {
		
		    //EMAIL CAN BE REMOVED SOOON
				
        $login=mysql_escape_string($login);
        $email=mysql_escape_string($email);
        $sql="SELECT ".USER_TABLE_ID.",
                  ".USER_TABLE_LOGIN.", ".USER_TABLE_PASSW.",
                  ".USER_TABLE_FIRST.", ".USER_TABLE_LAST."
              FROM
                  ".USER_TABLE."
              WHERE
                  ".USER_TABLE_LOGIN."='".$login."'";
             
        $result=$this->db->query($sql);
				
        if ( $result->size() == 1 ) {
				
            $row=$result->fetch();
            if ( $password = $this->generatePassword() ) {
						    //echo "pot here";
							 
							  //echo $password;
                $sql="UPDATE
                          ".USER_TABLE."
                      SET
                          ".USER_TABLE_PASSW."='".md5($password)."'
                      WHERE
                          ".USER_TABLE_ID."='".$row[USER_TABLE_ID]."'";
                $result=$this->db->query($sql);
                if (!$result->isError()) {
								
								     
                    //$row[USER_TABLE_PASSW]=$password;
                    //return $row;
										//echo $password;
										return $password;
					
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
    * Add a list of words to generate passwords with
    * @param array
    * @return void
    * @access public
    */
    function addWords ($words) {
        $this->words=$words;
    }

    /**
    * Generates a random but memorable password
    * @return string the password
    * @access private
    */
    function generatePassword () {
		
		   
			   
        srand((double)microtime()*1000000);
        $seperators=range(0,9);
        $seperators[]='_';
        $count=count($this->words);
        if ( $count == 0 )
            return false;
        $password=array();
        for ( $i=0;$i<4;$i++ ) {
            if ( $i % 2 == 0 ) {
                shuffle ($this->words);
                $password[$i]=trim($this->words[0]);
            } else {
                shuffle ( $seperators );
                $password[$i]=$seperators[0];
            }
        }
        shuffle($password);
				
        return implode ('',$password);
				
    }
		
		
		 function changeUsername(& $auth, $password, $newLogin) {
		 		
				// Instantiate the Session class
        $session=new Session();
		    $oldLogin=$session->get(USER_LOGIN_VAR);
				
        $newLogin=mysql_escape_string($newLogin);
				$password=mysql_escape_string($password);
				
				
				
				//MD5
				$password=md5($password);
				
        

        // Check the the current login (from session) and given password match
        $sql="SELECT
                  *
              FROM
                  ".USER_TABLE."
              WHERE
                  ".USER_TABLE_LOGIN."='".$oldLogin."'
              AND
                  ".USER_TABLE_PASSW."='".$password."'";
        $result=$this->db->query($sql);
        if ( $result->size() != 1 ){
				
				    $this->addError('password');			
            return false;
        }
				else{   
				
				      
							
				       //grab the record ID for the LOG
				       $results=$result->fetch();
							 //print_r($results);
							 $recordID=$results['user_id'];
							 //echo $recordID;
							 
							 
				 		 	 // check for dupes
				  	 	 if(! $this->existsLogin($newLogin) ){
							 
							 			// Update the login
										//echo $sql;
										
                    $sql  = "UPDATE " . USER_TABLE ;
			
										
										$sql .= " SET ".USER_TABLE_LOGIN."='".$newLogin."'" ;
										$sql .=	"	WHERE ".USER_TABLE_LOGIN."='".$session->get(USER_LOGIN_VAR)."'" ;
                    
									//	echo $sql;
										
										$result=$this->db->query($sql);
										
                    if ( !$result->isError() ) {
                            
														// Set the session variable for the password
                						$auth->storeAuth($newLogin,$password);
														
														//LOG the change
														$log=new ChangeLog($this->db);
														//Parameters: table, field, record_id, 
														$log->record('user','login',$recordID,$oldLogin, $newLogin);
														
                					
                            return true;
                    } else {
                        	  return false;
                    }
							 
							 }		
							 else{
							      $this->addError('username');					 
							 			return false;
							 }
					
					}


    }
		
		
		
		
		
		
		
		

    /**
    * Changes a password both in the database
    * and in the current session variable.
    * Assumes the new password has been
    * validated correctly elsewhere.
    * @param string old password
    * @param string new password
    * @return boolean TRUE on success
    * @access public
    */
    function changePassword(& $auth,$oldPassword,$newPassword) {
		
        $oldPassword=mysql_escape_string($oldPassword);
        $newPassword=mysql_escape_string($newPassword);
	
	//echo 'OLD p/word is ' .$oldPassword;
	
        // Instantiate the Session class
        $session=new Session();

        // Check the the login and old password match
        $sql="SELECT
                  *
              FROM
                  ".USER_TABLE."
              WHERE
                  ".USER_TABLE_LOGIN."='".$session->get(USER_LOGIN_VAR)."'
              AND
                  ".USER_TABLE_PASSW."='".md5($oldPassword)."'";
       
		  //echo $sql;
		  
		  $result=$this->db->query($sql);
        if ( $result->size() != 1 )
            return false;

        // Update the password
        $sql="UPDATE
                  ".USER_TABLE."
              SET
                  ".USER_TABLE_PASSW."='".md5($newPassword)."'
              WHERE
                  ".USER_TABLE_LOGIN."='".$session->get(USER_LOGIN_VAR)."'";
        $result=$this->db->query($sql);
        if ( !$result->isError() ) {
            // Set the session variable for the password
            
						
						$auth->storeAuth($session->get(USER_LOGIN_VAR),$newPassword);
						
						
						
            return true;
        } else {
            return false;
        }
    }
		
			function createUser($login,$password, $email, $first, $last,$sig        ) {
        
				  //create only if login and email don't already exisit
					//NOTE email may be blank!!!!!
					
					if ($this->existsLogin($login)){
						     $this->addError('Username');
								 return false;	 												 
					}
					
					
					
					if (   $email !=''    && $emailStatus=$this->existsEmail($email)){
					       $this->addError('Email');
								 return false;	 	
					
					}
					
					//Does it always get here????
					
					
		      //create the user
		
            $sql="INSERT INTO ".USER_TABLE." SET
                    ".USER_TABLE_LOGIN."='".$login."',
                    ".USER_TABLE_PASSW."='".md5($password)."',";
										
						if($email !=''){				
										
                    $sql .= USER_TABLE_EMAIL."='".$email."',";
						}				
										
										
            $sql .= USER_TABLE_FIRST."='".$first."',
                    ".USER_TABLE_LAST."='".$last."'";
            
						//echo $sql;
						//exit;
						
            $result=$this->db->query($sql);
            if ( $result->isError() ) {
               return FALSE;
           
				
				    }
						else{ 
	
  					    //User successfully created - get the ID
  							$sql='SELECT user_id FROM '.USER_TABLE.' WHERE '.USER_TABLE_LOGIN.'="'.$login.'"';
			
  							//echo $sql;
  							
                $result=$this->db->query($sql);
  							$row=$result->fetch();
  							
  							$this->newUserID=$row['user_id'];
			
						    return true;
		
						}						 
						
									  
      }
		  
			function checkLoginEmail($login, $email){
			
					if (!$this->existsLogin($login)){
						     $this->addError('Username not recognised');																	 
					}

									
					//if no errors so far, check for a matching email address
			    if(!($this->getErrors()) ){
							
								$sql='SELECT user_id FROM '.USER_TABLE.'  
								WHERE  '.USER_TABLE_LOGIN.' = "'. $login .'"
								AND  '.USER_TABLE_EMAIL.' = "'. $email .'"
								
								';
								
      					 //echo $sql;
      					 $result=$this->db->query($sql);
      				 		
								 //echo 	$result->size();
									
      					 if($result->size() == 1){
      							  return true;
      					 }
      					 else{							
								      $this->addError('This is not the email address we have on record for you');		 
      					      return false;
											
      					}
			     }
					 else{
					 			return false;
					 }			
					 
			
			}
			
		  
			function existsLogin($login){
			
			     $sql='SELECT user_id FROM '.USER_TABLE.'  WHERE  '.USER_TABLE_LOGIN.' = "'. $login .'";';
					 //echo $sql;
					 $result=$this->db->query($sql);
				 		
					 if($result->size() != 0){
							  return true;
						}
						else{
								 
								 return false;
						}
			}
			
			function existsEmail($email){
			
						 $sql='SELECT user_id FROM '.USER_TABLE.'  WHERE  '.USER_TABLE_EMAIL.' = "'. $email .'";';
				     //echo $sql;
						 $result=$this->db->query($sql);
				     
						 if($result->size() != 0){
				          return true;
				     }
						 		 
						 else{
						 
							 return false;
						}		
			}
			
			
			function addError($theError){
			    $this->errorArray[]=$theError; 
			
			}
			
			
			function getErrors(){
			    return $this->errorArray; 
			}
			
			function getNewUserID(){
							 return $this->newUserID;
			}
		
		  function getTotalUsers(){
			        
					$sql='SELECT user_id FROM '.USER_TABLE.';' ;
						
    			//echo $sql;
    			$result=$this->db->query($sql);
    				 		
					//echo 	$result->size();
							
    			return $result->size();
							
			}
			function getActiveUsers(){
			
			    $sql='SELECT user_id FROM '.USER_TABLE.'
					   WHERE active="1" ;' ;
						
    			//echo $sql;
    			$result=$this->db->query($sql);
    				 		
					//echo 	$result->size();
							
    			return $result->size();
			
			}
}
?>