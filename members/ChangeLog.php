<?php


// Name of CHANGE LOG table
@define ( 'CHANGE_LOG_TABLE','change_log');

// Name of table being chagned
@define ( 'CHANGE_LOG_TABLE_NAME','table_name');
// Name of FIELD being chagned
@define ( 'CHANGE_LOG_FIELD_NAME','field_name');
// ID of records being changed of table being chagned
@define ( 'CHANGE_LOG_RECORD_ID','record_id');
// Name of OLD column in CHANGE LOG table
@define ( 'CHANGE_LOG_OLD','old');
// Name of NEW column in CHANGE LOG table
@define ( 'CHANGE_LOG_NEW','new');
// Name of TIMESTAMP column in CHANGE LOG table
@define ( 'CHANGE_LOG_DATE','the_date');
// Name of USER column in CHANGE LOG table
@define ( 'CHANGE_LOG_USER','user');



/**
* AccountMaintenance Class<br />
* Provides functionality for users to manage their own accounts
* @access public
* @package SPLIB
*/
class ChangeLog {
    
		function ChangeLog (&$db) {
        $this->db=& $db;
				//$this->itemName=$itemName;
				
    }

    function record($table,$field, $id, $old,$new,$user=null) {
        
				
        $sql="INSERT INTO 
                  ".CHANGE_LOG_TABLE."
									(
									" .CHANGE_LOG_TABLE_NAME.",
									" .CHANGE_LOG_FIELD_NAME.",
									" .CHANGE_LOG_RECORD_ID.",
									" .CHANGE_LOG_OLD.",
									" .CHANGE_LOG_NEW.",
									" .CHANGE_LOG_USER.",
									" .CHANGE_LOG_DATE.")
									VALUES
									('".$table."',
									'".$field."',
									'".$id."',
                  '".$old."', 
									'".$new."',
									'".$user."',
									".time().")";
									
				//echo $sql;			
				//exit;		
       //$result=$this->db->query($sql);
        if( $result=$this->db->query($sql) ){
           return true;
				}	 
				else{
            return false;
				}
    }
		
		function getLastChange($table,$field, $id, $user=null){
		     
				  $sql="SELECT * FROM 
                  ".CHANGE_LOG_TABLE."
								WHERE
								" .CHANGE_LOG_TABLE_NAME." = '".$table."'
								AND
								" .CHANGE_LOG_FIELD_NAME." = '".$field."'
								AND
									" .CHANGE_LOG_RECORD_ID." = ".$id
								;
									
									
					
					
					
					
					if($user){
					    //Why is this here????
							$sql .= "	AND 	" .CHANGE_LOG_RECORD_ID." = ".$id;		
					}				
								
					$sql .= " ORDER BY " .CHANGE_LOG_DATE ;
					
					$sql .= " DESC LIMIT 1";
								 
					
					//echo $sql;				
					if( $result=$this->db->query($sql) ){
					
					    //echo $result->size();
					    
							$results=$result->fetch();
							return $results;
				  }	 
				  else{
             return false;
				  }
		}

    
}
?>