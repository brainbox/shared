<?php

$validation_expressions = array(
                     
        'not_empty' => array(
                'reg_exp' => '^.+$',
                'feedback' => 'Please enter a value'
        ),

        'only_letters' => array(
                'reg_exp' => '^[a-zA-Z]+$',
                'feedback' => 'Please enter a value containing only letters'
        ),
        
        'valid_email' => array( 
			'reg_exp' => '^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$', 
			'feedback' => 'Please enter a valid email address'
		),
		'numeric_only' => array(
			'feedback' => 'Please enter a numeric value only', 
			'reg_exp' => '^[0-9]*$'
		)
);

?>