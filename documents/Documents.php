<?php

Class Documents{

    function __construct($dbPDO){
        
        $this->dbPDO=$dbPDO;

        $sql="SELECT * from documents ORDER BY upload_date DESC";
        //echo $sql;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            //print_r($row);
            $this->items[]=$row;
        }
        //print_r($this->items);
        //exit;
        
    }
    
    /**
    * Factory class
    */
    public function fetch(){
    
	if(isset($this->items)){
    		
	    $row=each($this->items);
	    if ( $row ) {
	    
		return new Document($this->dbPDO,$row['value']);
	    
	    }
	    else {
		reset ( $this->items);
		return false;
	    }
	}
	else{
	    return false;
	}		 
    
    }
    
    /**
    * factory Class
    */
    public function getByID($id){
    
        foreach($this->items as $name=>$value){
	
	    if($value['id']==$id){
                    //echo 'asdfdasdasd';
		return new Document($this->dbPDO,$value);																			 
	    }
	
	}
	//only gets this far if article not found
	reset($this->items);   //Reset the pointer
	
	return false;
    
    }

     /**
    * Filename must be unique.
    *
    *
    */

    public function addDocument($fileData)
    {
        //echo '<hr>';
        //print_r($fileData);
        
        //Filename must NOT be in the database;
        $sql="SELECT count(*) FROM documents WHERE filename='" . $fileData['name'] ."'";
        //echo $sql;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        //print_r($result);
        //exit;
        //echo 'The result is ' . $result[0];
        //exit;
        if ($result[0]>0)
        {
            //echo 'File exisits!!!!!!!!';
            return false;
            die();
        }
        else{
            
            //process the document
            $sql="INSERT INTO documents (filename, mimetype, filesize, create_date, upload_date, user_id)
                VALUES (
                '" . $fileData['name'] ."',
                '" . $fileData['type'] ."',
                '" . $fileData['size'] ."',
                " . time() . ",
                " . time() . ",
                1)";
                
            //echo $sql;
            $stmt = $this->dbPDO->prepare($sql);
            $stmt->execute();
            
            //Need the insert ID
            $documentID=$this->dbPDO->lastInsertId();
            
            //Stick the file in the right place.
            require ('config/shackology_config.php');
            //print_r($config);
            //exit;
            $baseFilePath=$config['baseFilePath'] ;
            //echo $baseFilePath;
            
            move_uploaded_file($fileData['tmp_name'],$baseFilePath . 'documents/' . $documentID);
            
            return true;
        }
        
        


    }   

 

 

} 

 

/**
* Created by factory
*
*/


Class Document{


    protected $id;
    protected $filename; // Don't need the stored filename - that can always be "invented". Might be handy, though, if I use get/set.
    protected $create_date;
    protected $upload_date;
    protected $filesize;
    protected $mime;
    protected $userID;



    function __construct($dbPDO,$data)
    {
        //echo 'HERE@';
        $this->dbPDO=$dbPDO;
        $this->id=$data['id'];
        $this->filename=$data['filename'];
        $this->filesize=$data['filesize'];
        
        $this->mime=$data['mimetype'];
        
        $this->create_date=$data['create_date'];
        $this->upload_date=$data['upload_date'];

    }
    
    /**
    * Magic Method
    */
    public function __get($nm){
        
        if (isset($this->$nm)) {
            return ($this->$nm);
        } else {
            return false;
        }
    }
    
    /**
    * Magic Method
    */
    public function __set($nm, $val){

        if($this->$nm !=$val){
       
            $this->$nm = $val;
        }
       
  
    }
    
    /**
     * Tricy rule: the filename can be the same as current filename... but NOT the same
     * as another filename!!!
     */ 


    public function addVersion($fileData, $folder)
    {
        
        
        //Filename must NOT be in the database;
        $sql="SELECT id FROM documents WHERE filename='" . $fileData['name'] ."'";
        //echo $sql;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        
        //May be no result - that's fine. If it's the current doc, that's fine
        
        if (!$result || $result['id']==$this->id)
        {
       
            
            
            //???The follwojng needs to be sorted
            //exit;
            
            //process the document
            $sql="UPDATE documents SET
                    filename='" . $fileData['name'] ."',
                    mimetype='" . $fileData['type'] ."',
                    filesize='" . $fileData['size'] ."',
                    upload_date=  " . time() . ",
                    user_id=1
                    WHERE id=" . $this->id;
               
                
            //echo $sql;
            $stmt = $this->dbPDO->prepare($sql);
            $stmt->execute();
            
           
            //Stick the file in the right place.
            //require ('config/shackology_config.php');
            //print_r($config);
            //exit;
            //$baseFilePath=$config['baseFilePath'] ;
            //echo $baseFilePath;
            
            move_uploaded_file($fileData['tmp_name'],BASE_FILE_PATH . '/documents/' . $this->id);
            
            return true;
        }
        else
        {
            //echo '<p>Filename in use by another Document';
            return false;
        }
        
        


    }   
    
    function mimeTypeHuman(){
        
    }

    function fileSizeHuman(){
        
        $size=$this->filesize;
        $decimals=1; //decimal places
        
        $suffix = array('Bytes','KB','MB','GB','TB','PB','EB','ZB','YB','NB','DB');
        $i = 0;
     
        while ($size >= 1024 && ($i < count($suffix) - 1)){
          $size /= 1024;
          $i++;
        }
           
        return round($size, $decimals).'&nbsp;'.$suffix[$i];
	
    }
    
    public function sendFile(){
	
        //$path = 
        //$fname = $this->filename;
        
        require_once('files/SendFile.php');
	
	$filePath = BASE_FILE_PATH . '/documents/' . $this->id;
	
	//echo $filePath;
	//exit;
	
        $sendFile=new SendFile($filePath, $this->filename);//path, filename
        $sendFile->serveFile();
        
        /*
        exit;
        
	session_write_close();
	ob_end_clean();
	
        
        //if(!is_file($path) || connection_status() != 0){
        //    echo 'oops';
	//    return false;
        //}
	//to prevent long file from getting cut off
	set_time_limit(0);
 
	//$name = basename($fname);
	
	
	//THIS IS MY ADDITION (FROM HARRY@S VERSION)
	if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 5') or strpos($_SERVER['HTTP_USER_AGENT'], 'Opera 7')) {
            $mimeType = 'application/x-download';
        }
	else{
	    $mimeType = 'application/octet-stream';
	}
	
	//echo $this->filename;
        //echo $this->mime;
        //exit;
	
	//echo '<p>the name is '.$name;
        //exit;
	header("Cache-Control: ");
	header("Pragma: ");
	header("Content-Type: " . $mimeType ); /// NOTE: This is NOT the mimetype ofd the FILE
	header("Content-Length: " . $this->fileSize );
	header('Content-Disposition: attachment; filename="' . $this->filename . '"');
	header("Content-Transfer-Encoding: binary\n");

        $path=BASE_FILE_PATH . '/documents/' . $this->id;
        
	if($file = fopen($path, 'rb'))
	{
            while((!feof($file)) && (connection_status()==0))
            {
                print(fread($file, 1024*8));
                flush();
            }
            fclose($file);
	}

	return connection_status() == 0 && !connection_aborted();
	
        */
    }


}
?>