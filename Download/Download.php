<?php

/**
 *
 *
 *
 *
 *
 */

class Download
{
    public   $extension ;
      
    public   $icon;
            
    public   $iconAlt;   
    
    private $mimeInfo=array(
                        
                        
                'applicaton/pdf'=>    array(
                                            'extension'=>'.pdf',
                                            'image'=>'pdficon.gif',
                                            'alt'  =>'PDF download file'),				
                            
                'application/pdf'=>    array(
                                             
                                              'extension'=>'.pdf',
                                              'image'=>'pdficon.gif',
                                            'alt'  =>'PDF file'),																	 											 
                                    
                'application/msword'=> array(
                                                  'extension'=>'.doc',
                                                'image'=>'wordicon.gif',
                                                'alt'  =>'Word file'),
                
                
                
                
                 'application/vnd.openxmlformats-officedocument.wordprocessingml.document'=> array(
                                                  'extension'=>'.docx',
                                                'image'=>'wordicon.gif',
                                                'alt'  =>'Word file'),
                
                        
                        
                        
                'application/vnd.ms-powerpoint'=> array(
                                  'extension'=>'.ppt',
                                            'image'=>'ppicon.gif',
                                            'alt'  =>'PowerPoint file'),
        
                'text/xml'=> 	array(
                                          'extension'=>'.xml',
                                            'image' =>'xmlicon.gif',
                                            'alt'   =>'XML  file'),	
                        
                'application/vnd.ms-excel'=> array(
                                            'extension'=>'.csv',
                                            'image' =>'csvicon.gif',
                                            'alt'   =>'CSV  file'),
                
          
            'application/postscript'=> array(
                                    'extension'=>'.eps',
                                    'image' =>'epsicon.gif',
                                    'alt'   =>'EPS file'),
            
                 
            'image/pjpeg'=> array(
                                'extension'=>'.jpg',
                                'image' =>'jpgicon.gif',
                                'alt'   =>'JPEG file'),
            
            'image/tiff'=> array(
                                'extension'=>'.jpg',
                                'image' =>'tifficon.gif',
                                'alt'   =>'TIFF file')
            );



    function __construct()
    {
        
        
    }


    function populate($data)
    {
        //echo '<pre>';
        //var_dump($data);
        
        
        
        $this->id = $data['id'];
        $this->filename = $data['filename'];
        $this->description = $data['description'];
        $this->mimetype = $data['mimetype'];
        $this->filesize = $data['filesize'];
        $this->uploadDateUNIX = $data['upload_date'];
        
        //echo '<Pre>';
        //var_dump($this);
        //exit;
        
        $this->applyBusinessRules();
        
    }

    function applyBusinessRules()
    {
        
        $this->fileSizeText = $this->human_size($this->filesize);
        
        if( isset($this->mimeInfo[$this->mimetype]) ){
            
            $this->extension =  $this->mimeInfo[$this->mimetype]['extension'];
      
            $this->icon =       $this->mimeInfo[$this->mimetype]['image'];
            
            $this->iconAlt =    $this->mimeInfo[$this->mimetype]['alt'];
                
        }
        
        
        
        
    }


    /**
     * Static call
     *
     *
     */

    function getByArticleId($dbPDO, $articleId, $section=null )
    {
               
        require_once 'Model/Download.php';
                
        $dataArray = Model_Download::getByArticleId($dbPDO, $articleId, $section );
        
        if(empty($dataArray)){
            return false;
        }
            
        $classArray = array();    
            
        foreach($dataArray as $data)
        {
            
            $class=  new Download();
            
            $class->populate($data);
            
            $classArray[] = $class;
            
            
            
            
        }
        
        return $classArray;
        
        
    }
    
    
    
function addDownloads($downloadLocation='download'){
		

            
				 if($this->theItem->areDownloads()){
				      
				    $pdfPresent=false;//used to test for the existence of a .pdf
		    
            require_once('HTML/Table.php'); 
            //Downloads
            $table =new HTML_Table(array( 'summary'=>    'Downloads',
            'class'=>			 'downloads'				));
            $classArray=array('class=col1','class=col2','class=col3');
            
            
            //get access to the Download Class
            while ( $theDownload=$this->theItem->getDownload() ) {
						
//Set up infor required to serve the correct icon
//NOTE: NO NEED TO HANDLE THE DIFF PDF TYPES HERE

$mimeInfo=array('applicaton/pdf'=>    array('image'=>'pdficon.gif',
'alt'  =>'PDF download file'),				
                    
                'application/pdf'=>    array('image'=>'pdficon.gif',
'alt'  =>'PDF file'),																	 											 
                         
                         
                         
            
                         
                                                                                                                                                                                                                                                 
                'application/msword'=> array('image'=>'wordicon.gif',
'alt'  =>'Word file'),
                
                             
                                                                                                                                                                                                                                                        
                'application/vnd.openxmlformats-officedocument.wordprocessingml.document'=> array('image'=>'wordicon.gif',
'alt'  =>'Word file'), 	  
                
                'application/vnd.ms-powerpoint'=> array('image'=>'ppicon.gif',
'alt'  =>'PowerPoint file'),

                 'text/xml'=> 							array(
                                                'image' =>'xmlicon.gif',
'alt'   =>'XML  file'),	
                
                'application/vnd.ms-excel'=> array(
                                                                                                                                        'image' =>'csvicon.gif',
'alt'   =>'CSV  file'),													 
                                                                                                                                 
          
            'application/postscript'=> array(
                'suffix'=>'.eps',
                'image' =>'epsicon.gif',
                'alt'   =>'EPS file'),
            
                 
            'image/pjpeg'=> array(
                'suffix'=>'.jpg',
                'image' =>'jpgicon.gif',
                'alt'   =>'JPEG file'),
            
            'image/tiff'=> array(
                'suffix'=>'.jpg',
                'image' =>'tifficon.gif',
                'alt'   =>'TIFF file') 		
                                                                                                        
                                                                                                               
                                                                                                               );
                                                                                                               
                                                                                                               
    //Keep track of .pdfs
    
    if(  ($theDownload->mimetype=='applicaton/pdf')  ||     ($theDownload->mimetype=='application/pdf')  ){
        $pdfPresent=true;
                    
    }							
															
																
/*						
print_r($mimeInfo);			
echo "the download is" . $theDownload->mimetype;
print_r($mimeInfo[$theDownload->mimetype]);		    
echo $mimeInfo[$theDownload->mimetype]['image'];
*/

//Note - need to remove any "&" characters from the thing
/*
$table->addRow( 
array('<a href="'.$this->baseURL.'download/'.$theDownload->getID().'"><img src="'.$this->baseURL.'images/'.$mimeInfo[$theDownload->mimetype]['image'].'" height="32" width="32" alt="'.$mimeInfo[$theDownload->mimetype]['alt'].'" /></a>',
str_replace('&','&amp;', $theDownload->description() ),
                '<a href="'.$this->baseURL.'download/'.$theDownload->getID().'">Download ('.$theDownload->mimetypelabel().' '.$theDownload->filesizelabel().')</a>'
                ),$classArray
       );
*/

$imageString='';
if(isset($mimeInfo[$theDownload->mimetype])){
    
    $imageString = '<img src="'.$this->baseURL.'images/'.$mimeInfo[$theDownload->mimetype]['image'].'" height="32" width="32" alt="'.$mimeInfo[$theDownload->mimetype]['alt'].'" />';
    
}

														
$table->addRow( 
array('<a href="'.$this->baseURL.$downloadLocation . '/'.$theDownload->getID().'">' . $imageString . '</a>',
'<a href="'.$this->baseURL.$downloadLocation . '/'.$theDownload->getID().'">' . str_replace('&','&amp;', $theDownload->description() ) . '&nbsp;&nbsp;(' .$theDownload->filesizelabel().')</a>',
                                                                        ''
),$classArray
);							
               

	    }
						
						$this->itemHTML .=  $table->toHTML();
						
			//add in the .pdf instruction if required
						if($pdfPresent==true){
						
						     $PDFtable =new HTML_Table(array( 'summary'=>    'Instructions for viewing PDFs',
																		'class'=>			 'pdf'				));
					 $classArray=array('class=col1','class=col2','class=col3');
						     $pdfInstruction='
	 
							   Acrobat Reader is a helper application manufactured by Adobe Systems Incorporated that
								 lets you view and print Portable Document Format (PDF) files. Click the button to download 
								 Acrobat Reader from the Adobe website.
														 
								       ';
											 
																			 
											 
						
							   $PDFtable->addRow( 
						   array('<img src="'.$this->baseURL.'images/'.$mimeInfo['applicaton/pdf']['image'].'" height="32" width="32" alt="'.$mimeInfo['applicaton/pdf']['alt'].'" />',
													 $pdfInstruction, 
													 '<a href="http://www.adobe.com/products/acrobat/readstep.html">
													    <img height="31" width="88" src="'.$this->baseURL.'images/getacro.gif" 
									      alt="Get Adobe Reader" /></a>'),
													 $classArray
												);
						     $this->itemHTML .=  $PDFtable->toHTML();
						}
				
				}
		
		}
                
                
    function human_size($size, $decimals = 1) {
        $suffix = array('Bytes','KB','MB','GB','TB','PB','EB','ZB','YB','NB','DB');
        $i = 0;
     
        while ($size >= 1024 && ($i < count($suffix) - 1)){
          $size /= 1024;
          $i++;
        }
     
        return round($size, $decimals).' '.$suffix[$i];
    }
    
    
    /**
     *
     *
     * Request is of the form:
     *
     *    /download/<section>/<filename>
     *
     * File stored as follows:
     *
     *   /download/<section>/<filename>
     *
     *
     * For both, the <section> is optional (if you know what I mean)
     *
     *
     */
    
    public static function serve( $pathVars ){
        
        //echo count($pathVars->pathArray);
        
        if(count($pathVars->pathArray)==2){
            
            $section = null;
            $filename = $pathVars->fetchByIndex(1);
            
        } else {
            
            $section = $pathVars->fetchByIndex(1);
            $filename = $pathVars->fetchByIndex(2);
             
        }
        
        //Going to need the clean filename
        $filename = urldecode($filename);
        
       
        
        $fullPath = BASE_FILE_PATH . '/download';
        
        if($section){
            $fullPath .= '/' . $section;
        }
        
        $fullPath .= '/'. $filename;
        
        
        if(file_exists($fullPath)){
            
            require_once('files/SendFile.php');
            $sendFile=new SendFile("$fullPath", "$filename"); 
            $sendFile->serveFile();
            die;
            
        } else {
            
            //handle 404 and redirect
            header("HTTP/1.0 404 Not Found");
            die;
            
        }
        
        
    }
    
    
}