<?php

/**
 *    The filename is unique to the ARTICLE
 *
 *    It is stored (filesystem) like this:
 *
 *       <insertID>-<filename>
 *
 *   It is presented (website) like this:
 *
 *       <filename>
 *
 *
 *
 *
 *
 *    @todo - the above WILL NEED SOME FILESYSTEM CHANGES
 *
 *    @todo - this should be promoited back to SHARED
 *
 */




class Model_Download

{
    
    
    
    
    public static function getByArticleId($dbPDO, $articleId, $section='' )
    {
        
        $sql = "SELECT *
                FROM downloads
                WHERE
                    1=1
                    AND article_id = $articleId ";
                    
        if($section)
        {
           $sql .= " AND section = '$section' ";
        }
        
        //echo $sql;
        
        $stmnt = $dbPDO->prepare($sql);
        
        $stmnt->execute();
        
        return $stmnt->fetchAll();
        
    }
    
    
    /** 
     *  2012-01 - Changing, so that overwriting is possible!!
     *
     *
     */
    
    function addFromPOST($dbPDO, $pathToUserFiles ){
        
        if(empty($_FILES['file']['name']) ){
            
            return false;
        }
        
        //process the document
        $sql="REPLACE INTO downloads (
                        
                            article_id
                        ,   filename
                        ,   description
                        ,   section
                        ,   mimetype
                        ,   filesize
                        ,   create_date
                )
            VALUES (
                            :articleId
                        ,   :filename
                        ,   :filename
                        ,   :section
                        ,   :type
                        ,   :size
                        ,   NOW()
                   
                )
                ";
                
            //echo $sql;
            
            //exit;
            $stmt = $this->dbPDO->prepare($sql);
            $stmt->bindParam(':articleId' , $_POST['articleId'] );
            $stmt->bindParam(':section' , $_POST['section'] );
            
            $stmt->bindParam(':filename' ,$_FILES['file']['name'] );
            $stmt->bindParam(':type' , $_FILES['file']['type'] );
            $stmt->bindParam(':size' , $_FILES['file']['size'] );
            
            $stmt->execute();
            
            //Need the insert ID
            $insertId=$this->dbPDO->lastInsertId();
            
            
            $fullPath = $pathToUserFiles . '/download'  ;         
            
           
            
            
            if( !empty($_POST['section']) ){
                
                $fullPath .= '/' . $_POST['section'];
                
            }
            
            
            $fullPath .= '/' . $_FILES['file']['name'] ;
            
            
            
            
            move_uploaded_file($_FILES['file']['tmp_name'], $fullPath );
            
            
                    
            return true;
    }
        
     
    /**
     *
     *  @todo - Is this used?
     *
     */
    
    function delete($id, $location )
    {
        
      
        //Filename must NOT be in the database;
        $sql="SELECT filename FROM
                downloads
                WHERE id= $id  ";
       
        //echo $sql;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetch();
        
        //Remove from db
         //Filename must NOT be in the database;
        $sql="DELETE FROM
                downloads
                WHERE id = $id  ";
       
        //echo $sql;
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->execute();
     
        
        
        
        //Remove from filesystem
        unlink($location . '/' .  $result['filename']);
        
    }
    
    function deleteFromPOST($dbPDO, $pathToUserFiles )
    {        
        
        //Remove from db
         //Filename must NOT be in the database;
        $sql="DELETE FROM
                downloads
                WHERE id = :id  ";
       
        $stmt = $this->dbPDO->prepare($sql);
        $stmt->bindParam(':id', $_POST['id'] );
        $stmt->execute();
     
        
        $fullPath = $pathToUserFiles . '/download';
        
        if(isset($_POST['section'])){
            
            $fullPath .= '/' . $_POST['section'];
            
        }
        
        $fullPath .= '/' . $_POST['filename'];
        
       
        //Remove from filesystem
        unlink( $fullPath);
        
    }
    
    
}