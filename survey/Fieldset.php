<?php
/**
 * Think block is anything that can live in a fieldset?
 *
 *
 */



Class FieldSet{
    
    public $legend;
    public $questionArray=array();
    public $intro;
    
    function __construct(){
        
        
        
    }
    
    function addQuestion($questionId,$type='', $label='')
    {
        $className= 'Question' . $type;
        $question = new $className($questionId,$label);
        $this->questionArray[] = $question;
        return $question;
        
        
    }
    
    
    
    
    public function render()
    {
        echo '<fieldset>';
        
       
        
        if($this->legend){
            
            echo '<legend>' . $this->legend . '</legend>';
        }
         echo $this->intro;
        
        //print_r($this->questionArray);
        //exit;
        echo '<ul>';
        foreach($this->questionArray as $question){
            echo '<li>';
            $question->render();
            echo '</li>';
            
        }
        echo '</ul>';
        echo '</fieldset>';
       
        
    }
    
    
}