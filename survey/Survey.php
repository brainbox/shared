<?php
Class Survey{
    
    /**
     * The data is provided by the model. Contains the question and type. Not really sure about the questions
     *
     * End result is an array of question objects
     *
     * Survey has one or more sections
     * A section has lots of fieldsets
     * Blocks have questions (at least one)
     * Questions may have answers
     *
     * 
     */
    
    public $sectionArray = array(); //Array of section Objects
    public $questionId = 0;
    public $className;
    
    
    public function __construct()
    {
       // echo 'heeeer';
       // die('kkkk');
        
    }
    
    function getNextId()
    {
        
        $this->questionId++;
        return $this->questionId;
        
    }
    
    function addSection()
    {
        $section = new Section();
        $this->sectionArray[] = $section;
        
        return $section;
        
    }
    
    
    
    
    
    function render()
    {
        $classString='';
        if($this->className){
            $classString= ' class="' . $this->className . '"';
        }
        
        echo '<form action="" method="post" ' . $classString . ' >';
        foreach($this->sectionArray as $section){
            
            $section->render();
            
        }
        
        echo '
            <fieldset class="submit">
            <ul>
            <li>
            <input type="submit" value = "Submit your responses" />
            </li>
            </ul>
            </fieldset>
            
            
            </form>';
            
       
       
        
    }
    
    
   
    
    
    function getResults($surveyId, $dbPDO)
    {
        
        //get respondents
        //get their answers
        
        require_once 'Respondent.php';
        
        $respondentArray = Respondent::getBySurveyId($surveyId, $dbPDO);    
        
        return $respondentArray; 
    }
    
    
   
    
    
    
}