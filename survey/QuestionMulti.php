<?php



Class QuestionMulti extends Question{
    
    
    function __construct($questionId, $label){
        
    	$this->questionId = $questionId;
	$this->label = $label;
        
    }
    
    function render(){
		
		
		$theString='
		
		<label for="' . $this->questionId. '" >' . $this->label . '</label>
		<table class="questions" id="block_' . $this->questionId . '">
		<tr>
		
		<th class="radio">Strongly disagree</th>
		<th class="radio">Disagree</th>
		<th class="radio">Neutral</th>
		<th class="radio">Agree</th>
		<th class="radio">Strongly agree</th>
		</tr>
		<tr>
		
		<td class="radio"><input type="radio" value="1" name="' . $this->questionId . '" /></td>
		<td class="radio"><input type="radio" value="2" name="' . $this->questionId . '" /></td>
		<td class="radio"><input type="radio" value="3" name="' . $this->questionId. '" id="' . $this->questionId . '" /></td>
		<td class="radio"><input type="radio" value="4" name="' . $this->questionId . '" /></td>
		<td class="radio"><input type="radio" value="5" name="' . $this->questionId . '" /></td>
		</tr>
		</table>
		';
		
		
		
		echo $theString;
	}
}

