<?php

require_once('survey/Question.php');


Class QuestionText extends Question{
    
     function __construct($questionId, $label){
        
    	$this->questionId = $questionId;
	$this->label = $label;
        
    }
    
    function render(){
		
		
			
		$theString='
		
		<label for="' . $this->questionId . '">' . $this->label . '</label>
		<input name="' . $this->questionId . '" id="' . $this->questionId . '" />';
		
		
		
		echo $theString;
	}
    
    
}