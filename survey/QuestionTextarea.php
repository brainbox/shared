<?php

require_once('survey/Question.php');

Class QuestionTextarea extends Question{
    
    
     
    function __construct($questionId, $label){
        
    	$this->questionId = $questionId;
        $this->label = $label;
    }
    
    function render(){
		
		
		$theString='
		
		<label for="' . $this->questionId . '">' . $this->label . '</label>
		<textarea name="' . $this->questionId . '" id="' . $this->questionId . '" cols="50" rows="5"></textarea>';
		
		
		
		echo $theString;
	}
    
    
}