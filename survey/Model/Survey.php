<?php



class Model_Survey
{
    
    function __construct($surveyID,$dbPDO)
    {
        $this->surveyID = $surveyID;
        $this->dbPDO = $dbPDO;
        
        
    }
    
   
    
    /**
     * The $_POST supplies a set of name/value pairs that are the questionID, response
     *
     *
     */
    
    public function save()
    {
        
        #Get an ID for the respondant
        $sql = "INSERT INTO sur2_respondent (the_date)
                VALUES ( NOW() );";
                
         $stmnt = $this->dbPDO->prepare($sql);
         $stmnt->execute();
        // print_r($stmnt->errorInfo());
        $respondentId = $this->dbPDO->lastInsertId();
        
        //exit;
        
    
        
        
        
        $sql = "INSERT INTO sur2_response
                    (
                    survey_id
                    ,   respondent_id
                    ,   question_id
                    ,   rating
                    ,   comment
                    )
                VALUES
                    (
                    :survey_id
                    ,   :respondent_id
                    ,   :question_id
                    ,   :rating
                    ,   :comment
                    );";
          
          //echo $sql;
                    
        $stmnt = $this->dbPDO->prepare($sql);         
                    
        foreach($_POST as $k=>$value){
            
            $rating  =null;
            $comment = null;
            //The value is either a rating or a comment
            
            if(is_numeric($value)){
                
                $rating = $value;
            } else {
                
                $comment = $value;
            }
            
            
            $stmnt->bindParam(':survey_id',         $this->surveyID);
             $stmnt->bindParam(':respondent_id',    $respondentId);
            $stmnt->bindParam(':question_id',       $k);
            $stmnt->bindParam(':rating',            $rating);
             $stmnt->bindParam(':comment',          $comment);
            
            $stmnt->execute();
            
            
        }
        
      
     
        
        
    }
    
    /**
     * 
     *
     *
     *
     */
    
    public function getQuestions()
    {
        
        
         $sql = "SELECT DISTINCT
                    id
                    ,   description
                FROM
                    sur2_question
              
                WHERE
                    survey_id = " .  $this->surveyID ."
                    
                      ORDER BY
                    sort_order"
                    ;
        //echo $sql;
        
        $stmnt = $this->dbPDO->prepare($sql);
        $stmnt->execute();
        
        return $stmnt->fetchAll();
        
        
    }
    
    
       /**
     * 
     *
     *
     *
     */
    
    public function getRespondents()
    {
        
        
         $sql = "SELECT DISTINCT
                    id
                    ,   the_date
                FROM
                    sur2_respondent
              
                WHERE
                    survey_id = " .  $this->surveyID ."
                    
                      ORDER BY
                        the_date ASC"
                    ;
       
        //echo $sql;
        
        $stmnt = $this->dbPDO->prepare($sql);
        $stmnt->execute();
        
        return $stmnt->fetchAll();
        
        
    }
    
    
    public function getResponses($questionID)
    {
        
        $sql = "SELECT
                    rating
                    ,   comment
                FROM
                    sur2_response
                WHERE
                    question_id = '$questionID'
                    ";
        
        ##echo '<p>' . $sql;
        //exit;
        
        $stmnt = $this->dbPDO->prepare($sql);
        $stmnt->execute();
        
        return $stmnt->fetchAll();
        
        
    }
    
     public function getResponse($questionID, $respondentID)
    {
        
        $sql = "SELECT
                    rating
                    ,   comment
                FROM
                    sur2_response
                WHERE
                    question_id = '$questionID'
                    AND respondent_id = $respondentID
                    ";
        
        //echo '<p>' . $sql;
        //exit;
        
        $stmnt = $this->dbPDO->prepare($sql);
        $stmnt->execute();
        
        return $stmnt->fetch();
        
        
    }
    
    
    
    
}