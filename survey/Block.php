<?php
/**
 * Think block is anything that can live in a fieldset?
 *
 *
 */



Class FieldSet{
    
    public $legend;
    protected $questionArray=array();
    
    function __construct(){
        
        
        
    }
    
    function addQuestion($question)
    {
       
        $this->questionArray[] = $question;
        
        
        
    }
    
    
    
    public function render()
    {
        echo '<fieldset>';
        
        if($this->legend){
            
            echo '<legend>' . $this->title . '<legend>';
        }
        
        
        foreach($this->questionArray as $question){
            
            $question->render();
            
        }
        echo '</fieldset>';
       
        
    }
    
    
}