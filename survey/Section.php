<?php

Class Section{
    
    public $title;
    public $intro;
    protected $blockArray=array();
    
    function __construct(){
        
        
        
    }
    
    function addFieldset()
    {
        $fieldset = new fieldset();
        $this->fieldsetArray[] = $fieldset;
        
        return $fieldset;
        
    }
    
    
    
    public function render()
    {
        
        echo '<h2>' . $this->title . '</h2>';
        echo $this->intro;
        
        foreach($this->fieldsetArray as $fieldset){
            
            $fieldset->render();
            
        }
       
    }
    
    
}