<?php
/**
* @package SPLIB
* @version $Id: RssGenerator.php,v 1.4 2003/08/01 20:08:37 harry Exp $
*/
/**
* RssGenerator Class
* Generates RSS 1.0 documents according to http://purl.org/rss/1.0/
* @access public
* @package SPLIB
*/

/**
 * Updated Jan 08 to PHP5 version / add utf8 encode for descriptions
 *
 */



class RssGenerator {
    /**
    * Stores the document as a DOM Document instance
    * @access private
    * @var resource
    */
    var $dom;

    /**
    * Stores the root element of the rss feed
    * as a DOM Element object
    * @access private
    * @var resource
    */
    var $rss;

    /**
    * Stores the channel element
    * as a DOM Element object
    * @access private
    * @var resource
    */
    var $channel;

    /**
    * Stores the item sequence for the channel
    * as a DOM Element object
    * @access private
    * @var resource
    */
    var $itemSequence;

    /**
    * Stores the image element
    * as a DOM Element object
    * @access private
    * @var resource
    */
    var $image;

    /**
    * An array of items as DOM Element instances
    * @access private
    * @var resource
    */
    var $items;

    /**
    * Stores the textinput element
    * as a DOM Element object
    * @access private
    * @var resource
    */
    var $textinput;

    /**
    * RssGenerator constructor
    * @access public
    */
    function __construct() {
        $this->dom=new DOMDocument('1.0');
        $this->_initialize();
    }

    /**
    * Creates the root rdf element
    * Sets up the channel and item sequence
    * @return void
    * @access private
    */
    private function _initialize() {
        
        $rss=$this->dom->createElement('rdf:RDF');

        // Namespace hack
        $rss->setAttribute('xmlns:rdf',
            'http://www.w3.org/1999/02/22-rdf-syntax-ns#');
        $rss->setAttribute('xmlns',
            'http://purl.org/rss/1.0/');
        $this->rss=$this->dom->appendChild($rss);
        $this->channel=$this->dom->createElement('channel');
        $this->itemSequence=$this->dom->createElement('rdf:Seq');
    }

    /**
    * Adds the url of the image to the channel element
    * @param string url of image
    * @return void
    * @access private
    */
    function addChannelImage($url) {
        $image=$this->dom->createElement('image');
        $image->setAttribute('rdf:resource',$url);
        $this->channel->appendChild($image);
    }

    /**
    * Adds a url of an item to the items sequence in the channel element
    * @param string url of an item
    * @return void
    * @access private
    */
    function addChannelItem ($url) {
        $li=$this->dom->createElement('rdf:li');
        $li->setAttribute('resource',$url);
        $this->itemSequence->appendChild($li);
    }

    /**
    * Adds a url of an item to the items sequence in the channel element
    * @param string url of an item
    * @return void
    * @access private
    */
    function addChannelTextInput($url) {
        $textInput=$this->dom->createElement('textinput');
        $textInput->setAttribute('rdf:resource',$url);
        $this->channel->appendChild($textInput);
    }

    /**
    * Makes the final appends to the rss document
    * @return void
    * @access private
    */
    function finalize () {
        $channelItems=$this->dom->createElement('items');
        $channelItems->appendChild($this->itemSequence);
        $this->channel->appendChild($channelItems);
        $this->rss->appendChild($this->channel);

        if ( isset ( $this->image ) )
            $this->rss->appendChild($this->image);

        if ( is_array ( $this->items ) ) {
            foreach ($this->items as $item) {
                $this->rss->appendChild($item);
            }
        }

        if ( isset ( $this->textinput ) )
            $this->rss->appendChild($this->textinput);
    }

    /**
    * Add the basic channel information
    * @param string title of the channel e.g. "Sitepoint"
    * @param string mail URL of channel e.g. "http://www.sitepoint.com"
    * @param string description of channel
    * @param string about URL e.g. "http://www.sitepoint.com/about/"
    * @return void
    * @access public
    */
    function addChannel ($title,$link,$desc,$aboutUrl) {
        $this->channel->setAttribute('rdf:about',$aboutUrl);
        $titleNode=$this->dom->createElement('title');
        $titleNodeText=$this->dom->createTextNode($title);
        $titleNode->appendChild($titleNodeText);
        $this->channel->appendChild($titleNode);
        $linkNode=$this->dom->createElement('link');
        $linkNodeText=$this->dom->createTextNode($link);
        $linkNode->appendChild($linkNodeText);
        $this->channel->appendChild($linkNode);
        $descNode=$this->dom->createElement('description');
        $descNodeText=$this->dom->createTextNode(utf8_encode($desc) ); //Added Jan 
        $descNode->appendChild($descNodeText);
        $this->channel->appendChild($descNode);
    }

    /**
    * Adds the channel logo description to the feed
    * @param string src: "http://www.sitepoint.com/images/sitepoint-logo.gif"
    * @param string alternative text to display for image
    * @param string link for image e.g. http://www.sitepoint.com
    * @return void
    * @access public
    */
    function addImage ($src,$alt,$link) {
        $this->addChannelImage($src);
        $this->image=$this->dom->createElement('image');
        $this->image->setAttribute('rdf:about',$src);
        $titleNode=$this->dom->createElement('title');
        $titleNodeText=$this->dom->createTextNode($alt);
        $titleNode->appendChild($titleNodeText);
        $this->image->appendChild($titleNode);
        $urlNode=$this->dom->createElement('url');
        $urlNodeText=$this->dom->createTextNode($src);
        $urlNode->appendChild($urlNodeText);
        $this->image->appendChild($urlNode);
        $linkNode=$this->dom->createElement('link');
        $linkNodeText=$this->dom->createTextNode($link);
        $linkNode->appendChild($linkNodeText);
        $this->image->appendChild($linkNode);
    }

    /**
    * Adds the description of the site search URL
    * @param string title of search e.g. "Search"
    * @param string descrption of search e.g. "Search Sitepoint..."
    * @param string search url: "http://www.sitepoint.com/search/search.php"
    * @param string GET variable for search e.g. "q" for "?q="
    * @return void
    * @access public
    */
    function addSearch ($title,$desc,$url,$var) {
        $this->addChannelTextInput($url);
        $this->textinput=$this->dom->createElement('textinput');
        $this->textinput->setAttribute('rdf:about',$url);
        $titleNode=$this->dom->createElement('title');
        $titleNodeText=$this->dom->createTextNode($title);
        $titleNode->appendChild($titleNodeText);
        $this->textinput->appendChild($titleNode);
        $descNode=$this->dom->createElement('description');
        $descNodeText=$this->dom->createTextNode(utf8_encode($desc) );//Added Jan 
        $descNode->appendChild($descNodeText);
        $this->textinput->appendChild($descNode);
        $nameNode=$this->dom->createElement('name');
        $nameNodeText=$this->dom->createTextNode($var);
        $nameNode->appendChild($nameNodeText);
        $this->textinput->appendChild($nameNode);
        $linkNode=$this->dom->createElement('link');
        $linkNodeText=$this->dom->createTextNode($url);
        $linkNode->appendChild($linkNodeText);
        $this->textinput->appendChild($linkNode);
    }

    /**
    * Adds an RSS item to the document
    * @param string title of item
    * @param string link for item
    * @param string description of item
    * @return void
    * @access public
    */
    function addItem ($title,$link,$desc) {
        $this->addChannelItem($link);
        $itemNode=$this->dom->createElement('item');
        $itemNode->setAttribute('rdf:about',$link);
        $titleNode=$this->dom->createElement('title');
        $titleNodeText=$this->dom->createTextNode($title);
        $titleNode->appendChild($titleNodeText);
        $itemNode->appendChild($titleNode);
        $linkNode=$this->dom->createElement('link');
        $linkNodeText=$this->dom->createTextNode($link);
        $linkNode->appendChild($linkNodeText);
        $itemNode->appendChild($linkNode);
        $descNode=$this->dom->createElement('description');
        $descNodeText=$this->dom->createTextNode(utf8_encode($desc) );//Added Jan 
        $descNode->appendChild($descNodeText);
        $itemNode->appendChild($descNode);
        $this->items[]=$itemNode;
    }

    /**
    * Returns the RSS document as a string
    * @return string XML document
    * @access public
    */
    function toString () {
        $this->finalize();
        return $this->dom->saveXML();
    }
    
    function toFile($filePath){
		     
        $this->finalize();
        
        $this->dom->save($filePath);
        
        //$fp=fopen($filePath,'w');
        //fwrite($fp,$this->dom->save(true) );
        //fclose($fp);		 
		}
}
?>