<?php

/**
 * 27 Apr - Slight change of emphasis: going to make this class more "front-end" oriented: less "raw" database
 * values, more business rules. A consequence is that the Admin site will now talk drectly to the Model
 *
 */


require_once('articles/Item.php');

Class News  extends Item {
    
    public $id;
    public $title;
    public $teaser; 
    
    public $description;
    
    public $onHold;
    
    private $subtitle; #Delimited string... Dangerous!!!
    public $path;
    public $pathOld;            # Original path to the news item on the old website
    public $published;
    public $depublish;        # Now a mysql datetime value
    #public $theDateUNIX;       # UNIX equivalent
    public $place;
    public $body;
                                  
    public $image;
    public $alt;
    public $height;
    public $width;
    public $lastUpdate;         # Now a mysql datetime value
    #public $lastUpdateUNIX;    # UNIX equivalent
    public $intro;              # Derived from body. Not updated
    public $bodyWithoutIntro;   # Derived from body. Not updated
  
    public $notes;
     
     
    //Derived 
    public $metaTitle;
   
    public $metaDescription;
    public $subtitleArray;
    
    public $isActive;
    
    
    public $metaTitleSuffix;
    
    

    function __construct(){
        
        
      
    }
    
    
    
    
    function getTitle($minimumCharacters = false){
        
        $string=$this->title;
        
        //Truncate if required
        if($minimumCharacters){
            $string = $this->truncate($string, $minimumCharacters);
        }
        
        return $string;
        
    }
    
    /**
     * Populates the class with values from the Model
     *
     */
    

    public function populate($data){
        
        //echo '------------------------------ppp<pre>';
        //print_r($data);
        //exit;
        
        
        if(isset($data['id']))
            $this->id = $data['id'];
        
        if(isset($data['title']))
            $this->title = $data['title'];
            
        if(isset($data['subtitle']))
            $this->subtitle = $data['subtitle'];
            
            
        if(isset($data['teaser']))
            $this->teaser = $data['teaser'];
            
        
        if(isset($data['meta_title']))
            $this->metaTitle = $data['meta_title'];
        
        if(isset($data['meta_description']))
            $this->metaDescription = $data['meta_description'];
            
            
            
            
        if(isset($data['path']))
            $this->path = $data['path'];
            
        if(isset($data['path_raw']))
            $this->pathOld = $data['path_raw'];    
            
        if(isset($data['body']))
            $this->body = $data['body'];
        
        
        if(isset($data['published']))
            $this->published = $data['published'];
            
            if(isset($data['depublish']))
            $this->depublish = $data['depublish'];
        
        if(isset($data['place']))
            $this->place = $data['place'];
            
        if(isset($data['image']))
            $this->image = $data['image'];
            
        if(isset($data['alt']))
            $this->alt = $data['alt'];
        
        if(isset($data['width']))
            $this->width = $data['width'];
         
        if(isset($data['height']))
            $this->height = $data['height'];
        
        if(isset($data['last_update']))
            $this->lastUpdate = $data['last_update'];
           
        
        
        if(isset($data['notes']))
            $this->notes = $data['notes'];
        
      
        $this->applyBusinessRules();
        
        
    
        
        
        
        
    }
    
    /**
     *
     *
     */
    
    protected function applyBusinessRules(){
        
        #echo $this->body;
        //Clean up
        $pattern = '/&(?!amp;)/';
        $replace = '&amp;';
	$this->title =      preg_replace($pattern,  $replace, $this->title);
        $this->body =       preg_replace($pattern,  $replace, $this->body);
        $this->alt =        preg_replace($pattern,  $replace, $this->alt);

         #$this->subtitle =   preg_replace('/&(?!amp;)/', '&amp;', $this->subtitle);
        
        //Derived parameters
        require_once('Markdown/markdown.php');
        $this->body=Markdown($this->body);
        $this->teaser=Markdown($this->teaser);
    
      
        
    
        if(!$this->metaTitle){
            
            $this->metaTitle = $this->title;
            $this->metaTitle = strip_tags( $this->metaTitle );
            
            $truncateCount = 69 - strlen($this->metaTitleSuffix);
            
            $this->metaTitle = $this->truncate($this->metaTitle,$truncateCount);
            
            $this->metaTitle .= $this->metaTitleSuffix;
            
            //echo strlen($this->metaTitle);
            
        }
        
         //echo $this->metaTitle;
        
        $this->metaTitle = htmlentities(strip_tags(($this->metaTitle) ),ENT_QUOTES,'UTF-8' );
        
        
        if($this->subtitle){
            $this->subtitleArray=explode('|',$this->subtitle);
            $this->subtitleArray = preg_replace($pattern,  $replace, $this->subtitleArray);
        }
        
                
        $this->_createMetaDescription();
        
        $this->teaser = $this->metaDescription; //For backwards compatibility
      
        
        if(strtotime($this->published)<time() ){
            
            $this->isActive=true;
        } else {
            $this->isActive=false;
        }
        
    }
    
    
    
    private function _createMetaDescription(){
        
        
        if($this->metaDescription){
            
            return true;
        
        }
        
        if(count($this->subtitleArray) ){
          
            $this->metaDescription = trim(implode('. ', $this->subtitleArray), '. ');
            
            return true;
            
        }
            
            
        #Grab the first paragraph
        $pattern = '/^.*?\<p>.*?\/p>/is'; #first two paras
        if(preg_match($pattern, $this->body, $matches) ){
      
            $this->metaDescription = strip_tags($matches[0]) ;
        
        }
       
        
    }
    
    private function splitBody(){
        
       
       
        $pattern = '/^.*?\<p>.*?\/p>.*?\/p>/is'; #firs two paras
        
        preg_match($pattern, $this->body, $matches); 
	
        #print_r($matches);
        
        $this->intro = $matches[0];
        
        $this->bodyWithoutIntro = str_replace($this->intro,'',$this->body);
        
        
    }
    
   
    public function getRecentByDays($dbPDO, $days){
        
        require_once ('News/Model/News.php');
        
        $model = new Model_News($dbPDO);
        
        $model->getRecentByDays($days);
        
        $newsArray = array();
        while( $row = $model->fetch() ){
            
            $news = new self();
            $news->populate($row);
            
            $newsArray[] = $news;
        }
        
        
        return $newsArray;
        
    }
   
    
   
}