<?php

/**
 * 17 Apr
 *
 * Updated Jan 2012
 *
 */


require_once ('articles/Model/Item.php');
Class Model_News extends Model_Item{

    
  
    public $top;
    public $isActive =  true; //Means NOT awkward AND the_date (published) is in the past. Override for admin
    public $featured; //for selecting only featured items

    
    public function __construct($dbPDO){
       
        $this->dbPDO=$dbPDO;
        
    }
    
    public function getByPath($path){
        
        
        
        $sql = "SELECT * from news
                WHERE 1 = 1                 
                AND path = '" .  $path . "' ";
                
                
        if($this->isActive){        
            $sql .= "
                    AND published < NOW()
                    AND (depublish > NOW() OR depublish is NULL)";        
        }
        
        $sql .= " LIMIT 1 ";
        
   
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
    
        
    }
    
    public function getRecent($count,$imageRequired=false){
        
        $sql = "SELECT * FROM news
                WHERE 1=1 ";
        
         if($this->isActive){        
            $sql .= "
                    AND published < NOW()
                    AND (depublish > NOW() OR depublish is NULL)";        
        }
        
        if($this->featured){
            
            
        }
        
        if($imageRequired){
            
            $sql .= " AND image IS NOT NULL ";
        }
        
        $sql .= " ORDER BY published DESC LIMIT " . $count;
        
        //echo $sql;
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
        
    
    }
    
    public function getRecentByDays($days){
        
        $sql = "SELECT * FROM news
                WHERE 1=1
                
                AND published >= DATE_SUB(CURRENT_DATE, INTERVAL $days DAY)
                
                ";
        
         if($this->isActive){        
            $sql .= "
                    AND published <= NOW()
                    AND (   depublish > NOW()
                            OR depublish is NULL
                            OR depublish = ''
                            
                            )";        
        }
        
        
        
      
        
        $sql .= "
        
            
        
            ORDER BY
                published DESC
                
                ";
        
        //echo $sql;
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
        
       
    
    }
    
    
    
    public function getItems($count=null){
        
        $sql = "SELECT * from news
                WHERE 1=1 ";
        
        if($this->isActive){        
            $sql .= "             
                    AND published < NOW()
                    AND (depublish > NOW() OR depublish IS NULL) ";        
        }
        
        if($this->featured){
            
            $sql .= "             
                    AND featured = '1' ";  
            
        }
        
        $sql .= " ORDER BY published DESC ";
        
        if($count){
            
             $sql .= "  LIMIT " . $count;
            
        }
       
        
        #echo $sql;
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
        #print_r($this->stmnt->ErrorInfo());
        
    }
    
    
        
    public function getItemsByYear($year = null){
        
        
        if(!$year){
            
            $year=date('Y');
        }
        
        $startDate = $year .'-1-1';
        #echo date('d M Y H:s',$startDate);
        
        $endDate = $year+1 .'-1-1';;
        #echo date('d M Y H:s',$endDate);
        
        
        $sql = "SELECT * from news
                WHERE
                    1=1
                    AND published >= '$startDate'
                    AND published < '$endDate' ";
                    
        if($this->isActive){        
            $sql .= "             
                    AND published < NOW()
                    AND (depublish > NOW() OR depublish IS NULL) ";        
        }
                    
          $sql .= "  ORDER BY published DESC
                ";
        //echo $sql;
        # exit;
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        try{
            $this->stmnt->execute();
                    
        } catch (Exception $e) {
           
            echo "Failed: " . $e->getMessage();
        }
        
    }
    
   
    /**
     *
     * @toto - this one return the result. Inconsistent with the others
     *
     *
     */
    
    public function getById($id){
        
     
        $sql = "SELECT * from news
                WHERE 1=1 ";
        
        if($this->isActive){        
            $sql .= "       
                    AND published < NOW()
                    AND (depublish IS NULL OR depublish > NOW() )
                    ";        
        } 
        
        $sql .= " AND id=$id";
        //echo $sql;
        $this->stmnt = $this->dbPDO->prepare($sql);
        
        $this->stmnt->execute();
        
        return $this->stmnt->fetch();
    
        
    }
   
   
    
    public function fetchAll(){
        
        return $this->stmnt->fetchAll();
        
        
    }
    
    public function fetch(){
        
        return $this->stmnt->fetch();
        
        
    }
    
    
    
    /**
     * 27 Apr 08 - changing so that it's updated with POSTED data... not with the News object
     * 19 May 08 - upaded from POST - to make use of filter
     *
     * 17 June 08 - updated so the form doesn't REPLY on javascript. Chabn
     *
     */
    
    public function updateFromPOST(){
        #print_r($data);
        #echo '<h1>News Model class needs to be udated';
       # exit;
        #echo '<p>SUBTITLE IS ' . $newsItem->lastUpdate;
        #exit;
        #echo '<textarea rows=20 cols=100>' . $newsItem->body . '</textarea>';
       # exit;
       
      
        
        $sql = "UPDATE news
                
                SET
                title = :title,
                
                
                
                body = :body,
                
                meta_title = :metaTitle,
                meta_description = :metaDescription,
                
                published = :published,
                depublish = :depublish
                

                WHERE id=:id";
        //echo $sql;
        #exit;
        
        $stmnt = $this->dbPDO->prepare($sql);
        
        $stmnt->bindParam(':id', $id);
        
        $stmnt->bindParam(':title', $title);
        $stmnt->bindParam(':body', $body);
        
        $stmnt->bindParam(':metaTitle', $metaTitle);
        $stmnt->bindParam(':metaDescription', $metaDescription);
        
        $stmnt->bindParam(':published', $published);
        $stmnt->bindParam(':depublish', $depublish);    
        
        
    
       
        
         $id = '';
        $title = ''; 
       // $subtitle = ''; 
        
        $featured = '';
        
        //$metaTitle = ''; 
        $metaDescription = ''; 
        $body = ''; 
        //$path = '';
        $published = NULL;
        $depublish = NULL;
        $expire = NULL;
        
        
        
        $id = $_POST[ 'id'];
        
        $title = $_POST[ 'title']; 
        $body = $_POST[ 'body'];
        
        $metaTitle = $_POST[ 'metaTitle'];
        $metaDescription = $_POST[ 'metaDescription']; 
        

        $published = $_POST[ 'published'];
        $depublish = $_POST[ 'depublish'];
        $expire = $_POST[ 'expire'];
        
        
     
        
        
        #echo $published;
       
        
        $dateArray = explode('-',$published);
        $published = $dateArray[2] . '-' .  $dateArray[1] . '-' .$dateArray[0] ;
        #echo $published;
        #echo '<hr>' . strtotime($published);
        #exit;
        
        
        //New logic for depublish: Expire (the radio control) may may be NEVER or DATE
        
        if($expire=='never'){
            
            $depublish = NULL;
        } else {
            $dateArray = explode('-',$depublish);
            $depublish = $dateArray[2] . '-' .  $dateArray[1] . '-' .$dateArray[0] ;
            
        }
        
        
        
        
      
         
         
         $this->dbPDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        try
        {
            $stmnt->execute();
        }
        catch (PDOException $e)
        {
            #echo 'hasdasdas here';
            print ("The statement failed.\n");
            print ("getCode: ". $e->getCode() . "\n");
            print ("getMessage: ". $e->getMessage() . "\n");
        }
        #print_r( $stmnt->ErrorInfo() ) ;
        #exit;
        
        
    }
    
    
    /**
     * Assumes that the image will be updated using $_POST and $_FILES data
     *
     */
    
    public function updateImage($imagePath){
        
        
        //Store the image
        #echo '<hr>';
        #print_r($config);
        #echo '<hr>';
        
        //Get image site
        $dims = getimagesize($_FILES['image']['tmp_name']);
        
        $width=$dims[0];
        $height=$dims[1];
        
        
        
        
        
        
        
        
        // Load the source image
        $original = imagecreatefromjpeg($_FILES['image']['tmp_name']);
        // Create a blank thumbnail (note slightly reduced height)
        $thumb = imagecreatetruecolor(303,162);
        //Resize it
        imagecopyresampled( $thumb, $original, 0, 0, 0, 0, 303, 162, $dims[0], $dims[1] );
        //Save the thumbnail
        $path= $imagePath . 'article_' . $_GET['id'] . '.jpg';
        
        //Fix possible double-forward slashes
        $path=str_replace('//','/',$path);
        
        #echo $path;
        #exit;
        imagejpeg($thumb,$path);
       
       
        //Store the main image
        $filename='main-'. $_GET['id'] .'.jpg';
        $path= $imagePath;
          //Fix possible double-forward slashes
        $path=str_replace('//','/',$path);
        
        move_uploaded_file($_FILES['image']['tmp_name'],$path . $filename);

        //tell the database 
       
        
        
        
        
          
        $sql = "UPDATE news
                
                SET
               
                image = :image,
                
                width = :width,
                height = :height
                
           
                WHERE id=:id";
        #echo $sql;
        #exit;
        
        $stmnt = $this->dbPDO->prepare($sql);
        
        $stmnt->bindParam(':id', $id);
      
        $stmnt->bindParam(':image', $image);
       
        $stmnt->bindParam(':width', $width);
        $stmnt->bindParam(':height', $height);
        
        $id = $_POST['id'];
        $image = $filename;
        $width = (int) $dims[0];
        $height = (int) $dims[1];
        
        #echo '<hr>';
        try
        {
            $stmnt->execute ();
        }
        catch (PDOException $e)
        {
          print ("The statement failed.\n");
          print ("getCode: ". $e->getCode () . "\n");
          print ("getMessage: ". $e->getMessage () . "\n");
        }
        
        #print_r($stmnt->errorInfo()) ;
     
        
        
    }
    
    
    
    
    
    
    /**
     * Tricky this here is that the path is a mandatory field... and it must be unique
     *
     * 
     */
    
    
    public function addItem($title = 'New Item'){
        
        // Use "NOW()" rather than an generated date to preserve the order of multiple items added as
        // part of the same session
        
        $sql = "INSERT INTO news (
                    title,
                    published,
                    depublish
                    
                    )
                VALUES (
                    :title,
                   NOW(),
                   :depublish
                

                    );";

       // echo $sql;

        $stmnt = $this->dbPDO->prepare($sql);
       
        $stmnt->bindParam(':title', $title);
        
        $stmnt->bindParam(':depublish', $depublish);
        
        //Publish today
        #$published = date('Y') . '-' . date('m'). '-' . date('d');
        //Depublish yesterday
        $depublish = date('Y',mktime(0,0,0,date('m'),date('d')-1,date('y'))) . '-' . date('m',mktime(0,0,0,date('m'),date('d')-1,date('y'))). '-' . date('d',mktime(0,0,0,date('m'),date('d')-1,date('y')));
     

        try
        {
            $stmnt->execute ();
        }
        catch (PDOException $e)
        {
            print ("The statement failed.\n");
            print ("getCode: ". $e->getCode () . "\n");
            print ("getMessage: ". $e->getMessage () . "\n");
        }
        
        #print_r($stmnt->errorInfo()) ;
        
        
    }
    
    /**
     * Usually called STATICALLY for the admin site
     *
     * IMPORTANT: depublish can be NULL
     */
    function status($published, $depublish){
	
        #echo 'pub' . $published;
        #echo 'expi' . $depublish;
        #exit;
        
        $today=time();
	
	if($published > $today){
	    return 'pending';
	}
        
        if($published < $today && ($depublish==null || $depublish > $today)  ){
            
            return 'active';
        }
        
	    return 'expired';
        
    }

}