<?PHP


Class Cleaner{

    var $cleanedString;
			
    //constructor
		function Cleaner($string){
		     
				 $this->string=$string;
								 
		}
	 	
		function getCleanHTML(){
		
				//Always start with the original String!!!!! 		 
		    $this->cleanedString = $this->string;
				
	   	  $this->_generalClean();
				
		    return $this->cleanedString;
		
		}
		
		function getCleanText($length=10000){
						 
		    //Always start with the original String!!!!! 		 
		    $this->cleanedString = $this->string;
				
		    //strip out any BBCode
			  $this->cleanedString = preg_replace("#\[(.*?)]#si", "", $this->string);
				
				//strip out any PHP or HTML tags
			  $this->cleanedString = strip_tags($this->cleanedString);
				
				//general clean-up
				$this->_generalClean();
				
				//kill inverted commas - they will prematurely close attributes!
			  $this->cleanedString= str_replace('"','',$this->cleanedString);
			 
			  $this->cleanedString= str_replace('\'','',$this->cleanedString);
				
				return substr($this->cleanedString,0,$length);
		
		}
		function _generalClean(){
		  
				   			
			 $tempString= $this->cleanedString;
			 //echo $tempString;
			 
			 
			 //For line ends, replace with space... just to be safe (in case there's a break between "<a"  and "href" for example )
			 $tempString=str_replace('\t',' ',$tempString);
			 //echo $tempString;
			 
			 $tempString=str_replace('\n',' ',$tempString);
			 //echo $tempString;
				
			 $tempString=str_replace('\r',' ',$tempString);
			 //echo $tempString;	
			 
			 $tempString=str_replace(chr(9),' ',$tempString);
			 //echo $tempString;
			 
			 $tempString=str_replace(chr(10),' ',$tempString);
			 //echo $tempString;
				
			 $tempString=str_replace(chr(13),' ',$tempString);
			 //echo $tempString;	
			 
			 
			
			 
			  //apostrophe
			 $tempString=str_replace(chr(145),'\'',$tempString);
			 
			 //apostrophe
			 $tempString=str_replace(chr(146),'\'',$tempString);
			 
			 
			 //curly double quotes
			 $tempString=str_replace(chr(147),'"',$tempString);
			 $tempString=str_replace(chr(148),'"',$tempString);
			 
			 //Elipsis
			 $tempString=str_replace(chr(133),'...',$tempString);
			 
			  $tempString=str_replace(chr(150),'-',$tempString);
			 //echo $tempString;	
			 
			 //echo $tempString;	
			 
			 $tempString=str_replace(' & ' ,' &amp; ',$tempString);
			 
			 //echo $tempString;	
			 		 
			 //kill spaces... there must be a better way...
			 $tempString= str_replace('       ',' ',$tempString);
			 $tempString= str_replace('      ',' ',$tempString);
			 $tempString= str_replace('     ',' ',$tempString);
			 $tempString= str_replace('    ',' ',$tempString);
			 $tempString= str_replace('   ',' ',$tempString);
			 $tempString= str_replace('  ',' ',$tempString);
			 			 
			 			 
			 $tempString= trim($tempString);
			
			 $this->cleanedString =  $tempString;
			 
		}	
	
		
		
		
}
?>