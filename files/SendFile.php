<?php
Class SendFile{
     function __construct($path, $fname){  
          $this->path=$path;
          $this->fname=$fname;
     }
     function serveFile(){
          session_write_close();
          
          //Be clean!
          ob_start(); // Just to prevent error on next line
          ob_end_clean();
          if(!is_file($this->path) || connection_status() != 0){
               //echo 'oops';
               return false;
          }
          //to prevent long file from getting cut off
          set_time_limit(0);
          $name = basename($this->fname);
          
          
          //THIS IS MY ADDITION (FROM HARRY@S VERSION)
          if (strpos($_SERVER['HTTP_USER_AGENT'], 'MSIE 5') or strpos($_SERVER['HTTP_USER_AGENT'], 'Opera 7')) {
               $mimeType = 'application/x-download';
          }
          else{
              $mimeType = 'application/octet-stream';
          }
         
          header("Cache-Control: ");
          header("Pragma: ");
          header("Content-Type: $mimeType");
          header("Content-Length: ".(string)(filesize($this->path)));
          header('Content-Disposition: attachment; filename="'.$name.'"');
          header("Content-Transfer-Encoding: binary\n");
          
     
          if($file = fopen($this->path, 'rb'))
          {
               while((!feof($file)) && (connection_status()==0))
               {
                    print(fread($file, 1024*8));
                    flush();
               }
               fclose($file);
          }
     
          return connection_status() == 0 && !connection_aborted();
     }
}