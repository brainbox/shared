<?PHP

class WebsiteEmail {
			
			var $myString="";
			var $emailDetails =array();
      
      // Default constructor,
    	function WebsiteEmail(){
    	
    	}
    	
    	function setStyles($styles){
    	
    	   $this->styles=$styles;
    	}
			function setAddresses($addressDetails){
			    
					$this->emailDetails=$addressDetails;
					//echo $this->emailDetails;
					
			}
			function setTitle($title){
			
			     $this->emailTitle=$title;
			}
			function setHTMLbody($HTMLbody){
			
			     $this->HTMLbody=$HTMLbody;
			}
			function setTextBody($textBody){
			
			     $this->textBody=$textBody;
			}
			function setStyle($styleDefinitions){
			
					 $this->styleDefinitions=$styleDefinitions;
			}
			
			
			
    	function sendWebsiteEmail(){
	
	        require_once 'phpmailer/class.phpmailer.php';

        	$mail=new phpmailer();
        	//echo '<pre>';							
					//print_r($this->emailDetails);					
													
    		 
    			foreach($this->emailDetails['to'] as $name=>$value){
    			    $mail->addAddress(     $value );
    			}			
					if(isset($this->emailDetails['cc'])){
          			foreach($this->emailDetails['cc'] as $name=>$value){
          			    $mail->addCC(     $value );
          			}
					}
					if(isset($this->emailDetails['bcc'])){	
          			foreach( $this->emailDetails['bcc'] as $name=>$value){
          			    $mail->addBCC(     $value );
          			}						
          }  
            
  				$mail->addBCC(	'gary@brainboxweb.co.uk');
								
					$mail->From     =      $this->emailDetails['from'];
          $mail->FromName =      $this->emailDetails['fromName'];
          $mail->AddReplyTo =    $this->emailDetails['replyTo'];			
								
        
          $mail->WordWrap =     	70;                              // set word wrap
          //$mail->AddAttachment($f_tmp_name,$f_name);      // attachment
			
			
          $mail->IsHTML(true);                               // send as HTML
          $mail->Subject  =  		  $this->emailTitle;
					
					
				
					
          $mail->Body     =  			$this->getStringHTML();
          $mail->AltBody  =  			$this->getStringText();
    							
																				
       	  if($mail->Send()){
					//if (1==1){
                    return true;
					}
					else{
							
                 trigger_error('Problem with sending site email');
								 return false;
                  
          }				
		}							
									
									
			
			//returns HTML suitable for an email
			function getStringHTML(){
							 
							$myString="";
							$tempString="";
							 
							//get the globally-defined email element 
							include('config/admin_config.php');
							
							//print_r($adminElements);
        			$htmlString  = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">';
        			$htmlString  .= '<html>';
        			$htmlString  .= '<head>';
							$htmlString  .= '<title>Powered By BRAINBOX</title>';
        			$htmlString  .= '<style type="text/css">';
        			$htmlString  .= '<!--';
        			
        			//add the global style declarations here!!!!
        			$htmlString  .= $adminElements['HTMLstyles'];
		
        			// add the "local" style declarationsn here!!!
							
							$htmlString  .= @$this->styleDefinitions;
							

        			$htmlString  .='-->';

        			$htmlString .= '</style>';
        			$htmlString  .= '</head>';
							$htmlString  .= '<body>';

        			//add the global top-of-the-email stuff here!!!!
        			$htmlString  .= $adminElements['HTMLheader'];
							
							//add in the body of the HTML email
							$htmlString  .= $this->HTMLbody;
							$htmlString  .= '<span></span>';
																													 
							        			
           	  //add global end-of-email stuff here
        			$htmlString .= $adminElements['HTMLfooter'];
        			
							
        			$htmlString .= '</body>';
							$htmlString .= '</html>';
							//echo $htmlString;
						 
						  return $htmlString;
			}
			//returns plain text (with line breaks) suitable for a plan text email
			function getStringText(){
							 $myString="";
							 $tempString="";
							 
						  //get the globally-defined email element 
							include('config/admin_config.php');
							
							$myString  = $this->textBody;
						  $myString .= $adminElements['textFooter'];
						 
						  return $myString;
			}
			
			
	
	
}
?>