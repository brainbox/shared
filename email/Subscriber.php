<?php

Class Subscribers{
	
	function __construct($dbPDO){
		
		$this->dbPDO=$dbPDO;
		
		
		$this->initialise();
	}
	
	function initialise(){
		
		$sql="SELECT id, name, email, active, secretkey, create_date from subscriber";
		$stmnt=$this->dbPDO->prepare($sql);
		
		$stmnt->execute();
		
		$this->data=$stmnt->fetchALL(PDO::FETCH_ASSOC);

	}
	
	
	function is_set(){
		
		//print_r($this->data);
		//exit;
		if(isset($this->data) && count($this->data)  ){
			//echo 'true';
			return true;
		}
		else{
			//echo 'false';
			return false;
		}
			
	}
	/*
	function initialse(){
		
		$sql = "SELECT slot_id, subscriber_id FROM slot_allocation";
			//echo $sql;
			$stmnt=$this->dbPDO->prepare($sql);
			$stmnt->execute();
			$this->data=$stmnt->fetch(PDO::FETCH_ASSOC);	
			//echo '<HR>====';
		
	}
	*/
	function fetch () {
		
		if(isset($this->data)&& count($this->data) ){
				
		
			$row=each($this->data);
			 if ( $row ) {
							
				return new Subscriber($row['value']);
			} else {
				reset ( $this->data);
				return false;
			}
		}
		else{
			return false;
		}		 
		
	}
	
	function getSlotInfo($slotID){
		
		
		$sql="SELECT slot_id as slot_id, subscriber.id, subscriber.name, subscriber.email, subscriber.active,subscriber.secretkey
			FROM slot, slot_allocation, subscriber
			WHERE slot.id=slot_allocation.slot_id
			AND subscriber.id=slot_allocation.subscriber_id
			AND  slot.id =" . $slotID;
			
		//echo $sql;
		$stmnt=$this->dbPDO->prepare($sql);
		$stmnt->execute();
		$this->slotdata=$stmnt->fetchAll(PDO::FETCH_ASSOC);	
	
	}
	
	
	function fetchSlotInfo() {
		
		
		if(isset($this->data)&& count($this->data) ){
			
		
			$row=each($this->slotdata);
			 if ( $row ) {
							
				return new Subscriber($row['value']);
			} else {
				reset ( $this->data);
				return false;
			}
		}
		else{
			return false;
		}	
		
	}

	
	function getByID($id) {
		//this may be wrong: stepping through an objext as if it's an array	
		//echo '<hr>' . $id;
		//print_r($this->data);
		if(isset($this->data)){
			foreach($this->data as $name=>$value){
	
				if($value['id']==$id){
				echo "<hr>got here";															
				return new Subscriber($value);																			 
				}
					
			 }
			  //only gets this far if ite not found
			    reset($this->data);   
			    return false;
						
		}
		else{
			return false;
		}
	}
	function getBySecretkey($secretkey) {
		
		if(isset($this->data)){
			foreach($this->data as $name=>$value){
	
				if($value['secretkey']==$secretkey){
																		
				return new Subscriber($value);																			 
				}		
			 }
			    reset($this->data);   
			    return false;		
		}
		else{
			return false;
		}
	}
	
	function addNew($data){
		
		//print_r($data);
		//exit;
		
		//Check for dupe;
		$sql="SELECT COUNT(*) FROM subscriber WHERE email=" . $data['email'];
		
		$stmnt=$this->dbPDO->prepare($sql);
	
		$stmnt->execute();
		
		$count=$stmnt->fetch(PDO::FETCH_ASSOC);	
		$count=$count['COUNT(*)'];
		//echo $count;
		
		If($count==0){
			
			//create the subscriber
			
			$sql="INSERT INTO subscriber (name, email, create_date)
				VALUES ('" . $data['name'] . "',  '" . $data['email'] ."', " . time() .")";
			//echo $sql;
			$stmnt=$this->dbPDO->prepare($sql);
			$stmnt->execute();
			$insertID=$this->dbPDO->lastInsertId();
			//echo $insertID;
			
			$key=md5 ('secret' . $insertID);
			//echo $key;
			$sql="UPDATE subscriber SET secretkey='$key' WHERE id=" .  $insertID;
			//echo $sql;
			$stmnt=$this->dbPDO->prepare($sql);
			$stmnt->execute();
			
			return true;
			
		}
		else{
			//not sure what to do!!!
			return false;
		}
		
		
	}
	
	
}


Class Subscriber{
	
	function __construct($data){
		//echo '<pre>';
		//print_r($data);
		$this->id=$data['id'];
		$this->name=$data['name'];
		$this->email=$data['email'];
		$this->active=$data['active'];
		$this->secretkey=$data['secretkey'];
		$this->create_date=$data['create_date'];
		
	}
	
	//Use overload for the simple cases
	public function __get($nm){
	     	
	       if (isset($this->$nm)) {
		     return $this->$nm;
	       } else {
		   return false;
	       }
	   }
}



?>