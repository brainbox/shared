<?php



abstract Class Alerts {
    
    public $alertName;
    
    public $copyEmailsToBrainbox = true;
    
    
    protected $toEmail;
    protected $toName;
    
    
    public $fromEmailAddress;
    public $fromEmailName;
    public $senderEmailAddress; // For bouncers

    
    protected $subject;
    protected $bodyHTML;
    protected $bodyText;
  
    protected $launchReport;
    
    protected $email;
    
    public $isTest = true; // override explicityl for live
    
    function __construct($dbPDO){
        
	$this->dbPDO=$dbPDO;
       
    }
    
    abstract  function initialise(); 
    
    
    protected function prepareEmailer(){
	
	require_once "phpmailer/class.phpmailer.php";
	
	$this->email = new PHPMailer();
	
	
	if($this->bodyHTML){
	    $this->email->IsHTML(true);
	}
	
	
	if($this->copyEmailsToBrainbox){
	    
	    $this->email->AddBCC( BRAINBOX_EMAIL_ADDRESS );
	    
	}
	
		
	$this->email->From = $this->fromEmailAddress;
	$this->email->FromName = $this->fromEmailName;
	
	//Address for BOUNCERS
	if($this->senderEmailAddress){
	    
	    $this->email->Sender = $this->senderEmailAddress;
	    
	} else {
	    
	    $this->email->Sender = $this->fromEmailAddress;

	    
	}
	
	
	$this->email->AddReplyTo($this->fromEmailAddress);
	
    }
	
	
    function sendEmail(){	

	
	if( !$this->isTest ){  
	    
	    $this->email->AddAddress($this->toEmail, 		$this->toName			);
	    
	} else {
	    
	    $this->email->AddAddress( BRAINBOX_EMAIL_ADDRESS , 	$this->toName . ' (test)' );
	    
	}
	
	if(!empty($this->CCarray) ){
	    
	    foreach ($this->CCarray  as $CC){
		
		 $this->email->AddCC($CC);
	   
		
	    }
	    
	}
	


	$this->email->Subject = $this->subject;
	
	
	if($this->bodyHTML){
	    
	    $this->email->AltBody = $this->bodyText;
	    $this->email->Body = $this->bodyHTML;
	    
	} else {
	    
	    $this->email->Body = $this->bodyText;
	}
	
        
	
	
	if ($this->email->Send() ) {
	    
	    //Good!
	
	} else {
	   
	    fwrite(STDOUT, "\nFailed to send email to " . $this->toEmail );
	    
	    if($this->isTest){
		
		fwrite(STDOUT, ' (test)');
		
	    }
	    
	}
	
	$this->email->ClearAddresses();
	$this->email->ClearAttachments(); //Just in case
	
    }
    
    
    abstract protected function _createLaunchReport();
    
    
    
    function __destruct(){

	
	$this->_createLaunchReport();
	
	if(!isset($this->email)){
	    
	    $this->prepareEmailer();
	    
	}
	
	
	
	$this->email->Subject = $this->alertName . ' [Launch Report]';
	
	
	$this->email->Body = $this->launchReport;
	
	
	$this->email->AddAddress( BRAINBOX_EMAIL_ADDRESS );
	
	
	if ($this->email->Send() ) {
	    
	    //Good!
	    
	} else {
	    
	   fwrite(STDOUT, "\nFailed to send launch report");
	    
	}
	   
	fwrite(STDOUT, "\n" . $this->alertName . " has run");
	
	
    }
    
}