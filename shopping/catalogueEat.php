<?PHP

@DEFINE (PRODUCT_TABLE,'product');
@DEFINE (ITEM_TABLE,'item');



class EatCatalogue extends catalogue { 

    function __construct($dbConn) { 

            $this->dbConn=$dbConn;


    }  
    
    function getProducts(){

        $sql = "SELECT id, product, description, price, image from " . PRODUCT_TABLE . " ORDER BY sort_order";
        
        // echo $sql;     	
        $result = $this->dbConn->query($sql);
        
        //echo "<pre>the result is ";
        //var_dump($result->fetch());
        
        if(($result->size())!=0){
        
            //echo "got here";
            while ( $row = $result->fetch() ) {
                //echo $row;
                $this->productArray[]=$row;
            }
            return $this->productArray;
        
        } else {
            
             return false;
        
        
        }
        
    }
	
	
 

} 
?>