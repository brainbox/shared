<?php

Class OrderModel{
    
    
    function __construct($dbPDO){
        
        
        $this->dbPDO=$dbPDO;
        
        
    }
    
    function getOrders(){
        
        $sql= "SELECT * from shop_order ORDER BY date DESC";
        #echo $sql;
        $this->stmnt = $this->dbPDO->prepare($sql);
        $this->stmnt->execute();
        
        
    }
    
    function fetch(){
        #print_r($this->stmnt->fetch() );
        return $this->stmnt->fetch();
        
    }
    
    
    
    
}