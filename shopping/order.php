<?PHP

/*
Need to be careful when retrieveing results... if they people click back and
forth, multiple CUSTOMERS can be created with the same SESSION.

UNLESS... test for the EXISTANCE, then OVERWRITE if necessary

The ORDER should be WRITTEN at the time control is given to PAYPAL.
*/

@DEFINE ('CUSTOMER_TABLE','client');
@DEFINE ('ORDER_TABLE','order');
@DEFINE ('ORDER_ITEM_TABLE','orderitem');

@DEFINE ('PRODUCT_TABLE','product');
@DEFINE ('ITEM_TABLE','item');



class order {
    
    protected $id;
    protected $customerId;
    protected $orderDate;
    protected $product;
    protected $quantity;
    protected $price;
    

	function order($dbConn) { 
	   
		 $this->dbConn=$dbConn;
		 
	}
    
    public function __get($member) {
        if (isset($this->$member)) {
            return $this->$member;
        } else {
            return false;
        }
    }
	
	function createOrder($customerID){
        
        if(!isset($_SESSION['shopping_session_id']) ) {
            $sql = "INSERT INTO " . ORDER_TABLE  . " (customer_id, date)
                        VALUES (  $customerID , " . time() . "  )";
                        
        } else {
            $sql = "INSERT INTO " . ORDER_TABLE  . " (customer_id, date, session)
                        VALUES (  $customerID , " . time() . " , '" . $_SESSION['shopping_session_id'] . "' )";
            
        }
       # echo $sql;
        #exit;
        $result = $this->dbConn->query($sql);
       
        return mysql_insert_id();									
	}
	
	function addOrderItems($orderID,$basket){
	
		foreach($basket as $name=>$value){		
		 
		 				//print_r($value);				
		 
      			$sql = "INSERT INTO " . ORDER_ITEM_TABLE . " (order_id, item_id, quantity, price)
      					 	 VALUES ( $orderID ,
      						  		" . $value['item_id'] . ",
      								" . $value['quantity'] . ",
      								'" . $value['price'] . "'
      											)";
      						 					 
      						 
      			//echo '<p>PROB SLQ?: ' . $sql;		
      					
      			$result = $this->dbConn->query($sql);

			}
	
	}
    
    function getCustomer(){
        
        require_once('shopping/customer.php');
        $customer=new customer($this->dbConn);
        
        #echo $this->customerId;
       # exit;
        if($customer->getCustomer($this->customerId) ){
            
            return $customer;
        
        } else {
            return false;
        }
    }
	
    
    function getItems(){
	
        $sql = "SELECT " . ORDER_ITEM_TABLE . ".quantity, " . ORDER_ITEM_TABLE . ".price,
            " . PRODUCT_TABLE . ".product, 
            " . PRODUCT_TABLE . ".description 
                
        
            FROM  " . ORDER_TABLE . ", " . ORDER_ITEM_TABLE . " , " . PRODUCT_TABLE . ", " . ITEM_TABLE . "
	        WHERE 
			1 					 
            AND  " . ORDER_TABLE . ".id =  " . ORDER_ITEM_TABLE . ".order_id
            AND  " . ORDER_ITEM_TABLE . ".item_id =  " . ITEM_TABLE . ".id
            AND  " . PRODUCT_TABLE . ".id =  " . ITEM_TABLE . ".product_id 
            AND " . ORDER_TABLE . ".id = $this->id";
			           
            $result = $this->dbConn->query($sql);        
                       
            $this->itemArray=array();
            
            if($result->size() ){
            
                require_once('orderItem.php');
                $this->total=0;
            
                while ( $row = $result->fetch() ) {
                    //echo $row;
                    $item = new OrderItem($this->dbConn);
                    $item->populate($row);
                    $this->itemArray[]=$item ;
                    
                    #Get the item total
                    $this->total = $this->total + ($row['price'] * $row['quantity']);
                }
                return $this->itemArray;
            
            } else {
                return false;
            }
		
	  }
    
    
    
	function getOrder($id){
	
        $this->id=$id;
	 
        $sql = "SELECT * FROM " . ORDER_TABLE .
                " WHERE
                1=1 AND "
             . ORDER_TABLE . ".id = $id";
			
			#echo $sql;
			$result = $this->dbConn->query($sql);
            
                       
            $this->itemArray=array();
            
            if($result->size() ){
            
                $row = $result->fetch();
            
                    $this->orderDate=$row['date'];
                    $this->customerId=$row['customer_id'];
                
                return true;
            
            } else {
                return false;
            }
		
	  }
    
	function getOrderByID($id){
	
        $this->id=$id;
	 
        $sql = "SELECT "
        
             . ORDER_TABLE . ".date, "
             . ORDER_TABLE . ".customer_id, "
            
            
            . ORDER_ITEM_TABLE . ".quantity, " . ORDER_ITEM_TABLE . ".price,
         " . PRODUCT_TABLE . ".product, 
            " . PRODUCT_TABLE . ".description 
                
        
            FROM  " . ORDER_TABLE . ", " . ORDER_ITEM_TABLE . " , " . PRODUCT_TABLE . ", " . ITEM_TABLE . "
	        WHERE 
			1 					 
            AND  " . ORDER_TABLE . ".id =  " . ORDER_ITEM_TABLE . ".order_id
            AND  " . ORDER_ITEM_TABLE . ".item_id =  " . ITEM_TABLE . ".id
            AND  " . PRODUCT_TABLE . ".id =  " . ITEM_TABLE . ".product_id 
            AND " . ORDER_TABLE . ".id = $id";
			
			#echo $sql;
			$result = $this->dbConn->query($sql);
            
            #print_r($result);
           # echo $result->size();
           # exit;
            
            $this->itemArray=array();
            
            if($result->size() ){
            
                while ( $row = $result->fetch() ) {
                    //echo $row;
                    $this->itemArray[]=$row;
                }
                return $this->itemArray;
            
            } else {
                return false;
            }
		
	  }
      function getOrderBySession($session){
	
	 
	 
        $sql = "SELECT " . ORDER_ITEM_TABLE . ".quantity, " . ORDER_ITEM_TABLE . ".price,
         " . PRODUCT_TABLE . ".product, 
            " . PRODUCT_TABLE . ".description 
                
        
            FROM  " . ORDER_TABLE . ", " . ORDER_ITEM_TABLE . " , " . PRODUCT_TABLE . ", " . ITEM_TABLE . "
	        WHERE 
			1 					 
            AND  " . ORDER_TABLE . ".id =  " . ORDER_ITEM_TABLE . ".order_id
            AND  " . ORDER_ITEM_TABLE . ".item_id =  " . ITEM_TABLE . ".id
            AND  " . PRODUCT_TABLE . ".id =  " . ITEM_TABLE . ".product_id 
            AND " . ORDER_TABLE . ".session = '" . $session . "'";
			
			//echo $sql;
			$result = $this->dbConn->query($sql);
            
           
				if(($result->size())!=0){
				
						#echo "FFFFgot here";
            while ( $row = $result->fetch() ) {
    						//echo $row;
                $this->itemArray[]=$row;
            }
						return $this->itemArray;
						
				}
				else{
						 return false;
		
				
				}
	  }
   
    function orderTotal() {
	   $contents =  $this->itemArray;
		 
	   #print_r ($contents);
           // exit;
	   $total=0;
		 
	   for($i=0; $i < count($contents); $i++){
		 
		
		 
			$total = $total + ($contents[$i]['price'] * $contents[$i]['quantity']);
                        
                         #echo 'total in the loop is ' . $total ;
	   }
	   $this->total = number_format($total, 2, '.', '');
	   return $this->total;
	}
      
      
      function getCustomerOrderByID($id, $customerID){
	
	 
	 
        $sql = "SELECT " . ORDER_ITEM_TABLE . ".quantity, " . ORDER_ITEM_TABLE . ".price,
         " . PRODUCT_TABLE . ".product, 
            " . PRODUCT_TABLE . ".description 
                
        
            FROM  " . ORDER_TABLE . ", " . ORDER_ITEM_TABLE . " , " . PRODUCT_TABLE . ", " . ITEM_TABLE . "
	        WHERE 
			1 					 
            AND  " . ORDER_TABLE . ".id =  " . ORDER_ITEM_TABLE . ".order_id
            AND  " . ORDER_ITEM_TABLE . ".item_id =  " . ITEM_TABLE . ".id
            AND  " . PRODUCT_TABLE . ".id =  " . ITEM_TABLE . ".product_id 
            AND " . ORDER_TABLE . ".id = $id 
            AND " . ORDER_TABLE . ".customer_id = $customerID";
			
			#echo $sql;
			$result = $this->dbConn->query($sql);
			
				if(($result->size())!=0){
				
						//echo "FFFFgot here";
            while ( $row = $result->fetch() ) {
    						//echo $row;
                $this->itemArray[]=$row;
            }
						return $this->itemArray;
						
				}
				else{
						 return false;
		
				
				}
	  }
      
    public function populate($data)
    {   
      #print_r($data);
      #exit;
        $this->id=$data['id'];
        $this->customerId=$data['customer_id'];
        $this->orderDate=$data['date'];

        
    }
      
      
     ###SHOULD BE MOVED TO AND EXTENDING CALSS 
      ###adm the rule is a 
	function getShipping()
    {
        
	#echo $this->cart_total();
	
        if($this->total > 50 ){
	
	    return 0; #free delivery
	
	} else {
	 
	    return 4;   #four quid
	}
        
    }

} 
?>