<?PHP
// Shopping Cart Class /////////////////////////////////////////////////////////
// Original Author: John Coggeshall, http://www.coggeshall.org/				  //
// Modified by Aarron Walter, aarron@classbot.com							  //
//																			  //
// Adapted from John Coggeshall's tutorial at: 								  //
// http://www.zend.com/zend/spotlight/php-shopping-cart1.php				  //
////////////////////////////////////////////////////////////////////////////////


class cartASLTIP extends cart { 
  //var $username = "root"; 
  //var $password = ""; 
  //var $database = "shopping"; 
  //var $hostname = "localhost"; 
	
  var $inv_table = "product"; 
  var $cart_table = "shortlist";  


	function __construct($cart_id,&$dbConn) { 
	   
		
     $this->dbConn=&$dbConn; 	
		 $this->cart_id = $cart_id; 
	}  
	
	function getTotal(){
			
			$query = "SELECT distinct serialised FROM ".$this->cart_table. " WHERE session='".$this->cart_id."'"; 
			//echo $query;
  		//$result = mysql_query($query, $this->dblink); 
  		$result = $this->dbConn->query($query) or die(mysql_error());
  	
  		if(!$result) { 
  			return 0; 
  		} 
			else{
			
			  return $result->size();
  		} 
	}
	
	
	function add_item( $serialised, $memberID) { 
		//Let them add dupes - check them after			 
		
	    $query = "INSERT INTO ".$this->cart_table . " (session, serialised, member_id, the_date) 
	            VALUES ('".$this->cart_id."', '$serialised', $memberID, " .  time() . " ) "; 
	  // echo $query;
					//mysql_query($query, $this->dblink); 
				$result = $this->dbConn->query($query);
			 
			
			return true; 
	}  
	
	
	function check_item($item_id) { 
	
		$query = "SELECT quantity FROM ".$this->cart_table. " WHERE session='".$this->cart_id."' AND item_id='$item_id' "; 
		//$result = mysql_query($query, $this->dblink); 
		$result = $this->dbConn->query($query) or die(mysql_error());
	
		if(!$result) { 
			return 0; 
		} 
	
		if(($result->size())!=0){
			$row = $result->fetch();											 
			//$row = mysql_fetch_object($result); 
			return $row['quantity']; 
		} 
	
	 }  
	
	
	function delete_item($shortlistItemId) { 
		$query = "DELETE FROM ".$this->cart_table." WHERE id=$shortlistItemId "; 
		
		echo $query;
		
		//mysql_query($query, $this->dblink); 
		$result = $this->dbConn->query($query) or die(mysql_error());
	
	 } 
	
	
	function clear_cart() { 
		$query = "DELETE FROM ".$this->cart_table." WHERE session='".$this->cart_id."' "; 
		
		
		
		//mysql_query($query, $this->dblink); 
		$result = $this->dbConn->query($query) or die(mysql_error());
	
	 }  
	
	
	
	function modify_quantity($item_id, $quantity) { 
	
		if($quantity <= 0)  return $this->delete_item($item_id);
	
		$query = "UPDATE ".$this->cart_table. " SET quantity='$quantity' WHERE session='".$this->cart_id."' AND item_id='$item_id' "; 
		//mysql_query($query, $this->dblink); 
		$result = $this->dbConn->query($query) or die(mysql_error());
	
	
		return true; 
	 }  
	
	
	function get_contents() {
	
	  //Shouldn't need to reprodice this - should be able to get this from the catalogue...
		// ... by passing a reference to the catalogue???
		//Can't work out how to do it!!!!!!
		
		$sql = "SELECT DISTINCT id, serialised  FROM shortlist WHERE session='".$this->cart_id."'";
		//$sql = "SELECT  serialised  FROM shortlist WHERE session='".$this->cart_id."'";
				 
		//echo $sql;
		$result = $this->dbConn->query($sql);
				
    if(($result->size())!=0){
		
				
				
				    while ( $row = $result->fetch() ) {
    						
                $contents[]=$row;
            }
				
				return $contents;
  				
		}
		else{
				 return false;
		}
		
		
	} 
	
	
	
	

} 
?>