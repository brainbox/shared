<?PHP
// Shopping Cart Class /////////////////////////////////////////////////////////
// Original Author: John Coggeshall, http://www.coggeshall.org/				  //
// Modified by Aarron Walter, aarron@classbot.com							  //
//																			  //
// Adapted from John Coggeshall's tutorial at: 								  //
// http://www.zend.com/zend/spotlight/php-shopping-cart1.php				  //
////////////////////////////////////////////////////////////////////////////////


@DEFINE ('PRODUCT_TABLE','product');
@DEFINE ('ITEM_TABLE','item');
@DEFINE ('CART_TABLE','shopping');

class cart { 
  

	function cart($cart_id,&$dbConn) { 
	
     $this->dbConn=&$dbConn; 	
		 $this->total=0;
		 $this->discountValue=0;
					 
	   //$this->dblink = mysql_connect($this->hostname, $this->username, $this->password); 
	   //mysql_select_db($this->database, $this->dblink); 
	   $this->cart_id = $cart_id; 
	}  
	
	
	function add_item($item_id, $quantity) { 
					 
		$qty = $this->check_item($item_id); 
	
		if($qty == 0) { 
    			$query = "INSERT INTO ". CART_TABLE . " (session, item_id, quantity) VALUES ('".$this->cart_id."', '$item_id', '$quantity') "; 
    			//mysql_query($query, $this->dblink); 
    			$result = $this->dbConn->query($query);
		} 
		else { 
    			#$quantity = $qty + 1;
    			$query = "UPDATE ".CART_TABLE . " SET quantity=quantity+$quantity WHERE session='".$this->cart_id."' AND item_id='$item_id' "; 
    			//mysql_query($query, $this->dblink); 
    			$result = $this->dbConn->query($query) or die(mysql_error());
		} 
		return true; 
	}  
	
	
	function check_item($item_id) { 
	
		$query = "SELECT quantity FROM ".CART_TABLE. " WHERE session='".$this->cart_id."' AND item_id='$item_id' "; 
		//$result = mysql_query($query, $this->dblink); 
		$result = $this->dbConn->query($query) or die(mysql_error());
	
		if(!$result) { 
			return 0; 
		} 
	
		if(($result->size())!=0){
			$row = $result->fetch();											 
			//$row = mysql_fetch_object($result); 
			return $row['quantity']; 
		} 
	
	 }  
	
	
	function delete_item($item_id) { 
		$query = "DELETE FROM ".CART_TABLE." WHERE session='" . $this->cart_id."' AND item_id='$item_id' "; 
		//mysql_query($query, $this->dblink); 
		$result = $this->dbConn->query($query) or die(mysql_error());
	
	 } 
	
	
	function clear_cart() {
	  
		$query = "DELETE FROM ".CART_TABLE." WHERE session='".$this->cart_id."' "; 
		//mysql_query($query, $this->dblink); 
		$result = $this->dbConn->query($query) or die(mysql_error());
	
	 }  
	
	
	
	function modify_quantity($item_id, $quantity) { 
	
		if($quantity <= 0)  return $this->delete_item($item_id);
	
		$query = "UPDATE ".CART_TABLE. " SET quantity='$quantity' WHERE session='".$this->cart_id."' AND item_id='$item_id' "; 
		
		//echo $query;
		//exit;
		//mysql_query($query, $this->dblink); 
		$result = $this->dbConn->query($query) or die(mysql_error());
	
	
		return true; 
	 }  
	
	
	function get_contents() {
	
	  //Shouldn't need to reprodice this - should be able to get this from the catalogue...
		// ... by passing a reference to the catalogue???
		//Can't work out how to do it!!!!!!
		
		$sql = 'SELECT item_id, ' . CART_TABLE . ' .quantity, '. PRODUCT_TABLE .'.product, '. PRODUCT_TABLE .'.description, '. ITEM_TABLE .'.description AS item_description, price FROM '. ITEM_TABLE .', '. PRODUCT_TABLE .', ' . CART_TABLE . ' 
				 WHERE session="'.$this->cart_id.'"
				 AND '. PRODUCT_TABLE .'.id='. ITEM_TABLE .'.product_id
				 AND '. CART_TABLE .'.item_id='. ITEM_TABLE .'.id';
		
		//echo $sql;
		
		$result = $this->dbConn->query($sql);
				
    if(($result->size())!=0){
		
				
				
				    while ( $row = $result->fetch() ) {
    						
                $contents[]=$row;
            }
				
				return $contents;
  				
		}
		else{
				 return false;
		}
		
		
	} 
	
	
	function cart_total() {
	   $contents = $this->get_contents();
		 
	   #print_r ($contents);
           // exit;
	   $total=0;
		 
	   for($i=0; $i < count($contents); $i++){
		 
		
		 
			$total = $total + ($contents[$i]['price'] * $contents[$i]['quantity']);
                        
                         #echo 'total in the loop is ' . $total ;
	   }
	   $this->total = number_format($total, 2, '.', '');
	   return $this->total;
	}
	function cart_total_discounted() {
	   
		 //need to call this total function... in case it hasn't already been called
		 $this->cart_total();
		 
		 $discountedTotal=$this->total - $this->discountValue;
		 $discountedTotal=   number_format($discountedTotal, 2, '.', '');
	   return $discountedTotal;
	}
	
	
	function quant_items() { 
	   $contents = $this->get_contents();
	   $q = 0;
	   for($i=0; $i < count($contents); $i++){
			$q = $q + $contents[$i]['quantity'];
	   }
	   return $q;
	 } 
   
	function getDiscount(){
	
	  //echo "basket value is " . $this->cart_total();
	
	   switch ($this->quant_items() ) {
		 
		    case 0:
         return 0;
         break;
		   case 1:
         return 0;
         break;
		 	 case 2:
			   $this->discountValue=number_format($this->cart_total()*0.05,2,'.', '');
         return array('name'=>'5% discount for two items','value'=>$this->discountValue );
         break;
			case 3:
			   $this->discountValue=number_format($this->cart_total()*0.10,2,'.', '');
         return array('name'=>'10% discount for three items','value'=>$this->discountValue );
         break;
      default:
         $this->discountValue=number_format($this->cart_total()*0.15,2,'.', '');
         return array('name'=>'15% discount for four or more items','value'=>$this->discountValue );
     }
		 
		 
		 
		 

	
	} 
	 
	 

} 
?>