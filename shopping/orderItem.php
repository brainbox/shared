<?php


@DEFINE ('ORDER_TABLE','order');
@DEFINE ('ORDER_ITEM_TABLE','orderitem');

@DEFINE ('PRODUCT_TABLE','product');
@DEFINE ('ITEM_TABLE','item');



class OrderItem {
    
    protected $id;
    protected $product;
    protected $quantity;
    protected $price;
    protected $itemTotal;
    

    function __construct()
    { 

        #$this->dbConn=$dbConn;

    }
    
    public function __get($member)
    {
        if (isset($this->$member)) {
            return $this->$member;
        }
    }

    public function populate($data)
    {   

        $this->id=$data['id'];
        $this->product=$data['product'];
        $this->price=$data['price'];
	$this->quantity=$data['quantity'];
	
	$this->itemTotal=number_format($this->price * $this->quantity,2);

    }


} 
?>