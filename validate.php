<?php
/**
 * Applies all regex-based rules automatically
 *
 *
 */




Class Validate{

    
    public $type=INPUT_GET; // For Filter
    protected $validationRules;
    public $errors=array();
    protected $validation_expressions;
    
    
    
    public function __construct($validationRules){
        
        //Trim everything
        $_POST = array_map('trim', $_POST);
        
        #print_r($_POST);
        
        $this->validationRules = $validationRules;
        
        $this->_getMasterValidationRules();
        $this->validate();
        
        
    }
    
    public function validate(){
        
        foreach ($this->validationRules as $field=>$value){
        
            #echo '<h1>testing ' . $field . '</h1>';   
            #echo '<pre>';
            #print_r($value);
            #exit;
            #File upload fields are NOT present in POST
            if(isset($value['type']) && $value['type']=='file'){
                
                continue;
                
            }
            
            
            $rules=$value['rules'];
            #echo '<hr>rules';    
            #print_r($rules);
            #exit;
                
            foreach ($rules as $ruleName=>$rule){
                
               
                    
                #echo '<h2>Applying rule ' . $ruleName . '</h2>';   
                   
              
                #print_r($validation_expressions);
               
                #Grab the master definition... if there is one!
                if($this->validation_expressions[$ruleName]){
                    $masterRule = $this->validation_expressions[$ruleName];
                    
                    #over-write it as necessary
                    if( isset($rule['reg_exp']) ){
                       
                       $masterRule['reg_exp'] = $rule['reg_exp'];
                       
                    }
                    if(isset($rule['feedback']) ){
                        
                        $masterRule['feedback'] = $rule['feedback'];
                        
                    }
                    #Pass it back
                    $rule=$masterRule;
   
                }
                #One way or the other, $rule should be complete    
    
                $regex = $rule['reg_exp'];
                #echo '<p>The REGEX is: ' . $regex . '</p>';
                #echo '<p>The value to be tested is: ' . $_POST[$field] . '</p>';
                
                #Apply the rule
                $match = preg_match('/' . $regex . '/',$_POST[$field] );
                
               # echo '<p>Match result' . $match;
                if(!$match){
                    #'<p>' . $field . ' FAILED</p>';   
                    #print_r($value);
                    #exit;
                    $this->errors[$value['friendly_name']]=$rule['feedback']   ;
                    break; //Just one fail per field!    
                }
                
            }
        
        }
        #echo '<pre>';
        #print_r($this->errors);
        
    }
    
    public function atLeastOneChecked($name, $fieldArray,$errorMessage){
        
        #echo '<pre>';
       # print_r($fieldArray);
       
        
        foreach($fieldArray as $name=>$value){
            
            if(array_key_exists($value,$_POST) ){
                
                //Job done
                return true;
                #exit;
                
            }
            
            
        }
        #Got this far? nothing is ticked
        $this->errors[$name]=$errorMessage;
        
        return false;
        
        
    }
    
    /**
     * If the first is present, the second can be entered... but must be differnet
     * If the second is present, the third can be entered... but must be different from the other two
     *
     */ 
    
    function cascade($array,$errorMessage){
        #echo '<pre>';
        #print_r($array);
        
        $count = count($array);
        
        for($i=0;$i<$count;$i++){
            
            
            //Is it empty?
            if(!$array[$i]){
                
                //Empty is not a problem... as long as the rest are empty!
                
                $remainder=array_slice($array,$i+1,100,1);
                //print_r($reminder);
                
                foreach($remainder as $name=>$value){
                    
                    if($value){
                        
                        #echo 'Please enter your languages consecutively, ensuring that each are different';
                        $this->errors['languages']=$errorMessage;
                        return false;
                        
                    }
                    
                }
            } else {//Have we seen it before?
            
                if($i==0){
                    continue; //Don't check the first one
                }
            
            
                $previous=array_slice($array,0,$i);
                
                #echo '<p>Current is ' . $i . ' and the previous is ' . print_r($previous,1) . '. We are looking for ' . $array[$i];
                
                $found = array_search($array[$i],$previous);
                
                if($found!==false){
                
                    #echo '<p>We\'ve seen this one before';
                    $this->errors['languages']=$errorMessage;
                    return false;
                }
                
            }   
         
        }
         
        return true; 
         
    }
        
    
    
    public function checkFileSize($field,$size,$errorMessage){
        
        if(!$_FILES[$field]['name']){
            
            return true; //No file = no problem
            
        }
        
        if ($_FILES[$field]['size'] < $size ){
            
            return true;
            
        }
        //Got this far? trouble
        $this->errors[]=$errorMessage;

        
    }
    
    
    
    
    
    public function checkFileType($field,$mimeArray,$errorMessage){
        
       
        #echo '<pre>';
       #print_r($_FILES);
       
        #print_r($mimeArray);
        #echo '</pre>';
        #exit;
        if(!$_FILES[$field]['name']){
            
            return true; //No file = no problem
            
        }
        if(in_array($_FILES[$field]['type'],$mimeArray)){
            
            return true;
            
        }
        //Got this far? trouble
        $this->errors[]=$errorMessage;

        
    }
    
    
    public function getErrors(){
        
        if(count($this->errors)){
            
            return array_unique($this->errors);
        } else {
            return false;
        }
        
        
    }
    
    public function addValidationRules($validationArray){
        
        #Use the $validationArray to addto/overwrite the masterArray
        foreach($validationArray as $name=>$value){
            
            
            
            if($validation_expressions[$ruleName]){
                $masterRule = $validation_expressions[$ruleName];
                
                #over-write it as necessary
                if( $rule['reg_exp']){
                    
                    $masterRule['reg_exp'] = $rule['reg_exp'];
                    
                }
                if($rule['feedback']){
                    
                    $masterRule['feedback'] = $rule['feedback'];
                    
                }
                #Pass it back
                $rule=$masterRule;

            }
            #One way or the other, $rule should be complete    
        }
        
    }
    
    private function _getMasterValidationRules(){
        
        
        
        require('validation_rules.php');
        
        $this->validation_expressions=$validation_expressions;
        
        #print_r($this->validation_expressions);
        
    }
    
    
    
    
    public function setType($type){
        
        if($type=='post'){
            $this->type=INPUT_POST;
        }
        if($type=='get'){
            $this->type=INPUT_GET;
        }
        
        
    }
    
    
    /** returns FALSE or the filtered string
     *
     */
    public function isEmail($name){
        
        
        return filter_input($this->type, $name, FILTER_VALIDATE_EMAIL);
     
        
    }
    
    /** returns the filtered string
     *
     */
    public function sanitize($name){
        
        
        return filter_input($this->type, $name, FILTER_SANITIZE_STRING);
    
    }


}