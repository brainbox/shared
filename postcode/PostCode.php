<?php
Class PostCode{
	
	
	//Constructor
	function PostCode(& $db){
		
	   $this->db = 	& $db;
	   	  		
	}
	
	/*
	function attach(PostcodeObserver $obs){
		$this->observers["$obs"] = $obs;
	}
	
	function detach(PostcodeObserver $obs){
		delete($this->observers["$obs"]);
	}
	protected function notify($params){
		
		foreach($this->observers as $obs){
			////echo 'MNOTIFYING';
			$obs->update($this, $params);
		}
	}
	*/
	
	
	function setPostcode($postcode){
		
		//echo $postcode;
		
		
		$this->postcode=strtoupper(trim($postcode) );
		
		//Get the outcode
		//$pattern = 	 '/([BGLMNS][1-9][0-9]?|[A-PR-UWYZ][A-HK-Y][1-9]?[0-9]|([EW]C?|NW?|S[EW])[1-9][0-9A-HJKMNPR-Y])/i';
		
		//preg_match($pattern, strtoupper($postcode), $matches);
		
		
		
		//@todo... what if we can't find one???
		//var_dump($matches);
		
		
		//$outCode = $matches[1];
		
		$outCode=substr($this->postcode, 0, strpos($this->postcode,' ') );
		$this->setOutCode($outCode);
	}
	function getPostcode(){
		
		return $this->postcode;	
	}
	
	function setOutCode($outCode){
		
		$this->outCode=$outCode;
		$this->_getStartingCoordinates();
		
	}	
	
	
	function _getStartingCoordinates(){
		
		$sql  = ' SELECT x,y FROM postcodes ';
		$sql .= " WHERE outcode='$this->outCode'";
		$sql .= ' LIMIT 1';
		//echo $sql;
		$result=$this->db->query($sql);
		$this->outCodeCoordinates=$result->fetch();
		
		//print_r($this->outCodeCoordinates);
		
		
	}
	function _testOutCode($outCode){
	 
		$sql  = ' SELECT * FROM postcodes ';
		$sql .= " WHERE outcode='$outCode'";
		$sql .= ' LIMIT 1';
		//echo $sql;
		$result=$this->db->query($sql);
		
		if($result->size() > 0){
			return true;
		}
		else{
			return false;
		}
		
	}
	
	
	function distanceBetweenTwo($targetOutCode){
		
      
          
	      $sql  = ' SELECT x,y FROM postcodes ';
	      $sql .= " WHERE outcode='$targetOutCode'";
	      $result=$this->db->query($sql);
			if($result->size() == 0){
			   
				 $postcodeResult = '<p>Ooops... we couldn\'t find that postcode. Please enter another.</p>';
			   
			}
			else{

	        $targetOutCodeCoordinates=$result->fetch();				
			$km= sqrt(   pow(($targetOutCodeCoordinates['x']-$this->outCodeCoordinates['x']),2) + pow(($targetOutCodeCoordinates['y']-$this->outCodeCoordinates['y']),2) )/1000;
  		 
		}
		return $km;	 
        
	}
	
	function _convertToMiles($km){
		
		return 0.621371192*$km;
	}
	function _convertToKm($miles){
		
		return $miles/0.621371192;
	}

}
?>
