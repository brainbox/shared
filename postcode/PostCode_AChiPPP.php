<?php
Class PostCode_AChiPPP extends PostCode{
	
	
	//Constructor
	function __construct(& $db, $exposed=true){
		
	   $this->db = 	& $db;
	  //$this->active=$active;
	   $this->exposed=$exposed;
	   	  		
	}
		
	
	function setDisorderIndex($disorderIndex){
		
		$this->disorderIndex=$disorderIndex;
		
	}
	
	
	
	
	function disorder(){
		
		//echo $this->disorderIndex;
		
		if(!isset($this->disorderIndex) ){
			
			return false;
		}
		
		
	      	$sql = ' SELECT specialty from specialty where id=' . $this->disorderIndex;
		//echo $sql;
		$result=$this->db->query($sql);
		if($result->size()==1){
		      $row= $result->fetch();	
		      
		      $this->disorder=$row['specialty'];
		      return $row['specialty'];
		}  
		else{
			return false;	
		}
	}
	
	function getSearchParameters(){
		
		$parameters=array();
		
		//Postcode
		$parameters[]=$this->postcode;
		
		//On-site
		if($this->onSite){
			$parameters[]='On-site visit';
		}
		
		//Age
		//$parameters[]= 'Age range ' . $this->age();
		
		//Disorder
		if($this->disorder() ){
			$parameters[]=$this->disorder;
		}
		
		return $parameters;
		
	}


	function getNearest($start=0, $number=10, $onSite=false){
         	
		$this->onSite=$onSite;
		
                  $resultCount=0;	
		    
                  if($onSite){
				$radius=5000;
				$maxRadius=1000000;
			}
			else{
                        $radius=$number * 1000;  // 	starting radius in metres
			    
			    
				$maxRadius=1000000;
				
				if($radius>$maxRadius){
					$radius=$maxRadius;		
				}
			}
		    
			//CAN'T ORDER THE RESULTS STRAIGHT FROM THE DATABASE... SO CAN'T USE LIMIT
			
			$requiredResults=$start+$number;		
					
			$loops=0; //To limit the number of times around the loop		
			While($resultCount<$requiredResults && $radius<=$maxRadius && $loops<20){

				
				$sql = 'SELECT dest.outcode, dest.x, dest.y, u.id, u.title, u.forename, u.surname, u.email, u.work, u.mobile ,u.mobile ,u.county, u.town, u.range, u.blurb ';
				$sql .= ' FROM ';
				$sql .= ' postcodes AS source, postcodes AS dest, member AS u ';
					
				
				
				if(isset($this->disorderIndex)){
				 		
				 	
						$sql .= ' , member_specialty_xref ';
				}	
				
				
	  		
				$sql .= ' WHERE ';
	  			$sql .= ' source.outcode ="' . $this->outCode . '" ';
				$sql .= ' AND ((dest.x < (source.x + ' . $radius   . ')  ';
				$sql .= ' AND dest.x > (source.x - ' . $radius   . '))  ';
				$sql .= ' AND (dest.y < (source.y + ' . $radius   . ')  ';
				$sql .= ' AND dest.y > (source.y - ' . $radius   . '))) ';
				
				#if($onSite){
					$sql .= " AND SUBSTRING_INDEX( u.postcodebase, ' ', 1 )  = dest.outcode  ";
				#}
				#else{
				#	$sql .= " AND SUBSTRING_INDEX(u.postcode, ' ', 1 )  = dest.outcode  ";
				#}
				//if(isset($this->ageIndex)){
					
				//		$sql .= ' AND u.id = member_age_xref.member_id ';
				//		$sql .= ' AND member_age_xref.age_id = ' . $this->ageIndex;
				//}
				
				if(isset($this->disorderIndex)){
					
						$sql .= ' AND u.id = member_specialty_xref.member_id ';
						//$sql .= ' AND treat.spec_id = speciality.spec_id ';
						$sql .= ' AND member_specialty_xref.specialty_id = ' . $this->disorderIndex;
				}
				
				//MUST be available
				$sql .= " AND u.available='1' ";
				
				
				
				//Now ALWAYS active
				//if($this->active){
				$sql .= " AND u.active='1' ";
					
				//}
				
				if($this->exposed){
					$sql .= " AND u.webuser='1' ";
					
				}
				
				
				
				$sql .= " AND u.search_parent='1' ";
				
				
				
				
				//$sql .= ' AND u.username != "%s"  ';
				$sql .= ' ORDER BY RAND(), dest.outcode ASC ';
				//$sql .= 'LIMIT 10 ';
				
				//echo $sql;
				//exit;	
				$result=$this->db->query($sql);
				if(! $onSite){
					$resultCount=$result->size();
				}
				else{//Getting the results total is more tricky for home visits
					$resultCount=0;
					while ($row = $result->fetch() ){
                                    
                                    
                                    
						$km= sqrt(   pow(($row['x']-$this->outCodeCoordinates['x']),2) + pow(($row['y']-$this->outCodeCoordinates['y']),2) )/1000;
						
						if ($km <= $row['range']){
							$resultCount++;
						}
					}
					
				}							
				//echo '<p>Result Count = ' .$resultCount;							
          		//double the area in case there's another loop
      			$radius=$radius * 1.41; // DO NOT ROUND THIS!!!!
				$loops ++;			
				
			}
			//should have result by now
			while ($row = $result->fetch() ){
			    	
					$km= sqrt(   pow(($row['x']-$this->outCodeCoordinates['x']),2) + pow(($row['y']-$this->outCodeCoordinates['y']),2) )/1000;
					$inRange=false;
					
				
					
				
					if(! $onSite){	
							//echo '<hr>' . $miles;
							$miles=round(0.621371192*$km, 1);
					      	//round $km too
							$km=round($km, 1);
							if($km<$row['range']){
								
								$inRange=true;
							}
							
				      
          
						
					}
					else{//It's an on-site search
						if ($km <= $row['range']){
								
								$miles=round(0.621371192*$km, 1);
							//round $km too
								$km=round($km, 1);
						    
							
								
							/*	
								$resultArray[]=array('title'=>		 $row['title'],
										 'first_name'=>	 $row['first_name'],
										 'lastname'=>	 $row['lastname'],
										 'address'=>	 $row['address'],
										 'misc'=>		 $row['misc'],
										 'km'=>			 $km,
										 'miles'=>		 $miles
										 );	
								*/
						 }	
						 else{//no good
							 continue;
						 }
					}    
					
					
					//Need to add the miles and km to $row
					//echo '<pre>';
					//print_r($row);
					
					$row['km']=$km;
					$row['miles']=$miles;
					$row['in_range']=$inRange;
					$row['searched_postcode']=$this->postcode;
					
					
					$this->therapistArray[]=$row;
					
					
			} 
			
			
			//Now need to sort and slice the array
			//sort it
			usort($this->therapistArray, array($this, "compare") );
			//print_r($this->therapistArray);
			//exit;
			
			//might need to chuck some results;
			$this->therapistArray=array_slice($this->therapistArray,$start,$number);
            
				
				
		}
	function getTherapistArray(){
		
		return	$this->therapistArray;	
	}
		
		
	function compare($a, $b){
		
		//return strcmp($a["distance"], $b["distance"]);
		return ($a["km"] > $b["km"]) ? +1 : -1;
			
	}
		
	function fetch () {
		
		
		$row=each($this->therapistArray);
		
		//echo '<pre>';
		//print_r($row);
						
		if ($row){
			
			return new Therapists($row['value']);
		}
		else {
		    reset ( $this->therapistArray );
		    return false;
		}
		
	}
	
	function findByName($surname){
         	
		$sql = 'SELECT * ';
		$sql .= ' FROM member ';
		$sql .= " WHERE lastname LIKE '$surname%' ";
				
		//if($this->active){ALWAYS acive
			$sql .= " AND status=1 ";	
		//}
		
		if($this->exposed){
			$sql .= " AND web_exposure='F' ";
		}
		$sql .= ' ORDER BY lastname, first_name';
		
		
		//echo $sql;
		//exit;	
		$result=$this->db->query($sql);
		$resultCount=$result->size();
		
		switch ($resultCount){
			
		case(0):
			return false;
			break;
		default:
			while($row=$result->fetch() ){
				$this->therapistArray[]=$row;
			}
			return $resultCount;
		}						
	}
	/*
	function findByID($id){
         	
		$sql = 'SELECT * ';
		$sql .= ' FROM member ';
		$sql .= " WHERE membership_number = $id";
				
		//if($this->active){
			$sql .= " AND status=1 ";	
		//}
		
		if($this->exposed){
			$sql .= " AND web_exposure='F' ";
		}
		
		//echo $sql;
			
		$result=$this->db->query($sql);
		
		if ($result->size() != 0){
			while($row=$result->fetch() ){
				$this->therapistArray[]=$row;
			}	
			return true;
		}
		else{
			return false;
		}				
	}
	*/
		
}

//THIS IS DUPLICATION - SHOULD USE THE "REAL" one if possible
Class Therapists{
   
	 //constructor
	 function Therapists($data){
	     
			// echo '<hr><hr>here';
	                
			
			 
			 $this->data=$data;
			 
			//print_r($data);
			//exit;
			 
			 
	 }
	 function membershipNumber(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['id'];
	 }
	 function title(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['title'];
	 }
	 function forename(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['forename'];
	 }
	 function surname(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['surname'];
	 }
	 function county(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['county'];
	 }
	 function town(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['town'];
	 }
		 
	 function email(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     //BIT OF A HACK - IDEALLY, THE DATA NEEDS TO BE FIXED
	     if(strpos($this->data['email'], '@') !=0){
		     //echo '<hr>';
	     	     return $this->data['email'];
	     }
		else{
			     return false;
		}
	 }
	 
	 function work(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['work'];
	 }
	 function home(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['home'];
	 }
	 function mobile(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['mobile'];
	 }
	 function miles(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['miles'];
	 }
	 function km(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['km'];
	 }
	 function status(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['status'];
	 }
	 function exposure(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['web_exposure'];
	 }
	 function blurb(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['blurb'];
	 }
	 function inRange(){
	     return $this->data['in_range'];
	 }





}		


		
?>
