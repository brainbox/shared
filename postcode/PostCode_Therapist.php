<?php
Class PostCode_Therapist extends PostCode{
	
	
	//Constructor
	function PostCode_Therapist($db, $dbPDO, $exposed=true){
		
		$this->db = 	$db;
		//$this->active=$active;
		$this->exposed=$exposed;
				
		$this->dbPDO = $dbPDO;
				
	}
		
	function setAgeIndex($ageIndex){
		
		$this->ageIndex=$ageIndex;
		
	}
	
	function setDisorderIndex($disorderIndex){
		
		$this->disorderIndex=$disorderIndex;
		
	}
	
	function setLanguageIndex($languageIndex){
		
		$this->languageIndex=$languageIndex;
		
	}
	
	
	
	function age(){
		
		if(!isset($this->ageIndex) ){
			
			return false;	
		}
		
		/*
		
		$ageArray[]=array('age_id'=>1,'description' => '0 - 1 (infant)'); 
		$ageArray[]=array('age_id'=>2,'description' => '2 - 4 (pre-school)');
		$ageArray[]=array('age_id'=>3,'description' => '5 - 10 (primary school)');
		$ageArray[]=array('age_id'=>4,'description' => '11 - 16 (secondary school)');
		$ageArray[]=array('age_id'=>5,'description' => '17 - 20 (adolescent*)');
		$ageArray[]=array('age_id'=>5,'description' => '21 - 64 (adult*)');
		$ageArray[]=array('age_id'=>5,'description' => '65 and above (retired*)');
		
		foreach($ageArray as $name=>$row){
		
			if($row['age_id']==$this->ageIndex) {
				
				return 	$row['description'];
			}	
		}
		*/
		
		$sql = ' SELECT description from age where id=' . $this->ageIndex;
		//echo $sql;
		$result=$this->db->query($sql);
		if($result->size()==1){
		      $row= $result->fetch();		
		      return $row['description'];
		}      
		else{
			return false;	
		}
		
		
	}
	function disorder(){
		
		//echo $this->disorderIndex;
		
		if(!isset($this->disorderIndex) ){
			
			return false;
		}
		
		
	      	$sql = ' SELECT disorder from disorder where id=' . $this->disorderIndex;
		//echo $sql;
		$result=$this->db->query($sql);
		if($result->size()==1){
		      $row= $result->fetch();	
		      
		      $this->disorder=$row['disorder'];
		      return $row['disorder'];
		}  
		else{
			return false;	
		}
	}
	
	function language(){
		
		

		if(!isset($this->languageIndex) ){
			
			return false;
		}
		
		
	      	$sql = ' SELECT languages.name from languages where id=' . $this->languageIndex;
		//echo $sql;
		//exit;
		$result=$this->db->query($sql);
		if($result->size()==1){
		      $row= $result->fetch();	
		      
		      $this->language=$row['name'];
		      return $row['name'];
		}  
		else{
			return false;	
		}
	}
	
	
	function getSearchParameters(){
		
		$parameters=array();
		
		//Postcode
		$parameters[]=$this->postcode;
		
		//On-site
		if($this->onSite){
			$parameters[]='On-site visit';
		}
		
		//Age
		$parameters[]= 'Age range ' . $this->age();
		
		//Disorder
		if($this->disorder() ){
			$parameters[]=$this->disorder;
		}
		
		//Disorder
		if($this->language() ){
			$parameters[]=$this->language;
		}
		
		
		return $parameters;
		
	}


	function getNearest($start=0, $number=10, $onSite=false){
		
		
		//Notify the observer;
		//$this->notify('poop' );
		
		
		
         	
		$this->onSite=$onSite;
		
		   $resultCount=0;	
		    
		    if($onSite){
				$radius=5000;
				$maxRadius=1000000;
			}
			else{
			    $radius=$number * 1000;  // 	starting radius in metres
			    
			    
				$maxRadius=1000000;
				
				if($radius>$maxRadius){
					$radius=$maxRadius;		
				}
			}
		    
			//CAN'T ORDER THE RESULTS STRAIGHT FROM THE DATABASE... SO CAN'T USE LIMIT
			
			$requiredResults=$start+$number;		
					
			$loops=0; //To limit the number of times around the loop		
			While($resultCount<$requiredResults && $radius<=$maxRadius && $loops<20){
	
			 
			   
				
				$sql = "SELECT dest.outcode, dest.x, dest.y, u.id, u.title, u.forename, u.surname, u.email, u.work, u.mobile ,u.mobile
						
						, u.postcodebase, u.range, u.blurb
						
						, 	l.postcode
						
						,	l.name
						,	l.address
						,	l.address2
						,	l.address3
						,	l.town
						,	l.county
						,	l.postcode_display_full
						
						
						";
						
						
						
				$sql .= ' FROM ';
				$sql .= ' postcodes AS source, postcodes AS dest, member AS u
						 , member_location as l
				
					
				
				';
					
				
				if(isset($this->ageIndex)){
				 		
				 	
						$sql .= ' , member_age_xref ';
				}
				
				if(isset($this->disorderIndex)){
				 		
				 	
						$sql .= ' , member_disorder_xref ';
				}	
				
				if(isset($this->languageIndex)){
					
					$sql .= ' , member_language_xref ';
					
				}
				
				
								
				
				
				
				$sql .= ' WHERE 1=1';
	  			$sql .= " AND u.id = l.member_id ";
				$sql .= ' AND source.outcode ="' . $this->outCode . '" ';
				
				$sql .= ' AND ((dest.x < (source.x + ' . $radius   . ')  ';
				$sql .= ' AND dest.x > (source.x - ' . $radius   . '))  ';
				$sql .= ' AND (dest.y < (source.y + ' . $radius   . ')  ';
				$sql .= ' AND dest.y > (source.y - ' . $radius   . '))) ';
				
				
				
				
				
				
				
				if($onSite){
					$sql .= " AND SUBSTRING_INDEX( u.postcodebase, ' ', 1 )  = dest.outcode  ";
				}
				else{
					$sql .= " AND SUBSTRING_INDEX(l.postcode, ' ', 1 )  = dest.outcode  ";
				}
				if(isset($this->ageIndex)){
					
                         $sql .= ' AND u.id = member_age_xref.member_id ';
                         $sql .= ' AND member_age_xref.age_id = ' . $this->ageIndex;
				}
				
				if(isset($this->disorderIndex)){
					
                         $sql .= ' AND u.id = member_disorder_xref.member_id ';
                         //$sql .= ' AND treat.spec_id = speciality.spec_id ';
                         $sql .= ' AND member_disorder_xref.disorder_id = ' . $this->disorderIndex;
                         
				}
				
				
				
				if(isset($this->languageIndex)){
					
					$sql .= ' AND u.id = member_language_xref.member_id ';
					//$sql .= ' AND treat.spec_id = speciality.spec_id ';
					$sql .= ' AND member_language_xref.language_id = ' . $this->languageIndex;
                         
				}
				
				
				//MUST be available.... and the location must be active
				$sql .= "
							AND u.available='1'		
							AND l.is_active = '1'
				
				";
				
				//Now ALWAYS active
				//if($this->active){
				$sql .= " AND u.active='1' ";
					
				//}
				
				if($this->exposed){
					$sql .= " AND u.webuser='1' ";
					
				}
				
				
				//$sql .= ' AND u.username != "%s"  ';
				$sql .= ' ORDER BY RAND(), dest.outcode ASC ';
				//$sql .= 'LIMIT 10 ';
				
				//echo '<hr>' . PHP_EOL . $sql;
				//exit;
				
				//NOTE: THIS LIST NOW CONTAINS DUPLICATE MEMBERS!!!!!!
				
				
				$result=$this->db->query($sql);
				if(! $onSite){
					$resultCount=$result->size();
				}
				else{//Getting the results total is more tricky for home visits
					$resultCount=0;
					while ($row = $result->fetch() ){
                              
						$km= sqrt(   pow(($row['x']-$this->outCodeCoordinates['x']),2) + pow(($row['y']-$this->outCodeCoordinates['y']),2) )/1000;
						
						if ($km <= $row['range']){
							$resultCount++;
						}
					}
				}							
				//echo '<p>Result Count = ' .$resultCount;							
          		//double the area in case there's another loop
      			$radius=$radius * 1.41; // DO NOT ROUND THIS!!!!
				$loops ++;			
				
                    //echo 'herer';
                    //exit;
                    
			}
			//should have result by now
			while ($row = $result->fetch() ){
				
				//echo '-----';
				
				//Buld the location string
				//@todo - this is the next thing to do!
				
				//Use the location id to build the location data
				
				//require_once 'members/ASLTIPmemberLocation.php';
				
				//var_dump($row);
				
				
				//cho '-----' . $row['id'] . ' '  . $row['postcode']  ; 
				//echo $row['id'];
				//var_dump($this->dbPDO);
				
				
				
				require_once 'members/ASLTIPmemberLocation.php';
				
				$location = ASLTIPmemberLocation::getByMemberIdAndPostcode($this->dbPDO, $row['id'], $row['postcode']);
				
				
				$row['postcode'] = $location->postcode;
				
				
				
				
				$locationString = ASLTIPmemberLocation::renderLocationString($location);
				
				/*
				echo $locationString;
				exit;
				
				
				if(strtolower($row['town']) == strtolower($row['county'])){
					
					$location = $row['county'];
					
				} else {
					
					$location =  $row['town'] . ' ' . $row['county'] ;
				}
				
				$postcode = explode(' ', $row['postcode']);
				
				$location = $location . ' ' . $postcode[0];
				*/
				
				$row['location'] = $locationString;
				$row['location_description'] = $location->description;
				
				
			    	
                    $km= sqrt(   pow(($row['x']-$this->outCodeCoordinates['x']),2) + pow(($row['y']-$this->outCodeCoordinates['y']),2) )/1000;
                    $miles=round(0.621371192*$km, 1);
                    $inRange=false;
                    
                    //echo '<hr>DISTANCE';
                    //echo $km;
                    //exit;
                    
               
                    if(! $onSite){
                         
                              
                         //If it's not an on-site SEARCH , need to check whether the therapist is in range.
                         //BUT ... needs to be in range of the SET OUT postcode
                         // so... this has over-simplified the situation.
                         //
                         // Need to get the location of the SET OFF postcode and
                         //
                         // can't think of how to do this, other than by 
                         
                         //echo '<hr>Not an on-ste search';
                         
                         $onSitePossible=false;
                         
                         //print_r($row);
                         //exit;
                         
                         
                         if($row['postcodebase'] && $row['range']  ){
                              
                              //On site *may* be possible
                              
                              //ACheck if there's even a need to calculate
                              //echo '<hr>postcode' . $row['postcode'] ;
                              //echo '<hr>postcodebase' . $row['postcodebase'];
                              //exit;
                              
                              //Both postcode are the same
                              if (  $row['postcodebase'] == $row['postcode'] ) {
                                   
                                   //the imple case - no extra work to do: we already know how far away they are
                                   //echo '<hr>' . $miles;
                                   //$miles=round(0.621371192*$km, 1);
                                   //round $km too
                                   //$km=round($km, 1);
                                   
                                   if($miles<$row['range']){
                                        
                                        $inRange=true;
                                   }
                                   
                                   
                              }
                              else{
                                   //the complex case: need another run to the database
                                   
                                   $postcodebase=$row['postcodebase'];
                                   //echo $postcodebase;
                                   
                                   $postcodeArray=explode(" ",$postcodebase);
                                   //echo '<hr>';
                                   //print_r($postcodeArray);
                                   //exit;
                                   $outcodeBase=$postcodeArray[0];
                                   //echo $outcodeBase;
                                   
                                   $sql="SELECT x, y FROM postcodes WHERE outcode = '$outcodeBase'";
                                   
                                   //echo $sql;
                                   //exit;
                                   $results=$this->db->query($sql);
                                   $row2=$results->fetch();
                                   //print_r($row2);
                                   //echo $row['x'] . '/';
                                   //echo $row['y'];
                                   
                                   //note... comparing the SEARCH FOR postcode with the BASE postcode
                                   
                                   $kmToBase= (sqrt(   pow(($row2['x']-$this->outCodeCoordinates['x']),2) + pow(($row2['y']-$this->outCodeCoordinates['y']),2) ) )/1000;
                                   
                                   //echo $km;
                                   //exit;
                                   
                                   //echo '<hr>' . $miles;
                                   $milesToBase=round(0.621371192*$kmToBase, 1);
                                   //round $km too
                                   //$kmToBase=round($kmToBase, 1);
                                   
                                   //echo '<hr>km to base is ' . $kmToBase;
                                   // echo '<hr>miles to base is ' . $milesToBase;
                                   //echo '<hr>Range is ' . $row['range'];
                                   //exit;
                                   
                                   if($milesToBase<$row['range']){
                                        
                                        $inRange=true;
                                   }
                                   
                                   
                              
                              }   
                              
                              //exit;
                              
                              
                         
                         }
                         
                         //echo '<hr>' . $km;
                         ///exit;
 
                         
                    }
                    else{
                        
                        //It's an on-site search
						if ($km <= $row['range']*1.6){
                                   
								
                              //$miles=round(0.621371192*$km, 1);
                              //round $km too
                              //$km=round($km, 1);
						    
							
								
							/*	
								$resultArray[]=array('title'=>		 $row['title'],
										 'first_name'=>	 $row['first_name'],
										 'lastname'=>	 $row['lastname'],
										 'address'=>	 $row['address'],
										 'misc'=>		 $row['misc'],
										 'km'=>			 $km,
										 'miles'=>		 $miles
										 );	
								*/
						 }	
						 else{//no good
							 continue;
						 }
					}    
					
					
					//Need to add the miles and km to $row
					//echo '<pre>';
					//print_r($row);
					
					$row['km']=round($km, 1);
					$row['miles']=$miles;
					$row['in_range']=$inRange;
					$row['searched_postcode']=$this->postcode;
					
                     //   print_r($row);
                      //  exit;
                         
					$this->therapistArray[]=$row;
					
					
			} 
			
			
			//Now need to sort and slice the array
			//sort it
               
               //Might be empty at this point
               if($this->therapistArray){
               
                    usort($this->therapistArray, array($this, "compare") );
                    //print_r($this->therapistArray);
                    //exit;
                    
                    //might need to chuck some results;
                    $this->therapistArray=array_slice($this->therapistArray,$start,$number);
            
               }
				
		}
	function getTherapistArray(){
		
		return	$this->therapistArray;	
	}
		
		
	function compare($a, $b){
		
		//return strcmp($a["distance"], $b["distance"]);
		return ($a["km"] > $b["km"]) ? +1 : -1;
			
	}
		
	function fetch () {
		
		
		$row=each($this->therapistArray);
		
		//echo '<pre>';
		//print_r($row);
						
		if ($row){
			
			return new Therapists($row['value']);
		}
		else {
		    reset ( $this->therapistArray );
		    return false;
		}
		
	}
	
	function findByName($surname){
         	
		$sql = 'SELECT * ';
		$sql .= ' FROM member ';
		$sql .= " WHERE lastname LIKE '$surname%' ";
				
		//if($this->active){ALWAYS acive
			$sql .= " AND status=1 ";	
		//}
		
		if($this->exposed){
			$sql .= " AND web_exposure='F' ";
		}
		$sql .= ' ORDER BY lastname, first_name';
		
		
		//echo $sql;
		//exit;	
		$result=$this->db->query($sql);
		$resultCount=$result->size();
		
		switch ($resultCount){
			
		case(0):
			return false;
			break;
		default:
			while($row=$result->fetch() ){
				$this->therapistArray[]=$row;
			}
			return $resultCount;
		}						
	}
	function findByID($id){
         	
		$sql = 'SELECT * ';
		$sql .= ' FROM member ';
		$sql .= " WHERE membership_number = $id";
				
		//if($this->active){
			$sql .= " AND status=1 ";	
		//}
		
		if($this->exposed){
			$sql .= " AND web_exposure='F' ";
		}
		
		//echo $sql;
			
		$result=$this->db->query($sql);
		
		if ($result->size() != 0){
			while($row=$result->fetch() ){
				$this->therapistArray[]=$row;
			}	
			return true;
		}
		else{
			return false;
		}				
	}
	
		
}


/**
 * This is used - but only by the ADmin search
 *
 *
 *
 */

Class Therapists{
   
	 //constructor
	 function Therapists($data){
	     
			// echo '<hr><hr>here';
	                
			
			 
			 $this->data=$data;
			
			//print_r($data);
			//exit;
			 
			 
	 }
	 function membershipNumber(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['id'];
	 }
	 function title(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['title'];
	 }
	 function forename(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['forename'];
	 }
	 function surname(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['surname'];
	 }
	 function town(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['town'];
	 }
	 
	 function location(){
	    
	     return $this->data['location'];
	 }
	 
	 
	 function county(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['county'];
	 }
		 
	 function email(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     //BIT OF A HACK - IDEALLY, THE DATA NEEDS TO BE FIXED
	     if(strpos($this->data['email'], '@') !=0){
		     //echo '<hr>';
	     	     return $this->data['email'];
	     }
		else{
			     return false;
		}
	 }
	 
	 function work(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['work'];
	 }
	 function home(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['home'];
	 }
	 function mobile(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['mobile'];
	 }
	 function miles(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['miles'];
	 }
	 function km(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['km'];
	 }
	 function status(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['status'];
	 }
	 function exposure(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['web_exposure'];
	 }
	 function blurb(){
	     // echo '<pre>';	  
	     // print_r($this->data);
	     return $this->data['blurb'];
	 }
	 function inRange(){
	     return $this->data['in_range'];
	 }





}		


		
?>
