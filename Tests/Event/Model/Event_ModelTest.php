<?php

//echo realpath('.');

$docRoot = dirname(dirname(dirname(dirname( dirname(__FILE__) ) ) ) );

set_include_path(get_include_path() . PATH_SEPARATOR . $docRoot. '/shared');
set_include_path(get_include_path() . PATH_SEPARATOR . $docRoot. '/includes');


require_once 'Event/Model/Event.php';

class Event_ModelTest extends PHPUnit_Framework_TestCase
{
   
 
    protected function setUp()
    {
        $dbPDO = "TO BE MOCKED!!!!";
        
        $this->mEvent = new Model_Event($dbPDO);
        
        
        $this->today = mktime( 0,0,0,date('m'),date('d'),date('y') );
        
    }
    
    
    public function testPrepareDataSimpleData()
    {
        
        $data = array();
        
        $data['id']=                666;
        $data['title']=             'The title';
        $data['body']=              'The body';
        $data['metaTitle']=         'The meta title';
        $data['metaDescription']=   'The meta description';
       
        $data['published']=         '10-01-2012';
        $data['depublish']=         '24-06-2012';
        
        $data['startDate']=         '14-01-2012'; // Human date
        $data['endDate']=           '16-01-2012';
    
        
        $dataNew = $this->mEvent->prepareData($data);
        
       
        
        $this->assertEquals(666,                        $dataNew['id']);
        $this->assertEquals('The title',                $dataNew['title']);
        $this->assertEquals('The meta title',           $dataNew['metaTitle']);
        $this->assertEquals('The meta description',     $dataNew['metaDescription']);
       
        $this->assertEquals('2012-01-10',               $dataNew['published']);
        $this->assertEquals('2012-06-24',               $dataNew['depublish']);
        
        $this->assertEquals('2012-01-14',               $dataNew['startDate']); // SQL date
        $this->assertEquals('2012-01-16',               $dataNew['endDate']);
        
            
       
    }
    
    
    public function testPrepareDataTestIsPublished()
    {
        
        $data = array();
      
        $data['published']=         '10-01-2040';  //Massively future!!!
        $data['depublish']=         '24-06-2012';
        
      
        
        $data['isPublished'] =      'true';
        
        
        $dataNew = $this->mEvent->prepareData($data);
       
        
        $this->assertEquals(null, $dataNew['depublish']); //depublish gets wiped for never expire
        
        //published must be in the past
        $this->assertTrue( strtotime($dataNew['published']) <= $this->today );
        
       
    }
    
    public function testPrepareDataTestNOTIsPublished()
    {
        
        $data = array();
      
        $data['published']=         '10-01-2013';  //Massively future!!!
        $data['depublish']=         '24-06-2012';
        
      
        
        $data['isPublished'] =      null;
        $data['usesIsPublished'] =  '1';
        
        
        $dataNew = $this->mEvent->prepareData($data);
       
        
        $this->assertEquals('2013-01-10', $dataNew['published']); //Should NOT be null
        
        //expire must be in the past        
        $this->assertTrue( strtotime($dataNew['depublish']) <= $this->today );
        
       
       
    }
    
    
    
    public function testPrepareDataTestOneDayEvent()
    {
        
        $data = array();
        
        $data['startDate']=         '14-01-2012'; // Human date
        $data['endDate']=           '16-01-2012';
        
        $data['oneday'] =           'true';
       
        
        
        $dataNew = $this->mEvent->prepareData($data);
      
        $this->assertEquals('2012-01-14',               $dataNew['startDate']); // SQL date
        $this->assertEquals(null,                       $dataNew['endDate']);   //End date gets wiped for one day event
        
        
       
    }
    
    
    public function testPrepareDataTestSameStartEndDates()
    {
        
        $data = array();
        
        $data['startDate']=         '19-01-2012'; // Human date
        $data['endDate']=           '19-01-2012';
        
        $dataNew = $this->mEvent->prepareData($data);
      
        $this->assertEquals(null,                       $dataNew['endDate']);   //End date gets wiped for one day event
       
    }
    
    
    
    
    
    
}

