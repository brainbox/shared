<?php
/**
 * Factory pattern
 * 
 * Will need to (a) implement autoload or (b) include the right files before calling PROCESSQUEUE
 * 
 * 
 *  
 */





Class Queue
{
	
     public function __construct($dbPDO)
     {
         
         $this->dbPDO = $dbPDO;
         
     }
    
    
    
    public function getPDO()
    {
        return $this->dbPDO;
	}
    
    
    public function addItemToQueue($func,$args)
    {
        if(!$this->dbPDO){
            throw new Exception;
        }
        
        $sql="INSERT INTO queue (func, args) 
                VALUES (:func, :args ) ";

        $stmnt=$this->dbPDO->prepare($sql);
        
        $stmnt->bindParam(':func', $func);
        $stmnt->bindParam(':args', serialize($args) );
        
        $stmnt->execute();
      
        return true;
	}
    
    /**
     * Get one at a time... and knock it off the lsit
     * 
     * 
     * @return type
     * @throws Exception 
     */
    
    public function fetch()
    {
        if(!$this->dbPDO){
            throw new Exception;
        }
        
        $sql="SELECT id, func, args FROM queue LIMIT 1";
		$stmnt=$this->dbPDO->prepare($sql);
      
        $stmnt->execute();
        $result = $stmnt->fetch();
        
        if($result){
            $sql="DELETE FROM
                        queue 

                    WHERE

                        id = " . $result['id'] . "

                    LIMIT 1";

            $stmnt=$this->dbPDO->prepare($sql);

            $stmnt->execute();
        }
        
        
        return $result;
        
        
	}
    
    
}
